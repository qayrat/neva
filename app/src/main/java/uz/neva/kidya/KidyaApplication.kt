package uz.neva.kidya

import android.os.Handler
import android.os.Looper
import androidx.core.os.HandlerCompat
import androidx.multidex.MultiDexApplication
import com.orhanobut.hawk.Hawk
import com.yandex.metrica.YandexMetrica
import com.yandex.metrica.YandexMetricaConfig
import com.yandex.metrica.push.YandexMetricaPush
import com.yariksoffice.lingver.Lingver
import com.yariksoffice.lingver.store.PreferenceLocaleStore
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import timber.log.Timber
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.di.appModule
import uz.neva.kidya.di.networkModule
import uz.neva.kidya.di.nevaNetworkModule
import uz.neva.kidya.di.viewModelsModule
import java.util.*
import java.util.concurrent.ExecutorService
import java.util.concurrent.Executors

class KidyaApplication : MultiDexApplication() {

    val BASE_URL = "http://bitrix.kidya.uz"
    val API_key = "c5f69714-f687-4077-8750-18581564afdb"

    companion object {
        const val LANGUAGE_ENGLISH = "en"
        const val LANGUAGE_RUSSIAN = "ru"
        const val LANGUAGE_UZBEKISTAN = "uz"
    }

    override fun onCreate() {
        super.onCreate()

        //Lingver change language init
        Lingver.init(this, PreferenceLocaleStore(this, Locale(LANGUAGE_RUSSIAN)))

        //Hawk Preference init
        Hawk.init(this).build()
        val executorService: ExecutorService = Executors.newFixedThreadPool(4)
        val mainThreadHandler: Handler = HandlerCompat.createAsync(Looper.getMainLooper())

        startKoin {
            androidLogger(org.koin.core.logger.Level.ERROR)
            androidContext(applicationContext)
            modules(listOf(appModule, nevaNetworkModule, viewModelsModule))
//            module { single { EmailValidatorUtilImpl } }
        }

        // Creating an extended library configuration.
        val config = YandexMetricaConfig.newConfigBuilder(API_key).build();
        // Initializing the AppMetrica SDK.
        YandexMetrica.activate(getApplicationContext(), config);
        // Automatic tracking of user activity.
        YandexMetrica.enableActivityAutoTracking(this);


        YandexMetricaPush.init(applicationContext)

        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        }
    }
}