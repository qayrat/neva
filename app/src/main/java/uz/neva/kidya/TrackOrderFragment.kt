package uz.neva.kidya

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_track_order.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.ui.check_out.BasketItemListAdapter
import uz.neva.kidya.ui.my_details.ProfileViewModel


class TrackOrderFragment : Fragment(R.layout.fragment_track_order) {
    private var orderId = ""
    private lateinit var basketItemListAdapter: BasketItemListAdapter
    private val profileViewModel: ProfileViewModel by viewModel()


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            orderId = it.getString("order_id").toString()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        goHome.setOnClickListener {
            findNavController().navigate(R.id.nav_home)
        }

        basketItemListAdapter = BasketItemListAdapter(requireContext(), this)
        basketRecycler2.layoutManager = LinearLayoutManager(requireContext())
        basketRecycler2.adapter = basketItemListAdapter

        profileViewModel.getGetOrderDetailsResponse(
            PrefManager.getUserID(requireContext()),
            orderId
        )

        profileViewModel.getOrderDetailsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        val list = resource.data.data
                        val listProduct = resource.data.products
                        basketItemListAdapter.updateList(listProduct as ArrayList<BasketItemObject>)
                        val currentOrder = list.first { e -> e.id == orderId }
                        val totalPrice =
                            currentOrder.total_price.replace("\\s".toRegex(), "").toInt()

                        order_price.text =
                            "${resources.getString(R.string.text_price_order)} $totalPrice ${resources.getString(R.string.text_type_money)}"
                        order_id.text = "${resources.getString(R.string.text_id_order)} ${currentOrder.id}"
                        order_date.text = "${resources.getString(R.string.text_data)} ${currentOrder.date}"

                        val orderStatues = resource.data.status_list
                        val orderStatusId =
                            resource.data.data.first { e -> e.id == orderId }.status_id
                        when (orderStatusId) {
                            "ON","N","P" -> {
                                view_order_placed.setBackgroundResource(R.drawable.shape_status_current)
                                view_order_confirmed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed2.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed3.setBackgroundResource(R.drawable.shape_status_complete)
                            }
                            "OT" -> {
                                view_order_placed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed.setBackgroundResource(R.drawable.shape_status_current)
                                view_order_confirmed2.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed3.setBackgroundResource(R.drawable.shape_status_complete)
                            }
                            "OS" -> {
                                view_order_placed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed2.setBackgroundResource(R.drawable.shape_status_current)
                                view_order_confirmed3.setBackgroundResource(R.drawable.shape_status_complete)
                            }
                            "OF","F" -> {
                                view_order_placed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed2.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed3.setBackgroundResource(R.drawable.shape_status_current)
                            }
                            "RT" -> {
                                order_confirmed4.text = resources.getString(R.string.text_return_order)
                                view_order_placed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed2.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed3.setBackgroundResource(R.drawable.shape_status_current)
                            }
                            else -> {
                                view_order_placed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed2.setBackgroundResource(R.drawable.shape_status_complete)
                                view_order_confirmed3.setBackgroundResource(R.drawable.shape_status_current)
                            }
                        }


                    }
                    is Resource.GenericError -> {
//                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbarWithMargin(it1) }
                    }
                }
            }
        })
    }



}