package uz.neva.kidya

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.RadioGroup
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.sanojpunchihewa.updatemanager.UpdateManager.Builder
import com.yariksoffice.lingver.Lingver
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_change_language.*
import kotlinx.android.synthetic.main.fragment_change_language.view.*
import kotlinx.android.synthetic.main.fragment_settings.*
import uz.neva.kidya.ui.category.selectedProduct.FOR_SETTINGS
import uz.neva.kidya.ui.category.selectedProduct.SelectedItemFragment
import uz.neva.kidya.utils.hide


class SettingsFragment : Fragment(R.layout.fragment_settings) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).toolbar.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        privacyPolicy.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("http://kidya.uz/terms-of-use"))
            startActivity(browserIntent)
        }

        f_a_q.setOnClickListener {
            findNavController().navigate(R.id.questionAnsweSettingsFragment)
        }

        ll_language.setOnClickListener {
            openChangeLanguageDialog()
        }
    }

    private fun openChangeLanguageDialog() {
        val bottomSheetDialog = ChangeLanguageDialog.newInstance()
        bottomSheetDialog.show(
            requireActivity().supportFragmentManager,
            "Bottom Sheet change language Dialog Fragment"
        )
    }



}

class ChangeLanguageDialog : BottomSheetDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(STYLE_NORMAL, R.style.BottomSheetDialogStyle)
        Log.d("TAGLINGVER", "onCreate: ")
    }


    override fun setupDialog(dialog: Dialog, style: Int) {
        val contentView = View.inflate(context, R.layout.fragment_change_language, null)
        dialog.setContentView(contentView)

        checkLanguage(contentView.languages)

        contentView.languages.setOnCheckedChangeListener { group, checkedId ->
            when (checkedId) {
                R.id.rb_russian -> changeLanguage(KidyaApplication.LANGUAGE_RUSSIAN,dialog)
                R.id.rb_uzbek -> changeLanguage(KidyaApplication.LANGUAGE_UZBEKISTAN,dialog)
                R.id.rb_english -> changeLanguage(KidyaApplication.LANGUAGE_ENGLISH,dialog)
            }

        }
    }

    private fun checkLanguage(languages: RadioGroup?) {
        languages?.let { radioGroup ->
            when (Lingver.getInstance().getLanguage()) {
                KidyaApplication.LANGUAGE_UZBEKISTAN -> radioGroup.check(R.id.rb_uzbek)
                KidyaApplication.LANGUAGE_RUSSIAN -> radioGroup.check(R.id.rb_russian)
                KidyaApplication.LANGUAGE_ENGLISH -> radioGroup.check(R.id.rb_english)
            }
        }
    }

    private fun changeLanguage(language: String,shapeDialog: Dialog) {
        val dialogFragment =
            MaterialAlertDialogBuilder(requireContext(), R.style.MyCustomAlertDialog)
        dialogFragment.apply {
            setTitle(requireActivity().resources.getString(R.string.change_title))
            setMessage(requireActivity().resources.getString(R.string.change_language_message))
            setCancelable(false)
        }
        dialogFragment.setNegativeButton(R.string.text_cancel) { dialog, which ->
            dialog.dismiss()
            shapeDialog.dismiss()
        }
        dialogFragment.setPositiveButton("OK") { dialog, which ->
            Lingver.getInstance().setLocale(requireContext(), language)
            startActivity(Intent(requireContext(), MainActivity::class.java).apply {
                flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
            })
        }

        dialogFragment.show()

    }

    companion object {
        fun newInstance(): ChangeLanguageDialog {
            return ChangeLanguageDialog()
        }
    }
}

interface OnLanguageChangeLanguage {
    fun onChange(language: String)
}