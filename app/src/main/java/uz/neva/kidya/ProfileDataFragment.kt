package uz.neva.kidya

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_profile_data.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.showSnackbarWithMargin

class ProfileDataFragment : Fragment(R.layout.fragment_profile_data) {

    private val profileViewModel: ProfileViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).toolbar.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }
        account_email

        profileViewModel.getProfile(PrefManager.getUserID(requireContext()))
        profileViewModel.getProfileResponse.observe(
                viewLifecycleOwner,
                {
                    it.getContentIfNotHandled()?.let { resource ->
                        when (resource) {
                            is Resource.Loading -> {
                            }
                            is Resource.Success -> {
                                account_full_name.text = resource.data.data.name ?: ""
                                PrefManager.saveName(requireContext(), resource.data.data.name + " " + resource.data.data.last_name ?: "")
                                PrefManager.savePhone(requireContext(), "+" + resource.data.data.phone ?: "")
                                PrefManager.saveCity(requireContext(), resource.data.data.address
                                        ?: "")
                                PrefManager.saveDistrict(requireContext(), resource.data.data.district
                                        ?: "")
                                PrefManager.saveStreet(requireContext(), resource.data.data.street
                                        ?: "")
                                PrefManager.saveEmail(requireContext(), resource.data.data.email ?: "")

                                if (PrefManager.getPhone(requireContext()).isNotEmpty())
                                    account_phone_number.text = PrefManager.getPhone(requireContext())

                                if (PrefManager.getUserID(requireContext()).isNotEmpty())
                                    account_user_id.text = PrefManager.getUserID(requireContext())

                                if (PrefManager.getProfileDate(requireContext()).isNotEmpty())
                                    account_date_birth.text = PrefManager.getProfileDate(requireContext())

                                if (PrefManager.getProfilePol(requireContext()).isNotEmpty())
                                    account_gender.text = PrefManager.getProfilePol(requireContext())

                                if (PrefManager.getEmail(requireContext()).isNotEmpty())
                                    account_email.text = PrefManager.getEmail(requireContext())

                                if (PrefManager.getProfileAddress(requireContext()).isNotEmpty())
                                    account_address.text = PrefManager.getProfileAddress(requireContext())
                            }
                            is Resource.GenericError -> {
                                showSnackbarWithMargin(resources.getString(R.string.edit_profile_something_wrong))
                            }
                            is Resource.Error -> {
                                showSnackbarWithMargin(resources.getString(R.string.edit_profile_something_wrong))
                            }

                        }
                    }
                })
        textView30.setOnClickListener {
            findNavController().navigate(R.id.accountEditFragment)
        }

        if (PrefManager.getPhone(requireContext()).isNotEmpty())
            account_phone_number.setText(PrefManager.getPhone(requireContext()))

        if (PrefManager.getUserID(requireContext()).isNotEmpty())
            account_user_id.setText(PrefManager.getUserID(requireContext()))

        if (PrefManager.getProfileDate(requireContext()).isNotEmpty())
            account_date_birth.setText(PrefManager.getProfileDate(requireContext()))

        if (PrefManager.getProfilePol(requireContext()).isNotEmpty())
            account_gender.setText(PrefManager.getProfilePol(requireContext()))

        if (PrefManager.getEmail(requireContext()).isNotEmpty())
            account_email.setText(PrefManager.getEmail(requireContext()))

        if (PrefManager.getProfileAddress(requireContext()).isNotEmpty())
            account_address.setText(PrefManager.getProfileAddress(requireContext()))
    }
}