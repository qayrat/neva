package uz.neva.kidya

import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.AlertDialog
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.google.gson.annotations.SerializedName
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_history_order.*
import kotlinx.android.synthetic.main.fragment_order.*
import kotlinx.android.synthetic.main.fragment_upcoming_order.*
import kotlinx.android.synthetic.main.item_order.view.*
import kotlinx.android.synthetic.main.support_view.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.setMargins


class OrderFragment : Fragment(R.layout.fragment_order) {
    private lateinit var pagerAdapter: OrderAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).toolbar.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)


        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        pagerAdapter = OrderAdapter(this)
        order_pager.adapter = pagerAdapter
        order_pager.isUserInputEnabled = false

        TabLayoutMediator(order_tab_layout, order_pager) { tab, position ->
            tab.text = when (position) {
                0 -> resources.getString(R.string.text_order_current)
                1 -> resources.getString(R.string.text_order_history)
                else -> ""
            }
        }.attach()

        order_tab_layout.setMargins(10, 0, 10, 0)
    }


}

class OrderAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> UpcomingOrderFragment()
            1 -> HistoryOrderFragment()
            else -> Fragment()
        }
    }

}

class HistoryOrderFragment : Fragment(R.layout.fragment_history_order) {

    private val itemAdapter = ItemAdapter<Order>()
    private val fastAdapter =
        FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<Order>() {
            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<Order>,
                item: Order
            ) {
                val bundle = Bundle()
                bundle.putString("order_id", item.id)
                findNavController().navigate(R.id.trackOrderFragment, bundle)
            }

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView.trackOrder
            }
        })
    private val profileViewModel: ProfileViewModel by viewModel()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        historyList.layoutManager = LinearLayoutManager(requireContext())
        historyList.adapter = fastAdapter

        profileViewModel.getGetOrderHistoryResponse(PrefManager.getUserID(requireContext()))
        profileViewModel.getOrderHistoryResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        val list = resource.data.data
                        itemAdapter.clear()
                        for (i in 0 until list.size) {
                            if (list[i].status_id == "F" || list[i].status_id == "RT" || list[i].status_id=="OF") {
                                itemAdapter.add(
                                    Order(
                                        list[i].id,
                                        list[i].price,
                                        list[i].price_delivery,
                                        list[i].total_price,
                                        list[i].date,
                                        null,
                                        list[i].status_id,
                                        list[i].status_name
                                    )
                                )
                            }
                        }
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

    }
}

class UpcomingOrderFragment : Fragment(R.layout.fragment_upcoming_order),
    Order.OnCancelClickListener {

    private val itemAdapter = ItemAdapter<Order>()
    private val fastAdapter =
        FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<Order>() {
            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<Order>,
                item: Order
            ) {
                val bundle = Bundle()
                bundle.putString("order_id", item.id)
                findNavController().navigate(R.id.trackOrderFragment, bundle)
            }

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView.trackOrder
            }
        })
    private val profileViewModel: ProfileViewModel by viewModel()


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        upcomingList.layoutManager = LinearLayoutManager(requireContext())
        upcomingList.adapter = fastAdapter

//        for (i in 0 until 5) {
//            itemAdapter.add(Order(i, "upcoming"))
//        }

        fastAdapter.addEventHook(Order.OrderCancelClickEvent(this))

        profileViewModel.getGetOrderHistoryResponse(PrefManager.getUserID(requireContext()))
        profileViewModel.getOrderHistoryResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        val list = resource.data.data
                        itemAdapter.clear()
                        for (i in 0 until list.size) {
                            if (list[i].status_id != "F" && list[i].status_id != "RT" && list[i].status_id!="OF") {
                                itemAdapter.add(
                                    Order(
                                        list[i].id,
                                        list[i].price,
                                        list[i].price_delivery,
                                        list[i].total_price,
                                        list[i].date,
                                        null,
                                        list[i].status_id,
                                        list[i].status_name
                                    )
                                )
                            }
                        }
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

    }

    override fun onCancel() {
        createAlertDialog()
    }

    private fun createAlertDialog() {
        val builder = AlertDialog.Builder(requireContext())
        val view = layoutInflater.inflate(R.layout.support_view, null)
        builder.setView(view)

        val dialog = builder.create()
        view.apply {
            call.setOnClickListener {
                val intent = Intent(Intent.ACTION_DIAL)
                intent.data = Uri.parse("tel:${PrefManager.getPhoneSupport(requireContext())}")
                startActivity(intent)
                dialog.dismiss()
            }
            chat.setOnClickListener {
                val name = PrefManager.getTelegramLink(requireContext())
                val intent =
                    Intent(Intent.ACTION_VIEW, Uri.parse("tg://resolve?domain=${name}"))
                startActivity(intent)
                dialog.dismiss()
            }
        }
        dialog.show()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.window?.setLayout(800, ViewGroup.LayoutParams.WRAP_CONTENT)
    }
}

data class Order(
    val id: String,
    val price: String? = "",
    val price_delivery: String? = "",
    val total_price: String? = "",
    val date: String? = "",
    val delivery: Any? = null,
    val status_id: String? = "",
    @SerializedName("status_name")
    val typeName: String? = "",
) : AbstractItem<Order.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<Order>(itemView) {
        override fun bindView(item: Order, payloads: List<Any>) {
//            itemView.historyType.isVisible = item.status_id == "F" || item.status_id=="RT"
//            itemView.UpcomingType.isVisible = item.status_id != "F" || item.status_id!="RT"
            itemView.cancel.isVisible = item.status_id != "F" && item.status_id != "RT" && item.status_id!="OF"
            itemView.apply {
                orderId.text = item.id
                val totalPrice = item.total_price?.replace("\\s".toRegex(), "")?.toInt() ?: 0
                orderPrice.text = (totalPrice).toString()
                orderType.text = item.typeName ?: ""
            }
        }

        override fun unbindView(item: Order) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_order
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)

    class OrderCancelClickEvent(val listener: OnCancelClickListener) : ClickEventHook<Order>() {
        override fun onClick(v: View, position: Int, fastAdapter: FastAdapter<Order>, item: Order) {
            listener.onCancel()
        }

        override fun onBindMany(viewHolder: RecyclerView.ViewHolder): List<View>? {
            if (viewHolder is VH) {
                return listOf(viewHolder.itemView.cancel)
            }
            return super.onBindMany(viewHolder)
        }
    }

    interface OnCancelClickListener {
        fun onCancel()
    }
}