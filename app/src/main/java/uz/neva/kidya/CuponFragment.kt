package uz.neva.kidya

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.viewpager2.adapter.FragmentStateAdapter
import com.google.android.material.tabs.TabLayoutMediator
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_cupon.*
import kotlinx.android.synthetic.main.fragment_used_coupon.*
import kotlinx.android.synthetic.main.item_coupon.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.setMargins
import uz.neva.kidya.utils.showSnackbar


class CuponFragment : Fragment(R.layout.fragment_cupon) {

    private lateinit var pagerAdapter: CouponAdapter
    private val itemAdapter = ItemAdapter<Coupon>()
    private val fastAdapter = FastAdapter.with(itemAdapter)
    private val checkOutViewModel: CheckOutViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).toolbar.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        pagerAdapter = CouponAdapter(this)

        pager.adapter = pagerAdapter
        pager.isUserInputEnabled = false

        TabLayoutMediator(tab_layout, pager) { tab, position ->
            tab.text = when (position) {
                0 -> "Имеется в наличии"
                1 -> "Использовал"
                else -> ""
            }
        }.attach()

        tab_layout.setMargins(10, 0, 10, 0)

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        availableList.layoutManager = LinearLayoutManager(requireContext())
        availableList.adapter = fastAdapter
        if (PrefManager.getUserID(requireContext()).isNotEmpty()) {
            checkOutViewModel.getCouponList(PrefManager.getUserID(requireContext()))
            checkOutViewModel.getCouponListResponse.observe(viewLifecycleOwner, Observer {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {

                        is Resource.Loading -> {

                        }

                        is Resource.Success -> {
                            val list = resource.data.data
                            for (i in 0 until list.size) {
                                itemAdapter.add(
                                    Coupon(
                                        i,
                                        list[i].coupon,
                                        date = "${list[i].end}"
                                    )
                                )
                            }
                        }

                        is Resource.GenericError -> {
                            showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                        }
                        is Resource.Error -> {
                            resource.exception.message?.let { it1 -> showSnackbar(it1) }
                        }
                    }
                }
            })
        }

    }

}

class CouponAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 2

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> AvailableCouponFragment()
            1 -> UsedCouponFragment()
            else -> Fragment()
        }
    }

}

class AvailableCouponFragment : Fragment(R.layout.fragment_available_coupon) {

    private val itemAdapter = ItemAdapter<Coupon>()
    private val fastAdapter = FastAdapter.with(itemAdapter)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
//        availableList.layoutManager = LinearLayoutManager(requireContext())
//        availableList.adapter = fastAdapter

//        for (i in 0 until 5) {
//            itemAdapter.add(Coupon(i))
//        }

    }
}

class UsedCouponFragment : Fragment(R.layout.fragment_used_coupon) {

    private val itemAdapter = ItemAdapter<Coupon>()
    private val fastAdapter = FastAdapter.with(itemAdapter)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        usedList.layoutManager = LinearLayoutManager(requireContext())
        usedList.adapter = fastAdapter

//        for (i in 0 until 5) {
//            itemAdapter.add(Coupon(i))
//        }
    }
}

data class Coupon(
    val id: Int,
    val coupon: String? = "",
    val shopName: String? = "",
    val couponType: String? = "",
    val date: String? = ""
) : AbstractItem<Coupon.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<Coupon>(itemView) {
        override fun bindView(item: Coupon, payloads: List<Any>) {
            itemView.shopName.text = item.coupon
            itemView.message.text = "До ${item.date}"
        }

        override fun unbindView(item: Coupon) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_coupon
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}