package uz.neva.kidya.data

import android.content.Context
import android.content.SharedPreferences

private const val TOKEN = "token"
private const val SERVER_TOKEN = "SERVER_TOKEN"
private const val CARD_ID = "CARD_ID"
private const val DATE_ORDER = "DATE_ORDER"
private const val HOUR_ORDER = "HOUR_ORDER"
private const val FIREBASE_TOKEN = "firebase_token"
private const val USER_ID = "userId"
private const val USER_ID_FOR = "USER_ID_FOR"
private const val PRODUCT_COUNT = "PRODUCT_COUNT"
private const val PRODUCT_COUNT2 = "PRODUCT_COUNT2"
private const val PROFILE_POL = "PROFILE_POL"
private const val PROFILE_DATE = "PROFILE_DATE"
private const val PROFILE_ADDRESS = "PROFILE_ADDRESS"
private const val PROFILE_INTERES = "PROFILE_INTERES"
private const val PRODUCT_UPDATE = "PRODUCT_UPDATE"
private const val NAME = "name"
private const val EMAIL = "email"
private const val PHONE = "phone"
private const val PHONE_DELIVERY = "PHONE_DELIVERY"
private const val NAME_DELIVERY = "NAME_DELIVERY"
private const val ADDRESS_DELIVERY = "ADDRESS_DELIVERY"
private const val CITY = "city"
private const val DISTRICT = "district"
private const val STREET = "street"
private const val MY_AGENT = "my_agent"
private const val LOCALE_LANG = "localeLang"
private const val LOCATION = "LOCATION"
private const val CURRENT_ORDER_ID = "current_order_id"
private const val ORDER_STATUS = "order_status"
private const val ORDER_ID = "ORDER_ID"
private const val LOT = "LOT"
private const val LANG = "LANG"
private const val ORDER_TITLE = "order_title"
private const val IS_LOGGED_IN = "is_logged_in"
private const val CARD_NUMBER = "CARD_NUMBER"
private const val IS_INSTALL = "IS_INSTALL"
private const val PHONE_SUPPORT = "PHONE_SUPPORT"
private const val TELEGRAM_LINK = "TELEGRAM_LINK"

private fun getInstance(context: Context): SharedPreferences {
    return context.getSharedPreferences(MY_AGENT, Context.MODE_PRIVATE)
}

fun Context.saveUserId(id: String) {
    getInstance(this).edit().putString(
        USER_ID, id
    ).apply()
}

fun Context.savePhone(phone: String) {
    getInstance(this).edit().putString(
        PHONE, phone
    ).apply()
}

fun Context.saveName(name: String) {
    getInstance(this).edit().putString(
        NAME, name
    ).apply()
}

fun Context.saveCity(city: String) {
    getInstance(this).edit().putString(
        CITY, city
    ).apply()
}

fun Context.saveDistrict(district: String) {
    getInstance(this).edit().putString(
        DISTRICT, district
    ).apply()
}

fun Context.saveStreet(street: String) {
    getInstance(this).edit().putString(
        STREET, street
    ).apply()
}


fun Context.saveToken(token: String) {
    getInstance(this).edit().putString(
        TOKEN, token
    ).apply()
}

fun Context.getToken(): String {
    return getInstance(
        this
    ).getString(TOKEN, "") ?: ""
}

fun Context.getUserId(): String {
    return getInstance(
        this
    ).getString(USER_ID, "") ?: ""
}


fun Context.isUserLoggedIn(): Boolean {
    return getToken()
        .isNotEmpty()
}

fun Context.setLoggedOut() {
    getInstance(this).edit().putBoolean(IS_LOGGED_IN, false).apply()
}

fun Context.logout() {
    this.saveToken("")
    this.saveUserId("")
    this.saveName("")
    this.savePhone("")
    this.setLoggedOut()
}

fun Context.isLoggedIn(): Boolean {
    return getInstance(this).getBoolean(IS_LOGGED_IN, false)
}

fun Context.loggedIn() {
    getInstance(this).edit().putBoolean(IS_LOGGED_IN, true).apply()
}

class PrefManager {
    companion object {

        private fun getInstance(context: Context): SharedPreferences {
            return context.getSharedPreferences(MY_AGENT, Context.MODE_PRIVATE)
        }

        fun saveToken(context: Context, token: String) {
            getInstance(context).edit().putString(
                TOKEN, token
            ).apply()
        }

        fun isInstallApp(context: Context, isInstall: Int) {
            getInstance(context).edit().putInt(
                IS_INSTALL, isInstall
            ).apply()
        }


        fun saveProductCount2(context: Context, count: Int) {
            getInstance(context).edit().putInt(
                PRODUCT_COUNT2, count
            ).apply()
        }

        fun isUpdateProduct(context: Context, update: Boolean) {
            getInstance(context).edit().putBoolean(
                PRODUCT_UPDATE, update
            ).apply()
        }

        fun saveProfilePol(context: Context, pol: String) {
            getInstance(context).edit().putString(PROFILE_POL, pol).apply()
        }

        fun getProfilePol(context: Context): String {
            return getInstance(context).getString(PROFILE_POL, "")!!
        }

        fun saveProfileDate(context: Context, date: String) {
            getInstance(context).edit().putString(PROFILE_DATE, date).apply()
        }

        fun getProfileDate(context: Context): String {
            return getInstance(context).getString(PROFILE_DATE, "")!!
        }

        fun getIsInstallApp(context: Context): Int {
            return getInstance(context).getInt(IS_INSTALL, 1)
        }

        fun saveProfileAddress(context: Context, address: String) {
            getInstance(context).edit().putString(PROFILE_ADDRESS, address).apply()
        }

        fun saveDeliveryAddress(context: Context, address: String) {
            getInstance(context).edit().putString(ADDRESS_DELIVERY, address).apply()
        }

        fun getProfileAddress(context: Context): String {
            return getInstance(context).getString(PROFILE_ADDRESS, "")!!
        }

        fun getDeliveryAddress(context: Context): String {
            return getInstance(context).getString(ADDRESS_DELIVERY, "")!!
        }

        fun saveProfileInteres(context: Context, interes: String) {
            getInstance(context).edit().putString(PROFILE_INTERES, interes).apply()
        }

        fun getProfileInteres(context: Context): String {
            return getInstance(context).getString(PROFILE_INTERES, "")!!
        }

        fun saveUserId(context: Context, id: String) {
            getInstance(context).edit().putString(
                USER_ID, id
            ).apply()
        }

        fun saveUserIdFor(context: Context, id: String) {
            getInstance(context).edit().putString(
                USER_ID_FOR, id
            ).apply()
        }


        fun getUpdater(context: Context): Boolean {
            return getInstance(context).getBoolean(PRODUCT_UPDATE, false)
        }

        fun getProductCount2(context: Context): Int {
            return getInstance(context).getInt(PRODUCT_COUNT, 0)
        }

        fun getToken(context: Context): String {
            return getInstance(
                context
            ).getString(TOKEN, "")!!
        }

        fun saveCardNumber(context: Context, number: String) {
            getInstance(context).edit().putString(CARD_NUMBER, number).apply()
        }

        fun getCardNumber(context: Context): String {
            return getInstance(context).getString(CARD_NUMBER, "")!!
        }

        fun removeToken(context: Context) {
            getInstance(context).edit().remove(
                TOKEN
            ).apply()
        }

        fun saveFirebaseToke(context: Context, firebase_token: String) {
            getInstance(context).edit().putString(
                FIREBASE_TOKEN, firebase_token
            ).apply()
        }

        fun getFirebaseToken(context: Context): String {
            return getInstance(
                context
            )
                .getString(FIREBASE_TOKEN, "")!!
        }

        fun saveName(context: Context, name: String) {
            getInstance(context).edit().putString(
                NAME, name
            ).apply()
        }

        fun saveNameDelivery(context: Context, name: String) {
            getInstance(context).edit().putString(
                NAME_DELIVERY, name
            ).apply()
        }

        fun saveCity(context: Context, name: String) {
            getInstance(context).edit().putString(
                CITY, name
            ).apply()
        }

        fun saveDistrict(context: Context, name: String) {
            getInstance(context).edit().putString(
                DISTRICT, name
            ).apply()
        }

        fun saveStreet(context: Context, name: String) {
            getInstance(context).edit().putString(
                STREET, name
            ).apply()
        }

        fun saveDeliveryLocation(context: Context, name: String) {
            getInstance(context).edit().putString(
                LOCATION, name
            ).apply()
        }

        fun getDeliveryLocation(context: Context): String {
            return getInstance(
                context
            ).getString(LOCATION, "")!!
        }

        fun getName(context: Context): String {
            return getInstance(
                context
            ).getString(NAME, "")!!
        }

        fun getNameDelivery(context: Context): String {
            return getInstance(
                context
            ).getString(NAME_DELIVERY, "")!!
        }

        fun getCity(context: Context): String {
            return getInstance(
                context
            ).getString(CITY, "")!!
        }

        fun getDistrict(context: Context): String {
            return getInstance(
                context
            ).getString(DISTRICT, "")!!
        }

        fun getStreet(context: Context): String {
            return getInstance(
                context
            ).getString(STREET, "")!!
        }


        fun saveEmail(context: Context, email: String) {
            getInstance(context).edit().putString(
                EMAIL, email
            ).apply()
        }

        fun getEmail(context: Context): String {
            return getInstance(
                context
            ).getString(EMAIL, "")!!
        }

        fun savePhone(context: Context, phone: String) {
            getInstance(context).edit().putString(
                PHONE, phone
            ).apply()
        }

        fun savePhoneDelivey(context: Context, phone: String) {
            getInstance(context).edit().putString(
                PHONE_DELIVERY, phone
            ).apply()
        }

        fun saveServerToken(context: Context, token: String) {
            getInstance(context).edit().putString(
                SERVER_TOKEN, token
            ).apply()
        }

        fun saveCardId(context: Context, cardId: String) {
            getInstance(context).edit().putString(
                CARD_ID, cardId
            ).apply()
        }

        fun saveDate(context: Context, date: String) {
            getInstance(context).edit().putString(
                DATE_ORDER, date
            ).apply()
        }

        fun saveHour(context: Context, hour: String) {
            getInstance(context).edit().putString(
                HOUR_ORDER, hour
            ).apply()
        }

        fun getDate(context: Context): String {
            return getInstance(
                context
            ).getString(DATE_ORDER, "")!!
        }

        fun getHour(context: Context): String {
            return getInstance(
                context
            ).getString(HOUR_ORDER, "")!!
        }

        fun getServerToken(context: Context): String {
            return getInstance(
                context
            ).getString(SERVER_TOKEN, "")!!
        }

        fun getCardId(context: Context): String {
            return getInstance(
                context
            ).getString(CARD_ID, "")!!
        }

        fun saveOrderID(context: Context, orderId: Int) {
            getInstance(context).edit().putInt(
                ORDER_ID, orderId
            ).apply()
        }

        fun saveCurrentLocationLang(context: Context, lang: String) {
            getInstance(context).edit().putString(
                LANG, lang
            ).apply()
        }

        fun saveCurrentLocationLot(context: Context, lot: String) {
            getInstance(context).edit().putString(
                LOT, lot
            ).apply()
        }

        fun getCurrentLocationLang(context: Context): String {
            return getInstance(context).getString(LANG, "")!!
        }

        fun getCurrentLocationLot(context: Context): String {
            return getInstance(context).getString(LOT, "")!!
        }

        fun getOrderId(context: Context): Int {
            return getInstance(context).getInt(ORDER_ID, 0)
        }

        fun getPhone(context: Context): String {
            return getInstance(
                context
            ).getString(PHONE, "")!!
        }

        fun getPhoneDeliver(context: Context): String {
            return getInstance(
                context
            ).getString(PHONE_DELIVERY, "")!!
        }

        fun getUserID(context: Context): String {
            return getInstance(
                context
            ).getString(USER_ID, "")!!
        }

        fun getUserIDFor(context: Context): String {
            return getInstance(
                context
            ).getString(USER_ID_FOR, "")!!
        }

        fun isUserLoggedIn(context: Context): Boolean {
            return getToken(context)
                .isNotEmpty()
        }

        fun saveLocale(context: Context, localeLang: String) {
            getInstance(context).edit().putString(LOCALE_LANG, localeLang).apply()
        }

        fun getLocale(context: Context): String {
            return getInstance(context).getString(LOCALE_LANG, "")!!
        }


        fun saveCurrentOrderId(context: Context, orderId: Int) {
            getInstance(context).edit().putInt(CURRENT_ORDER_ID, orderId).apply()
        }

        fun getCurrentOrderId(context: Context): Int {
            return getInstance(context).getInt(CURRENT_ORDER_ID, -1)
        }

        fun saveOrderStatus(context: Context, orderStatus: Boolean) {
            getInstance(context).edit().putBoolean(ORDER_STATUS, orderStatus).apply()
        }

        fun isOrderActive(context: Context): Boolean {
            return getInstance(context).getBoolean(ORDER_STATUS, false)
        }


        fun saveOrderTitle(context: Context, orderTitle: String) {
            getInstance(context).edit().putString(ORDER_TITLE, orderTitle).apply()
        }

        fun getOrderTitle(context: Context): String {
            return getInstance(context).getString(ORDER_TITLE, "") ?: ""
        }

        fun savePhoneSupport(context: Context, phoneSupport: String) {
            getInstance(context).edit().putString(PHONE_SUPPORT, phoneSupport).apply()
        }

        fun getPhoneSupport(context: Context): String {
            return getInstance(context).getString(PHONE_SUPPORT, "") ?: ""
        }

        fun saveTelegramLink(context: Context, telegramLink: String) {
            getInstance(context).edit().putString(TELEGRAM_LINK, telegramLink).apply()
        }

        fun getTelegramLink(context: Context): String {
            return getInstance(context).getString(TELEGRAM_LINK, "") ?: ""
        }
    }
}

