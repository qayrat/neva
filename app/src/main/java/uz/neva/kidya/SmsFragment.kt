package uz.neva.kidya

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.net.Uri
import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.auth.api.phone.SmsRetriever
import kotlinx.android.synthetic.main.fragment_sms.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.ui.log_in.LoginViewModel
import uz.neva.kidya.ui.log_in.PHONE_NUMBER
import uz.neva.kidya.utils.PrefUtils
import uz.neva.kidya.utils.showSnackbar
import uz.neva.kidya.utils.showSnackbarWithMargin

const val SENDER_PHONE = "NEVA"
const val REQ_USER_CONSENT = 1

class SmsFragment : Fragment(R.layout.fragment_sms) {

    private val viewModel: LoginViewModel by viewModel()
    private var smsBroadcastReceiver: SmsBroadcastReceiver? = null
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private var phoneNumber = ""
    private var addedPosition = 0
    private var buyNow = ""
    private lateinit var timer: CountDownTimer
    private var filter = IntentFilter("android.provider.Telephony.SMS_RECEIVED")
    private var firstCode = ""
    private var secondCode = ""
    private var thirdCode = ""
    private var fourthCode = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            buyNow = it.getString("buyNow") ?: ""
        }
        registerToSmsBroadcastReceiver()
    }

    override fun onResume() {
        super.onResume()
        requireActivity().registerReceiver(smsBroadcastReceiver, filter)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        phoneNumber = arguments?.getString(PHONE_NUMBER) ?: ""

        loginTextSecond.text = phoneNumber

        timer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                textSignUp.text =
                    "${resources.getString(R.string.text_send)} (${millisUntilFinished / 1000} ${resources.getString(R.string.text_c)})"
            }

            override fun onFinish() {
                textSignUp.text = resources.getString(R.string.text_send)
            }

        }
        timer.start()

        SmsRetriever
            .getClient(requireContext())
            .startSmsUserConsent(SENDER_PHONE)


        isCodeValid()



        buttonContinue.setOnClickListener {
            if (firstCode.isNotEmpty()
                && secondCode.isNotEmpty()
                && thirdCode.isNotEmpty()
                && fourthCode.isNotEmpty()
            ) {
                val code = "$firstCode$secondCode$thirdCode$fourthCode"

                buttonContinue.startAnimation {
                    viewModel.getLoginSecondStepResponse(
                        phoneNumber.removeRange(0, 3),
                        code,
                        PrefManager.getFirebaseToken(requireContext())
                    )
                }
            }
        }

        viewModel.loginSecondStepResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(true, progressIndicatorCode, logIn)
                        buttonContinue.revertAnimation()
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(false, progressIndicatorCode, logIn)
                        if (resource.data.status == "OK") {
                            if (resource.data.popup != null) {
                                if (resource.data.popup == "true") {
                                    createAlertDialog()
                                }
                            }
                            PrefManager.saveUserId(requireContext(), resource.data.user_id!!)
                            PrefManager.saveUserIdFor(requireContext(), resource.data.user_id!!)
                            PrefManager.saveName(
                                requireContext(),
                                resource.data.current_user?.name!! + " " + resource.data.current_user?.last_name!!
                            )
                            PrefManager.savePhone(
                                requireContext(),
                                resource.data.current_user?.phone ?: ""
                            )

                            PrefManager.saveStreet(
                                requireContext(),
                                resource.data.current_user?.street ?: ""
                            )
                            PrefManager.saveDistrict(
                                requireContext(),
                                resource.data.current_user?.district ?: ""
                            )
                            PrefManager.saveCity(
                                requireContext(),
                                resource.data.current_user?.address ?: ""
                            )
                            PrefManager.saveServerToken(
                                requireContext(),
                                resource.data.token ?: ""
                            )
                            Log.d("TAGTOKENLO", "onViewCreated: ${resource.data.token}")
                            if (buyNow == "true") {
                                val product = PrefUtils.getProductForBuyNow()
                                checkOutViewModel.getAddToBasketResponse(
                                    PrefManager.getUserID(requireContext()),
                                    "basket",
                                    product.quantity,
                                    product.id,
                                    "add",
                                    "",
                                    product.razmer ?: ""
                                )
                            } else {
                                if (PrefUtils.getProductListCount() > 0) {
                                    val product = PrefUtils.getProduct()
                                    checkOutViewModel.getAddToBasketResponse(
                                        PrefManager.getUserID(requireContext()),
                                        "basket",
                                        product[addedPosition].quantity,
                                        product[addedPosition].id,
                                        "add",
                                        "",
                                        product[addedPosition].razmer ?: ""
                                    )
                                } else {
                                    buttonContinue.revertAnimation()
                                    findNavController().popBackStack(R.id.nav_login, true)
                                    findNavController().popBackStack(R.id.smsFragment, true)
                                }
                            }
                        } else {
                            showSnackbar(resource.data.error!!)
                        }

                    }
                    is Resource.GenericError -> {
                        buttonContinue.revertAnimation()
//                        changeUiStateEnabled(false, progressIndicatorCode, logIn)
                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
//                        changeUiStateEnabled(false, progressIndicatorCode, logIn)
                        buttonContinue.revertAnimation()
                    }
                }
            }
        })

        checkOutViewModel.addToBasketResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(
//                            true,
//                            progressIndicatorAddToCart,
//                            selectedItemAddToCart
//                        )
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(
//                            false,
//                            progressIndicatorAddToCart,
//                            selectedItemAddToCart
//                        )
                        if (buyNow == "true") {
                            checkOutViewModel.getGetCheckOutResponse(
                                PrefManager.getUserID(requireContext()),
                            )
                        } else {
                            addedPosition++
                            showSnackbarWithMargin(resources.getString(R.string.text_added_snackbar_message))
                            (requireActivity() as MainActivity).updateBadge(resource.data.meta.count!!.toInt())
                            if (addedPosition + 1 <= PrefUtils.getProductListCount()) {
                                val product = PrefUtils.getProduct()
                                checkOutViewModel.getAddToBasketResponse(
                                    PrefManager.getUserID(requireContext()),
                                    "basket",
                                    product[addedPosition].quantity,
                                    product[addedPosition].id,
                                    "add",
                                    "",
                                    product[addedPosition].razmer ?: ""
                                )
                            } else {
                                buttonContinue.revertAnimation()
                                PrefUtils.clearProduct()
                                findNavController().navigate(R.id.nav_home)
                            }
                        }


                    }
                    is Resource.GenericError -> {
//                        changeUiStateEnabled(
//                            false,
//                            progressIndicatorAddToCart,
//                            selectedItemAddToCart
//                        )
                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        changeUiStateEnabled(
//                            false,
//                            progressIndicatorAddToCart,
//                            selectedItemAddToCart
//                        )
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        checkOutViewModel.getCheckOutResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(true, progressIndicatorCheckOut, checkOutButton)
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
//                        showSnackbarWithMargin("Оформлен")
//                        (requireActivity() as MainActivity).updateBadge(0)
//                        progress_bar.isVisible = false
                        buttonContinue.revertAnimation()
                        PrefManager.saveOrderID(requireContext(), resource.data.order_id.toInt())
                        findNavController().navigate(R.id.shipmentDetailsFragment)


                    }
                    is Resource.GenericError -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        policy.setOnClickListener {
            val browserIntent =
                Intent(Intent.ACTION_VIEW, Uri.parse("http://kidya.uz/terms-of-use"))
            startActivity(browserIntent)
        }

        textSignUp.setOnClickListener {
            viewModel.getLoginFirstStepResponse(
                phoneNumber.removeRange(0, 3)
            )
        }
    }

    private fun createAlertDialog() {
        val builder = AlertDialog.Builder(requireContext())
        builder.setTitle(resources.getString(R.string.text_congratulations))
        builder.setMessage(resources.getString(R.string.text_congratulations_message))

        builder.setPositiveButton("OK") { dialog, which ->

        }
        builder.show()

    }

    private fun registerToSmsBroadcastReceiver() {
        smsBroadcastReceiver = SmsBroadcastReceiver().also {
            it.smsBroadcastReceiverListener =
                object : SmsBroadcastReceiver.SmsBroadcastReceiverListener {
                    override fun onSuccess(intent: Intent?) {
                        intent?.let { context -> startActivityForResult(context, REQ_USER_CONSENT) }
                    }
                }
        }
        val intentFilter = IntentFilter(SmsRetriever.SMS_RETRIEVED_ACTION)
        requireActivity().registerReceiver(smsBroadcastReceiver, intentFilter)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        if (resultCode != Activity.RESULT_CANCELED) {
            when (requestCode) {
                REQ_USER_CONSENT -> {
                    if (resultCode == Activity.RESULT_OK && data != null) {
                        val message = data.getStringExtra(SmsRetriever.EXTRA_SMS_MESSAGE)
                        val code = message?.let { fetchVerificationCode(it) }
                        Log.d("TAGSMSBROAD", "onActivityResult: $code")
                        if (code != null && code.isNotEmpty()) {
                            val codeList: Array<String> =
                                code.toCharArray().map { it.toString() }.toTypedArray()
                            codeFirst.setText(codeList[0])
                            codeSecond.setText(codeList[1])
                            codeThird.setText(codeList[2])
                            codeFourth.setText(codeList[3])
                        }
                    }
                }
            }
        }
    }

    private fun fetchVerificationCode(sms: String): String =
        sms.substringAfter(" - ").substring(0, 4)

    private fun isCodeValid() {

        codeFirst.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (codeFirst.length() == 1) {
                    firstCode = s.toString()
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        firstCode = ""
                    } else if (s.isNotEmpty()) {
                        codeFirst.clearFocus()
                        codeSecond.requestFocus()
                        codeSecond.isCursorVisible = true
                    }
                }
            }

        })

        codeSecond.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (codeSecond.length() == 1) {
                    secondCode = s.toString()
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        secondCode = ""
                        codeSecond.clearFocus()
                        codeFirst.requestFocus(1)
                        codeFirst.isCursorVisible = true
                    } else if (s.isNotEmpty()) {
                        codeSecond.clearFocus()
                        codeThird.requestFocus()
                        codeThird.isCursorVisible = true
                    }
                }
            }

        })

        codeThird.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (codeThird.length() == 1) {
                    thirdCode = s.toString()
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        thirdCode = ""
                        codeThird.clearFocus()
                        codeSecond.requestFocus(1)
                        codeSecond.isCursorVisible = true
                    } else if (s.isNotEmpty()) {
                        codeThird.clearFocus()
                        codeFourth.requestFocus()
                        codeFourth.isCursorVisible = true
                    }
                }
            }

        })

        codeFourth.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {
            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (codeFourth.length() == 1) {
                    fourthCode = s.toString()
                }
            }

            override fun afterTextChanged(s: Editable?) {
                if (s != null) {
                    if (s.isEmpty()) {
                        fourthCode = ""
                        codeFourth.clearFocus()
                        codeThird.requestFocus(1)
                        codeThird.isCursorVisible = true
                    }
                }
            }

        })
    }



    override fun onPause() {
        super.onPause()
        requireActivity().unregisterReceiver(smsBroadcastReceiver)
    }

    override fun onStop() {
        super.onStop()
        timer.cancel()
        timer.onFinish()
    }

}