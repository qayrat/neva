package uz.neva.kidya

import android.app.Dialog
import android.content.Context
import android.os.Bundle
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_bottomsheet.view.*
import kotlinx.android.synthetic.main.fragment_payment_method.*
import kotlinx.android.synthetic.main.item_payment.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.hideKeyboard


class PaymentMethodFragment : Fragment(R.layout.fragment_payment_method), SetOnMainCardListener {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).toolbar.hide()
    }

    private var cardNumber = ""
    private var cardName = ""
    private var cardId = ""
    private var cardExpire = ""
    private var cardModel: CardModel? = null
    private val homeViewModel: HomeViewModel by viewModel()
    private val itemAdapter = ItemAdapter<CardModel>()
    private val fastAdapter =
        FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<CardModel>() {
            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<CardModel>,
                item: CardModel
            ) {
                val bottomSheetDialog = BottomSheetFragment.newInstance(this@PaymentMethodFragment)
                bottomSheetDialog.show(
                    requireActivity().supportFragmentManager,
                    "Bottom Sheet Dialog Fragment"
                )
                cardModel = item
            }

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView
            }
        })
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }
        cardList.layoutManager = LinearLayoutManager(requireContext())
        cardList.adapter = fastAdapter


        et_number_card.addTextChangedListener(CreditCardNumberFormattingTextWatcher())
        et_expire.addTextChangedListener(CreditCardExpireFormattingTextWatcher())

        addCard.setOnClickListener {
            upload.isVisible = true
            addCardView.isVisible = true
            addCard.isVisible = false
            cardList.isVisible = false
            emptyView.isVisible = false
            emptyBasketText.isVisible = false
            emptyBasketSubText.isVisible = false
            view5.isVisible = false
            add.isVisible = false
        }
        add.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isNotEmpty()) {
                upload.isVisible = true
                addCardView.isVisible = true
                addCard.isVisible = false
                cardList.isVisible = false
                emptyView.isVisible = false
                emptyBasketText.isVisible = false
                emptyBasketSubText.isVisible = false
                view5.isVisible = false
                add.isVisible = false
            } else {
                findNavController().navigate(R.id.nav_login)
            }

        }

        upload.setOnClickListener {
            if (et_number_card.text.toString().isEmpty()) {
                et_number_card.error = resources.getString(R.string.text_enter_info)
            } else {
                cardNumber = et_number_card.text.toString().replace("\\s".toRegex(), "")
            }
            if (et_expire.text.toString().isEmpty()) {
                et_expire.error = resources.getString(R.string.text_enter_info)
            } else {
                cardExpire = et_expire.text.toString().replace("/", "")
            }
            if (et_card_name.text.toString().isEmpty()) {
                et_card_name.error = resources.getString(R.string.text_enter_info)
            } else {
                cardName = et_card_name.text.toString()
            }

            if (cardName.isNotEmpty() && cardExpire.isNotEmpty() && cardName.isNotEmpty()) {
                upload.startAnimation {
                    homeViewModel.addCard(
                        PrefManager.getUserID(requireContext()),
                        PrefManager.getServerToken(requireContext()),
                        cardName,
                        cardNumber,
                        cardExpire
                    )
                }
            }
        }



        homeViewModel.addCardResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        upload.apply {
                            revertAnimation()
                            background = resources.getDrawable(
                                R.drawable.button_save,
                                requireActivity().theme
                            )
                        }
                        cardAdd.isVisible = false
                        add_verify.isVisible = true
                        upload.isVisible = false
                        save.isVisible = true
                        val cardList = resource.data.data
                        cardId = cardList[0].keys.first()
                        addCard.isVisible = false
                        emptyView.isVisible = false
                        emptyBasketText.isVisible = false
                        emptyBasketSubText.isVisible = false
                        view5.isVisible = false
                        add.isVisible = false

                        for (i in 0 until cardList.size) {
                            if (i == cardList.size - 1) {
                                itemAdapter.add(
                                    CardModel(
                                        cardList[i].keys.first().toInt(),
                                        cardName = cardList[i].values.first().name,
                                        cardCode = cardList[i].values.first().number,
                                        isSelectedCard = true
                                    )
                                )
                                PrefManager.saveCardId(requireContext(), cardList[i].keys.first())
                            } else {
                                itemAdapter.add(
                                    CardModel(
                                        cardList[i].keys.first().toInt(),
                                        cardName = cardList[i].values.first().name,
                                        cardCode = cardList[i].values.first().number
                                    )
                                )
                            }
                        }
                    }
                    is Resource.Error -> {
                        upload.apply {
                            revertAnimation()
                            background = resources.getDrawable(
                                R.drawable.button_save,
                                requireActivity().theme
                            )
                        }
                    }
                    is Resource.GenericError -> {
                        upload.apply {
                            revertAnimation()
                            background = resources.getDrawable(
                                R.drawable.button_save,
                                requireActivity().theme
                            )
                        }
                    }
                }
            }
        })

        save.setOnClickListener {
            if (et_card_verify_code.text.toString().isEmpty()) {
                et_card_verify_code.error = resources.getString(R.string.text_enter_info)
            } else {
                if (cardName.isNotEmpty() && cardExpire.isNotEmpty() && cardName.isNotEmpty()) {

                    save.startAnimation {
                        homeViewModel.verifyCard(
                            PrefManager.getServerToken(requireContext()),
                            et_card_verify_code.text.toString().replace("\\s".toRegex(), ""),
                            cardId
                        )
                    }
                }
            }
        }

        homeViewModel.verifyCardResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        homeViewModel.getCardList(
                            PrefManager.getUserID(requireContext()),
                            PrefManager.getServerToken(requireContext())
                        )
                        progress_payment.isVisible = true
                        emptyViewLayout.isVisible = false
                    }
                    is Resource.Error -> {

                    }
                    is Resource.GenericError -> {

                    }
                }
            }
        })

        if (PrefManager.getUserIDFor(requireContext()).isNotEmpty()) {
            homeViewModel.getCardList(
                PrefManager.getUserIDFor(requireContext()),
                PrefManager.getServerToken(requireContext())
            )
            homeViewModel.getCardListResponse.observe(viewLifecycleOwner, {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {
                        is Resource.Loading -> {

                        }
                        is Resource.Success -> {
                            save.apply {
                                revertAnimation()
                                background = resources.getDrawable(
                                    R.drawable.button_save,
                                    requireActivity().theme
                                )
                                isVisible = false
                            }
                            progress_payment.isVisible = false
                            hideKeyboard()
                            cardList.isVisible = resource.data.data.isNotEmpty()
                            addCardView.isVisible = false
                            val cardList = resource.data.data
                            addCard.isVisible = cardList.isNotEmpty()
                            emptyViewLayout.isVisible = cardList.isEmpty()
                            emptyView.isVisible = cardList.isEmpty()

                            add_verify.isVisible = false
                            save.isVisible = false

                            emptyBasketText.isVisible = cardList.isEmpty()
                            emptyBasketSubText.isVisible = cardList.isEmpty()
                            view5.isVisible = cardList.isEmpty()
                            add.isVisible = cardList.isEmpty()



                            itemAdapter.clear()
                            if (cardList.size == 1) {
                                itemAdapter.add(
                                    CardModel(
                                        cardList[0].keys.first().toInt(),
                                        cardName = cardList[0].values.first().name,
                                        cardCode = cardList[0].values.first().number,
                                        isSelectedCard = true
                                    )
                                )
                                PrefManager.saveCardId(
                                    requireContext(),
                                    cardList[0].keys.first()
                                )
                            } else if (cardList.size > 1) {
                                val cardNumber = PrefManager.getCardNumber(requireContext())
                                for (i in 0 until cardList.size) {
                                    if (cardList[i].values.first().number == cardNumber) {
                                        itemAdapter.add(
                                            CardModel(
                                                cardList[i].keys.first().toInt(),
                                                cardName = cardList[i].values.first().name,
                                                cardCode = cardList[i].values.first().number,
                                                isSelectedCard = true
                                            )
                                        )
                                    } else {
                                        itemAdapter.add(
                                            CardModel(
                                                cardList[i].keys.first().toInt(),
                                                cardName = cardList[i].values.first().name,
                                                cardCode = cardList[i].values.first().number
                                            )
                                        )
                                    }

                                }
                            }

//                            for (i in 0 until cardList.size) {
//                                if (i == cardList.size - 1) {
//                                    itemAdapter.add(
//                                        CardModel(
//                                            cardList[i].keys.first().toInt(),
//                                            cardName = cardList[i].values.first().name,
//                                            cardCode = cardList[i].values.first().number,
//                                            isSelectedCard = true
//                                        )
//                                    )
//                                    PrefManager.saveCardId(
//                                        requireContext(),
//                                        cardList[i].keys.first()
//                                    )
//                                } else {
//                                    itemAdapter.add(
//                                        CardModel(
//                                            cardList[i].keys.first().toInt(),
//                                            cardName = cardList[i].values.first().name,
//                                            cardCode = cardList[i].values.first().number
//                                        )
//                                    )
//                                }
//                            }
                        }
                        is Resource.Error -> {
                            save.apply {
                                revertAnimation()
                                background = resources.getDrawable(
                                    R.drawable.button_save,
                                    requireActivity().theme
                                )
                            }
                        }
                        is Resource.GenericError -> {
                            save.apply {
                                revertAnimation()
                                background = resources.getDrawable(
                                    R.drawable.button_save,
                                    requireActivity().theme
                                )
                            }
                        }
                    }
                }
            })

            homeViewModel.deleteCardResponse.observe(viewLifecycleOwner, {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {
                        is Resource.Loading -> {

                        }
                        is Resource.Success -> {
                            cardModel?.let {
                                itemAdapter.remove(itemAdapter.getAdapterPosition(it))
                                fastAdapter.notifyAdapterItemChanged(fastAdapter.getPosition(it))
                            }
                            PrefManager.saveCardNumber(requireContext(), "")
                        }
                        is Resource.Error -> {

                        }
                        is Resource.GenericError -> {

                        }
                    }
                }
            })
        }
    }

    override fun setMain() {
        cardModel?.let {
            itemAdapter.itemList.items.forEach { e -> e.isSelectedCard = false }
            fastAdapter.notifyAdapterDataSetChanged()
            it.isSelectedCard = !it.isSelectedCard
            PrefManager.saveCardId(requireContext(), it.id.toString())
            PrefManager.saveCardNumber(requireContext(), it.cardCode.toString())
            fastAdapter.notifyAdapterItemChanged(fastAdapter.getPosition(it))
        }
    }

    override fun onDelete() {
        homeViewModel.deleteCard(
            PrefManager.getServerToken(requireContext()),
            cardModel?.id.toString()
        )
    }
}

data class CardModel(
    val id: Int,
    var isSelectedCard: Boolean = false,
    val cardName: String? = "",
    val cardCode: String? = "",
    var cardType: String? = "",
    var paymentType: String = "",
    var cardDate: String? = ""
) : AbstractItem<CardModel.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<CardModel>(itemView) {

        override fun bindView(item: CardModel, payloads: List<Any>) {
            itemView.iconSelectedCard.isVisible = item.isSelectedCard
            itemView.cardName.text = item.cardName
            itemView.stars.text = "${
                item.cardCode?.substring(
                    0,
                    4
                )
            } **** **** ${item.cardCode?.substring(item.cardCode.length - 4, item.cardCode.length)}"
            if (item.isSelectedCard) {
                PrefManager.saveCardNumber(itemView.context, item.cardCode.toString())
            }
        }

        override fun unbindView(item: CardModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_payment
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}

class BottomSheetFragment(val listener: SetOnMainCardListener) : BottomSheetDialogFragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        val contentView = View.inflate(context, R.layout.fragment_bottomsheet, null)
        dialog.setContentView(contentView)
        contentView.ll_set_main.setOnClickListener {
            listener.setMain()
            dialog.dismiss()
        }
        contentView.ll_delete.setOnClickListener {
            listener.onDelete()
            dialog.dismiss()
        }
        (contentView.parent as View).setBackgroundColor(resources.getColor(android.R.color.transparent))
    }

    companion object {
        fun newInstance(listener: SetOnMainCardListener): BottomSheetFragment {
            return BottomSheetFragment(listener)
        }
    }
}

interface SetOnMainCardListener {
    fun setMain()
    fun onDelete()
}

class CreditCardNumberFormattingTextWatcher : TextWatcher {
    private var lock = false
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun afterTextChanged(s: Editable) {
        if (lock || s.length > 16) {
            return
        }
        lock = true
        var i = 4
        while (i < s.length) {
            if (s.toString()[i] != ' ') {
                s.insert(i, " ")
            }
            i += 5
        }
        lock = false
    }
}

class CreditCardExpireFormattingTextWatcher : TextWatcher {
    private var lock = false
    override fun onTextChanged(s: CharSequence, start: Int, before: Int, count: Int) {}
    override fun beforeTextChanged(s: CharSequence, start: Int, count: Int, after: Int) {}
    override fun afterTextChanged(s: Editable) {
        if (lock || s.length > 16) {
            return
        }
        lock = true
        var i = 2
        while (i < s.length) {
            if (s.toString()[i] != '/') {
                s.insert(i, "/")
            }
            i += 3
        }
        lock = false
    }
}