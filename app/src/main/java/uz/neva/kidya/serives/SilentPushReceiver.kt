package uz.neva.kidya.serives

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.util.Log
import com.yandex.metrica.push.YandexMetricaPush

class SilentPushReceiver():BroadcastReceiver(){
    override fun onReceive(context: Context?, intent: Intent?) {
        val payload=intent?.getStringExtra(YandexMetricaPush.EXTRA_PAYLOAD)
        Log.i("yandexpush",payload.toString())
    }

}