package uz.neva.kidya

import android.app.AlertDialog
import android.app.DatePickerDialog
import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import android.widget.RadioButton
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_add_baby.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.PrefUtils
import uz.neva.kidya.utils.showSnackbarWithMargin
import java.text.SimpleDateFormat
import java.util.*
import kotlin.random.Random

class AddBabyFragment : Fragment(R.layout.fragment_add_baby) {

    private var gExpand = false
    private var expand = false

    private var babyName = ""
    private var babyBirthday = ""
    private var babyGander = ""
    private val profileViewModel: ProfileViewModel by viewModel()
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        baby_gander.setOnCheckedChangeListener { group, checkedId ->
            val pol = group.findViewById(checkedId) as RadioButton
            text_gander.text = pol.text.toString()
            babyGander = pol.text.toString()
            expandable_layout_gender.isExpanded = true
        }


        textView30.setOnClickListener {
            if (baby_name.text.toString().isEmpty()) {
                baby_name.error = resources.getString(R.string.text_not_fill)
            } else {
                babyName = baby_name.text.toString()
            }

            if (editDate.text.toString().isEmpty()) {
                editDate.error = resources.getString(R.string.text_not_fill)
            } else {
                babyBirthday = editDate.text.toString()
            }

            baby_gander.setOnCheckedChangeListener { group, checkedId ->
                val pol = group.findViewById(checkedId) as RadioButton
                text_gander.text = pol.text.toString()
                babyGander = pol.text.toString()
            }



            if (babyName.isNotEmpty() && babyBirthday.isNotEmpty() && babyGander.isNotEmpty()) {
                val babyModel = BabyModel(
                    Random.nextInt(),
                    babyName,
                    babyBirthday,
                    babyGander
                )
                PrefUtils.addBaby(babyModel)
                showSnackbarWithMargin(resources.getString(R.string.text_added))
                findNavController().popBackStack()
            }
        }

        val calendar = Calendar.getInstance()
        val date =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabel(calendar)
            }

        editDate.setOnClickListener {
            val datePicker = DatePickerDialog(
                requireContext(), AlertDialog.THEME_HOLO_LIGHT, date, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.show()
            datePicker.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.menu_selected_color))
            datePicker.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.menu_selected_color))

        }
        linearLayout5.setOnClickListener {
            if (!gExpand) {
                expandable_layout_gender.isExpanded = true
                expand_icon_gender.setImageDrawable(resources.getDrawable(R.drawable.back_up))
                gExpand = true
            } else {
                expandable_layout_gender.isExpanded = false
                expand_icon_gender.setImageDrawable(resources.getDrawable(R.drawable.back_down))
                gExpand = false
            }
        }

    }

    private fun updateLabel(calendar: Calendar) {
        val myFormat = "MM/dd/yy"

        val sdf = SimpleDateFormat(myFormat, Locale.US)

        editDate.setText(sdf.format(calendar.time))
    }

}