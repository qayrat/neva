package uz.neva.kidya

import android.app.AlertDialog.THEME_HOLO_LIGHT
import android.app.DatePickerDialog
import android.app.DatePickerDialog.OnDateSetListener
import android.content.Context
import android.content.IntentSender
import android.location.Address
import android.location.Geocoder
import android.location.LocationManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.RadioButton
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.location.LocationStatus
import kotlinx.android.synthetic.main.fragment_account_edit.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.nevaModel.profile.ProfileData
import uz.neva.kidya.network.dto.nevaModel.profile.ProfileUpdateModel
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.formatPhoneMask
import uz.neva.kidya.utils.getMaskedPhoneWithoutSpace
import uz.neva.kidya.utils.showSnackbarWithMargin
import java.text.SimpleDateFormat
import java.util.*


class AccountEditFragment : Fragment(R.layout.fragment_account_edit) {

    private var expand = false
    private var gExpand = false
    var chosenAddressName = ""
    var chosenAddressNumber = ""
    var chosenAddressCity = ""
    var chosenAddressStreet = ""
    var chosenAddressDistrict = ""
    var chosenAddressEmail = ""
    var latitude = 0.0
    var longitude = 0.0
    private val profileViewModel: ProfileViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        chosenAddressName = PrefManager.getName(requireContext())
        chosenAddressEmail = PrefManager.getEmail(requireContext())
        chosenAddressNumber = PrefManager.getPhone(requireContext())
        chosenAddressCity = PrefManager.getCity(requireContext())
        chosenAddressDistrict = PrefManager.getDistrict(requireContext())
        chosenAddressStreet = PrefManager.getStreet(requireContext())
        MapKitFactory.setApiKey("1080018e-efa1-4eb6-821b-82f01120bf4c")
        MapKitFactory.initialize(requireContext())
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        editPhoneNumber.formatPhoneMask(true)

        if (PrefManager.getProfilePol(requireContext()).isNotEmpty())
            text_gender.text = PrefManager.getProfilePol(requireContext())

        if (PrefManager.getProfileDate(requireContext()).isNotEmpty())
            editDate.setText(PrefManager.getProfileDate(requireContext()))

        profile_data.setText(PrefManager.getName(requireContext()))
        editPhoneNumber.setText(PrefManager.getPhone(requireContext()))
        profile_email.setText(PrefManager.getEmail(requireContext()))
        profile_address.setText(PrefManager.getCity(requireContext()))

        val calendar = Calendar.getInstance()
        val date =
            OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                calendar.set(Calendar.YEAR, year)
                calendar.set(Calendar.MONTH, monthOfYear)
                calendar.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                updateLabel(calendar)
            }


//        if (checkIfGpsEnabled()) {
//            findMe()
//        } else
//            requestLocationSettingsOn()


        editDate.setOnClickListener {
            val datePicker = DatePickerDialog(
                requireContext(), THEME_HOLO_LIGHT, date, calendar
                    .get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH)
            )
            datePicker.show()
            datePicker.getButton(DatePickerDialog.BUTTON_POSITIVE)
                .setTextColor(resources.getColor(R.color.menu_selected_color))
            datePicker.getButton(DatePickerDialog.BUTTON_NEGATIVE)
                .setTextColor(resources.getColor(R.color.menu_selected_color))


        }

        profile_pol.setOnCheckedChangeListener { group, checkedId ->
            val pol = group.findViewById<RadioButton>(checkedId) as RadioButton
            text_gender.text = pol.text.toString()
            PrefManager.saveProfilePol(requireContext(), pol.text.toString())
            expandable_layout_gender.isExpanded = false
        }

        save_user_data.setOnClickListener {

            if (profile_data.text.toString().isEmpty()) {
                profile_data.error = resources.getString(R.string.text_not_fill)
            } else {
                PrefManager.saveName(requireContext(), profile_data.text.toString())
            }

            if (editPhoneNumber.text.toString().isEmpty()) {
                editPhoneNumber.error = resources.getString(R.string.text_not_fill)
            } else {
                PrefManager.savePhone(requireContext(), editPhoneNumber.text.toString())
            }

            if (editDate.text.toString().isEmpty()) {
                editDate.error = resources.getString(R.string.text_not_fill)
            } else {
                PrefManager.saveProfileDate(requireContext(), editDate.text.toString())
            }

            profile_pol.setOnCheckedChangeListener { group, checkedId ->
                val pol = group.findViewById<RadioButton>(checkedId) as RadioButton
                text_gender.text = pol.text.toString()
                PrefManager.saveProfilePol(requireContext(), pol.text.toString())
                expandable_layout_gender.isExpanded = false
            }

            if (profile_email.text.toString().isNotEmpty()) {
                PrefManager.saveEmail(requireContext(), profile_email.text.toString())
            } else {
                PrefManager.saveEmail(requireContext(), "")
            }

            if (profile_address.text.toString().isEmpty()) {
                profile_address.error = resources.getString(R.string.text_not_fill)
            } else {
                PrefManager.saveProfileAddress(requireContext(), profile_address.text.toString())
                Timber.d(PrefManager.getProfileAddress(requireContext()))
            }


            if (PrefManager.getUserID(requireContext()).isNotEmpty() &&
                PrefManager.getName(requireContext()).isNotEmpty() &&
                editPhoneNumber.getMaskedPhoneWithoutSpace().isNotEmpty() &&
                PrefManager.getProfileAddress(requireContext()).isNotEmpty() &&
                PrefManager.getProfileDate(requireContext()).isNotEmpty()
            ) {
//                profileViewModel.updateProfile(
//                    user_id = PrefManager.getUserID(requireContext()),
//                    name = PrefManager.getName(requireContext()),
//                    phone = editPhoneNumber.getMaskedPhoneWithoutSpace(),
//                    address = PrefManager.getProfileAddress(requireContext()),
//                    district = PrefManager.getDistrict(requireContext()),
//                    street = PrefManager.getCity(requireContext()),
//                    gender = if (PrefManager.getProfilePol(requireContext()) == resources.getString(
//                            R.string.text_male
//                        )
//                    ) "M" else "F",
//                    birthday = PrefManager.getProfileDate(requireContext()).replace("/", "."),
//                    email = PrefManager.getEmail(requireContext()),
//                    interest = ""
//                )
                profileViewModel.updateProfile2(ProfileUpdateModel(
                        entity = "user",
                        method = "profileUpdate",
                        data = ProfileData(
                                name = PrefManager.getName(requireContext()),
                                lastName = PrefManager.getName(requireContext()),
                                personalPhone = editPhoneNumber.getMaskedPhoneWithoutSpace(),
                                personalStreet = profile_address.text.toString(),
                                personalGender = if (PrefManager.getProfilePol(requireContext()) ==
                                        resources.getString(R.string.text_male)) "M" else "F",
                                personalNotes = "",
                                personalBirthday = PrefManager.getProfileDate(requireContext())
                                        .replace("/", "."),
                                email = PrefManager.getEmail(requireContext())
                        )
                ), PrefManager.getUserID(requireContext()))
                progressIndicatorProfile.isVisible = true
            }
        }

        profileViewModel.updateProfileResponse.observe(
            viewLifecycleOwner,
            {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {
                        is Resource.Loading -> {

                        }
                        is Resource.Success -> {

                            progressIndicatorProfile.isVisible = false
                            PrefManager.saveName(requireContext(), resource.data.data.name + " " + resource.data.data.last_name)
                            PrefManager.savePhone(requireContext(), "+" + resource.data.data.phone)
                            PrefManager.saveCity(requireContext(), resource.data.data.address ?: "")
                            PrefManager.saveDistrict(requireContext(), resource.data.data.district ?: "")
                            PrefManager.saveStreet(requireContext(), resource.data.data.street ?: "")
                            PrefManager.saveProfileDate(requireContext(), resource.data.data.birthday ?: "")

                            showSnackbarWithMargin(resources.getString(R.string.text_added))
                            findNavController().popBackStack()

                        }
                        is Resource.GenericError -> {
                            progressIndicatorProfile.isVisible = false
                        }
                        is Resource.Error -> {
                            progressIndicatorProfile.isVisible = false
//                            showSnackbarWithMargin(resource.exception.message ?: "")
                        }

                    }
                }
            })

        profileViewModel.updateProfileResponse2.observe(
                viewLifecycleOwner,
                {
                    it.getContentIfNotHandled()?.let { resource ->
                        when (resource) {
                            is Resource.Loading -> {
                            }
                            is Resource.Success -> {
                                progressIndicatorProfile.isVisible = false
                                showSnackbarWithMargin(resources.getString(R.string.text_added))
                                findNavController().popBackStack()
                            }
                            is Resource.GenericError -> {
                                progressIndicatorProfile.isVisible = false
                                showSnackbarWithMargin(resources.getString(R.string.edit_profile_something_wrong))
                            }
                            is Resource.Error -> {
                                progressIndicatorProfile.isVisible = false
                                showSnackbarWithMargin(resources.getString(R.string.edit_profile_something_wrong))
                            }

                        }
                    }
                })

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }
        if (chosenAddressName.removePrefix(" ").isNotEmpty())
            profile_data.setText(chosenAddressName)

        if (chosenAddressNumber.isNotEmpty()) {
            editPhoneNumber.setText(chosenAddressNumber)
        }

        if (chosenAddressEmail.isNotEmpty())
            profile_email.setText(chosenAddressEmail)





        linearLayout5.setOnClickListener {
            if (!gExpand) {
                expandable_layout_gender.isExpanded = true
                expand_icon_gender.setImageDrawable(resources.getDrawable(R.drawable.back_up))
                gExpand = true
            } else {
                expandable_layout_gender.isExpanded = false
                expand_icon_gender.setImageDrawable(resources.getDrawable(R.drawable.back_down))
                gExpand = false
            }
        }

    }

    private fun updateLabel(calendar: Calendar) {
        val myFormat = "dd.MM.yyyy" //In which you need put here

        val sdf = SimpleDateFormat(myFormat, Locale.US)

        editDate.setText(sdf.format(calendar.time))
    }

    private fun checkIfGpsEnabled(): Boolean {
        val lm: LocationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun requestLocationSettingsOn() {
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
            .addLocationRequest(createLocationRequest()!!)

        val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
            // All loc_pin settings are satisfied. The client can initialize
            // loc_pin requests here.
            findMe()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                        requireActivity(),
                        1001
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.warning_enable_permission),
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
        }
    }


    private fun createLocationRequest(): LocationRequest? {
        return LocationRequest.create()?.apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    private fun findMe() {
        progressIndicatorProfile.isVisible = true
        MapKitFactory.getInstance().createLocationManager()
            .requestSingleUpdate(object : com.yandex.mapkit.location.LocationListener {
                override fun onLocationUpdated(location: com.yandex.mapkit.location.Location) {
                    Log.d(
                        "TAGLATLONG",
                        "onRequestPermissionsResult:2 ${location.position.latitude} ${location.position.longitude}"
                    )
                    latitude = location.position.latitude
                    longitude = location.position.longitude

                    val addresses: List<Address>
                    val geocoder = Geocoder(requireContext(), Locale.getDefault())
                    try {
                        addresses = geocoder.getFromLocation(
                            latitude,
                            longitude,
                            1
                        ) // Here 1 represent max location result to returned, by documents it recommended 1 to 5


                        val address: String =
                            addresses[0].getAddressLine(0) // If any additional address line present than only, check with max available address lines by getMaxAddressLineIndex()

                        profile_address.setText(address)
                    } catch (e: Exception) {
                        showSnackbarWithMargin(resources.getString(R.string.text_not_allowed_location))
                    }

                    progressIndicatorProfile.isVisible = false
                }

                override fun onLocationStatusUpdated(locationStatus: LocationStatus) {
                    progressIndicatorProfile.isVisible = false
                    Log.d("TagCheck", locationStatus.name)
                }
            })
    }

    override fun onStart() {
        super.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        MapKitFactory.getInstance().onStop()
    }
}