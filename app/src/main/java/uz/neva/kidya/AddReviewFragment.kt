package uz.neva.kidya

import android.content.Context
import android.os.Bundle
import android.util.DisplayMetrics
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.fragment_add_review.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.utils.showSnackbar

class AddReviewFragment : Fragment(R.layout.fragment_add_review) {

    private var productId = ""
    private var productRating: Float = 0.0f
    private val categoriesViewModel: CategoriesViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            productId = it.getString("product_id").toString()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val displayMetrics = DisplayMetrics()
        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.R) {
            requireContext().display?.getRealMetrics(displayMetrics)
        }else{
            requireActivity().windowManager.defaultDisplay.getRealMetrics(displayMetrics)
        }

        var width = displayMetrics.widthPixels
        var height = 280

        categoriesViewModel.getSelectedProductResponse(
            PrefManager.getUserID(requireContext()),
            productId,
            width = width,
            height = height
        )

        categoriesViewModel.selectedProductResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        val data = resource.data.product
                        if (data.fields.preview_picture != null && data.fields.preview_picture != "")
                            if (data.fields.preview_picture[0] == '/') {
                                Glide.with(requireContext())
                                    .load(Const.BASE_URL + data.fields.preview_picture)
                                    .override(
                                        201, 156
                                    ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                                    .into(product_image)
                            } else {
                                Glide.with(requireContext()).load(data.fields.preview_picture)
                                    .override(
                                        201,
                                        156
                                    ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                                    .into(product_image)
                            }
                        else
                            product_image.setImageDrawable(resources.getDrawable(R.drawable.ic_baseline_broken_image_24))

                    }
                    is Resource.GenericError -> {
//
                    }
                    is Resource.Error -> {

                    }
                }
            }
        })

        checkOutBack.setOnClickListener {
            findNavController().popBackStack()
        }

        ratingBar.setOnRatingBarChangeListener { ratingBar, rating, fromUser ->
            ratingBar.rating = rating
            productRating = rating
            Toast.makeText(requireContext(), "$rating", Toast.LENGTH_SHORT).show()
        }

        review.setOnClickListener {
            if (review_text.text.toString().isEmpty()) {
                review_text.error = "Не указано"
            } else {
                categoriesViewModel.addReviews(
                    productId,
                    PrefManager.getUserID(requireContext()),
                    review_text.text.toString(),
                    productRating
                )
            }
        }

        categoriesViewModel.selectedProductReviewsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        if (resource.data.status == "OK") {
                            showSnackbar(resources.getString(R.string.text_send_message))
                            findNavController().popBackStack()
                        }
                    }
                    is Resource.GenericError -> {
                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 ->
                            showSnackbar(it1)
                        }
                    }
                }
            }
        })
    }
}