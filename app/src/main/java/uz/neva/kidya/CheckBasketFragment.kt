package uz.neva.kidya

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_check_basket.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.ui.check_out.BasketItemListAdapter
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.showSnackbarWithMargin


class CheckBasketFragment : Fragment(R.layout.fragment_check_basket) {
    private lateinit var basketItemListAdapter: BasketItemListAdapter
    private val profileViewModel: ProfileViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkOutBack.setOnClickListener {
            findNavController().popBackStack()
        }

        basketItemListAdapter = BasketItemListAdapter(requireContext(), this)
        basketRecycler2.layoutManager = LinearLayoutManager(requireContext())
        basketRecycler2.adapter = basketItemListAdapter

            profileViewModel.getGetOrderDetailsResponse(
                PrefManager.getUserID(requireContext()),
                PrefManager.getOrderId(requireContext()).toString()
            )

        profileViewModel.getOrderDetailsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        val list = resource.data.products
                        basketItemListAdapter.updateList(list as ArrayList<BasketItemObject>)
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbarWithMargin(it1) }
                    }
                }
            }
        })

    }
}