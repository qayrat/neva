package uz.neva.kidya

import android.annotation.SuppressLint
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.View
import androidx.core.view.isVisible
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_sub_category_product.*
import kotlinx.android.synthetic.main.fragment_sub_category_product.empty_page
import kotlinx.android.synthetic.main.fragment_sub_category_product.selectedCategoryName
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.ui.category.subcategories.OnSortDialogClick
import uz.neva.kidya.ui.category.subcategories.SortDialogFragment
import uz.neva.kidya.ui.category.subcategories.SubCategoryFragment


class SubCategoryProductFragment : Fragment(R.layout.fragment_sub_category_product),
    OnSortDialogClick {

    companion object {
        val TAG = "SubCategoryProductFragment"
    }

    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private var sectionId: String? = null
    private var sectionName: String? = null
    private lateinit var categoryAdapter2: SelectedCategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            sectionId = it.getString("section_id")
            sectionName = it.getString("section_name")
        }
        categoryAdapter2 = SelectedCategoryAdapter(requireContext())
        categoriesViewModel.getProductTotalCount(sectionId?:"")
        getFeedPosts(sectionId ?: "")
    }

    @SuppressLint("LongLogTag")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        Log.d(TAG, "onViewCreated: $sectionId $sectionName")

        sub_product_back.setOnClickListener {
            findNavController().popBackStack()
        }

        filterLay.setOnClickListener {
            val dialogFragment = SortDialogFragment(this)
            dialogFragment.show(requireActivity().supportFragmentManager, "dialog")
        }

        section_name.text = sectionName

        setupRecyclerView()

        sub_product_list.adapter = categoryAdapter2

        categoriesViewModel.isSuccess.observe(viewLifecycleOwner, { isHasProduct ->
            shimmer_sub_product_container.stopShimmer()
            shimmer_sub_product_container.visibility = View.GONE
            sub_product_list.isVisible = isHasProduct
            empty_page.isVisible = !isHasProduct
        })

        categoriesViewModel.totalCount.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        selectedCategoryName.text =
                            "${resources.getString(R.string.text_result)} ${resource.data.pagination?.total_count}"
                    }
                    else -> {
                        Log.d(SubCategoryFragment.TAG, "onViewCreated: ${resource.toString()}")
                    }
                }
            }
        })
    }


    private fun getFeedPosts(sectionId: String) {
        lifecycleScope.launch {
            categoriesViewModel.getSelectedCategoryProductsResponse(
                sectionId,
                PrefManager.getUserID(requireContext())
            ).collect { product ->
                categoryAdapter2.submitData(product)
                return@collect
            }
        }
        categoryAdapter2.addLoadStateListener { loadState ->
            if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached && categoryAdapter2.itemCount < 1) {
                categoriesViewModel.setData(false)
            } else {
                if (categoryAdapter2.itemCount > 0) {
                    categoriesViewModel.setData(true)
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        shimmer_sub_product_container.startShimmer()
    }

    override fun onPause() {
        shimmer_sub_product_container.stopShimmer()
        super.onPause()
    }


    private fun setupRecyclerView() {
        sub_product_list.layoutManager =
            GridLayoutManager(requireContext(), 2, LinearLayoutManager.VERTICAL, false)
        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()
        sub_product_list.addItemDecoration(SpacesItemDecoration(marginpixels))
    }

    @SuppressLint("SetTextI18n")
    override fun newProduct() {

        lifecycleScope.launch {
            categoriesViewModel.getFilteredResult(
                PrefManager.getUserID(requireContext()),
                section_id = sectionId.toString(),
                sort = "id:desc"
            ).collect { product ->
                categoryAdapter2.submitData(product)
                return@collect
            }

        }
    }

    @SuppressLint("SetTextI18n")
    override fun priceUp() {
        lifecycleScope.launch {
            categoriesViewModel.getFilteredResult(
                PrefManager.getUserID(requireContext()),
                section_id = sectionId.toString(),
                sort = "price:asc"
            ).collect { product ->
                categoryAdapter2.submitData(product)
                return@collect
            }

        }
    }

    @SuppressLint("SetTextI18n")
    override fun priceDown() {
        lifecycleScope.launch {
            categoriesViewModel.getFilteredResult(
                PrefManager.getUserID(requireContext()),
                section_id = sectionId.toString(),
                sort = "price:desc"
            ).collect { product ->
                categoryAdapter2.submitData(product)
                return@collect
            }

        }
    }

    @SuppressLint("SetTextI18n")
    override fun priceReview() {
        lifecycleScope.launch {
            categoriesViewModel.getFilteredResult(
                PrefManager.getUserID(requireContext()),
                section_id = sectionId.toString(),
                sort = "id:asc"
            ).collect { product ->
                categoryAdapter2.submitData(product)
                return@collect
            }
        }
    }
}