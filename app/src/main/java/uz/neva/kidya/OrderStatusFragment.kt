package uz.neva.kidya

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.View
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_order_status.*


class OrderStatusFragment : Fragment(R.layout.fragment_order_status) {

    private var orderId = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            orderId = it.getString("order_id").toString()
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        btn_something.setOnClickListener {
            findNavController().navigate(R.id.nav_home)
        }

        trackOrder.setOnClickListener {
            val bundle = Bundle()
            bundle.putString("order_id", orderId)
            findNavController().navigate(R.id.trackOrderFragment, bundle)
        }

        cancel.setOnClickListener {
            val intent = Intent(Intent.ACTION_DIAL)
            intent.data = Uri.parse("tel:+998781488008")
            startActivity(intent)
        }

    }

}