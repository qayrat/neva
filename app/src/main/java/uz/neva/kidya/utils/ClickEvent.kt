package uz.neva.kidya.utils

import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.ProductSortObject

sealed class ClickEvents {
    data class onItemClick(val product:ProductObject) : ClickEvents()
    data class onAddToFav(val product:ProductObject,var index:Int) : ClickEvents()
}

sealed class ClickEventsSort {
    data class onItemClick(val product:ProductSortObject) : ClickEventsSort()
    data class onAddToFav(val product:ProductSortObject,var index:Int) : ClickEventsSort()
}