package uz.neva.kidya.utils

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.Rect
import android.net.Uri
import android.os.Build
import android.util.DisplayMetrics
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.view.ViewGroup
import android.view.ViewTreeObserver
import android.view.animation.AccelerateDecelerateInterpolator
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.FrameLayout
import android.widget.TextView
import androidx.annotation.CallSuper
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.core.content.ContextCompat
import androidx.core.widget.doOnTextChanged
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.navigation.NavController
import androidx.navigation.fragment.findNavController
import com.google.android.material.snackbar.Snackbar
import com.google.android.material.tabs.TabLayout
import com.google.android.material.textfield.TextInputEditText
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import kotlinx.android.synthetic.main.fragment_cupon.*
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.ui.log_in.LogInFragment
import java.text.NumberFormat
import java.util.*


fun Fragment.log(message: String, tag: String = "DefaultTag") {
    Log.d(tag, message)
}

fun Context.log(message: String, tag: String = "DefaultTag") {
    Log.d(tag, message)
}

/**
 * Animating view with slide animation
 */
fun View.slideDown() {
    this.animate().translationY(500f).alpha(0.0f).setDuration(50).setInterpolator(
        AccelerateDecelerateInterpolator()
    )
    this.hide()
}

fun View.slideUp() {
    this.show()
    this.animate().translationY(0f).alpha(1.0f).setDuration(150).setInterpolator(
        AccelerateDecelerateInterpolator()
    )
}

/**
 * Hide the view. (visibility = View.INVISIBLE)
 */
fun View.hide(): View {
    if (visibility != View.GONE) {
        visibility = View.GONE
        Log.d("DefaultTag", "actually hidden")
    }
    return this
}

fun View.show(): View {
    if (visibility != View.VISIBLE) {
        visibility = View.VISIBLE
    }
    return this
}

/**
 * Extension method to provide hide keyboard for [Activity].
 */
fun Activity.hideSoftKeyboard() {
    if (currentFocus != null) {
        val inputMethodManager = getSystemService(
            Context
                .INPUT_METHOD_SERVICE
        ) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
    }
}

fun Fragment.hideKeyboard() {
    activity?.hideSoftKeyboard()
}

fun Fragment.showKeyboard(view: View) {
    activity?.showKeyboard(view)
}

fun Activity.showKeyboard(view: View) {
    val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    imm.showSoftInput(view, InputMethodManager.SHOW_IMPLICIT)
}

fun Fragment.makePhoneCall(phone: String) {
    val call = Intent(Intent.ACTION_DIAL);
    call.data = Uri.parse("tel:$phone")
    requireActivity().startActivity(call)
}

interface OnBackPressed {
    fun onBackPressed()
}

/**
 * Transforms static java function Snackbar.make() to an extension function on View.
 */
fun Fragment.showSnackbar(snackbarText: String, timeLength: Int = Snackbar.LENGTH_SHORT) {
    Snackbar.make(requireView(), snackbarText, timeLength)
        .setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.menu_selected_color))
        .setActionTextColor(ContextCompat.getColor(requireContext(), android.R.color.white)).show()
}

fun Fragment.showSnackbarWithMargin(snackbarText: String, timeLength: Int = Snackbar.LENGTH_SHORT) {
    val snackbar = Snackbar.make(requireView(), snackbarText, timeLength)
    val snackBarView: View = snackbar.getView()
    val params = snackBarView.layoutParams as FrameLayout.LayoutParams

    params.setMargins(
        params.leftMargin + 48,
        params.topMargin,
        params.rightMargin + 48,
        params.bottomMargin + 48
    )

    snackBarView.layoutParams = params

    snackBarView.setTranslationY(-(convertDpToPixel(48f, requireActivity())));
    snackbar.setBackgroundTint(
        ContextCompat.getColor(
            requireContext(),
            R.color.menu_selected_color
        )
    )
        .setActionTextColor(ContextCompat.getColor(requireContext(), android.R.color.white))
    snackbar.show()
}

fun Fragment.showSnackbarAuth(snackbarText: String, timeLength: Int = Snackbar.LENGTH_SHORT) {
    val snackbar = Snackbar.make(requireView(), snackbarText, timeLength)
        .setAction("Авторизироваться") {
            findNavController().navigate(R.id.nav_login)
        }
        .setBackgroundTint(ContextCompat.getColor(requireContext(), R.color.menu_selected_color))
        .setActionTextColor(ContextCompat.getColor(requireContext(), android.R.color.white))
    val snackBarView: View = snackbar.getView()
    val params = snackBarView.layoutParams as FrameLayout.LayoutParams

    params.setMargins(
        params.leftMargin + 48,
        params.topMargin,
        params.rightMargin + 48,
        params.bottomMargin + 48
    )

    snackBarView.layoutParams = params

    snackBarView.setTranslationY(-(convertDpToPixel(48f, requireActivity())));
    snackbar.show()
}

fun Context.dpToPx(valueInDp: Float): Float {
    val metrics = resources.displayMetrics
    return TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, valueInDp, metrics)
}

fun EditText.hideErrorIfFilled() {
    this.doOnTextChanged { text, start, before, count ->
        if (this.text.toString().isNotEmpty() and !this.error.isNullOrEmpty()) {
            this.error = null
        }
    }
}

fun EditText.showErrorIfNotFilled() {
    if (this.text.toString().isEmpty()) {
        this.error = context.getString(R.string.warning_fill_the_fields)
    }
}

fun EditText.showError() {
    this.error = context.getString(R.string.warning_fill_the_fields)
}

fun TextInputEditText.showError(error: String) {
    this.error = error
}

fun AppCompatButton.showError(error: String) {
    this.error = error
}

fun AppCompatButton.hideError() {
    this.error = null
}

/**
 * Change language
 */
fun Fragment.setAppLocale(localeCode: String, context: Context) {
    activity?.setAppLocale(localeCode, context)
}

fun Activity.setAppLocale(localeCode: String, context: Context) {
    val resources = context.resources
    val dm = resources.displayMetrics
    val config = resources.configuration
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
        config.setLocale(Locale(localeCode.toLowerCase(Locale.ROOT)))
        PrefManager.saveLocale(context, localeCode.toLowerCase(Locale.ROOT))
    } else {
        PrefManager.saveLocale(context, localeCode.toLowerCase(Locale.ROOT))
        config.setLocale(Locale(localeCode.toLowerCase(Locale.ROOT)))
    }
    resources.updateConfiguration(config, dm)
}


fun EditText.formatPhoneMask(icon: Boolean = false) {
    val affineFormats: MutableList<String> = ArrayList()
    affineFormats.add("+998 [00] [000] [00] [00]")

    val listener =
        MaskedTextChangedListener.installOn(
            this,
            "+998 [00] [000] [00] [00]",
            affineFormats, AffinityCalculationStrategy.WHOLE_STRING,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(
                    maskFilled: Boolean,
                    extractedValue: String,
                    formattedValue: String
                ) {
                    if (icon) {
                        if (maskFilled)
                            this@formatPhoneMask.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.ic_baseline_check_circle_green,
                                0
                            )
                        else
                            this@formatPhoneMask.setCompoundDrawablesWithIntrinsicBounds(
                                0,
                                0,
                                R.drawable.check_stop,
                                0
                            )

                    }

                }
            }
        )
}

fun EditText.getMaskedPhoneWithoutSpace(): String {
    var phone = this.text.toString()
    if (phone.startsWith("+"))
        phone = phone.substring(1, phone.length)
    return phone.replace(" ", "")
}

fun Fragment.changeUiStateEnabled(isLoading: Boolean, progressBar: View, viewButton: View) {
    viewButton.isEnabled = !isLoading
    if (isLoading) progressBar.visible() else progressBar.gone()
}

fun View.visible() {
    this.visibility = View.VISIBLE
}

fun View.gone() {
    this.visibility = View.GONE
}


fun TextView.formatCurrencyMask(price: Float) {
    val format = NumberFormat.getCurrencyInstance(Locale("ru", "UZ"))
    this.text = format.format(price)
}

/**
 * This method converts dp unit to equivalent pixels, depending on device density.
 *
 * @param dp A value in dp (density independent pixels) unit. Which we need to convert into pixels
 * @param context Context to get resources and device specific display metrics
 * @return A float value to represent px equivalent to dp depending on device density
 */
fun convertDpToPixel(dp: Float, context: Context): Float {
    return dp * (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}

/**
 * This method converts device specific pixels to density independent pixels.
 *
 * @param px A value in px (pixels) unit. Which we need to convert into db
 * @param context Context to get resources and device specific display metrics
 * @return A float value to represent dp equivalent to px value
 */
fun convertPixelsToDp(px: Float, context: Context): Float {
    return px / (context.resources.displayMetrics.densityDpi.toFloat() / DisplayMetrics.DENSITY_DEFAULT)
}


// Add these extension functions to an empty kotlin file
fun Activity.getRootView(): View {
    return findViewById<View>(android.R.id.content)
}

fun Context.convertDpToPx(dp: Float): Float {
    return TypedValue.applyDimension(
        TypedValue.COMPLEX_UNIT_DIP,
        dp,
        this.resources.displayMetrics
    )
}


fun Activity.isKeyboardOpen(): Boolean {
    val visibleBounds = Rect()
    this.getRootView().getWindowVisibleDisplayFrame(visibleBounds)
    val heightDiff = getRootView().height - visibleBounds.height()
    val marginOfError = Math.round(this.convertDpToPx(50F))
    return heightDiff > marginOfError
}

fun Activity.isKeyboardClosed(): Boolean {
    return !this.isKeyboardOpen()
}

fun TabLayout.setMargins(left: Int, top: Int, right: Int, bottom: Int) {
    for (i in 0 until this.tabCount) {
        val tab = (this.getChildAt(0) as ViewGroup).getChildAt(i)
        val p = tab.layoutParams as ViewGroup.MarginLayoutParams
        p.setMargins(left, top, right, bottom)
        tab.requestLayout()
    }
}

class KeyboardEventListener(
    private val activity: AppCompatActivity,
    private val callback: (isOpen: Boolean) -> Unit
) : LifecycleObserver {

    private val listener = object : ViewTreeObserver.OnGlobalLayoutListener {
        private var lastState: Boolean = activity.isKeyboardOpen()

        override fun onGlobalLayout() {
            val isOpen = activity.isKeyboardOpen()
            if (isOpen == lastState) {
                return
            } else {
                dispatchKeyboardEvent(isOpen)
                lastState = isOpen
            }
        }
    }

    init {
        // Dispatch the current state of the keyboard
        dispatchKeyboardEvent(activity.isKeyboardOpen())
        // Make the component lifecycle aware
        activity.lifecycle.addObserver(this)
        registerKeyboardListener()
    }

    private fun registerKeyboardListener() {
        activity.getRootView().viewTreeObserver.addOnGlobalLayoutListener(listener)
    }

    private fun dispatchKeyboardEvent(isOpen: Boolean) {
        when {
            isOpen -> callback(true)
            !isOpen -> callback(false)
        }
    }

    @OnLifecycleEvent(value = Lifecycle.Event.ON_PAUSE)
    @CallSuper
    fun onLifecyclePause() {
        unregisterKeyboardListener()
    }

    private fun unregisterKeyboardListener() {
        activity.getRootView().viewTreeObserver.removeOnGlobalLayoutListener(listener)
    }
}