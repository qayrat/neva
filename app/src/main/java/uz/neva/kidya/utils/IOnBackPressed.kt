package uz.neva.kidya.utils

interface IOnBackPressed {
    fun onBackPressed(): Boolean
}