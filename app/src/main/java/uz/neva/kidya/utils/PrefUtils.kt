package uz.neva.kidya.utils

import com.orhanobut.hawk.Hawk
import uz.neva.kidya.BabyModel
import uz.neva.kidya.network.dto.BasketItemObject

object PrefUtils {
    const val PREF_FOR_BASKET = "pref_for_basket"
    const val PREF_FOR_BUY_NOW = "pref_for_buy_now"
    const val PREF_FOR_ADD_BABY = "pref_for_add_baby"

    fun setProduct(item: BasketItemObject) {
        val items = Hawk.get(PREF_FOR_BASKET, arrayListOf<BasketItemObject>())
        items.add(item)
        Hawk.put(PREF_FOR_BASKET, items)
    }

    fun addBaby(baby: BabyModel) {
        val items = Hawk.get(PREF_FOR_ADD_BABY, arrayListOf<BabyModel>())
        items.add(baby)
        Hawk.put(PREF_FOR_ADD_BABY, items)
    }

    fun getProduct(): ArrayList<BasketItemObject> {
        val items = Hawk.get<ArrayList<BasketItemObject>>(PREF_FOR_BASKET)
        return items ?: arrayListOf()
    }

    fun getBaby(): ArrayList<BabyModel> {
        val items = Hawk.get<ArrayList<BabyModel>>(PREF_FOR_ADD_BABY)
        return items ?: arrayListOf()
    }

    fun clearProduct() {
        val products = Hawk.get<ArrayList<BasketItemObject>>(PREF_FOR_BASKET)
        if (products != null) {
            products.clear()
            Hawk.put(PREF_FOR_BASKET, products)
        }
    }

    fun removeProduct(item: BasketItemObject) {
        val products = Hawk.get<ArrayList<BasketItemObject>>(PREF_FOR_BASKET)
        if (products.isNotEmpty()) {
            products.remove(item)
            Hawk.put(PREF_FOR_BASKET, products)
        }
    }

    fun removeProductQuantity(item: BasketItemObject) {
        val products = Hawk.get<ArrayList<BasketItemObject>>(PREF_FOR_BASKET)
        products.firstOrNull { e -> e == item }?.quantity?.toInt() ?: 1 - 1
        Hawk.put(PREF_FOR_BASKET, products)
    }

    fun getProductListCount(): Int {
        val items = Hawk.get(PREF_FOR_BASKET, arrayListOf<BasketItemObject>())
        return items.size
    }

    fun setProductForBuyNow(item: BasketItemObject) {
        Hawk.put(PREF_FOR_BUY_NOW, item)
    }

    fun getProductForBuyNow(): BasketItemObject {
        return Hawk.get(PREF_FOR_BUY_NOW)
    }
}