package uz.neva.kidya.utils

import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.adapters.ItemAdapter
import uz.neva.kidya.network.dto.NotificationModel
import uz.neva.kidya.ui.check_out.BasketItemListAdapter

class SwipeToDelete(var adapter: BasketItemListAdapter) :
    ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {

    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        TODO("Not yet implemented")
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        var position = viewHolder.adapterPosition
        adapter.deleteItem(position)
    }

}

class SwipeDeleteToNotification(
    var adapter: ItemAdapter<NotificationModel>,
    var listener: OnDeleteNotificationItemListener
) : ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
    override fun onMove(
        recyclerView: RecyclerView,
        viewHolder: RecyclerView.ViewHolder,
        target: RecyclerView.ViewHolder
    ): Boolean {
        return false
    }

    override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
        val position = viewHolder.absoluteAdapterPosition
        listener.onSwipeToDelete(adapter.getAdapterItem(position))
    }

}

interface OnDeleteNotificationItemListener {
    fun onSwipeToDelete(item: NotificationModel)
}