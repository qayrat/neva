package uz.neva.kidya

import android.Manifest
import android.annotation.SuppressLint
import android.content.pm.PackageManager
import android.location.Location
import android.os.Bundle
import android.util.Log
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import android.widget.Toolbar
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import androidx.core.view.get
import androidx.core.view.isVisible
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.AppBarConfiguration
import androidx.navigation.ui.setupActionBarWithNavController
import androidx.navigation.ui.setupWithNavController
import com.google.android.gms.location.*
import com.google.android.gms.tasks.OnCompleteListener
import com.google.android.material.badge.BadgeDrawable
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.firebase.analytics.FirebaseAnalytics
import com.google.firebase.analytics.ktx.analytics
import com.google.firebase.ktx.Firebase
import com.google.firebase.messaging.FirebaseMessaging
import com.sanojpunchihewa.updatemanager.UpdateManager
import com.sanojpunchihewa.updatemanager.UpdateManager.UpdateInfoListener
import com.sanojpunchihewa.updatemanager.UpdateManagerConstant
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.content_main.*
import kotlinx.android.synthetic.main.drawer_inner.*
import kotlinx.android.synthetic.main.drawer_inner.view.*
import kotlinx.android.synthetic.main.fragment_notification.*
import kotlinx.android.synthetic.main.fragment_search_result.*
import kotlinx.android.synthetic.main.nav_header_main.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.notification.NotificationViewModel
import uz.neva.kidya.utils.*
import java.util.*
import kotlin.collections.HashMap


private const val MAIN_STATE = "MAIN_STATE"

class MainActivity : AppCompatActivity() {
    var state = 1
    var inAuth = false
    private var locationRequest: LocationRequest? = null
    private var locationCallback: LocationCallback? = null
    val statuses: HashMap<String, String> = HashMap()
    lateinit var badge: BadgeDrawable
    private val viewModel: NotificationViewModel by viewModel()
    private lateinit var appBarConfiguration: AppBarConfiguration
    lateinit var bottomNavigationView: BottomNavigationView
    private lateinit var firebaseAnalytics: FirebaseAnalytics
    private val locationRequestCode = 1000
    private var wayLatitude = 0.0
    private var wayLongitude: Double = 0.0
    private var mFusedLocationClient: FusedLocationProviderClient? = null
    private var updateManager: UpdateManager? = null

    @SuppressLint("RestrictedApi")
    override fun onCreate(savedInstanceState: Bundle?) {
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
        super.onCreate(savedInstanceState)

        updateManager = UpdateManager.Builder(this).mode(UpdateManagerConstant.FLEXIBLE)
        updateManager?.start()

        setTheme(R.style.Theme_KIDYA_NoActionBar)
        setContentView(R.layout.activity_main)
        val toolbar: androidx.appcompat.widget.Toolbar = findViewById(R.id.toolbar)
        toolbar.setNavigationIcon(R.drawable.icon_back)
        setSupportActionBar(toolbar)

        checkVersion()

        bottomNavigationView = findViewById(R.id.bottom_navigation)
        bottomNavigationView.itemIconTintList = null

        updateManager?.addUpdateInfoListener(object : UpdateInfoListener {
            override fun onReceiveVersionCode(code: Int) {
                Log.d("TAGUPDATECODE", "onReceiveVersionCode: $code")
            }

            override fun onReceiveStalenessDays(days: Int) {
                // Number of days passed since the user was notified of an update through the Google Play
                // If the user hasn't notified this will return -1 as days
                // You can decide the type of update you want to call
                Log.d("TAGUPDATECODE", "onReceiveStalenessDays: $days")
            }
        })

        statuses["DF"] = "Отгружен"
        statuses["DN"] = "Ожидает обработки"
        statuses["F"] = "Выполнен"
        statuses["N"] = "Принят, ожидается оплата"

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment?
        val navController = navHostFragment!!.navController
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        appBarConfiguration = AppBarConfiguration(
            setOf(
                R.id.nav_home,
                R.id.nav_category,
                R.id.nav_checkOut,
                R.id.nav_favourites,
                R.id.profileFragment
            )
        )
        setupActionBarWithNavController(navController, appBarConfiguration)
        bottomNavigationView.setupWithNavController(navController)

        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create();
        locationRequest?.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY)
        locationRequest?.setInterval(20 * 1000);
        badge = bottomNavigationView.getOrCreateBadge(R.id.action_bag)
        badge.isVisible = true
        badge.number = 0
        badge.backgroundColor = ContextCompat.getColor(this, R.color.red)
        badge.badgeTextColor = ContextCompat.getColor(this, R.color.white)

        if (badge.number < 1) {
            if (badge != null) {
                badge.isVisible = false
                badge.clearNumber()
            }
        }

        if (PrefManager.getUserID(this).isEmpty()) {
            var s = 0
            PrefUtils.getProduct().forEach { e -> s += e.quantity.toInt() }
            updateBadge(s)
        }

        linearLayout.setOnClickListener {
            navController.navigate(R.id.nav_searchResult)
        }
        viewModel.getNotifications(PrefManager.getUserID(this))
        bottomNavigationView.setOnNavigationItemSelectedListener {
            when (it.itemId) {
                R.id.action_home -> {
                    if (navController.currentDestination?.id != R.id.nav_home)
                        navController.navigate(R.id.nav_home)
                }
                R.id.action_grid -> {
                    if (navController.currentDestination?.id != R.id.nav_category)
                        navController.navigate(R.id.nav_category)
                }
                R.id.action_menu -> {
                    if (navController.currentDestination?.id != R.id.profileFragment)
                        navController.navigate(R.id.profileFragment)
                }
                R.id.action_bag -> {
                    if (navController.currentDestination?.id != R.id.nav_checkOut)
                        navController.navigate(R.id.nav_checkOut)
                }
                R.id.action_heart -> {

                    if (navController.currentDestination?.id != R.id.nav_favourites) {
                        if (PrefManager.getUserID(this).isEmpty()) {
                            navController.navigate(R.id.nav_login)
                        } else
                            navController.navigate(R.id.nav_favourites)
                    }
                }
            }
            true
        }

        viewModel.getNotificationsResponse.observe(this, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        val count = resource.data.data.size
                        updateNotify(count)
                    }
                    else -> {

                    }
                }
            }
        })


        bellIcon.setOnClickListener {
            if (navController.currentDestination?.id != R.id.nav_notification) {
                if (PrefManager.getUserID(this).isEmpty()) {
                    navController.navigate(R.id.nav_login)
                } else {
                    navController.navigate(R.id.nav_notification)
                }
            }
        }


        mainContent.setOnClickListener {
            hideSoftKeyboard()
        }

        FirebaseMessaging.getInstance().token.addOnCompleteListener(OnCompleteListener { task ->
            if (!task.isSuccessful) {
                Log.w("FBTAG", "Fetching FCM registration token failed", task.exception)
                return@OnCompleteListener
            }

            // Get new FCM registration token
            val token = task.result

            // Log and toast
            val msg = getString(R.string.msg_token_fmt, token)
            Log.d("FBTAG", token.toString())
            PrefManager.saveFirebaseToke(this, token.toString())
            // Toast.makeText(baseContext, msg, Toast.LENGTH_SHORT).show()
        })

        FirebaseMessaging.getInstance().subscribeToTopic("all")
            .addOnCompleteListener {task->
                if (task.isSuccessful){

                }
            }

        // Obtain the FirebaseAnalytics instance.
        firebaseAnalytics = Firebase.analytics

        navController.addOnDestinationChangedListener { controller, destination, arguments ->

            toolbar.isVisible = true
            inAuth = false
            bottomNavigationView.show()
            hideSoftKeyboard()
            when (destination.id) {
                R.id.nav_login -> {
                    inAuth = true
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.nav_home -> {
                    toolbar.isVisible = true
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_category -> {
                    toolbar.isVisible = true
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_myDetailsFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_myOrders -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_selectedOrder -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_subCategory -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.nav_returnProducts -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_favourites -> {
                    toolbar.isVisible = true
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_messages -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_selected_message -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_checkOut -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_selectedItem -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = true
                }
                R.id.nav_searchResult -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.nav_notification -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.nav_FAQ -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.nav_deals -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.nav_map -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.smsFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.profileDataFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.accountEditFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.babiesFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.addBabyFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.cuponFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.settingsFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.paymentMethodFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.orderStatusFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.trackOrderFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.discountFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.selectedShopFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.checkBasketFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.addReviewFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.questionAnsweSettingsFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.recommendedFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
                R.id.profileFragment -> {
                    toolbar.isVisible = true
                    bottomNavigationView.isVisible = true
                }
                R.id.subCategoryProductFragment -> {
                    toolbar.isVisible = false
                    bottomNavigationView.isVisible = false
                }
            }

        }


        locationCallback = object : LocationCallback() {
            override fun onLocationResult(locationResult: LocationResult) {
                if (locationResult == null) {
                    return
                }
                for (location in locationResult.locations) {
                    if (location != null) {
                        wayLatitude = location.latitude
                        wayLongitude = location.longitude
                        Log.d(
                            "TAGLATLONG",
                            "onRequestPermissionsResult:3 $wayLatitude $wayLongitude"
                        )
                    }
                }
            }
        }
        // check permission
        if (ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_FINE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
            && ActivityCompat.checkSelfPermission(
                this,
                Manifest.permission.ACCESS_COARSE_LOCATION
            ) != PackageManager.PERMISSION_GRANTED
        ) {
            // reuqest for permission
            ActivityCompat.requestPermissions(
                this,
                arrayOf(
                    Manifest.permission.ACCESS_FINE_LOCATION,
                    Manifest.permission.ACCESS_COARSE_LOCATION
                ),
                locationRequestCode
            )
        } else {
            // already permission granted
            mFusedLocationClient!!.getLastLocation().addOnSuccessListener(
                this
            ) { location: Location? ->
                if (location != null) {
                    wayLatitude = location.latitude
                    wayLongitude = location.longitude
                    Log.d(
                        "TAGLATLONG",
                        "onRequestPermissionsResult:1 $wayLatitude $wayLongitude"
                    )
                }
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)
        when (requestCode) {
            1000 -> {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.isNotEmpty()
                    && grantResults[0] === PackageManager.PERMISSION_GRANTED
                ) {
                    if (ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_FINE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(
                            this,
                            Manifest.permission.ACCESS_COARSE_LOCATION
                        ) != PackageManager.PERMISSION_GRANTED
                    ) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return
                    }
                    mFusedLocationClient!!.getLastLocation()
                        .addOnSuccessListener(this) { location ->
                            if (location != null) {
                                wayLatitude = location.getLatitude()
                                wayLongitude = location.getLongitude()
                                Log.d(
                                    "TAGLATLONG",
                                    "onRequestPermissionsResult:2 $wayLatitude $wayLongitude"
                                )
                            }
                        }
                } else {
                    Toast.makeText(this, "Permission denied", Toast.LENGTH_SHORT).show()
                }
            }
        }
    }

    private fun checkVersion() {
        if (android.os.Build.VERSION.SDK_INT >= 23) {
            window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR;
        } else {
            window.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS)
            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)
            window.statusBarColor = ContextCompat.getColor(this, R.color.white)
        }
    }

    fun updateNotify(count: Int) {
        notificationTotal.isVisible = count != 0
        notificationTotal.text = if (count > 99) {
            "99+"
        } else {
            count.toString()
        }
    }

    fun updateBadge(count: Int) {
        badge.number = count
        if (badge.number < 1) {
            if (badge != null) {
                badge.isVisible = false
                badge.clearNumber()
            }
        } else {
            badge.isVisible = count != 0
        }
    }


    override fun onResume() {
        super.onResume()

        KeyboardEventListener(this) { isOpen ->
            if (isOpen) bottomNavigationView.hide()
            else if (!inAuth) bottomNavigationView.show()
        }
    }

    override fun onStop() {
        super.onStop()
        Log.d("TAGSTOP", "onStop: STOP")
    }

    override fun onDestroy() {
        super.onDestroy()
        Log.d("TAGSTOP", "onDestroy: Destroy")
    }

}
