package uz.neva.kidya.network.dto

import android.view.View
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_frequently.view.*
import kotlinx.android.synthetic.main.item_frequently_2.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.utils.show

class MCatalog(
    val product: ProductObject

)

class MFrequently(
    val frequently: List<ProductObject2>?
)

class MMoreProduct(
    val more_products:Map<String,ProductObject3>
)

data class ProductObject2(
    val fields: FieldObject,
    val params: List<ParamsObject>,
    val offers: List<OfferObject>?
) : AbstractItem<ProductObject2.SelectedViewHolder>() {

    override var identifier: Long
        get() = fields.id.toLong()
        set(value) {}

    class SelectedViewHolder(itemView: View) : FastAdapter.ViewHolder<ProductObject2>(itemView) {
        override fun bindView(item: ProductObject2, payloads: List<Any>) {

            val itemItemName = itemView.selectedCategoryItemName

            //        private var itemRating=itemView.selectedCategoryItemRatingBar
            val itemPrice = itemView.selectedCategoryItemPrice
            val itemOldPrice = itemView.selectedCategoryItemOldPrice
            val itemBrand = itemView.selectedCategoryItemBrand
            val itemIsFav = itemView.isFavourite
            val itemTag = itemView.tagback

            if (item.fields.preview_picture != null && item.fields.preview_picture != "")
                if (item.fields.preview_picture[0] == '/') {
                    Glide.with(itemView.context)
                        .load(Const.BASE_URL + item.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(200, 200)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemView.selectedCategoryItemImage)
                } else {
                    Glide.with(itemView.context).load(item.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(200, 200)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemView.selectedCategoryItemImage)
                }
            else
                itemView.selectedCategoryItemImage.setImageDrawable(
                    itemView.context.resources.getDrawable(
                        R.drawable.ic_baseline_broken_image_24
                    )
                )

            itemTag.visibility = View.GONE
            itemItemName.text = item.fields.name
            itemPrice.text =
                "${(item.fields.catalog_price_1 ?: "0").replace(",", " ")} сум"

            if (item.fields.percent != null) {
                itemOldPrice.show()
                itemOldPrice.text =
                    "${(item.fields.old_price ?: "0").replace(",", " ")} сум"
                itemPrice.text =
                    "${(item.fields.catalog_price_1) ?: "0".replace(",", " ")} сум"
                itemTag.visibility = View.VISIBLE
                itemTag.text = "-${item.fields.percent ?: ""}% off"
            }

            for (items in item.params) {

                when (items.name) {
                    "Поставщик" -> {

                        itemBrand.text = items.value[0]
                    }
                    "Цена со скидкой" -> {
                        itemOldPrice.show()
                        itemOldPrice.text = "${item.fields.catalog_price_1 ?: "0".replace(","," ")} сум"
                        itemPrice.text = "${items.value[0].replace(",", " ")} сум"
                        itemTag.visibility = View.VISIBLE
                        itemTag.text = "${item.fields.percent ?: ""}-% off"
                    }
                    "Цена" -> {
                        if (itemTag.visibility == View.GONE)
                            itemPrice.text = "${items.value[0].replace(",", " ")} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(itemView.context).load(items.value[0])
                                .thumbnail(0.2f)
                                .override(200, 200)
                                .placeholder(R.drawable.placeholder_neva)
                                .into(itemView.selectedCategoryItemImage)
                    }

                }
            }


            if (item.fields.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_favorited)
            } else
                itemIsFav.setImageResource(R.drawable.ic_favourite)
        }

        override fun unbindView(item: ProductObject2) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_frequently
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): SelectedViewHolder = SelectedViewHolder(v)
}

data class ProductObject3(
    val fields: FieldObject,
    val params: List<ParamsObject>,
    val offers: List<OfferObject>?
) : AbstractItem<ProductObject3.SelectedViewHolder>() {

    override var identifier: Long
        get() = fields.id.toLong()
        set(value) {}

    class SelectedViewHolder(itemView: View) : FastAdapter.ViewHolder<ProductObject3>(itemView) {
        override fun bindView(item: ProductObject3, payloads: List<Any>) {

            val itemItemName = itemView.selectedCategoryItemName2

            //        private var itemRating=itemView.selectedCategoryItemRatingBar
            val itemPrice = itemView.selectedCategoryItemPrice2
            val itemOldPrice = itemView.selectedCategoryItemOldPrice2
            val itemBrand = itemView.selectedCategoryItemBrand2
            val itemIsFav = itemView.isFavourite2
            val itemTag = itemView.tagback2

            if (item.fields.preview_picture != null && item.fields.preview_picture != "")
                if (item.fields.preview_picture[0] == '/') {
                    Glide.with(itemView.context)
                            .load(Const.BASE_URL + item.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(200, 200)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemView.selectedCategoryItemImage2)
                } else {
                    Glide.with(itemView.context).load(item.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(200, 200)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemView.selectedCategoryItemImage2)
                }
            else
                itemView.selectedCategoryItemImage.setImageDrawable(
                    itemView.context.resources.getDrawable(
                        R.drawable.ic_baseline_broken_image_24
                    )
                )

            itemTag.visibility = View.GONE
            itemItemName.text = item.fields.name
            itemPrice.text =
                "${(item.fields.catalog_price_1 ?: "0").replace(",", " ")} сум"

            if (item.fields.percent != null) {
                itemOldPrice.show()
                itemOldPrice.text =
                    "${(item.fields.old_price ?: "0").replace(",", " ")} сум"
                itemPrice.text =
                    "${(item.fields.catalog_price_1) ?: "0".replace(",", " ")} сум"
                itemTag.visibility = View.VISIBLE
                itemTag.text = "-${item.fields.percent ?: ""}% off"
            }

            for (items in item.params) {

                when (items.name) {
                    "Поставщик" -> {

                        itemBrand.text = items.value[0]
                    }
                    "Цена со скидкой" -> {
                        itemOldPrice.show()
                        itemOldPrice.text = "${item.fields.catalog_price_1 ?: "0".replace(","," ")} сум"
                        itemPrice.text = "${items.value[0].replace(",", " ")} сум"
                        itemTag.visibility = View.VISIBLE
                        itemTag.text = "${item.fields.percent ?: ""}-% off"
                    }
                    "Цена" -> {
                        if (itemTag.visibility == View.GONE)
                            itemPrice.text = "${items.value[0].replace(",", " ")} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(itemView.context).load(items.value[0])
                                .thumbnail(0.2f)
                                .override(200, 200)
                                .placeholder(R.drawable.placeholder_neva)
                                .into(itemView.selectedCategoryItemImage2)
                    }

                }
            }


            if (item.fields.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_favorited)
            } else
                itemIsFav.setImageResource(R.drawable.ic_favourite)
        }

        override fun unbindView(item: ProductObject3) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_frequently_2
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): SelectedViewHolder = SelectedViewHolder(v)
}