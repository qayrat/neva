package uz.neva.kidya.network.dto

data class PopularsList (
    val data: Map<String,Boolean>
)

data class PopularsListObject(
    val hit:Boolean,
    val top_clothes:Boolean,
    val top_shoes:Boolean,
    val top_food:Boolean,
    val top_toy:Boolean
)