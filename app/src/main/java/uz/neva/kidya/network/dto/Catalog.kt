package uz.neva.kidya.network.dto

import android.view.View
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.flash_sale_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import java.io.Serializable

class Catalog(
    val id: String? = "",
    val name: String? = "",
    val data: List<ProductObject>,
    val links: LinkObjects?,
    val pagination: PaginationObject?
) {
    @Transient
    var updatedPosition = -1
    var updatedStatus = false
}

class CatalogNew(
        val id: String? = "",
        val name: String? = "",
        val data: List<ProductObjectNew>,
        val links: LinkObjects?,
        val pagination: PaginationObject?
) {
    @Transient
    var updatedPosition = -1
    var updatedStatus = false
}

class CatalogSort(
    val id: String? = "",
    val name: String? = "",
    val data: List<ProductSortObject>,
    val links: LinkObjects?,
    val pagination: PaginationObject?
) {
    @Transient
    var updatedPosition = -1
    var updatedStatus = false
}

data class FlashSaleModel(
    val data: List<FlashSaleProduct>,
    val active: String? = null,
    val current_date: String? = null,
    val diff: DifModel? = null,
    val links: LinkObjects? = null,
    val pagination: PaginationObject? = null
)

class DifModel(
    val day:Int,
    val hour:Int,
    val minute:Int,
    val second:Int
)

data class ProductCount(
    val pagination: PaginationObject?
)

data class ProductObject(
    val fields: FieldObject,
    val params: List<ParamsObject>,
    val offers: List<OfferObject>?
) : Serializable

data class ProductSortObject(
    val fields: FieldObject? = null,
    val params: List<ParamsObject>,
    val offers: List<OfferObject>?
) : Serializable

data class ProductObjectNew(
        val fields: FieldObject? = null,
        val params: List<ParamsObject>? = null,
        val offers: List<OfferObject>?
) : Serializable

data class OfferObject(
    val id: String,
    val name: String,
    val color: String?,
    val razmer: String?,
    val old_price: String?,
    val price: String?,
    val more_photo: List<String>?
)

data class ParamsObject(
    val name: String,
    val value: List<String>
)

data class FieldObject(
    val id: String = "",
    val iblock_section_id: String = "",
    val preview_picture: String? = "",
    val name: String? = "",
    val preview_text: String? = "",
    val detail_text: String? = "",
    val catalog_price_1: String? = "".replace(",", " "),
    val suppiler_id: String? = "",
    val suppiler_name: String?,
    val rating: Float? = 0f,
    var in_favourites: String = "",
    val percent: String?,
    val old_price: String?,
) : Serializable

data class FlashSaleProduct(
    val fields: FieldObject
) : AbstractItem<FlashSaleProduct.VH>() {

    class VH(val itemView: View) : FastAdapter.ViewHolder<FlashSaleProduct>(itemView) {
        override fun bindView(item: FlashSaleProduct, payloads: List<Any>) {
            itemView.apply {
                if (item.fields.preview_picture != null && item.fields.preview_picture != "")
                    if (item.fields.preview_picture[0] == '/') {
                        Glide.with(context)
                                .load(Const.BASE_URL + item.fields.preview_picture)
                                .thumbnail(0.2f)
                                .override(250, 250)
                                .placeholder(R.drawable.placeholder_neva)
                                .transition(DrawableTransitionOptions.withCrossFade())
                                .into(ic_item_flash_sale)
                    } else {
                        Glide.with(context).load(item.fields.preview_picture)
                                .thumbnail(0.2f)
                                .override(250, 250)
                                .placeholder(R.drawable.placeholder_neva)
                                .transition(DrawableTransitionOptions.withCrossFade())
                                .into(ic_item_flash_sale)
                    }
                else
                    ic_item_flash_sale.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_broken_image_24))

                tv_new_price.text = "${item.fields.catalog_price_1} ${itemView.context.resources.getString(R.string.text_type_money)}"

                if (item.fields.old_price != null && item.fields.old_price != "") {
                    val text = "${item.fields.old_price} ${itemView.context.resources.getString(R.string.text_type_money)}"
                    tv_old_price.text = text
                } else {
                    old_price_line.visibility = View.GONE
                    tv_old_price.visibility = View.GONE
                }
            }
        }

        override fun unbindView(item: FlashSaleProduct) {}
    }

    override val layoutRes: Int
        get() = R.layout.flash_sale_item
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}
//
//    "fields": {
//    "id": "1",
//    "preview_picture": "/upload/iblock/a23/a2328bf9f4618ec9372154dd9f9b0e22.jpg",
//    "name": "Растущая Кухня",
//    "preview_text": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec ante arcu, fringilla eget ullamcorper sed, ultrices non massa. ",
//    "detail_text": "Morbi tempor semper felis vitae malesuada. Ut porta ex quis molestie luctus. Cras congue sem est, auctor pharetra leo imperdiet eget. Fusce laoreet, magna at volutpat tempor, justo quam vulputate libero, at suscipit erat neque non lectus. Suspendisse efficitur porta quam ut sollicitudin. Ut sodales dignissim molestie. Suspendisse facilisis, nibh non dignissim convallis, orci dui maximus ligula, eget feugiat ante magna at ante. Vivamus in tristique elit, eu pharetra felis. Sed non efficitur est.",
//    "iblock_id": "1",
//    "sort": "100",
//    "detail_text_type": "text",
//    "preview_text_type": "text",
//    "catalog_price_1": "99000.00",
//    "catalog_price_id_1": "6",
//    "catalog_group_id_1": "1",
//    "catalog_currency_1": "UZS",
//    "catalog_quantity_from_1": null,
//    "catalog_quantity_to_1": null,
//    "catalog_extra_id_1": null,
//    "catalog_group_name_1": "Базовая цена",
//    "catalog_can_access_1": "Y",
//    "catalog_can_buy_1": "Y",
//    "catalog_quantity": "0",
//    "catalog_quantity_trace": "N",
//    "catalog_quantity_trace_orig": "D",
//    "catalog_weight": "0",
//    "catalog_vat_id": null,
//    "catalog_vat_included": "N",
//    "catalog_can_buy_zero": "N",
//    "catalog_can_buy_zero_orig": "D",
//    "catalog_purchasing_price": null,
//    "catalog_purchasing_currency": null,
//    "catalog_quantity_reserved": "0",
//    "catalog_subscribe": "Y",
//    "catalog_subscribe_orig": "D",
//    "catalog_width": null,
//    "catalog_length": null,
//    "catalog_height": null,
//    "catalog_measure": "5",
//    "catalog_type": "1",
//    "catalog_available": "Y",
//    "catalog_bundle": "N",
//    "catalog_price_type": "S",
//    "catalog_recur_scheme_length": null,
//    "catalog_recur_scheme_type": "D",
//    "catalog_trial_price_id": null,
//    "catalog_without_order": "N",
//    "catalog_select_best_price": "N",
//    "catalog_negative_amount_trace": "N",
//    "catalog_negative_amount_trace_orig": "D",
//    "catalog_vat": null
//},
//"props": {
//    "suppiler": {
//        "id": "1",
//        "timestamp_x": "2020-10-12 19:25:33",
//        "iblock_id": "1",
//        "name": "Поставщик",
//        "active": "Y",
//        "sort": "100",
//        "code": "SUPPILER",
//        "default_value": "",
//        "property_type": "S",
//        "row_count": "1",
//        "col_count": "30",
//        "list_type": "L",
//        "multiple": "N",
//        "xml_id": null,
//        "file_type": "",
//        "multiple_cnt": "5",
//        "tmp_id": null,
//        "link_iblock_id": "0",
//        "with_description": "N",
//        "searchable": "N",
//        "filtrable": "N",
//        "is_required": "N",
//        "version": "1",
//        "user_type": "UserID",
//        "user_type_settings": null,
//        "hint": "",
//        "property_value_id": "38",
//        "value": 3,
//        "description": "",
//        "value_enum": null,
//        "value_xml_id": null,
//        "value_sort": null,
//        "~value": 3,
//        "~description": "",
//        "~name": "Поставщик",
//        "~default_value": ""
//    },
//    "color": {
//        "id": "2",
//        "timestamp_x": "2020-11-25 10:19:23",
//        "iblock_id": "1",
//        "name": "Цвет",
//        "active": "Y",
//        "sort": "200",
//        "code": "COLOR",
//        "default_value": "",
//        "property_type": "L",
//        "row_count": "1",
//        "col_count": "30",
//        "list_type": "L",
//        "multiple": "N",
//        "xml_id": null,
//        "file_type": "",
//        "multiple_cnt": "5",
//        "tmp_id": null,
//        "link_iblock_id": "0",
//        "with_description": "N",
//        "searchable": "N",
//        "filtrable": "N",
//        "is_required": "N",
//        "version": "1",
//        "user_type": null,
//        "user_type_settings": null,
//        "hint": "",
//        "property_value_id": "39",
//        "value": "Синий",
//        "description": null,
//        "value_enum": "Синий",
//        "value_xml_id": "BLUE",
//        "value_sort": "200",
//        "~value": "Синий",
//        "~description": null,
//        "~name": "Цвет",
//        "~default_value": "",
//        "value_enum_id": "2"
//    },
//    "more_photo": {
//        "id": "3",
//        "timestamp_x": "2020-11-25 14:11:56",
//        "iblock_id": "1",
//        "name": "Картинки",
//        "active": "Y",
//        "sort": "500",
//        "code": "MORE_PHOTO",
//        "default_value": "",
//        "property_type": "F",
//        "row_count": "1",
//        "col_count": "30",
//        "list_type": "L",
//        "multiple": "Y",
//        "xml_id": null,
//        "file_type": "jpg, gif, bmp, png, jpeg",
//        "multiple_cnt": "5",
//        "tmp_id": null,
//        "link_iblock_id": "0",
//        "with_description": "N",
//        "searchable": "N",
//        "filtrable": "N",
//        "is_required": "N",
//        "version": "1",
//        "user_type": null,
//        "user_type_settings": null,
//        "hint": "",
//        "property_value_id": false,
//        "value": false,
//        "description": false,
//        "value_enum": null,
//        "value_xml_id": null,
//        "value_sort": null,
//        "~value": false,
//        "~description": false,
//        "~name": "Картинки",
//        "~default_value": "",
//        "value_enum_id": "2"
//    }
//}
//},
//)