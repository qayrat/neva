package uz.neva.kidya.network.dto

import java.io.Serializable

class MyOrderHistory (
    val data:List<OrderObject>,
    val products:List<BasketItemObject>?,
    val status_list:ListObject?,
    //val paysystem: PaymentObject?=null,
    //val props: PropsObject
):Serializable

data class OrderObject(
    val id:String="43",
    val price:String="50000",
    val price_delivery:String= "0",
    val total_price:String= "50000",
    val status_id:String= "N",
    val status_name:String?,
    val date:String= "29.11.2020 21:41:56"
):Serializable

data class ListObject(
    val N:String= "Принят",
val P:String= "Оплата",
val ON:String= "В обработке",
val OT:String= "Курьер назначен",
val OS: String="В пути",
val OF: String="Доставлен",
val RT: String="Возврат",
val F: String="Выполнен"
)