package uz.neva.kidya.network.dto.nevaModel.response

class EmptyResponse(
        val status: String,
        val data: String
)