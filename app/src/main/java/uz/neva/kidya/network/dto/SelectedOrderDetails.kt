package uz.neva.kidya.network.dto

import java.io.Serializable

class SelectedOrderDetails (
    val data:List<OrderObject>,
    val products:List<BasketItemObject>?,
    val paysystem: String?=null,
    val props: PropsObject,
    val status_list:Map<String,String>
):Serializable

data class PropsObject(
    val fio:String,
    val phone:String,
    val region:String,
    val city:String,
    val street:String,
    val comment:String
):Serializable