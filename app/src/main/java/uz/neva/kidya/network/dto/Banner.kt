package uz.neva.kidya.network.dto

import java.io.Serializable

data class Banner(
    val data:List<BannerObject>
)

data class BannerObject(
    val id: Int,
    val link:String,
    val name:String?="",
    val type:String,
    val preview_picture:String,
    val entity:String?=null,
    val entity_id:String,
    val section_id:String?="",
    val date:String,
    val detail_text:String,
    val preview_text: String,
    val text:String,
    val link_to: String? = ""
):Serializable