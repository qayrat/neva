package uz.neva.kidya.network

import retrofit2.Response
import retrofit2.http.*
import uz.neva.kidya.network.dto.*
import uz.neva.kidya.network.dto.homeBanner.HomeBannerData
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.profile.ProfileUpdateModel
import uz.neva.kidya.network.dto.nevaModel.response.EmptyResponse
import uz.neva.kidya.ui.category.selectedProduct.FAQData
import uz.neva.kidya.ui.check_out.CouponModel
import uz.neva.kidya.ui.check_out.CouponModelInfo
import uz.neva.kidya.ui.home.SelectedShop
import uz.neva.kidya.ui.home.ShopInfoObject
import uz.neva.kidya.ui.search.LiveSearchResponse
import uz.neva.kidya.ui.search.SearchHistoryModel
import kotlin.collections.HashMap

interface KidyaApi {

    @GET("messages/")
    suspend fun getMessages(
        @Query("user_from") userId: String,
        @Query("page") position: Int
    ): Messages
//
//    @GET("catalog/show.php")
//    suspend fun getPopularsList(
//    ): Response<PopularsList>

    @GET("messages/")
    suspend fun getSelectedMessages(
        @Query("user_from") userId: String,
        @Query("user_to") supplierId: String,
        @Query("page") position: Int,
    ): Messages

    @FormUrlEncoded
    @POST("make_order/return.php")
    suspend fun returnProduct(
        @Field("user_id") userId: String,
        @Field("order_id") order_id: String,
        @FieldMap products: HashMap<String, String>
    ): Response<Any>

    @FormUrlEncoded
    @POST("messages/")
    suspend fun sendMessages(
        @Field("user_from") userId: String,
        @Field("user_to") supplierId: String,
        @Field("text") text: String,
        @Field("action") action: String = "send"
    ): Response<Messages>

    @GET("notify/")
    suspend fun getNotifications(
        @Query("user_id") userId: String,
        @Query("type") supplierId: String = "unread"
    ): Response<Notifications>

    @GET("notify/read.php")
    suspend fun notificationsMarkAsRead(
        @Query("user_id") userId: String,
        @Query("id") id: String? = null,
        @Query("all") all: String? = null
    ): Response<Any>

    @GET("news")
    suspend fun getHomeNews(
        @Query("count") count: String = "6"
    ): Response<News>

    @GET("reviews")
    suspend fun getReviews(
        @Query("element_id") id: String
    ): Response<Reviews>

    @FormUrlEncoded
    @POST("reviews/")
    suspend fun addReviews(
        @Field("action") action: String = "add",
        @Field("element_id") id: String,
        @Field("user_id") user_id: String,
        @Field("text") text: String,
        @Field("rating") rating: Float,
    ): Response<Reviews> // add review

    @FormUrlEncoded
    @POST("reviews/")
    suspend fun getReview(
        @Field("element_id") id: String,
    ): Response<Reviews> //get all reviews list

    @GET("news")
    suspend fun getSelectedNews(
        @Query("element_id") id: String
    ): Response<News>

    @GET("news")
    suspend fun getNews(
        @Query("page") position: Int,
        @Query("count") count: String = ""
    ): News

    @GET("banners")
    suspend fun getBanners(): Response<Banner>

    @GET("catalog/shop.php")
    suspend fun getShopInfo(
        @Query("width") width: Int? = null,
        @Query("height") height: Int? = null
    ): Response<List<ShopInfoObject>>

    @GET("/search/history.php")
    suspend fun getSearchHistory(
        @Query("user_id") user_id: String
    ): Response<SearchHistoryModel>

    @GET("/search/live.php")
    suspend fun getLiveSearch(
        @Query("query") searchString: String
    ):Response<LiveSearchResponse>

    @GET("/search/history.php")
    suspend fun clearHistory(
        @Query("user_id") user_id: String,
        @Query("clear") clear: String = "true",
        @Query("all") all: String = "true"
    ): Response<SearchHistoryModel>

    @GET("/search/history.php")
    suspend fun clearOnlyOneHistory(
        @Query("user_id") user_id: String,
        @Query("clear") clear: String = "true",
        @Query("id") id: String
    ): Response<SearchHistoryModel>

    @GET("catalog/shop.php")
    suspend fun getSelectedShop(
            @Query("user_id") user_id: String,
        @Query("id") id: String,
        @Query("page") page: String,
        @Query("sort") sort: String,
    ): Catalog

    @GET("catalog/")
    suspend fun getFilter(
        @Query("section_id") id: String,
        @Query("all_product") all: String = "true",
        @Query("sort") sort: String,
        @Query("page") page: Int
    ): Catalog

    @GET("catalog/")
    suspend fun getCatalogFilter(
        @Query("section_id") id: String,
        @Query("all_product") all: String = "true",
        @Query("sort") sort: String,
        @Query("page") page: Int
    ): CatalogSort

    @GET("catalog/")
    suspend fun getCategories(
        @Query("width") width: Int? = null,
        @Query("height") height: Int? = null
    ): Response<Categories>

    @GET("catalog/filter.php")
    suspend fun getFilters(
        @Query("section_id") section_id: String = "",
    ): Response<Filters>


    @GET("catalog/filter.php")
    suspend fun getFiltersSale(
        @Query("sale") section_id: String = "Y",
    ): Response<Filters>


    @FormUrlEncoded
    @POST("catalog/")
    suspend fun getCategories(
        @Field("section_id") section_id: String = "",
        @Query("page") position: Int
    ): Categories

    @FormUrlEncoded
    @POST("catalog/")
    suspend fun getSubCategories(
        @Field("section_id") section_id: String = "",
        @Field("count") position: Int? = 1000
    ): Response<Categories>

    @GET("basket/")
    suspend fun getCartCount(
        @Query("user_id") user_id: String
    ): Response<CartCount>

    @FormUrlEncoded
    @POST("search/")
    suspend fun getSearchResult(
        @Field("user_id") user_id: String = "",
        @Query("query") search_value: String,
        @Query("page") position: Int
    ): Catalog

    @FormUrlEncoded
    @POST("search/")
    suspend fun getSearchResultProductsSize(
        @Field("user_id") user_id: String = "",
        @Query("query") search_value: String,
    ):Response<ProductCount>

    @GET("catalog/flash_sale.php")
    suspend fun getFlashSale(
        @Query("count") id: Int?=100
    ): Response<FlashSaleModel>

    @FormUrlEncoded
    @POST("catalog/")
    suspend fun getFilterResult(
        @Field("user_id") user_id: String = "",
        @Field("section_id") section_id: String = "",
        @FieldMap property: HashMap<String, String>,
        @Query("page") position: Int
    ): Catalog



    @FormUrlEncoded
    @POST("auth/")
    suspend fun authFirstStep(
        @Field("phone") phone: String,
        @Field("step") state: String = "1"
    ): Response<Any>

    @FormUrlEncoded
    @POST("auth/")
    suspend fun authSecondStep(
        @Field("phone") phone: String,
        @Field("sms_code") code: String,
        @Field("firebase") fcm: String,
        @Field("step") state: String = "2"
    ): Response<AuthResponse>


    @FormUrlEncoded
    @POST("catalog/")
    suspend fun getSelectedCategoryProducts(
        @Field("user_id") user_id: String = "", @Field("section_id") section_id: String,
        @Query("page") position: Int,
    ): Catalog

    @FormUrlEncoded
    @POST("catalog/")
    suspend fun getAllSelectedCategoryProducts(
        @Field("user_id") user_id: String = "", @Field("section_id") section_id: String,
        @Field("all_product") all_product: String,
        @Query("page") position: Int,
    ): Catalog

    @FormUrlEncoded
    @POST("catalog/")
    suspend fun getAllSelectedCategoryProductsCount(
        @Field("section_id") section_id: String,
        @Field("all_product") all_product: String? = "true",
    ):Response<ProductCount>

    @FormUrlEncoded
    @POST("catalog/shop.php")
    suspend fun getBrandShopProductsSize(
        @Field("id") id: String,
    ):Response<ProductCount>

    @GET("catalog/home-page.php")
    suspend fun getHomePageData(
        @Query("user_id") userId: String? = ""
    ): List<Catalog>

    @GET("catalog/recommend.php")
    suspend fun getHomeRecommended(
        @Query("user_id") user_id: String? = "",
        @Query("page") page: Int,
        @Query("count") count: String = "",
        @Query("width") width: Int? = null,
        @Query("height") height: Int? = null,
    ): Catalog

    @GET("catalog/hits.php")
    suspend fun getHomeHits(
        @Query("user_id") user_id: String? = "",
        @Query("page") page: Int,
        @Query("count") count: String = "",
        @Query("width") width: Int? = null,
        @Query("height") height: Int? = null,
    ): Catalog

    @GET("catalog/discount.php")
    suspend fun getHomeDiscount(
        @Query("user_id") user_id: String? = "",
        @Query("page") page: Int,
        @Query("count") count: String = "",
        @Query("width") width: Int? = null,
        @Query("height") height: Int? = null,
    ): Catalog


    @FormUrlEncoded
    @POST("catalog/hits.php")
    suspend fun getDealsCategoryProducts(
        @Field("user_id") user_id: String = "",
        @Field("show") show: String = "",
        @Query("page") position: Int,
        @Query("count") count: String = ""
    ): Catalog


    @FormUrlEncoded
    @POST("catalog/history.php")
    suspend fun getViewHistoryProducts(
        @Field("user_id") user_id: String = "",
        @Query("page") position: Int,
        @Query("count") count: String = ""
    ): Catalog

    @FormUrlEncoded
    @POST("basket/")
    suspend fun addToBasket(
        @Field("user_id") user_id: String,
        @Field("type") type: String,
        @Field("quantity") quantity: String,
        @Field("id") id: String,
        @Field("action") action: String,
        @Field("color") color: String,
        @Field("razmer") rezmer: String
    ): Response<Basket>

    @GET("basket/")
    suspend fun getBasket(@Query("user_id") user_id: String): Response<Basket>


    @GET("profile/coupon_list.php")
    suspend fun getCouponList(
        @Query("user_id") user_id: String
    ): Response<CouponModel>

    @GET("make_order/coupon.php")
    suspend fun getCouponInfo(
        @Query("user_id") user_id: String,
        @Query("coupon") coupon: String
    ): Response<CouponModelInfo>

    @FormUrlEncoded
    @POST("favs/")
    suspend fun getFavs(
        @Field("user_id") user_id: String,
        @Query("page") position: Int
    ): Catalog

    @FormUrlEncoded
    @POST("favs/")
    suspend fun addToFavs(
        @Field("user_id") user_id: String,
        @Field("type") type: String = "favs",
        @Field("id") id: String,
        @Field("action") action: String
    ): Response<Catalog>


    @FormUrlEncoded
    @POST("profile/")
    suspend fun getProfile(@Field("user_id") user_id: String): Response<Profile>

    @FormUrlEncoded
    @POST("profile/")
    suspend fun updateProfile(
        @Field("user_id") user_id: String,
        @Field("edit") edit: Boolean = true,
        @Field("name") name: String,
        @Field("phone") phone: String,
        @Field("address") address: String,
        @Field("district") district: String,
        @Field("street") street: String,
        @Field("gender") gender: String,
        @Field("birthday") birthday: String,
        @Field("email") email: String? = "",
        @Field("interest") interest: String? = "",
    ): Response<Profile>

    @FormUrlEncoded
    @POST("profile/")
    suspend fun updateBabyProfile(
        @Field("user_id") user_id: String,
        @Field("edit") edit: String = "true",
        @Field("kids_name") kidsName1: String,
        @Field("kids_birthday") kidsBirthday1: String,
        @Field("kids_gender") kidsGender1: String,
    ): Response<Profile>


    @FormUrlEncoded
    @POST("profile/orders/list.php")
    suspend fun getOrderHistory(@Field("user_id") user_id: String): Response<MyOrderHistory>

    @FormUrlEncoded
    @POST("profile/orders/detail.php")
    suspend fun getOrderDetails(
        @Field("user_id") user_id: String,
        @Field("order_id") order_id: String,
    ): Response<SelectedOrderDetails>

    @FormUrlEncoded
    @POST("make_order/delivery_add.php")
    suspend fun makeOrderDelivery(
        @Field("user_id") user_id: String,
        @Field("start_time") startTime: String,
        @Field("end_time") endTime: String,
        @Field("order_id") order_id: String,
        @Field("valid_date") valid: String
    ): Response<Any>

    @FormUrlEncoded
    @POST("profile/")
    suspend fun updateChildData(
        @Field("user_id") user_id: String,
        @Field("order_id") order_id: String,
    ): Response<Any>

    @GET("make_order/index2.php")
    suspend fun checkOutFirstStep(
        @Query("user_id") user_id: String,
        @Query("coupon") coupon: String?=null,
        @Query("action") action: String = "save",
        @Query("paysystem") paysystem: String = "payme",
        @Query("delivery") delivery: String = "new"
    ): Response<CheckOut>

    @FormUrlEncoded
    @POST("make_order/payment.php")
    suspend fun makePayment(@Field("order_id") order_id: String): Response<Payment>

    @GET("faq")
    suspend fun getFAQ(
        @Query("user_id") user_id: String? = null
    ): Response<FAQData>

    @FormUrlEncoded
    @POST("catalog/product.php")
    suspend fun getSelectedProduct(
        @Field("user_id") user_id: String? = "",
        @Field("product_id") product_id: String,
        @Field("width") width: Int? = null,
        @Field("height") height: Int? = null,
    ): Response<MCatalog>

    @FormUrlEncoded
    @POST("catalog/frequently.php")
    suspend fun getSelectedFrequentlyProducts(
        @Field("product_id") product_id: String
    ): Response<MFrequently>

    @FormUrlEncoded
    @POST("catalog/more.php")
    suspend fun getSelectedMoreProduct(
        @Field("product_id") product_id: String
    ): Response<MMoreProduct>

    @FormUrlEncoded
    @POST("feedback/")
    suspend fun sendFeedback(
        @Field("name") name: String,
        @Field("phone") phone: String,
        @Field("text") text: String
    ): Response<Any>


    @GET("profile/payme/subscribe.php")
    suspend fun getCardList(
        @Query("user_id") user_id: String,
        @Query("token") token: String,
    ): Response<CardResource>

    @GET("profile/payme/subscribe.php")
    suspend fun addCard(
        @Query("user_id") user_id: String,
        @Query("token") token: String,
        @Query("add") add: String = "true",
        @Query("name") name: String,
        @Query("number") number: String,
        @Query("expire") expire: String,
    ): Response<CardResource>

    @GET("profile/payme/subscribe.php")
    suspend fun verifyCard(
        @Query("token") token: String,
        @Query("verify") verify: String = "true",
        @Query("code") code: String,
        @Query("id_card") id_card: String,
    ): Response<VerifyCardResource>

    @GET("profile/payme/subscribe.php")
    suspend fun deleteCard(
        @Query("token") token: String,
        @Query("delete") delete: String = "true",
        @Query("id_card") id_card: String
    ): Response<Any>


    @GET("profile/payme/subscribe.php")
    suspend fun payCard(
        @Query("token") token: String,
        @Query("pay") pay: String = "true",
        @Query("order_id") order_id: String,
        @Query("id_card") id_card: String,
        @Query("prop[fio]") fio:String,
        @Query("prop[phone]") phoneNumber:String,
        @Query("prop[region]") region:String,
        @Query("prop[location]") location:String?=null,
        @Query("prop[street]") street: String
    ): Response<PayModel>

    @GET("catalog/magic-friday.php")
    suspend fun getHomeBanner() : Response<HomeBannerData>

    @GET("catalog/hits.php")
    suspend fun getHomeHitsCount(
        @Query("user_id") user_id: String? = "",
        @Query("page") page: Int,
        @Query("count") count: String = "",
        @Query("width") width: Int? = null,
        @Query("height") height: Int? = null,
    ): Response<Catalog>

    @POST("rest/")
    suspend fun updateProfile(
            @Header("userId") userId: String,
            @Body data: ProfileUpdateModel
    ) : Response<EmptyResponse>

    @FormUrlEncoded
    @POST("catalog/")
    suspend fun getAllSelectedCategoryProductsNew(
            @Field("user_id") user_id: String = "",
            @Field("section_id") section_id: String,
            @Field("all_product") all_product: String,
            @Query("page") position: Int,
            @Query("sort") sort: String,
    ): CatalogNew

    @POST("rest/")
    suspend fun putAnalyticsApi(
            @Body data: AnalyticsModel
    ) : Response<EmptyResponse>
}