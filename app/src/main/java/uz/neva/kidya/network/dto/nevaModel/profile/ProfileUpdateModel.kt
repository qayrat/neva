package uz.neva.kidya.network.dto.nevaModel.profile

import com.google.gson.annotations.SerializedName

class ProfileUpdateModel(
        val entity: String,
        val method: String,
        val data: ProfileData
)

class ProfileData(
        val name: String,
        @SerializedName("last_name") val lastName: String,
        @SerializedName("personal_phone") val personalPhone: String,
        @SerializedName("personal_street") val personalStreet: String,
        @SerializedName("personal_gender") val personalGender: String,
        @SerializedName("personal_notes") val personalNotes: String,
        @SerializedName("personal_birthday") val personalBirthday: String,
        @SerializedName("email") val email: String
)