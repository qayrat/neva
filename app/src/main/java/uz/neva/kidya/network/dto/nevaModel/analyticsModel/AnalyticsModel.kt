package uz.neva.kidya.network.dto.nevaModel.analyticsModel

class AnalyticsModel(
        val entity: String,
        val method: String,
        val data: Any
)

class DeviceData(
        val type: String,
        val name: String
)

class ViewProductData(
        val type: String,
        val product: Int,
        val dateto: String,
        val datefrom: String
)

class ViewPageData(
        val type: String,
        val page: String
)