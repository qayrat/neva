package uz.neva.kidya.network.dto

data class CheckOut(
    val data:List<BasketItemObject>,
    val paysystem: String,
    val delivery: String,
    val order_id:String

)

data class PaymentObject(
    val id:String,
    val name:String
)

data class DeliveryObject(
    val id:String,
    val name:String
)