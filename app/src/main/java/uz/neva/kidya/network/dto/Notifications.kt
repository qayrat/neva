package uz.neva.kidya.network.dto

import android.view.View
import androidx.core.view.isVisible
import com.google.gson.annotations.SerializedName
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_notification.view.*
import uz.neva.kidya.R

data class Notifications(
    val data: List<NotificationModel>
)

data class NotificationObject(
    val id: String,
    val text: String,
    val date: String,
    val preview_text: String,
) {
    @Transient
    var collapsed = false
}

data class NotificationModel(
    val id: Int,
    val text: String? = null,
    val date: String? = null,
    val preview_text: String? = null,
    val productImage: String? = null,
    val shopImage: String? = null,
    var isSelect: Boolean = false,
    var isReaded: Boolean = false,
    val notify_type:String? = null,
    val object_id:String? = null
) : AbstractItem<NotificationModel.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<NotificationModel>(itemView) {
        override fun bindView(item: NotificationModel, payloads: List<Any>) {
//            itemView.ll_notification.isSelected = item.isSelect
            itemView.item_readed.isVisible = item.isReaded
            itemView.item_name.text =
                if (item.preview_text == null || item.preview_text == "") "NEVA" else item.preview_text
            itemView.item_category.text = item.text ?: ""
            itemView.item_date.text = item.date ?: ""
        }

        override fun unbindView(item: NotificationModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_notification
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}