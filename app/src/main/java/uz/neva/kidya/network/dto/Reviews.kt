package uz.neva.kidya.network.dto

data class Reviews(
    val data:List<ReviewObject>?,
    val status:String
)

data class ReviewObject(
    val id:String,
    val text:String,
    val user_name:String,
    val rating:Float,
    val date:String
)