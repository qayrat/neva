package uz.neva.kidya.network.dto

import uz.neva.kidya.ui.check_out.Links

data class Profile(
    val data:ProfileObject,
    val status:String,
    val links: Links
)

data class ProfileObject (
    val login:String?=null,
    val name:String?=null,
    val last_name:String?=null,
    val phone:String?=null,
    val address:String?=null,
    val district:String?=null,
    val street:String? = null,
    val coupon:String?=null,
    val gender:String?=null,
    val birthday:String?=null,
    val email:String?=null,
    val mobile:String?=null,
    val kids:List<KidsModel>?=null
)
data class KidsModel(
    val name: String,
    val gender: String,
    val birthday: String,
    val interest: String?,
)