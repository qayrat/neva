package uz.neva.kidya.network.dto

import java.io.Serializable

data class News(
    val data: List<NewsObject>,
    val links:LinkObjects,
    val pagination:PaginationObject
)

data class NewsObject(
    val id:Int,
    val name:String,
    val date:String?="",
    val preview_text:String?,
    val detail_text:String?,
    val preview_picture: String?,
    val detail_picture: String?
): Serializable

data class LinkObjects(
    val self: String,
    val first: String? = "",
    val prev: Any? = null,
    val next: String? = "",
    val last: String?=""
)
data class PaginationObject(
    val total_count: Int,
    val page_count: Int,
    val current_page: Int,
    val page_size: Int
)