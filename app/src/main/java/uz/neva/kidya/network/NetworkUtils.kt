package uz.neva.kidya.network

import android.content.Context
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.Build
import android.util.Log
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import org.json.JSONObject
import retrofit2.HttpException
import retrofit2.Response
import uz.neva.kidya.exeption.NoConnectivityException
import uz.neva.kidya.network.dto.ErrorResponse
import java.io.IOException
import java.lang.Exception
import java.net.ConnectException
import java.net.InetSocketAddress
import java.net.Socket

suspend fun <T : Any> safeApiCall(
    apiCall: suspend () -> Response<T>
): Resource<T> {
    try {
        val response = apiCall()
        if (response.isSuccessful && response.body() != null) {
//            var json = Gson()
//            if (response.body() != null) {
//                var customObject = response.body() as T
//                if (customObject != null) {
//                    var castedObject = json.toJson(customObject)
//                    Log.d("ErrorTag:", "CastedObject: $castedObject")
//                } else
//                    Log.d("ErrorTag:", "Null")
//            } else
//                Log.d("ErrorTag:", "If responsbody null raw: " + response.raw().toString())
            return Resource.Success<T>(response.body() as T)
        } else {
            if (response.errorBody() != null) {
                val errorResponse = ErrorResponse(jsonResponse = JSONObject(response.errorBody()!!.string()))
                return Resource.GenericError(errorResponse)
            } else
            {
                val errorResponse = ErrorResponse(jsonResponse = JSONObject(response.errorBody()!!.string()))
                return Resource.GenericError(errorResponse)
            }
        }
    } catch (throwable: Throwable) {
        Log.d("ErrorTag", throwable.message.toString())
        when (throwable) {
            is ConnectException,
            is NoConnectivityException -> {
                return Resource.Error(NoConnectivityException())
            }
            is HttpException -> {
                val errorResponse: ErrorResponse = throwable.response()?.body() as ErrorResponse
                return Resource.GenericError(errorResponse)
            }
            is IOException -> {
                return Resource.Error(Exception("IOException: " + throwable.message))
            }
            else -> {
                return Resource.Error(Exception(throwable.message))
            }
        }
    }
}

fun Context.isNetworkAvailable(): Boolean {
    var result = false
    val cm = this.getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager?
    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
        cm?.run {
            cm.getNetworkCapabilities(cm.activeNetwork)?.run {
                result = hasTransport(NetworkCapabilities.TRANSPORT_WIFI) || hasTransport(
                    NetworkCapabilities.TRANSPORT_CELLULAR
                )
            }
        }
    } else {
        cm?.run {
            cm.activeNetworkInfo?.run {
                result =
                    type == ConnectivityManager.TYPE_WIFI || type == ConnectivityManager.TYPE_MOBILE
            }
        }
    }
    return result
}

suspend fun Context.isNetworkConnectedSuspend(): Boolean {
    return withContext(Dispatchers.IO) {
        try {
            val timeoutMs = 1500
            val socket = Socket()
            val socketAddress = InetSocketAddress("8.8.8.8", 53)

            socket.connect(socketAddress, timeoutMs)
            socket.close()

            true
        } catch (e: IOException) {
            false
        }
    }
}