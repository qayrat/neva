package uz.neva.kidya.network.dto.categorySort

class CategorySortData(
    val sectionId: String,
    val sort: String
)