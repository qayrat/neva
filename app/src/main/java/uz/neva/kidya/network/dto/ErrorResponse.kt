package uz.neva.kidya.network.dto

import org.json.JSONObject


data class ErrorResponse(
    val jsonResponse: JSONObject = JSONObject()
)