package uz.neva.kidya.network.dto


data class Categories(
    val data: List<CategoriesObject>,
    val links: LinkObjects,
    val pagination: PaginationObject
)

data class CategoriesObject(
    val id: String = "1",
    val picture: String? = null,
    val has_sub: Boolean = false,
    val name: String = "Игрушки и игры",
    val name_en: String? = null,
    val name_uz: String? = null,
    var isSelect: Boolean = false
)