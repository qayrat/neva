package uz.neva.kidya.network.dto

data class Messages (
    val data: List<MessagesObject>,
    val links:LinkObjects,
    val pagination: PaginationObject
)

data class MessagesObject(
    val id:String,
    val suppiler:String?,
    val last_message:String?,
    val last_date:String?,
    val sender_id:String,
    val text:String?,
    val date:String?
)