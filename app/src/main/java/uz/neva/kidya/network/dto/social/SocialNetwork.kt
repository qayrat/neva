package uz.neva.kidya.network.dto.social

import com.google.gson.annotations.SerializedName

class SocialResponse(
    val phone: String,
    val contact: String,
    val address: String,
    @SerializedName("social_networks")
    val socialNetworks: ArrayList<SocialNetwork>
)

class SocialNetwork(
    val name: String,
    val url: String,
    val nickname: String,
    val image: String
)