package uz.neva.kidya.network.dto

import uz.neva.kidya.CardModel
import uz.neva.kidya.ui.check_out.Links

data class Payment(
    val data:String
)

data class CardResource(
    val data: List<Map<String,CardModelPay>>,
    val status:String?,
    val links: Links
)

data class CardModelPay(
    val name:String,
    val number: String
)

data class VerifyCardResource(
    val data:VerifyData,
    val status: String?,
    val links: Links
)

data class VerifyData(
    val jsonrpc: String,
    val id: String,
    val result: VerifyResult
)

data class VerifyResult(
    val card:CardVerify
)

data class CardVerify(
    val number:String,
    val expire:String,
    val token:String,
    val recurrent:Boolean,
    val verify:Boolean,
    val type:String
)

data class PayModel(
    val data:Pay,
    val status: String?,
    val links: Links
)

data class Pay(
    val jsonrpc:String?=null,
    val id:String?=null,
    val result:ResultPay?=null,
    val error:ErrorPay?=null
)

data class ResultPay(
    val receipt:Any?=null
)


data class ErrorPay(
    val message:String,
    val code:String,
    val data: String
)
