package uz.neva.kidya.network.dto

data class Filters(
    val data:List<FilterObject>
)

data class FilterObject(
    val name:String,
    val code:String,
    val values:List<String>
)