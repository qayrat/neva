package uz.neva.kidya.network.dto

import java.io.Serializable

class Basket (
    val data:List<BasketItemObject>,
    val meta: BasketMetaObject
)

data class BasketItemObject(
    val id: String="29",
    var quantity: String="1",
    val name: String?= "Тестовый товар",
    val preview_picture: String?="/upload/iblock/dae/dae6a9c6b84e1f556e54c1f57c049d8a.jpg",
    val price: String?= "1000000",
    val suppiler: String?= null,
    val color: String?= null,
    val razmer: String?= null,
    val returned: Boolean?,
    val params: List<ParamsObject>?=null
):Serializable

data class BasketMetaObject(
    val total:String? ="0",
    val basket_price:String? ="0",
    val delivery_price:String? ="0",
    val count:Int?= 0,
    val message:String?="",
    val coupon_status:String?=""
)