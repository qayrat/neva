package uz.neva.kidya.network.dto

class AuthResponse (
    val status:String="",
    val popup:String? = null,
    val error:String?="",
    val user_id:String?="",
    val token:String? = "",
    val current_user:ProfileObject?
)