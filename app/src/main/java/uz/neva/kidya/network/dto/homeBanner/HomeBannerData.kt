package uz.neva.kidya.network.dto.homeBanner

class HomeBannerData(
    val data: BannerImage
)

class BannerImage(
    val image: String
)