package uz.neva.kidya.network.dto

import java.util.*
import kotlin.collections.ArrayList

class MockData {
    companion object {
        fun getDiscountList(): List<DiscountItem> {
            var list = ArrayList<DiscountItem>()
            list.add(
                DiscountItem(
                    "@drawable/imageitem",
                    "Розовое платье с кружевами",
                    5,
                    "228 000 сум",
                    "Kids World Shop"
                )
            )
            list.add(
                DiscountItem(
                    "@drawable/imageitem",
                    "Малиновый сарафан",
                    5,
                    "121 600 сум",
                    "Kids World Shop"
                )
            )
            list.add(
                DiscountItem(
                    "@drawable/imageitem",
                    "Розовое платье с кружевами",
                    5,
                    "228 000 сум",
                    "No Brand Shop"
                )
            )
            list.add(
                DiscountItem(
                    "@drawable/imageitem",
                    "Малиновый сарафан",
                    5,
                    "121 600 сум",
                    "Kids World Shop"
                )
            )

            return list
        }

        fun getSelectedCategoryObject(): List<SelectedCategoryObject> {
            var list = ArrayList<SelectedCategoryObject>()
            list.add(SelectedCategoryObject("imageitem", "Малиновый сарафан", 5, "121 600 сум", "Kids World Shop",false,false))
            list.add(SelectedCategoryObject("imageitem", "Малиновый сарафан", 5, "121 600 сум", "Kids World Shop",true,true))
            list.add(SelectedCategoryObject("imageitem", "Малиновый сарафан", 5, "121 600 сум", "Kids World Shop",false,false))
            list.add(SelectedCategoryObject("imageitem", "Малиновый сарафан", 5, "121 600 сум", "Kids World Shop",true,true))
            return list
        }

        fun getNewsList(): List<NewsItem> {
            var list = ArrayList<NewsItem>()
            list.add(
                NewsItem(
                    "@drawable/imageitem",
                    "Распродажа во всех маназиназ Zara Kids",
                    "30 сентября",
                    ""
                )
            )
            list.add(NewsItem("@drawable/imageitem", "Новая коллекция в Bershka", "1 октября",""))
            list.add(NewsItem("@drawable/imageitem", "Розовое платье с кружевами", "1 октября",""))
            list.add(NewsItem("@drawable/imageitem", "Малиновый сарафан", "30 сентября",""))

            return list
        }

        fun getCategoryList(): List<CategoryItem> {
            var list=ArrayList<CategoryItem>()
            list.add(CategoryItem("ic_skirt"))
            list.add(CategoryItem("ic_top"))
            list.add(CategoryItem("ic_shirt"))
            list.add(CategoryItem("ic_socks"))

            return list
        }

        fun getCategoryObjectList(): List<CategoryObject> {
            var list=ArrayList<CategoryObject>()
            list.add(CategoryObject("ic_skirt","Юбки"))
            list.add(CategoryObject("ic_top","Кофточки"))
            list.add(CategoryObject("ic_shirt","Футболки"))
            list.add(CategoryObject("ic_socks","Носки"))
            list.add(CategoryObject("ic_top","Кофточки"))
            list.add(CategoryObject("ic_skirt","Юбки"))

            return list
        }

        fun getNotificationItemList(): List<NotificationItem>{
            var list=ArrayList<NotificationItem>()
//
//            list.add(NotificationItem("Акция «Счастливые часы»","29 сентября","Исправлению подверглись лишь явные орфографические пунктуационные погрешности например, аффилиация начинает лирический реципиент.\n" +
//                    "Зачин осознаёт мифологический возврат к стереотипам, таким образом, очевидно, что в нашем языке царит дух карнавала, пародийного отстранения.",false,false))
//            list.add(NotificationItem("Доставка заказа","29 сентября","Ваш заказ  №4829753 передан курьеру Доставка произойдет в течении" +
//                    "например, аффилиация начинает лирический\n" +
//                    "реципиент. ",false,false))
//            list.add(NotificationItem("Акция «Счастливые часы»","29 сентября","Исправлению подверглись лишь явные\n" +
//                    "орфографические пунктуационные погрешности\n" +
//                    "например, аффилиация начинает лирический\n" +
//                    "реципиент. ",false,false))
//            list.add(NotificationItem("Акция «Счастливые часы»","29 сентября","Исправлению подверглись лишь явные\n" +
//                    "орфографические пунктуационные погрешности\n" +
//                    "например, аффилиация начинает лирический\n" +
//                    "реципиент. ",false,false))
//            list.add(NotificationItem("Акция «Счастливые часы»","29 сентября","Исправлению подверглись лишь явные\n" +
//                    "орфографические пунктуационные погрешности\n" +
//                    "например, аффилиация начинает лирический\n" +
//                    "реципиент. ",false,false))
//

            return list
        }

        fun getOrdersList():List<OrderItem>{
            var orderslist=ArrayList<OrderItem>()
            var itemslist = ArrayList<Item>()
            var secondlist=ArrayList<Item>()

            itemslist.add(
                Item(
                    "imageitem",
                    "Розовое платье с кружевами",
                    5,
                    "228 000 сум",
                    "Kids World Shop"
                )
            )

            orderslist.add(OrderItem(1513135,"30 сентября",225000,15000,245000,"not known",itemslist.size,"humo",itemslist))

            secondlist.add(
                Item(
                    "imageitem",
                    "Розовое платье с кружевами",
                    5,
                    "228 000 сум",
                    "Kids World Shop"
                )
            )

            secondlist.add(
                Item(
                    "imageitemsecond",
                    "Малиновый  сарафан",
                    5,
                    "228 000 сум",
                    "Kids World Shop"
                )
            )

            secondlist.add(
                Item(
                    "imageitem",
                    "Розовое платье",
                    5,
                    "228 000 сум",
                    "Kids World Shop"
                )
            )

            orderslist.add(OrderItem(95153134,"10 сентября",125000,15000,145000,"delivered",secondlist.size,"humo",secondlist))
            return orderslist
        }

        fun getMessagesList():List<Message>{
            var list=ArrayList<Message>()
            list.add(Message("No Brand shop","27 сентября","Добрый день, Татьяна! Чем могу помочь..."))
            list.add(Message("Kids World Shop","27 сентября","Скидка -20% только до конца сентя..."))
            list.add(Message("No Brand shop","27 сентября","Добрый день, Татьяна! Чем могу помочь..."))
            return list
        }

        fun getColors():List<Color>{
            var list=ArrayList<Color>()
//            list.add(Color("#323232",false))
//            list.add(Color("#EE999A",false))
//            list.add(Color("#FFE4A1",false))
//            list.add(Color("#587EA7",false))
//            list.add(Color("#B3D7AD",false))
//            list.add(Color("#907DBE",false))
//            list.add(Color("#CCCCCC",false))
//            list.add(Color("#A0C4C8",false))
//            list.add(Color("#FBBB00",false))
//            list.add(Color("#8FC2F9",false))
            return list
        }

        fun getSizes():List<Size>{
            var list=ArrayList<Size>()
//            list.add(Size("13",false))
//            list.add(Size("14",false))
//            list.add(Size("15",false))
//            list.add(Size("16",false))
//            list.add(Size("17",false))
//            list.add(Size("18",false))
//            list.add(Size("19",false))
//            list.add(Size("20",false))
//            list.add(Size("21",false))
//            list.add(Size("22",false))
            return list
        }

        fun getMaterial():List<Material>{
            var list=ArrayList<Material>()
//            list.add(Material("Шерсть",false))
//            list.add(Material("Вельвет",false))
//            list.add(Material("Лайкра",false))
//            list.add(Material("Ситец",false))
//            list.add(Material("ХБ",false))
//            list.add(Material("Синтетика",false))
            return list
        }

        fun getPayment():List<Payment>{
            var list=ArrayList<Payment>()
            list.add(Payment("PayMe",false))
            list.add(Payment("UzCard",false))
            return list
        }


    }

    data class DiscountItem(
        val itemImageLink: String = "@drawable/imageitem",
        val itemItemName: String = "Розовое платье\nс кружевами",
        val itemRating: Int = 5,
        val itemPrice: String = "228 000 сум",
        val itemBrand: String = "Kids World Shop"
    )

    data class NewsItem(
        val itemImageLink: String = "@drawable/imageitem",
        val itemItemTitle: String = "Распродажа во всех маназиназ Zara Kids",
        val itemDate: String = "30 сентября",
        val itemContent: String=""
    )

    data class CategoryItem(
        val itemIcon: String = ""
    )

    data class CategoryObject(
        val itemIcon: String = "",
        val ItemName:String=""
    )

    data class SelectedCategoryObject(
        val itemImageLink: String = "@drawable/imageitem",
        val itemItemName: String = "Розовое платье\nс кружевами",
        val itemRating: Int = 5,
        val itemPrice: String = "228 000 сум",
        val itemBrand: String = "Kids World Shop",
        var itemIsFav: Boolean=false,
        var itemTag: Boolean= false
    )

    data class NotificationItem(
        val title:String="",
        val date:String="",
        val content:String="",
        var isRead:Boolean=true,
        var collapsed:Boolean=false
    )

    data class Child(
        val name:String="",
        val sex:String="",
        val age:Date

    )

    data class OrderItem(
        val number:Int,
        val date:String,
        val price:Int,
        val deliveryFee:Int,
        val total:Int,
        val timeline: String,
        val numberOfItems:Int,
        val status:String,
        val itemsList:ArrayList<Item>
    )

    data class Item(
        val itemImageLink: String = "@drawable/imageitem",
        val itemItemName: String = "Розовое платье\nс кружевами",
        val itemRating: Int = 5,
        val itemPrice: String = "228 000 сум",
        val itemBrand: String = "Kids World Shop"
    )

    data class Message(
        val brand:String,
        val date:String,
        val content:String
    )

    data class Color(
        val hex:String,
        var isPicked:Boolean
    )

    data class Image(
        val image:String,
        var isPicked: Boolean
    )

    data class Size(
        val size:String,
        var isPicked:Boolean,
        var isAvailable:Boolean=true
    )

    data class Material(
        val material:String,
        var isPicked:Boolean
    )

    data class Payment(
        val material:String,
        var isPicked:Boolean
    )
}