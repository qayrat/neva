package uz.neva.kidya.network

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import uz.neva.kidya.network.dto.social.SocialResponse

interface ContactService {

    @GET("api/contacts")
    fun getContacts(): Call<SocialResponse>

    companion object {
        private const val CONTACT_URL = "http://neva.uz/"
        fun create(): ContactService {
            val logger = HttpLoggingInterceptor()
            logger.level = HttpLoggingInterceptor.Level.BASIC
            val client = OkHttpClient.Builder()
                .addInterceptor(logger)
                .build()
            return Retrofit.Builder()
                .baseUrl(CONTACT_URL)
                .client(client)
                .addConverterFactory(GsonConverterFactory.create())
                .build()
                .create(ContactService::class.java)
        }
    }
}