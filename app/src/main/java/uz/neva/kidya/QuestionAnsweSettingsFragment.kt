package uz.neva.kidya

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.fragment_question_answe_settings.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.category.selectedProduct.QuestionAnswerModel
import uz.neva.kidya.ui.home.HomeViewModel


class QuestionAnsweSettingsFragment : Fragment(R.layout.fragment_question_answe_settings) {
    private val homeViewModel: HomeViewModel by viewModel()
    private val itemAdapter = ItemAdapter<QuestionAnswerModel>()
    private val fastAdapter = FastAdapter.with(itemAdapter)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questionList2.layoutManager = LinearLayoutManager(requireContext())
        questionList2.adapter = fastAdapter

        homeViewModel.getFaq()
        homeViewModel.getFAQResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        itemAdapter.add(resource.data.data)
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })
    }
}