package uz.neva.kidya.exeption

import java.io.IOException

class NoConnectivityException : IOException() {
    override val message: String?
        get() = "Нет связи с сервером. Возможно отключена сеть!"
}