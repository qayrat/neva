package uz.neva.kidya.ui.category.selectedProduct

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import kotlinx.android.synthetic.main.fragment_details.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.ProductObject

class DetailsFragment(val product: ProductObject) : Fragment(R.layout.fragment_details) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        brandText.text = product.fields.suppiler_name

        orderText.text = product.params[0].value[0]

        if (product.fields.detail_text != null) {
            var descriptionText: String = fromHtml(product.fields.detail_text).toString()
            descriptionText = descriptionText.replace("<p>", "")
            descriptionText = descriptionText.replace("</p>", "")
            descriptionText = descriptionText.replace("<br />", "")
            description.text = descriptionText
        }

        if (product.params[2]?.name == "Материал (состав)") {
            materialText.isVisible = true
            materialText.text = product.params[2]?.value[0]
        }else{
            materialText.text = resources.getString(R.string.text_material_undefined)
            materialText.isVisible = false
        }
    }

    @Suppress("DEPRECATION")
    fun fromHtml(source: String): Spanned {
        return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
            Html.fromHtml(source)
        } else {
            Html.fromHtml(source)
        }
    }
}