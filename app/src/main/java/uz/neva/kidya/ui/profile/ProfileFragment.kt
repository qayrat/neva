package uz.neva.kidya.ui.profile

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_profile.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.data.logout
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show
import uz.neva.kidya.utils.showSnackbarWithMargin

class ProfileFragment : Fragment(R.layout.fragment_profile) {

    private val profileViewModel: ProfileViewModel by viewModel()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomNavigationView: BottomNavigationView = requireActivity().findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.action_menu

        (requireActivity() as MainActivity).toolbar.hide()
        profileViewModel.getProfile(PrefManager.getUserID(requireContext()))
        profileViewModel.getProfileResponse.observe(
            viewLifecycleOwner,
            {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {
                        is Resource.Loading -> {
                        }
                        is Resource.Success -> {
                            textView25.setText(resource.data.data.name)
                        }
                        is Resource.GenericError -> {

                        }
                        is Resource.Error -> {

                        }

                    }
                }
            })

        profileViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Profile")
        ))
        profileViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        Timber.d("${resource.data.status}, ${resource.data.data}")
                    }
                    is Resource.GenericError -> {
                        Timber.d(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
                        Timber.d(resource.exception.localizedMessage)
                    }
                }
            }
        })

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        searchIcon.setOnClickListener {
            findNavController().navigate(R.id.nav_searchResult)
        }

        ll_support.setOnClickListener {
            openSupportDialog()
        }

        ll_support_telegram.setOnClickListener {
            openSupportSocialDialog()
        }

        ll_profile.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isNotEmpty())
                findNavController().navigate(R.id.profileDataFragment)
            else
                findNavController().navigate(R.id.nav_login)
        }

        ll_baby.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isNotEmpty())
                findNavController().navigate(R.id.babiesFragment)
            else
                findNavController().navigate(R.id.nav_login)
        }

        ll_cuppon.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isNotEmpty())
                findNavController().navigate(R.id.cuponFragment)
            else
                findNavController().navigate(R.id.nav_login)
        }

        ll_orders.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isNotEmpty())
                findNavController().navigate(R.id.orderFragment)
            else
                findNavController().navigate(R.id.nav_login)
        }

        ll_settings.setOnClickListener {
            findNavController().navigate(R.id.settingsFragment)
        }

        ll_payment_method.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isNotEmpty())
                findNavController().navigate(R.id.paymentMethodFragment)
            else
                findNavController().navigate(R.id.nav_login)
        }

        imageView13.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isNotEmpty()) {
                openLogOutDialog()
            }
        }

        if (PrefManager.getName(requireContext()).removePrefix(" ").isNotEmpty())
            textView25.text = PrefManager.getName(requireContext())

    }

    private fun openLogOutDialog() {
        val dialogFragment =
            MaterialAlertDialogBuilder(requireContext(), R.style.MyCustomAlertDialog)
        dialogFragment.apply {
            setMessage(requireActivity().resources.getString(R.string.text_log_out_message))
        }
        dialogFragment.setNegativeButton(R.string.text_cancel) { dialog, which ->
            dialog.dismiss()
        }
        dialogFragment.setPositiveButton("OK") { dialog, which ->
            (requireActivity() as MainActivity).logout()
            PrefManager.saveNameDelivery(requireContext(), "")
            PrefManager.saveDeliveryAddress(requireContext(), "")
            PrefManager.savePhoneDelivey(requireContext(), "")
            PrefManager.saveCardNumber(requireContext(), "")
            PrefManager.saveCardId(requireContext(), "")
            textView25.text = resources.getString(R.string.text_name)
            showSnackbarWithMargin(resources.getString(R.string.text_log_out))
        }

        dialogFragment.show()
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).toolbar.hide()
    }

    private fun openSupportDialog() {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:${PrefManager.getPhoneSupport(requireContext())}")
        startActivity(intent)
    }

    private fun openSupportSocialDialog() {
        val user = PrefManager.getTelegramLink(requireContext())
        val intent = Intent(Intent.ACTION_VIEW, Uri.parse("http://telegram.me/${user}"))
        startActivity(intent)
    }
}