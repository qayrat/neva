package uz.neva.kidya.ui.category

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yariksoffice.lingver.Lingver
import kotlinx.android.synthetic.main.category_item.view.*
import kotlinx.android.synthetic.main.item_sub_category.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.CategoriesObject


class CategoryListAdapter(
    val context: Context,
    val isSub: Boolean = false,
    val listener: SubCategoryItemClickListener? = null
) :
    PagingDataAdapter<CategoriesObject, CategoryListAdapter.ViewHolder>(CATEGORIES) {

    var category: ArrayList<CategoriesObject>? = null
    var selectedPosition: Int = -1
    var selectedLayout: View?=null
    var selectedTextView: TextView?=null
    fun updateList(category: ArrayList<CategoriesObject>) {
        this.category = category
        notifyDataSetChanged()
    }


    var onItemClick: ((CategoriesObject, View) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        if (!isSub) LayoutInflater.from(parent.context)
            .inflate(R.layout.category_item, parent, false)
        else LayoutInflater.from(parent.context).inflate(R.layout.item_sub_category, parent, false)
    )
//
//    override fun getItemCount()=category?.size?:0

    override fun onBindViewHolder(holder: CategoryListAdapter.ViewHolder, position: Int) {
        holder.bindData(getItem(position) as CategoriesObject)
        getItem(position)
        if (isSub && position == 0) {
            holder.itemView.performClick()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        private val itemImageLink = if (!isSub) itemView.categoryImage else null
        private val itemCategoryName =
            if (!isSub) itemView.categoryName else itemView.subCategoryName
        private val itemLayout =
            if (!isSub) itemView.ll_category_image else itemView.ll_sub_category

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(getItem(absoluteAdapterPosition) as CategoriesObject, itemView)
            }
        }

        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindData(category: CategoriesObject) {
            if (!isSub) {
                if (category.picture != null)
                    Glide.with(context)
                        .load(Const.BASE_URL + category.picture)
                        .thumbnail(0.2f)
                        .override(250,250)
                        .placeholder(R.drawable.placeholder_neva)
                        .into(itemImageLink!!)
                else
                    itemImageLink?.setImageDrawable(
                        context.resources.getDrawable(
                            R.drawable.ic_baseline_broken_image_24,
                            itemView.context.theme
                        )
                    )
                setName(category)
            } else {
                setName(category)
                itemCategoryName.setOnClickListener(this)
                itemLayout.setOnClickListener(this)
                if (category.isSelect){
                    itemLayout.isSelected = true
                    itemCategoryName.setTextColor(Color.WHITE)
                }else{
                    itemLayout.isSelected = false
                    itemCategoryName.setTextColor(Color.parseColor("#87B2BF"))
                }
            }
        }

        private fun setName(category: CategoriesObject) {
            when (Lingver.getInstance().getLanguage()) {
                "ru" -> {
                    itemCategoryName.text = category.name
                }
                "en" -> {
                    itemCategoryName.text = category.name_en
                }
                "uz" -> {
                    if (category.name_uz != null) {
                        itemCategoryName.text = category.name_uz
                    } else {
                        itemCategoryName.text = category.name
                    }
                }
                else -> {
                    itemCategoryName.text = category.name
                }
            }
        }

        override fun onClick(v: View?) {
            Log.d("TAGITEMS", "onClick: Click $absoluteAdapterPosition")
            listener?.onClick(category!![absoluteAdapterPosition])

            if (!category!![absoluteAdapterPosition].isSelect) {
                category!![absoluteAdapterPosition].isSelect = true
                itemLayout.isSelected = true
                itemCategoryName.setTextColor(Color.WHITE)
                if (selectedPosition!=-1){
                    category!![selectedPosition].isSelect = false
                    selectedLayout?.isSelected = false
                    selectedTextView?.setTextColor(Color.parseColor("#87B2BF"))
                }
                selectedPosition = absoluteAdapterPosition
                selectedLayout = itemLayout
                selectedTextView = itemCategoryName
            }else{
                category!![absoluteAdapterPosition].isSelect = false
                itemLayout.isSelected = false
                itemCategoryName.setTextColor(Color.parseColor("#87B2BF"))
            }

        }
    }

    companion object {
        val CATEGORIES = object : DiffUtil.ItemCallback<CategoriesObject>() {
            override fun areItemsTheSame(
                oldItem: CategoriesObject,
                newItem: CategoriesObject
            ): Boolean = oldItem.id == newItem.id


            override fun areContentsTheSame(
                oldItem: CategoriesObject,
                newItem: CategoriesObject
            ): Boolean =
                oldItem == newItem

        }
    }
}

interface SubCategoryItemClickListener {
    fun onClick(item: CategoriesObject)
}