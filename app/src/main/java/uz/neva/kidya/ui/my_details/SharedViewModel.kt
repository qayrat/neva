package uz.neva.kidya.ui.my_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import uz.neva.kidya.network.dto.MockData


class SharedViewModel : ViewModel() {
    var mapArguments = MutableLiveData<MapArguments>()
    var firstUse:Boolean =true
    var continueEditing:Boolean =false
    var startedAddingMarket:Boolean=false
    var finishedAddingMarket:Boolean=false
    var child = MutableLiveData<MockData.Child>()
}

data class MapArguments(
    var addressName: String,
    var lat: Double,
    var lng: Double
)
