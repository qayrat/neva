package uz.neva.kidya.ui.my_details

import android.annotation.SuppressLint
import android.app.DatePickerDialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.ArrayAdapter
import androidx.fragment.app.DialogFragment
import kotlinx.android.synthetic.main.add_child.*
import org.koin.android.ext.android.inject
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.MockData
import java.util.*

class AddChildDialog(var listener: NoticeDialogListener) : DialogFragment() {
    private val c: Calendar = Calendar.getInstance()
    val year = c.get(Calendar.YEAR)
    val month = c.get(Calendar.MONTH)
    val day = c.get(Calendar.DAY_OF_MONTH)
    private val viewModel: SharedViewModel by inject()

    interface NoticeDialogListener {
//        fun onDialogPositiveClick(dialog: DialogFragment)
//        fun onDialogNegativeClick(dialog: DialogFragment)
        fun onChildDataAdded(child: MockData.Child)
    }

//    // Override the Fragment.onAttach() method to instantiate the NoticeDialogListener
//    override fun onAttach(context: Context) {
//        super.onAttach(context)
//        // Verify that the host activity implements the callback interface
//        try {
//            // Instantiate the NoticeDialogListener so we can send events to the host
//           // listener = context as NoticeDialogListener
//        } catch (e: ClassCastException) {
//            // The activity doesn't implement the interface, throw exception
//            throw ClassCastException((context.toString() +
//                    " must implement NoticeDialogListener"))
//        }
//    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

//    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
//        return activity?.let {
//            val builder = AlertDialog.Builder(it)
//            // Get the layout inflater
//            val inflater = requireActivity().layoutInflater;
//
//            // Inflate and set the layout for the dialog
//            // Pass null as the parent view because its going in the dialog layout
//            builder.setView(inflater.inflate(R.layout.add_child, null))
//                // Add action buttons
////                .setPositiveButton(R.string.signin,
////                    DialogInterface.OnClickListener { dialog, id ->
////                        // sign in the user ...
////                    })
////                .setNegativeButton(R.string.cancel,
////                    DialogInterface.OnClickListener { dialog, id ->
////                        getDialog().cancel()
////                    })
//
//            builder.create()
//        } ?: throw IllegalStateException("Activity cannot be null")
//    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dialog?.requestWindowFeature((Window.FEATURE_NO_TITLE))


        return inflater.inflate(R.layout.add_child, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        var childdobday=0
        var childdobmonth=0
        var childdobyear=0
        addChildClose.setOnClickListener{
            Log.i("dialog","snding data")
//            listener.onChildDataAdded(MockData.Child(addChildName.text.toString(),addChildsexspinner.selectedItem.toString(),
//                Date(200,2,2)
//            ))
//            viewModel.child.postValue(MockData.Child(addChildName.text.toString(),addChildsexspinner.selectedItem.toString(),1))
            this.dismiss()
        }

        addChildAddButton.setOnClickListener{
            listener.onChildDataAdded(MockData.Child(addChildName.text.toString(),addChildsexspinner.selectedItem.toString(),
                Date(childdobyear,childdobmonth,childdobday)
            ))
            this.dismiss()
        }

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.child_gender,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
            addChildsexspinner.adapter = adapter
        }

        val dpd = DatePickerDialog(requireContext() ,R.style.datePicker,DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

            var monthName=""
            // Display Selected date in textbox
            when(monthOfYear)
            {
                1->monthName="Января"
                2->monthName="Февраля"
                3->monthName="Марта"
                4->monthName="Апреля"
                5->monthName="Майа"
                6->monthName="Июня"
                7->monthName="Июля"
                8->monthName="Августа"
                9->monthName="Сентября"
                10->monthName="Октября"
                11->monthName="Ноября"
                12->monthName="Декабря"
            }
            AddChildDOB.text = "$dayOfMonth $monthName $year"
            childdobday=dayOfMonth
            childdobmonth=monthOfYear
            childdobyear=year

        }, year, month, day)

        AddChildDOB.setOnClickListener{
            dpd.show()
        }

    }
    override fun onStart() {
        super.onStart()

        dialog?.window?.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
    }


}