package uz.neva.kidya.ui.brandShop

import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import timber.log.Timber
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.ui.category.selectedProduct.STARTING_PAGE_INDEX
import java.io.IOException

class BrandShopPagingSource(
        private val api: KidyaApi,
        val userId: String,
        val section_id: String = "",
        val sort: String
) : PagingSource<Int, ProductObject>() {
    override fun getRefreshKey(state: PagingState<Int, ProductObject>): Int? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductObject> {
        val position = params.key ?: STARTING_PAGE_INDEX

        return try {
            val response = api.getSelectedShop(userId, section_id, position.toString(), sort)
            LoadResult.Page(
                    data = response.data,
                    prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                    nextKey = when {
                        response.pagination?.total_count == 1 -> {
                            null
                        }
                        response.data.isEmpty() -> {
                            null
                        }
                        response.pagination?.current_page == response.pagination?.total_count -> {
                            position + 1

                        }
                        response.pagination?.current_page ?: 0 < response.pagination?.page_count ?: 0 -> {
                            position + 1
                        }
                        else -> null
                    }
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}