package uz.neva.kidya.ui.category.selectedProduct

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.fragment_q_a.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.ui.home.HomeViewModel

const val FOR_SETTINGS = "FOR_SETTINGS"

class QuestionAnswerFragment(val product: ProductObject) : Fragment(R.layout.fragment_q_a) {

    private val itemAdapter = ItemAdapter<QuestionAnswerModel>()
    private val fastAdapter = FastAdapter.with(itemAdapter)
    private val homeViewModel: HomeViewModel by viewModel()
    private var state = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        state = arguments?.getString(FOR_SETTINGS) ?: ""
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        questionList.layoutManager = LinearLayoutManager(requireContext())
        questionList.adapter = fastAdapter

        homeViewModel.getFaq(product.fields.suppiler_id ?: "0")

        homeViewModel.getFAQResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        itemAdapter.add(resource.data.data)
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

    }
}