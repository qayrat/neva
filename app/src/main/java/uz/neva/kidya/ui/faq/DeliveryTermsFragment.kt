package uz.neva.kidya.ui.faq

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_delivery_terms.*
import uz.neva.kidya.Const
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.utils.hide

class DeliveryTermsFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_delivery_terms, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        deliveryBack.setOnClickListener{
            findNavController().popBackStack()
        }

        val mainActivity=requireActivity() as MainActivity
        val webclient = FAQFragment.MywebClient(requireContext())
        webView.setWebViewClient(webclient)
        webView.loadUrl(Const.BASE_URL+"/info/delivery.php")
        webView.settings.javaScriptEnabled = true

        //textView27.text=HtmlCompat.fromHtml(resources.getString(R.string.privacy_policy),HtmlCompat.FROM_HTML_MODE_LEGACY)
        mainActivity.toolbar.hide()
        mainActivity.bottom_navigation.hide()
    }

    override fun onResume() {
        super.onResume()
        val mainActivity=requireActivity() as MainActivity

        mainActivity.toolbar.hide()
        mainActivity.bottom_navigation.hide()
    }
}