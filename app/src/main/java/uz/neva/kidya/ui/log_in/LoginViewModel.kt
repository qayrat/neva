package uz.neva.kidya.ui.log_in

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.AuthResponse
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event

class LoginViewModel constructor(private val repository: KidyaRepository): ViewModel(){
    val loginFirstStepResponse= MutableLiveData<Event<Resource<Any>>>()
    val loginSecondStepResponse= MutableLiveData<Event<Resource<AuthResponse>>>()

    fun getLoginFirstStepResponse(phone:String){
        viewModelScope.launch {
            repository.authFirstStep(phone).onEach{
                loginFirstStepResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getLoginSecondStepResponse(phone:String,code:String,fcm:String){
        viewModelScope.launch {
            repository.authSecondStep(phone,code,fcm).onEach{
                loginSecondStepResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }
}