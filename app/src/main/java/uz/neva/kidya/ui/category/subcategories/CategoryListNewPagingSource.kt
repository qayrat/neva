package uz.neva.kidya.ui.category.subcategories

import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import timber.log.Timber
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.dto.ProductObjectNew
import uz.neva.kidya.ui.category.selectedProduct.STARTING_PAGE_INDEX
import java.io.IOException

class CategoryListNewPagingSource(
        private val api: KidyaApi,
        val section_id: String = "",
        val isAllProduct: Boolean,
        val user_id: String = "",
        val sort: String
) : PagingSource<Int, ProductObjectNew>() {
    override fun getRefreshKey(state: PagingState<Int, ProductObjectNew>): Int? = null

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductObjectNew> {
        val position = params.key ?: STARTING_PAGE_INDEX

        return try {
            val response = api.getAllSelectedCategoryProductsNew(user_id, section_id, "true", position, sort)
            val feedPost = response.data
            LoadResult.Page(
                    data = feedPost,
                    prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                    nextKey = when {
                        response.pagination?.total_count == 1 -> {
                            null
                        }
                        feedPost.isEmpty() -> {
                            null
                        }
                        response.pagination?.current_page == response.pagination?.total_count -> {
                            position + 1

                        }
                        response.pagination?.current_page ?: 0 < response.pagination?.page_count ?: 0 -> {
                            position + 1
                        }
                        else -> null
                    }
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }
}