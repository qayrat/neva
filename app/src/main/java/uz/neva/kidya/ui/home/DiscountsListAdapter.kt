package uz.neva.kidya.ui.home

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.discounts_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.utils.ClickEvents

class DiscountsListAdapter(val context: Context) :
    PagingDataAdapter<ProductObject, DiscountsListAdapter.ViewHolder>(
        SelectedCategoryAdapter.PRODUCTS
    ) {
    var selectedCategory: List<ProductObject>? = null

    var onItemClick: ((ProductObject) -> Unit)? = null
    var click: ((ClickEvents) -> (Unit))? = null

    fun update(int: Int, fav: Boolean) {
        (getItem(int) as ProductObject).fields.in_favourites = if (fav) "Y" else "N"
        notifyItemChanged(int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.discounts_item, parent, false)
    )

    override fun onBindViewHolder(holder: DiscountsListAdapter.ViewHolder, position: Int) {
        holder.bindData(getItem(position) as ProductObject)
        getItem(position)
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.discountItemImage
        private val itemItemName = itemView.discountItemName
        private val itemPrice = itemView.discountItemPrice
        private val itemBrand = itemView.discountItemBrand
        private val itemIsFav = itemView.isFavouriteD
//        private val itemTag=itemView.tagback
//        private val newTag=itemView.newtag

        init {
            itemView.setOnClickListener {
                click?.invoke(ClickEvents.onItemClick((getItem(absoluteAdapterPosition) as ProductObject)))

            }
            itemView.isFavouriteD.setOnClickListener {
                Log.i("fav", "sending index$absoluteAdapterPosition")
                click?.invoke(
                    ClickEvents.onAddToFav(
                        (getItem(absoluteAdapterPosition) as ProductObject),
                        absoluteAdapterPosition
                    )
                )
            }
        }

        fun bindData(selectedCategory: ProductObject) {
            if (selectedCategory.fields.preview_picture != null && selectedCategory.fields.preview_picture != "")
                if (selectedCategory.fields.preview_picture[0] == '/') {
                    Glide.with(context)
                        .load(Const.BASE_URL + selectedCategory.fields.preview_picture)
                        .override(
                            100,
                            100
                        ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                        .into(itemImageLink)
                } else {
                    Glide.with(context).load(selectedCategory.fields.preview_picture)
                        .override(
                            100,
                            100
                        ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                        .into(itemImageLink)
                }
            else
                itemImageLink.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_broken_image_24))

            itemPrice.text = "${selectedCategory.fields.catalog_price_1 ?: "0".replace(","," ")} сум"
            for (item in selectedCategory.params) {

                when (item.name) {
                    "Поставщик" -> {
                        itemBrand.text = item.value[0]
                    }
                    "Цена со скидкой" -> {
                        itemPrice.text = "${item.value[0]} сум"
                    }
                    "Цена" -> {
                        itemPrice.text = "${item.value[0]} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(context).load(item.value[0])
                            .override(
                                100,
                                100
                            ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                            .into(itemImageLink)
                    }
                }
            }
            itemItemName.text = selectedCategory.fields.name

            if (selectedCategory.fields.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_favorited)
                itemIsFav.tag = "fav"
            } else {
                itemIsFav.setImageResource(R.drawable.ic_favourite)
                itemIsFav.tag = "notfav"
            }

//            if(!selectedCategory.itemTag)
//            {
//                itemTag.visibility=View.GONE
//                newTag.visibility=View.GONE
//            }

        }
    }

    companion object {
        val PRODUCTS = object : DiffUtil.ItemCallback<ProductObject>() {
            override fun areItemsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean = oldItem.fields.id == newItem.fields.id


            override fun areContentsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean =
                oldItem == newItem

        }
    }


}