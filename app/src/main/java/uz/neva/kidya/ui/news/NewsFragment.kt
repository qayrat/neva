package uz.neva.kidya.ui.news

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_news.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.ui.category.SpacesItemDecoration

class NewsFragment : Fragment() {
    private lateinit var newsitemadapter: NewsListAdapter
    private val newsViewModel: NewsViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_news, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        newsitemadapter= NewsListAdapter(requireContext())
        newsRecyclerView.layoutManager= LinearLayoutManager(requireContext())
        newsRecyclerView.addItemDecoration(SpacesItemDecoration((resources.displayMetrics.density*16+0.5f).toInt(),true,1))

        getFeedPosts()

        newsitemadapter?.onItemClick={
            val bundle=Bundle()
            bundle.putSerializable("news",it)
            findNavController().navigate(R.id.action_nav_newsFragment_to_nav_selectedNewsFragment,bundle)
        }

        NewsBack.setOnClickListener{
            findNavController().popBackStack()
        }

    }

    private fun getFeedPosts(){
        lifecycleScope.launch {
            newsViewModel.getNewsRecourse().collect { news ->
                newsRecyclerView.adapter = newsitemadapter
                newsitemadapter.submitData(news)
                return@collect
            }
        }
    }


    companion object {
        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                return Html.fromHtml(source)
            }
        }

    }
}