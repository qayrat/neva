package uz.neva.kidya.ui.notification

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.Notifications
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event

class NotificationViewModel constructor(private val repository: KidyaRepository): ViewModel(){

    val getNotificationsResponse= MutableLiveData<Event<Resource<Notifications>>>()
    val notificationMarkAsReadResponse= MutableLiveData<Event<Resource<Any>>>()
    val notificationAllResponse= MutableLiveData<Event<Resource<Any>>>()

    fun getNotifications(id:String) {
        viewModelScope.launch {
            repository.getNotifications(id).onEach{
                getNotificationsResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun notificationMarkAsRead(user_id:String,id:String?=null,all:String?=null) {
        viewModelScope.launch {
            repository.notificationsMarkAsRead(user_id, id, all).onEach{
                notificationMarkAsReadResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun notificationAllRead(user_id:String,all:String?=null) {
        viewModelScope.launch {
            repository.notificationsMarkAsRead(user_id, all = all).onEach{
                notificationAllResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }
}