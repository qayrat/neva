package uz.neva.kidya.ui.news

import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.text.HtmlCompat
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target
import kotlinx.android.synthetic.main.fragment_selected_news.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.NewsObject

class SelectedNewsFragment : Fragment() {

    private val newsViewModel: NewsViewModel by viewModel()
    private var isFavourite = false

    private var title = ""
    private var image = ""
    private var content = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selected_news, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectedNewsBack.setOnClickListener {
            findNavController().popBackStack()
        }

        share.setOnClickListener {
            val shareIntent = Intent()
            shareIntent.action = Intent.ACTION_SEND
            shareIntent.type = "text/plain"
            shareIntent.putExtra(Intent.EXTRA_TEXT,"$title \n$content")
            shareIntent.putExtra(Intent.EXTRA_SUBJECT,"Subject")
            startActivity(Intent.createChooser(shareIntent,"Выберите отправителя"))
        }

        addFavourite.setOnClickListener {
            if (!isFavourite){
                addFavourite.setImageResource(R.drawable.ic_favorited)
                isFavourite = true
            }
            else{
                addFavourite.setImageResource(R.drawable.ic_favourite)
                isFavourite = false
            }
        }


        arguments?.let {
            if (it.get("news") != null) {
                val args = it.get("news") as NewsObject
                newsTitle.text = args.name
                title = args.name
                newsContent.text = HtmlCompat.fromHtml(
                    args.detail_text.toString(),
                    HtmlCompat.FROM_HTML_MODE_LEGACY
                )
                content = newsContent.text.toString()
                //val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
                if (args.date != null) {
                    //val date = LocalDate.parse(args.date, formatter)
                    //val monthsru = requireContext().resources.getStringArray(R.array.months_ru)
                    newsDate.text = args.date
                }

                if (args.detail_picture != null)
                    if (args.detail_picture[0] == '/') {
                        Glide.with(requireContext())
                            .load("${Const.BASE_URL}${args.detail_picture}").override(
                                Target.SIZE_ORIGINAL,
                                Target.SIZE_ORIGINAL
                            ).into(newsArticleImg)
                        image = "${Const.BASE_URL}${args.detail_picture}"
                    } else {
                        Glide.with(requireContext()).load(args.detail_picture).override(
                            Target.SIZE_ORIGINAL,
                            Target.SIZE_ORIGINAL
                        ).into(newsArticleImg)
                        image = args.detail_picture
                    }
            } else if (it.getString("newsId") != null) {
                Log.i("news", "news from banner")
                newsViewModel.getSelectedNewsRecourse(it.getString("newsId").toString())
            }

        }

        newsViewModel.getSelectedNewsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        newsTitle.text = resource.data.data[0].preview_text
                        newsContent.text = resource.data.data[0].detail_text
                        title = resource.data.data[0].preview_text ?: ""
                        content = resource.data.data[0].detail_text ?: ""
                        //val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
                        if (resource.data.data[0].date != null) {
                            //val date = LocalDate.parse(resource.data.data[0].date, formatter)
                            //val monthsru = requireContext().resources.getStringArray(R.array.months_ru)
                            newsDate.text = resource.data.data[0].date
                        }

                        if (resource.data.data[0].detail_picture != null)
                            if (resource.data.data[0].detail_picture?.get(0) ?: "" == '/') {
                                Glide.with(requireContext())
                                    .load("${Const.BASE_URL}${resource.data.data[0].detail_picture}")
                                    .override(
                                        Target.SIZE_ORIGINAL,
                                        Target.SIZE_ORIGINAL
                                    ).into(newsArticleImg)

                                image = "${Const.BASE_URL}${resource.data.data[0].detail_picture}"
                            } else {
                                Glide.with(requireContext())
                                    .load(resource.data.data[0].detail_picture).override(
                                        Target.SIZE_ORIGINAL,
                                        Target.SIZE_ORIGINAL
                                    ).into(newsArticleImg)
                                image = resource.data.data[0].detail_picture?:""
                            }


                    }
                    is Resource.GenericError -> {
//                        showSnackbarWithMargin(
//                            resource.errorResponse.jsonResponse.getString(
//                                "error"
//                            )
//                        )
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

    }

}