package uz.neva.kidya.ui.check_out


import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.material_item.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.MockData

class PaymentMethodAdapter(): RecyclerView.Adapter<PaymentMethodAdapter.ViewHolder>(){

    var material: List<MockData.Payment>?= MockData.getPayment()

    fun updateList(size: List<MockData.Payment>) {
        this.material = size
        notifyDataSetChanged()

    }

    var onItemClick: ((MockData.Payment) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.material_item, parent, false)
    )

    override fun getItemCount()=material?.size?:0

    override fun onBindViewHolder(holder: PaymentMethodAdapter.ViewHolder, position: Int) {
        holder.bindData(material!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener  {
        val materialisPicked=itemView.matIsPicked
        val materialName=itemView.materialName

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(material!![adapterPosition])
            }
        }

        fun bindData(mat: MockData.Payment)
        {
            materialisPicked.setImageResource(R.drawable.ic_check_border)
            materialisPicked.setOnClickListener(this)
            materialName.text=mat.material
            materialName.setOnClickListener(this)

        }

        override fun onClick(p0: View?) {
            if(!material!![adapterPosition].isPicked)
            {
                Log.i("color","color picked")
                material!![adapterPosition].isPicked=true
                materialisPicked.setImageResource(R.drawable.ic_check_checked)
            }
            else
            {
                Log.i("color","color unpicked")
                material!![adapterPosition].isPicked=false
                materialisPicked.setImageResource(R.drawable.ic_check_border)
            }
        }
    }
}