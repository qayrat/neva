package uz.neva.kidya.ui.shipmentDetails

import android.app.Dialog
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.Window
import androidx.fragment.app.DialogFragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_order_response_dialog.*
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager

class OrderResponseDialogFragment : DialogFragment() {
    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater.inflate(R.layout.fragment_order_response_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val message = arguments?.getString("orderResponseMessageDialog")
        var actionButton = 0

        when (message) {
            "Недостаточно средств на карте" -> {
                tv_title.text = getString(R.string.oh_no)
                tv_description.text = getString(R.string.insufficient_funds_card)
                action_btn.visibility = View.GONE
                iv_image.setImageResource(R.drawable.funds_card)
            }
            "Карта не найдена." -> {
                tv_title.text = getString(R.string.oops)
                tv_description.text = getString(R.string.card_not_linked)
                action_btn.text = getString(R.string.add_card)
                iv_image.setImageResource(R.drawable.card_not_linked)
                actionButton = 1
            }
            else -> {
                tv_title.text = getString(R.string.something_wrong)
                tv_description.text = getString(R.string.something_wrong_desc)
                action_btn.text = getString(R.string.support)
                iv_image.setImageResource(R.drawable.something_wrong_desc)
                actionButton = 2
            }
        }

        iv_close_btn.setOnClickListener {
            dismiss()
        }
        action_btn.setOnClickListener {
            when (actionButton) {
                1 -> {
                    dismiss()
                    findNavController().navigate(R.id.paymentMethodFragment)
                }
                2 -> {
                    val intent = Intent(Intent.ACTION_DIAL)
                    intent.data = Uri.parse("tel:${PrefManager.getPhoneSupport(requireContext())}")
                    startActivity(intent)
                    dismiss()
                }
            }
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val dialog = super.onCreateDialog(savedInstanceState)
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE)
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        return dialog
    }
}