package uz.neva.kidya.ui.viewhistory

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_deals.*
import kotlinx.android.synthetic.main.fragment_my_view_history.*
import kotlinx.android.synthetic.main.fragment_my_view_history.blockerView
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.*

class MyViewHistoryFragment : Fragment() {
    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private lateinit var categoryAdapter: SelectedCategoryAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_view_history, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        categoryAdapter = SelectedCategoryAdapter(requireContext())

        getFeedPosts()

        historyRecycler.layoutManager =
            GridLayoutManager(requireContext(), 2, LinearLayoutManager.VERTICAL, false)
        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()
        historyRecycler.addItemDecoration(
            SpacesItemDecoration(
                resources.getDimension(R.dimen.fab_margin_16).toInt()
            )
        )
        categoryAdapter?.click = {
            when (it) {
                is ClickEvents.onAddToFav -> {

                    Log.i("fav", "fagment recived index ${it.index}")
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)

                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
            }
        }


        viewHistoryBack.setOnClickListener {
            findNavController().popBackStack()
        }
        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        blockerView.show()
                    }
                    is Resource.Success -> {
                        blockerView.hide()
                        categoryAdapter.update(resource.data.updatedPosition,resource.data.updatedStatus)

//                        if (resource.data.updatedStatus)
//                            showSnackbarWithMargin("Добавлена")
//                        else
//                            showSnackbarWithMargin("Удален")
                    }
                    is Resource.GenericError -> {

                        blockerView.hide()
//                        showSnackbarWithMargin(
//                            resource.errorResponse.jsonResponse.getString(
//                                "error"
//                            )
//                        )
                    }
                    is Resource.Error -> {

                        blockerView.hide()
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {
            categoryAdapter.loadStateFlow.collectLatest { loadStates ->
                if (loadStates.refresh is LoadState.Loading)
                    progressIndicatorViewHistory.show()
                else
                    progressIndicatorViewHistory.hide()
            }
        }
    }

    companion object {
        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                return Html.fromHtml(source)
            }
        }

    }


    private fun getFeedPosts(){
        lifecycleScope.launch {
            categoriesViewModel.getViewHistoryResponse(PrefManager.getUserID(requireContext()),1).collect { product->
                historyRecycler.adapter = categoryAdapter
                categoryAdapter.submitData(product)

                return@collect
            }
        }
    }
    fun addToFavs(product: ProductObject, int:Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
//            showSnackbarAuth("вам необходимо")
        } else {
            Log.i("fav", "fagment inside function index ${int}")
            if (product.fields.in_favourites!="Y") {
                checkOutViewModel.getAddToFavsResponse(PrefManager.getUserID(requireContext()), "favs", product.fields.id, "add",int,true)

            } else {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "delete",
                    int,
                    false
                )
            }
        }
    }
}