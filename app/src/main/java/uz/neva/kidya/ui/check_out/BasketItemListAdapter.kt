package uz.neva.kidya.ui.check_out

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.FrameLayout
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.google.android.material.snackbar.BaseTransientBottomBar
import com.google.android.material.snackbar.Snackbar
import kotlinx.android.synthetic.main.item_basket_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.utils.PrefUtils
import uz.neva.kidya.utils.convertDpToPixel

class BasketItemListAdapter(val context: Context, val fragment: Fragment) :
    RecyclerView.Adapter<BasketItemListAdapter.ViewHolder>() {


    var itemList: ArrayList<BasketItemObject> = arrayListOf<BasketItemObject>()
    fun updateList(itemList: ArrayList<BasketItemObject>) {
        this.itemList = itemList
        notifyDataSetChanged()
    }

    fun deleteItem(position: Int) {
        val tempSave = itemList.get(position)
        itemList?.removeAt(position)
        if (fragment is CheckOutFragment)
            fragment.deleteFromBasket(tempSave.id)

        notifyDataSetChanged()

        val snackbar = Snackbar.make(fragment.requireView(), "Удалена", Snackbar.LENGTH_SHORT)
            .setAction("Отменить") {
                itemList?.add(position, tempSave)
                notifyDataSetChanged()
            }
            .setBackgroundTint(ContextCompat.getColor(context, R.color.menu_selected_color))
            .setActionTextColor(ContextCompat.getColor(context, android.R.color.white))
            .addCallback(object : BaseTransientBottomBar.BaseCallback<Snackbar>() {
                override fun onShown(transientBottomBar: Snackbar?) {
                    super.onShown(transientBottomBar)
                }

                override fun onDismissed(transientBottomBar: Snackbar?, event: Int) {
                    super.onDismissed(transientBottomBar, event)
                    if (event == Snackbar.Callback.DISMISS_EVENT_ACTION) {
                        if (fragment is CheckOutFragment)
                            fragment.undoDelete(tempSave)
                    }
                }
            })
        val snackBarView: View = snackbar.getView()
        val params = snackBarView.layoutParams as FrameLayout.LayoutParams

        params.setMargins(
            params.leftMargin + 48,
            params.topMargin,
            params.rightMargin + 48,
            params.bottomMargin + 48
        )

        snackBarView.layoutParams = params

        snackBarView.setTranslationY(-(convertDpToPixel(48f, context)));
        snackbar.show()


    }

    var onItemClick: ((BasketItemObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_basket_item, parent, false)
    )

    override fun getItemCount() = itemList?.size ?: 0

    override fun onBindViewHolder(holder: BasketItemListAdapter.ViewHolder, position: Int) {
        holder.bindData(itemList!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.checkOutItemImage
        private val itemName = itemView.checkOutItemName
        private val itemPrice = itemView.checkOutItemPrice
        private val itemBrand = itemView.checkOutItemBrand
        private val itemQuantity = itemView.quantityInCart
        var count = ""


        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(itemList!![absoluteAdapterPosition])
            }
            itemView.incQuantity.setOnClickListener {
                count = (count.toInt() + 1).toString()
                if (fragment is CheckOutFragment) fragment.updateBasket(
                    itemList!![absoluteAdapterPosition],
                    count
                )
                itemView.decQuantity.isVisible = count != "1"
                itemView.deleteProduct.isVisible = count == "1"
//                itemQuantity.text = "$count шт."
            }
            itemView.deleteProduct.setOnClickListener {
                deleteItem(absoluteAdapterPosition)
            }
            itemView.decQuantity.setOnClickListener {
                if (count.toInt() != 0) {
                    count = (count.toInt() - 1).toString()
                    itemView.decQuantity.isVisible = count != "1"
                    itemView.deleteProduct.isVisible = count == "1"
//                    itemQuantity.text = "$count шт."
                    if (fragment is CheckOutFragment) fragment.updateBasket(
                        itemList!![absoluteAdapterPosition],
                        count
                    )
                } else {
                    itemList!!.remove(itemList!![absoluteAdapterPosition])
                    PrefUtils.removeProduct(itemList[absoluteAdapterPosition])
                    notifyDataSetChanged()
                }
            }
        }

        fun bindData(item: BasketItemObject) {
            itemView.incQuantity.isVisible = fragment is CheckOutFragment
            itemView.decQuantity.isVisible = fragment is CheckOutFragment && item.quantity > "1"
            itemView.deleteProduct.isVisible = fragment is CheckOutFragment && item.quantity == "1"
            count = item.quantity
            if (item.preview_picture != null && item.preview_picture.isNotEmpty())
                if (item.preview_picture[0] == '/')
                    Glide.with(context)
                            .load(Const.BASE_URL + item.preview_picture)
                            .thumbnail(0.1f)
                            .override(150,150)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
                else
                    Glide.with(context)
                            .load(item.preview_picture)
                            .thumbnail(0.1f)
                            .override(150,150)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
            else
                itemImageLink.setImageResource(
                    context.resources.getIdentifier(
                        "logotypesmall",
                        "drawable",
                        "uz.usoft.kidya"
                    )
                )

            itemName.text = item.name ?: ""
            itemPrice.text = item.price ?: ""
            itemBrand.text = item.suppiler ?: ""
            itemQuantity.text = (item.quantity ?: "1") + " шт."
        }
    }
}
