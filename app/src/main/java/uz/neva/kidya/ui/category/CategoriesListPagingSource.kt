package uz.neva.kidya.ui.category

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.dto.CategoriesObject
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class CategoriesListPagingSource(private val api: KidyaApi, val section_id: String = "") :
    PagingSource<Int, CategoriesObject>() {
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, CategoriesObject> {
        val position = params.key ?: STARTING_PAGE_INDEX

        return try {
            val response = api.getCategories(section_id, position)
            val feedPost = response.data

            Log.d("pagination", "a ${response.data}")
            LoadResult.Page(
                data = feedPost,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = when {
                    response.pagination.total_count == 1 -> {
                        null
                    }
                    feedPost.isEmpty() -> {
                        null
                    }
                    response.pagination.current_page == response.pagination.total_count -> {
                        position + 1

                    }
                    response.pagination.current_page < response.pagination.page_count -> {
                        position + 1
                    }
                    else -> null
                }
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, CategoriesObject>): Int? = null
}