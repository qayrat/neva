package uz.neva.kidya.ui.category.selectedProduct

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_feedback.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.ReviewObject

class ReviewsAdapter(): RecyclerView.Adapter<ReviewsAdapter.ViewHolder>(){

    var reviews: List<ReviewObject>?= arrayListOf()
    var selected:String=""

    fun updateList(reviews: List<ReviewObject>) {
        this.reviews = reviews
        notifyDataSetChanged()
    }

    var onItemClick: ((ReviewObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_feedback, parent, false)
    )

    override fun getItemCount()=reviews?.size?:0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(reviews!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener  {
        val feedbackText=itemView.feedbackText
        val feedbackRating=itemView.selectedItemFeedbackRatingBar
        val feedbackUser=itemView.feedbackUser
        //val innerCircle=itemView.colorInner

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(reviews!![adapterPosition])
            }
        }

        fun bindData(review: ReviewObject)
        {
            feedbackText.text=review.text
            feedbackUser.text=review.user_name
            feedbackRating.rating=review.rating
        }

        override fun onClick(p0: View?) {

        }
    }
}