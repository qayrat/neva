package uz.neva.kidya.ui.faq

import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.core.content.ContextCompat.startActivity
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_f_a_q.*
import uz.neva.kidya.Const
import uz.neva.kidya.R

class FAQFragment : Fragment() {

    lateinit var contextV: Context
    override fun onCreate(savedInstanceState: Bundle?) {

        super.onCreate(savedInstanceState)
        arguments?.let {
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_f_a_q, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        faqBack.setOnClickListener {
            findNavController().popBackStack()
        }

        val webclient = MywebClient(requireContext())
        webView.setWebViewClient(webclient)
        webView.loadUrl(Const.BASE_URL+"/info/policy.php")
        webView.settings.javaScriptEnabled = true


    }

    class MywebClient(val context: Context) : WebViewClient() {
        override fun shouldOverrideUrlLoading(
            view: WebView?,
            request: WebResourceRequest?
        ): Boolean {
            Log.i("webview",request?.url.toString())
            Log.i("webview",Const.BASE_URL+"/info/policy.php")
            if(request?.url.toString()==Const.BASE_URL+"/info/delivery.php")
                return super.shouldOverrideUrlLoading(view, request)
            else
            {
                val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(request?.url.toString()))
                startActivity(context,browserIntent,null)
            }
            return true
        }
    }

}