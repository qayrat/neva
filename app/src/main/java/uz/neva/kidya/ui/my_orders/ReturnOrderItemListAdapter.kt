package uz.neva.kidya.ui.my_orders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_return_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show

class ReturnOrderItemListAdapter(val context: Context)  : RecyclerView.Adapter<ReturnOrderItemListAdapter.ViewHolder>() {

    var itemList: ArrayList<BasketItemObject> = arrayListOf<BasketItemObject>()


    val productsIdList:MutableList<String> = mutableListOf()

    fun updateList(itemList: ArrayList<BasketItemObject>) {
        this.itemList = itemList
        notifyDataSetChanged()
    }

    var onItemClick: ((BasketItemObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_return_item, parent, false)
    )

    override fun getItemCount() = itemList.size

    override fun onBindViewHolder(holder: ReturnOrderItemListAdapter.ViewHolder, position: Int) {
        holder.bindData(itemList[position])
    }

    fun getReturnProductList():HashMap<String,String>
    {

        val products=HashMap<String,String>()
        for ( i in 0 until productsIdList.size)
            products["products[${i}]"]=productsIdList[i]
        return products
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.checkOutItemImage
        private val itemName = itemView.checkOutItemName
        private val itemPrice = itemView.checkOutItemPrice
        private val itemBrand = itemView.checkOutItemBrand
        private val itemTint = itemView.tint

        init {
            itemView.checkBox.setOnClickListener {
                onItemClick?.invoke(itemList!![adapterPosition])
                if(itemView.checkBox.isChecked)
                {
                    itemTint.show()
                    productsIdList.add(itemList[adapterPosition].id)
                }else {
                    itemTint.hide()
                    productsIdList.remove(itemList[adapterPosition].id)
                }
            }
        }


        fun bindData(item: BasketItemObject) {


            if (item.preview_picture != null)
                if(item.preview_picture[0]=='/')
                    Glide.with(context).load(Const.BASE_URL + item.preview_picture)
                        .placeholder(
                            R.drawable.ic_baseline_cloud_download_24
                        ).into(itemImageLink)
                else
                    Glide.with(context).load(item.preview_picture)
                        .placeholder(
                            R.drawable.ic_baseline_cloud_download_24
                        ).into(itemImageLink)
            else
                itemImageLink.setImageResource(
                    context.resources.getIdentifier(
                        "ic_top",
                        "drawable",
                        "uz.neva.kidya"
                    )
                )
            itemName.text = item.name
            itemPrice.text = item.price
            itemBrand.text = item.suppiler ?: ""
        }
    }
}