package uz.neva.kidya.ui.my_orders

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_basket_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show

class SelectedOrderItemListAdapter (val context: Context): RecyclerView.Adapter<SelectedOrderItemListAdapter.ViewHolder>() {


    var itemList: ArrayList<BasketItemObject> = arrayListOf<BasketItemObject>()
    fun updateList(itemList: ArrayList<BasketItemObject>) {
        this.itemList = itemList
        notifyDataSetChanged()
    }

    var onItemClick: ((BasketItemObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_basket_item, parent, false)
    )

    override fun getItemCount() = itemList?.size ?: 0

    override fun onBindViewHolder(holder: SelectedOrderItemListAdapter.ViewHolder, position: Int) {
        holder.bindData(itemList!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.checkOutItemImage
        private val itemName = itemView.checkOutItemName
        private val itemPrice = itemView.checkOutItemPrice
        private val itemBrand = itemView.checkOutItemBrand
        private val itemQuantity = itemView.quantityInCart
        private val incQuantity = itemView.incQuantity
        private val decQuantity = itemView.decQuantity

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(itemList!![adapterPosition])
            }
        }

        fun bindData(item: BasketItemObject) {

                incQuantity.hide()
                decQuantity.hide()

            if(item.returned!!)
                itemView.tint.show()
            else itemView.tint.hide()
            if (item.preview_picture != null)
                if(item.preview_picture[0]=='/')
                Glide.with(context).load(Const.BASE_URL + item.preview_picture)
                    .placeholder(
                        R.drawable.ic_baseline_cloud_download_24
                    ).into(itemImageLink)
                else
                    Glide.with(context).load(item.preview_picture)
                        .placeholder(
                            R.drawable.ic_baseline_cloud_download_24
                        ).into(itemImageLink)
            else
                itemImageLink.setImageResource(
                    context.resources.getIdentifier(
                        "ic_top",
                        "drawable",
                        "uz.usoft.kidya"
                    )
                )
            itemName.text = item.name ?: ""
            itemPrice.text = item.price ?: ""
            itemBrand.text = item.suppiler ?: ""
            itemQuantity.text = (item.quantity ?: "1") + " шт."
        }
    }
}
