package uz.neva.kidya.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.selected_category_item2.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.utils.ClickEvents
import uz.neva.kidya.utils.show

class SelectedCategoryAdapter2(val context: Context) :
    PagingDataAdapter<ProductObject, SelectedCategoryAdapter2.ViewHolder>(
        PRODUCTS
    ) {

    var selectedCategory: List<ProductObject>? = null

    var onItemClick: ((ProductObject) -> Unit)? = null
    var click: ((ClickEvents) -> (Unit))? = null

    fun update(int: Int, fav: Boolean) {
        (getItem(int) as ProductObject).fields.in_favourites = if (fav) "Y" else "N"
        notifyItemChanged(int)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(getItem(position) as ProductObject)
        getItem(position)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.selected_category_item2, parent, false)
    )

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.selectedCategoryItemImage
        private val itemItemName = itemView.selectedCategoryItemName

        //        private var itemRating=itemView.selectedCategoryItemRatingBar
        private val itemPrice = itemView.selectedCategoryItemPrice
        private val itemOldPrice = itemView.selectedCategoryItemOldPrice
        private val itemBrand = itemView.selectedCategoryItemBrand
        private val itemIsFav = itemView.isFavourite
        private val itemTag = itemView.tagback


        init {
            itemView.setOnClickListener {
                click?.invoke(ClickEvents.onItemClick((getItem(absoluteAdapterPosition) as ProductObject)))
            }
            itemView.isFavourite.setOnClickListener {
                click?.invoke(
                    ClickEvents.onAddToFav(
                        (getItem(absoluteAdapterPosition) as ProductObject),
                        absoluteAdapterPosition
                    )
                )
            }
        }

        fun bindData(selectedCategory: ProductObject) {
            if (selectedCategory.fields.preview_picture != null && selectedCategory.fields.preview_picture != "")
                if (selectedCategory.fields.preview_picture[0] == '/') {
                    Glide.with(context)
                        .load(Const.BASE_URL + selectedCategory.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(250, 250)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
                } else {
                    Glide.with(context).load(selectedCategory.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(250, 250)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
                }
            else
                itemImageLink.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_broken_image_24))


//            itemRating.rating=selectedCategory.fields.rating?:0f
            itemTag.visibility = View.GONE
            itemItemName.text = selectedCategory.fields.name
            itemPrice.text =
                "${(selectedCategory.fields.catalog_price_1 ?: "0").replace(",", " ")} сум"

            if (selectedCategory.fields.percent != null) {
                itemOldPrice.show()
                itemOldPrice.text =
                    "${(selectedCategory.fields.old_price ?: "0").replace(",", " ")} сум"
                itemPrice.text =
                    "${(selectedCategory.fields.catalog_price_1) ?: "0".replace(",", " ")} сум"
                itemTag.visibility = View.VISIBLE
                itemTag.text = "-${selectedCategory.fields.percent ?: ""}%"
            }

            for (item in selectedCategory.params) {

                when (item.name) {
                    "Поставщик" -> {

                        itemBrand.text = item.value[0]
                    }
                    "Цена со скидкой" -> {
                        itemOldPrice.show()
                        itemOldPrice.text = "${selectedCategory.fields.catalog_price_1 ?: "0".replace(","," ")} сум"
                        itemPrice.text = "${item.value[0].replace(",", " ")} сум"
                        itemTag.visibility = View.VISIBLE
                        itemTag.text = "${selectedCategory.fields.percent ?: ""}-% off"
                    }
                    "Цена" -> {
                        if (itemTag.visibility == View.GONE)
                            itemPrice.text = "${item.value[0].replace(",", " ")} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(context).load(item.value[0])
                                .thumbnail(0.2f)
                                .override(250, 250)
                                .placeholder(R.drawable.placeholder_neva)
                                .into(itemImageLink)
                    }

                }
            }


            if (selectedCategory.fields.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_favorited)
            } else
                itemIsFav.setImageResource(R.drawable.ic_favourite)

//            if(!selectedCategory.itemTag)
//            {
//                itemTag.visibility=View.GONE
//                newTag.visibility=View.GONE
//            }

        }
    }

    companion object {
        val PRODUCTS = object : DiffUtil.ItemCallback<ProductObject>() {
            override fun areItemsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean = oldItem.fields.id == newItem.fields.id

            override fun areContentsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean =
                oldItem == newItem

        }
    }


}