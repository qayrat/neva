package uz.neva.kidya.ui.adapter

import android.view.View
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_tag.view.*
import uz.neva.kidya.R

class NewsModel(
    val id: Int,
    val image: String?,
    val description: String?,
    val link: String?
) : AbstractItem<NewsModel.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<NewsModel>(itemView) {
        override fun bindView(item: NewsModel, payloads: List<Any>) {

        }

        override fun unbindView(item: NewsModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_news
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}

class TagModel(
    val id: Int,
    val tagText: String
) : AbstractItem<TagModel.VH>() {
    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<TagModel>(itemView) {
        override fun bindView(item: TagModel, payloads: List<Any>) {
            itemView.tagTextItem.text = item.tagText
        }

        override fun unbindView(item: TagModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_tag
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}