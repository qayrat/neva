package uz.neva.kidya.ui.category.selectedProduct

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.second_fragment_collection_object.view.*
import timber.log.Timber
import uz.neva.kidya.R

class ImagesAdapter(val context: Context): RecyclerView.Adapter<ImagesAdapter.ViewHolder>(){

    var images: List<String>?= null

    fun updateList(images: List<String>) {
        this.images = images
        notifyDataSetChanged()
    }

    //var onItemClick: ((MockData.Color) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.second_fragment_collection_object, parent, false)
    )

    override fun getItemCount()=images?.size?:0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(images!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {

        val image=itemView.imageView3

        init {
            itemView.setOnClickListener {
            }
        }

        fun bindData(images: String)
        {
            Timber.d(images)
            Glide.with(context).load(images)
                    .thumbnail(0.1f)
                    .override(500, 500)
                    .placeholder(R.drawable.placeholder_neva)
                    .into(image)
        }
    }
}