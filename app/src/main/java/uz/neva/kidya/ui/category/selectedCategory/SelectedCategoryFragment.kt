package uz.neva.kidya.ui.category.selectedCategory

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_selected_category.*
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import uz.neva.kidya.R
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.filter.FilterDialog
import uz.neva.kidya.utils.ClickEvents
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show

class SelectedCategoryFragment : Fragment(), FilterDialog.NoticeDialogListener {
    var sectionId = ""
    var sectionName = " "
//    private val categoriesViewModel: CategoriesViewModel by viewModel()
//    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private lateinit var categoryAdapter: SelectedCategoryAdapter


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            sectionId = it.getString("section_id").toString()
            sectionName = it.getString("section_name").toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selected_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        categoryAdapter = SelectedCategoryAdapter(requireContext())
        selectedCategoryRecyclerView.layoutManager =
            GridLayoutManager(requireContext(), 2, LinearLayoutManager.VERTICAL, false)

//        if (sectionName != "Скидки")
//            getFeedPosts(sectionId)
//        else
//            getFeedPostsDiscount()


        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()
        selectedCategoryRecyclerView.addItemDecoration(SpacesItemDecoration(marginpixels))

        selectedCategoryBack.setOnClickListener {
            findNavController().popBackStack()
        }

        selectedCategoryName.text = sectionName
        filterLay.setOnClickListener {
            val newDialog =
                if (sectionName != "Скидки") FilterDialog(this, sectionId) else FilterDialog(
                    this,
                    sectionId,
                    true
                )
            val fm = requireActivity().supportFragmentManager
            newDialog.show(fm, "success")
            newDialog.isCancelable = true
        }


        categoryAdapter?.click = {
            when (it) {
                is ClickEvents.onAddToFav -> {
//                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {

                }
            }
        }

//        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
//            it.getContentIfNotHandled()?.let { resource ->
//                when (resource) {
//                    is Resource.Loading -> {
//                        blockerView.show()
//                    }
//                    is Resource.Success -> {
//                        blockerView.hide()
//
//                        categoryAdapter.update(
//                            resource.data.updatedPosition,
//                            resource.data.updatedStatus
//                        )
//
//
////                        if (resource.data.updatedStatus)
//////                            showSnackbarWithMargin("Добавлена")
////                        else
//////                            showSnackbarWithMargin("Удален")
//                    }
//                    is Resource.GenericError -> {
//
//                        blockerView.hide()
//                        changeUiStateEnabled(
//                            false,
//                            progressIndicatorAddToCart,
//                            itemFavourite
//                        )
////                        showSnackbarWithMargin(
////                            resource.errorResponse.jsonResponse.getString(
////                                "error"
////                            )
////                        )
//                    }
//                    is Resource.Error -> {
//
//                        blockerView.hide()
////                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
//                    }
//                }
//            }
//        })

        viewLifecycleOwner.lifecycleScope.launch {
            categoryAdapter.loadStateFlow.collectLatest { loadStates ->
                if (loadStates.refresh is LoadState.Loading)
                    progressIndicatorSelectedCategories.show()
                else
                    progressIndicatorSelectedCategories.hide()
            }
        }
    }

//    private fun getFeedPostsDiscount() {
//        lifecycleScope.launch {
//            categoriesViewModel.getSaleProductsResponse(PrefManager.getUserID(requireContext()))
//                .collect { product ->
//                    selectedCategoryRecyclerView.adapter = categoryAdapter
//                    categoryAdapter.submitData(product)
//                    return@collect
//                }
//        }
//    }
//
//    private fun getFeedPosts(section_id: String) {
//        lifecycleScope.launch {
//            categoriesViewModel.getSelectedCategoryProductsResponse(
//                section_id,
//                PrefManager.getUserID(requireContext())
//            ).collect { product ->
//                selectedCategoryRecyclerView.adapter = categoryAdapter
//                categoryAdapter.submitData(product)
//                return@collect
//            }
//        }
//    }


    companion object {
        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                return Html.fromHtml(source)
            }
        }

    }

//    fun addToFavs(product: ProductObject, int: Int) {
//        if (PrefManager.getUserID(requireContext()).isEmpty()) {
////            showSnackbarAuth("вам необходимо")
//        } else {
//
//            if (product.fields.in_favourites != "Y") {
//                checkOutViewModel.getAddToFavsResponse(
//                    PrefManager.getUserID(requireContext()),
//                    "favs",
//                    product.fields.id,
//                    "add",
//                    int,
//                    true
//                )
//
//            } else {
//                checkOutViewModel.getAddToFavsResponse(
//                    PrefManager.getUserID(requireContext()),
//                    "favs",
//                    product.fields.id,
//                    "delete",
//                    int,
//                    false
//                )
//            }
//        }
//    }

    override fun onFilterApplied(
        color: ArrayList<String>,
        supplier: ArrayList<String>,
        from: String,
        to: String,
        size: ArrayList<String>,
        hit: String
    ) {
        Log.i("filter", "color $color, supplier $supplier")
        val properties = HashMap<String, String>()
        if (color.isNotEmpty())
            for (index in 0 until color.size)
                properties["property_color[$index]"] = color[index].substring(1)
        if (supplier.isNotEmpty())
            for (index in 0 until supplier.size)
                properties["property_suppiler[$index]"] = supplier[index]
        properties["property_base_from"] = from
        properties["property_base_to"] = to
        if (size.isNotEmpty())
            for (index in 0 until size.size)
                properties["property_razmer[$index]"] = size[index]
        if (hit.isNotBlank())
            properties["property_hit"] = hit

//        lifecycleScope.launch {
//            if (sectionName != "Скидки") {
//                categoriesViewModel.getFilteredResult(
//                    PrefManager.getUserID(requireContext()),
//                    sectionId,
//                    properties = properties
//                ).collect { product ->
//                    selectedCategoryRecyclerView.adapter = categoryAdapter
//                    categoryAdapter.submitData(product)
//                    return@collect
//                }
//            } else {
//                categoriesViewModel.getFilteredSaleResult(
//                    PrefManager.getUserID(requireContext()),
//                    sectionId,
//                    properties = properties
//                ).collect { product ->
//                    selectedCategoryRecyclerView.adapter = categoryAdapter
//                    categoryAdapter.submitData(product)
//                    return@collect
//                }
//            }
//        }

    }


}