package uz.neva.kidya.ui.my_details

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.MyOrderHistory
import uz.neva.kidya.network.dto.Payment
import uz.neva.kidya.network.dto.Profile
import uz.neva.kidya.network.dto.SelectedOrderDetails
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.profile.ProfileUpdateModel
import uz.neva.kidya.network.dto.nevaModel.response.EmptyResponse
import uz.neva.kidya.network.dto.social.SocialResponse
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event

class ProfileViewModel constructor(private val repository: KidyaRepository) : ViewModel() {
    val getOrderHistoryResponse = MutableLiveData<Event<Resource<MyOrderHistory>>>()
    val getOrderDetailsResponse = MutableLiveData<Event<Resource<SelectedOrderDetails>>>()
    val makePaymentResponse = MutableLiveData<Event<Resource<Payment>>>()
    val getProfileResponse = MutableLiveData<Event<Resource<Profile>>>()
    val updateProfileResponse = MutableLiveData<Event<Resource<Profile>>>()
    val updateProfileResponse2 = MutableLiveData<Event<Resource<EmptyResponse>>>()
    val updateKidsResponse = MutableLiveData<Event<Resource<Profile>>>()
    val returnProductResponse = MutableLiveData<Event<Resource<Any>>>()
    val interests = ArrayList<String>()
    val analyticsResponse = MutableLiveData<Event<Resource<EmptyResponse>>>()

    fun getGetOrderHistoryResponse(user_id: String) {
        viewModelScope.launch {
            repository.getOrderHistory(user_id).onEach { getOrderHistoryResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun addInterest(interest: String) {
        val s = interests.find { e -> e == interest }
        if (s == null) {
            interests.add(interest)
        }
    }

    fun removeInterest(interest: String) {
        val s = interests.find { e -> e == interest }
        if (s != null) {
            interests.remove(interest)
        }
    }


    fun getGetOrderDetailsResponse(user_id: String, order_id: String) {
        viewModelScope.launch {
            repository.getOrderDetails(user_id, order_id)
                .onEach { getOrderDetailsResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun getReturnProductResponse(
        user_id: String,
        order_id: String,
        products: HashMap<String, String>
    ) {
        viewModelScope.launch {
            repository.returnProduct(user_id, order_id, products)
                .onEach { returnProductResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }


    fun getMakePaymentResponse(order_id: String) {
        viewModelScope.launch {
            repository.makePayment(order_id).onEach { makePaymentResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getProfile(user_id: String) {
        viewModelScope.launch {
            repository.getProfile(user_id).onEach { getProfileResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun updateProfile(
        user_id: String,
        name: String,
        phone: String,
        address: String,
        district: String,
        street: String,
        gender: String,
        birthday: String,
        email: String? = "",
        interest: String? = ""
    ) {
        viewModelScope.launch {
            repository.updateProfile(
                user_id = user_id,
                name = name,
                phone = phone,
                address = address,
                district = district,
                street = street,
                gender = gender,
                birthday = birthday,
                email = email,
                interest = interest
            ).onEach { updateProfileResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun updateKids(
        user_id: String,
        kidsName1: String,
        kidsBirthday1: String,
        kidsGander1: String
    ) {
        viewModelScope.launch {
            repository.updateBaby(
                user_id,
                kidsName1 = kidsName1,
                kidsBirthday1 = kidsBirthday1,
                kidsGander1 = kidsGander1,
            )
                .onEach { updateKidsResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun updateProfile2(model: ProfileUpdateModel, userId: String) {
        viewModelScope.launch {
            repository.updateProfile(model, userId)
                    .onEach { updateProfileResponse2.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun putAnalytics(model: AnalyticsModel) {
        viewModelScope.launch {
            repository.putAnalytics(model)
                    .onEach { analyticsResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }
}