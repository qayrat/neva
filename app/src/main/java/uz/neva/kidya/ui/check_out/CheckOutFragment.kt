package uz.neva.kidya.ui.check_out

import android.app.Dialog
import android.content.pm.PackageManager
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import com.google.android.material.snackbar.Snackbar
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_check_out.*
import kotlinx.android.synthetic.main.fragment_promo_bottomsheet.*
import kotlinx.android.synthetic.main.fragment_promo_bottomsheet.view.*
import kotlinx.android.synthetic.main.fragment_used_coupon.*
import kotlinx.android.synthetic.main.item_coupon_data.view.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.*
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.network.dto.BasketMetaObject
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.selectedCategory.RecommendedProductAdapter
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.*

class CheckOutFragment : Fragment(), OnSelectCouponListener {
    var chosenAddress = ""
    var chosenAddressName = ""
    var chosenAddressNumber = ""
    var chosenAddressCity = ""
    var chosenAddressStreet = ""
    var chosenAddressDistrict = ""
    var chosenAddressComment = ""
    var chosenAddressLatLong = ""
    val addressBundle = Bundle()
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private val profileViewModel: ProfileViewModel by viewModel()
    private lateinit var paymentMethodAdapter: PaymentMethodAdapter
    private lateinit var basketItemListAdapter: BasketItemListAdapter
    private var product: BasketItemObject? = null
    private var count = ""
    private val homeViewModel: HomeViewModel by viewModel()
    private lateinit var recommendeditemadapter: RecommendedProductAdapter
    var updateAdapter = ""
    private var basketData: ArrayList<BasketItemObject> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            chosenAddress = it.getString("chosenAddress") ?: ""
            chosenAddressLatLong = it.getString("chosenAddressLatLong") ?: ""

            chosenAddressName =
                it.getString("chosenAddressName") ?: PrefManager.getName(requireContext())
            chosenAddressNumber =
                it.getString("chosenAddressNumber") ?: PrefManager.getPhone(requireContext())
            chosenAddressCity =
                it.getString("chosenAddressCity") ?: PrefManager.getCity(requireContext())
            chosenAddressDistrict =
                it.getString("chosenAddressDistrict") ?: PrefManager.getDistrict(requireContext())
            chosenAddressStreet =
                it.getString("chosenAddressStreet") ?: PrefManager.getStreet(requireContext())
            chosenAddressComment = it.getString("chosenAddressComment") ?: ""
        }
        recommendeditemadapter = RecommendedProductAdapter(requireContext())
        getFeedPostsPopularShoes()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_check_out, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        checkOutViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Basket")
        ))

        val bottomNavigationView: BottomNavigationView = requireActivity().findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.action_bag

        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()
//        checkOutPhone.formatPhoneMask()
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.city,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
//            checkOutAddressSpinnerOneTwo.adapter = adapter
        }
        val list = ArrayList<BasketItemObject>()

        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            list.addAll(PrefUtils.getProduct())

            progressbar.visibility = View.GONE
            basketLayout.visibility = View.VISIBLE
            emptyBasketView.isVisible = list.isEmpty()
            checkOut.isVisible = list.isNotEmpty()
            priceCount.isVisible = list.isNotEmpty()

            var totalPrice = 0
            list.forEach { e ->
                val price = e.price?.replace(",", "")?.replace(" ", "")?.toInt() ?: 0
                totalPrice += price
            }

            var s = 0
            PrefUtils.getProduct().forEach { e -> s += e.quantity.toInt() }
            (requireActivity() as MainActivity).updateBadge(s)

            checkOutPrice.text = "$totalPrice ${resources.getString(R.string.text_type_money)}"
            checkOutDeliveryFee.text = "0 ${resources.getString(R.string.text_type_money)}"
            checkOutTotal.text = "${totalPrice} ${resources.getString(R.string.text_type_money)}"
        }

        promoCode.isVisible = PrefManager.getUserID(requireContext()).isNotEmpty()


        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.district,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
//            checkOutAddressSpinnerOne.adapter = adapter
        }

        recommendedList.layoutManager = GridLayoutManager(requireContext(), 2)
        recommendedList.addItemDecoration(SpacesItemDecoration(marginpixels, true, 2))
        recommendedList.adapter = recommendeditemadapter
        recommendeditemadapter.rclick = {

            when (it) {
                is ClickEvents.onAddToFav -> {

                    updateAdapter = "shoes"
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {
                }
            }
        }
        chosenAddressName = PrefManager.getName(requireContext())
        chosenAddressNumber = PrefManager.getPhone(requireContext())
        chosenAddressCity = PrefManager.getCity(requireContext())
        chosenAddressDistrict = PrefManager.getDistrict(requireContext())
        chosenAddressStreet = PrefManager.getStreet(requireContext())


        paymentMethodAdapter = PaymentMethodAdapter()


        basketItemListAdapter = BasketItemListAdapter(requireContext(), this)
        basketRecycler.layoutManager = LinearLayoutManager(requireContext())
        basketRecycler.adapter = basketItemListAdapter

        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            basketItemListAdapter.updateList(list)
        }

        var itemTouchHelper = ItemTouchHelper(SwipeToDelete(basketItemListAdapter))
        itemTouchHelper.attachToRecyclerView(basketRecycler)


        checkOut.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isEmpty())
                findNavController().navigate(R.id.nav_login)
            else {
                checkOut.startAnimation {
                    checkOutViewModel.getGetCheckOutResponse(
                        PrefManager.getUserID(requireContext()),
                        coupon = if (promo_text_code.text.toString() != resources.getString(R.string.promo_code)) promo_text_code.text.toString() else null
                    )
                }
            }
        }

        checkOutViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                    }
                    is Resource.GenericError -> {
                    }
                    is Resource.Error -> {
                    }
                }
            }
        })

        checkOutViewModel.getCheckOutResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(true, progressIndicatorCheckOut, checkOutButton)
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
//                        showSnackbarWithMargin("Оформлен")
                        (requireActivity() as MainActivity).updateBadge(0)
                        PrefUtils.clearProduct()
                        PrefManager.saveOrderID(
                            requireContext(),
                            resource.data.order_id.toInt()
                        )
                        checkOut.apply {
                            revertAnimation()
                            background = resources.getDrawable(R.drawable.button_save)
                        }
                        findNavController().navigate(R.id.shipmentDetailsFragment)


                    }
                    is Resource.GenericError -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        checkOutViewModel.addToBasketResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        blockerView.visibility = View.VISIBLE
                    }
                    is Resource.Success -> {
//                        checkOutViewModel.getGetBasketResponse(
//                            PrefManager.getUserID(
//                                requireContext()
//                            )
//                        )
                        if (product != null) {
                            basketItemListAdapter.itemList.firstOrNull { e -> e.id == product!!.id }?.quantity =
                                count
                            basketItemListAdapter.notifyItemChanged(
                                basketItemListAdapter.itemList.indexOf(
                                    product
                                )
                            )
                        }
                        checkOutViewModel.getGetBasketResponse(PrefManager.getUserID(requireContext()))

//                        blockerView.visibility = View.GONE
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
//                        blockerView.visibility = View.GONE
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
//                        blockerView.visibility = View.GONE
                    }
                }
            }
        })
        if (PrefManager.getUserID(requireContext()).isNotEmpty()) {
            val listCouponData = ArrayList<CouponData>()
            checkOutViewModel.getCouponList(PrefManager.getUserID(requireContext()))
            checkOutViewModel.getCouponListResponse.observe(viewLifecycleOwner, Observer {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {

                        is Resource.Loading -> {

                        }

                        is Resource.Success -> {
                            val list = resource.data.data
                            for (i in 0 until list.size) {
                                listCouponData.add(
                                    CouponData(
                                        i,
                                        false,
                                        list[i].coupon,
                                        list[i].name,
                                        "от ${list[i].start} \nдо ${list[i].end}"
                                    )
                                )
                            }
                        }

                        is Resource.GenericError -> {
                            showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                        }
                        is Resource.Error -> {
                            resource.exception.message?.let { it1 -> showSnackbar(it1) }
                        }
                    }
                }
            })

            promoCode.setOnClickListener {
                val bottomSheetDialog =
                    PromoBottomSheetFragment.newInstance(listCouponData, this)
                bottomSheetDialog.show(
                    requireActivity().supportFragmentManager,
                    "Promo Bottom Sheet Dialog Fragment"
                )
//                val checked = isCheckDiscount(basketData)
//                if (!checked) {
//                    val bottomSheetDialog =
//                        PromoBottomSheetFragment.newInstance(listCouponData, this)
//                    bottomSheetDialog.show(
//                        requireActivity().supportFragmentManager,
//                        "Promo Bottom Sheet Dialog Fragment"
//                    )
//                } else {
//                    val fragment = CheckDiscountDialogFragment()
//                    fragment.show(childFragmentManager, "CheckDiscountDialog")
//                }
            }

            checkOutViewModel.getGetBasketResponse(PrefManager.getUserID(requireContext()))
            progressbar.visibility = View.VISIBLE
            basketLayout.visibility = View.GONE
            checkOutViewModel.getBasketResponse.observe(viewLifecycleOwner, Observer {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {
                        is Resource.Loading -> {

                        }
                        is Resource.Success -> {
//                            if (resource.data.data.isEmpty()){
//                                profileViewModel.getGetOrderDetailsResponse(
//                                    PrefManager.getUserID(requireContext()),
//                                    PrefManager.getOrderId(requireContext()).toString()
//                                )
//                            }

                            progressbar.visibility = View.GONE
                            basketLayout.visibility = View.VISIBLE
                            emptyBasketView.isVisible = resource.data.data.isEmpty()
                            checkOut.isVisible = resource.data.data.isNotEmpty()
                            priceCount.isVisible = resource.data.data.isNotEmpty()
                            basketData = resource.data.data as ArrayList<BasketItemObject>

                            val price = resource.data.meta.basket_price ?: "0"
                            val totalPrice = resource.data.meta.total ?: "0"
                            val deliveryPrice = resource.data.meta.delivery_price ?: "0"
                            checkOutTotal.text =
                                "${totalPrice} ${resources.getString(R.string.text_type_money)}"
                            checkOutDeliveryFee.text =
                                "${deliveryPrice} ${resources.getString(R.string.text_type_money)}"
                            checkOutPrice.text =
                                "${price} ${resources.getString(R.string.text_type_money)}"

                            (requireActivity() as MainActivity).updateBadge(resource.data.meta.count!!.toInt())
                            basketItemListAdapter.updateList(resource.data.data as ArrayList<BasketItemObject>)
                        }
                        is Resource.GenericError -> {
                            progressbar.visibility = View.GONE
                            basketLayout.visibility = View.VISIBLE
                            emptyBasketView.visibility = View.VISIBLE
                            checkOut.visibility = View.GONE
                            priceCount.visibility = View.GONE
                            showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                        }
                        is Resource.Error -> {
                            progressbar.visibility = View.GONE
                            basketLayout.visibility = View.VISIBLE
                            emptyBasketView.visibility = View.VISIBLE
                            checkOut.visibility = View.GONE
                            priceCount.visibility = View.GONE
                            resource.exception.message?.let { it1 -> showSnackbar(it1) }
                        }
                    }
                }
            })
        }

        profileViewModel.getOrderDetailsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        val list = resource.data.products
                        basketItemListAdapter.updateList(list as ArrayList<BasketItemObject>)
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbarWithMargin(it1) }
                    }
                }
            }
        })

        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        blockerView.show()
                    }
                    is Resource.Success -> {
//                        blockerView.hide()

                        when (updateAdapter) {
                            "sale" -> {
                                getFeedPostsPopularShoes()
                            }
                            "deal" -> {
                                getFeedPostsPopularShoes()
                            }
                            "clothes" -> {
                                getFeedPostsPopularShoes()
                            }
                            "shoes" -> {
                                recommendeditemadapter.update(
                                    resource.data.updatedPosition,
                                    resource.data.updatedStatus
                                )
                            }
                            "food" -> {
                                getFeedPostsPopularShoes()
                            }
                            "toys" -> {
                                getFeedPostsPopularShoes()
                            }
                            "view" -> {
                            }
                        }

                    }
                    is Resource.GenericError -> {
//                        blockerView.hide()
//                        showSnackbarWithMargin(
//                            resource.errorResponse.jsonResponse.getString(
//                                "error"
//                            )
//                        )
                    }
                    is Resource.Error -> {
//                        blockerView.hide()
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })
        checkOutViewModel.getCouponInfoResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {

                        if (resource.data.meta.coupon_status == "Купон применен") {
                            val price = resource.data.meta.basket_price ?: "0"
                            val totalPrice = resource.data.meta.total ?: "0"
                            val deliveryPrice = resource.data.meta.delivery_price ?: "0"
                            checkOutTotal.text = "${totalPrice} сум"
                            checkOutDeliveryFee.text = "${deliveryPrice} сум"
                            checkOutPrice.text = "${price} сум"
                            showSnackbarWithMargin(
                                "Купон применен",
                                Snackbar.LENGTH_LONG
                            )
                        } else if (resource.data.meta.coupon_status == "Купон не найден") {
                            showSnackbarWithMargin(
                                resources.getString(R.string.text_coupon_status),
                                Snackbar.LENGTH_LONG
                            )
                        } else {
                            showSnackbarWithMargin(resource.data.meta.coupon_status ?: "")
                        }
                    }
                    else -> {

                    }
                }
            }
        })


        checkOutBack.setOnClickListener {
            findNavController().popBackStack()
        }

        checkOutLogIn.setOnClickListener {
            findNavController().navigate(R.id.nav_home)
        }

        basketItemListAdapter.onItemClick = {
//            val bundle = Bundle()
//            bundle.putString("section_id", "")
//            bundle.putString("product_id", it.id)
//            findNavController().navigate(R.id.nav_selectedItem, bundle)
        }
    }

    private fun isCheckDiscount(list: ArrayList<BasketItemObject>): Boolean {
        Timber.d(PrefManager.getUserID(requireContext()))
        var isDiscount = false
        Timber.d("list params: ${list[0].params}")
        for (i in 0 until list.size) {
            if (list[i].params != null) {
                for (j in list[i].params!!.indices) {
                    Timber.d("params[] name: ${list[i].params!![j].name}")
                    if (list[i].params!![j].name == "Процент скидки") {
                        Timber.d("discount: ${list[i].params!![j].name}")
                        isDiscount = true
                        break
                    }
                }
            }
        }
        return isDiscount
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as MainActivity).toolbar.hide()
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).bottom_navigation.show()
    }

    override fun onStop() {
        super.onStop()
        (requireActivity() as MainActivity).toolbar.show()
    }

    fun addToFavs(product: ProductObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            showSnackbarAuth(resources.getString(R.string.text_need_auth))
        } else {
            if (product.fields.in_favourites != "Y") {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "add",
                    int,
                    true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "delete",
                    int,
                    false
                )
            }
        }
    }

    private fun getFeedPostsPopularShoes() {
        lifecycleScope.launch {
            homeViewModel.getHomeRecommendedData(
                PrefManager.getUserID(requireContext()), isHome = true
            ).collect { product ->
                recommendeditemadapter.submitData(product)

                return@collect
            }
        }
    }

    fun undoDelete(product: BasketItemObject) {
        checkOutViewModel.getAddToBasketResponse(
            PrefManager.getUserID(requireContext()),
            "basket",
            product.quantity,
            product.id,
            "add"
        )
    }

    fun deleteFromBasket(productId: String) {
        checkOutViewModel.getAddToBasketResponse(
            PrefManager.getUserID(requireContext()),
            "basket",
            "",
            productId,
            "delete"
        )
    }

    fun updateBasket(product: BasketItemObject, count: String) {
        checkOutViewModel.getAddToBasketResponse(
            PrefManager.getUserID(requireContext()),
            "basket",
            count,
            product.id,
            "update",
            product.color ?: "",
            product.razmer ?: ""
        )
        this.product = product
        this.count = count
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        when (requestCode) {
            1020 -> {
                // If request is cancelled, the result arrays are empty.
                if ((grantResults.isNotEmpty() && grantResults[0] == PackageManager.PERMISSION_GRANTED && grantResults[1] == PackageManager.PERMISSION_GRANTED)) {
                    // permission was granted, yay! Do the
                    // contacts-related task you need to do.
                    findNavController().navigate(R.id.action_nav_checkOut_to_nav_map, addressBundle)
                } else {
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.warning_enable_permission),
                        Toast.LENGTH_SHORT
                    ).show()
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return
            }

            // Add other 'when' lines to check for other
            // permissions this app might request.
            else -> {
                // Ignore all other requests.
            }
        }

    }

    override fun onSelect(couponCode: String) {
        promo_text_code.text = couponCode
        checkOutViewModel.getCouponInfo(
            PrefManager.getUserID(requireContext()),
            couponCode
        )
    }
}

class PromoBottomSheetFragment(val list: List<CouponData>, val listener: OnSelectCouponListener) :
    BottomSheetDialogFragment() {

    private lateinit var contentView: View
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private val itemAdapter = ItemAdapter<CouponData>()
    private val fastAdapter =
        FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<CouponData>() {
            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<CouponData>,
                item: CouponData
            ) {
                item.isSelect = true
                contentView.promo_code.setText(item.promoCode.toString())
                fastAdapter.notifyAdapterDataSetChanged()
            }

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView
            }
        })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(DialogFragment.STYLE_NORMAL, R.style.DialogStyle)
    }

    override fun setupDialog(dialog: Dialog, style: Int) {
        val contentView = View.inflate(context, R.layout.fragment_promo_bottomsheet, null)
        dialog.setContentView(contentView)
        (contentView.parent as View).setBackgroundColor(resources.getColor(android.R.color.transparent))
        this.contentView = contentView
        contentView.promoList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        contentView.promoList.adapter = fastAdapter

        itemAdapter.add(list)

        contentView.cancelButton.setOnClickListener {
            dialog.dismiss()
        }

        contentView.saveButton.setOnClickListener {
            if (contentView.promo_code.text.toString().isNotEmpty()) {
                listener.onSelect(contentView.promo_code.text.toString())
                dialog.dismiss()
            } else {
                contentView.promo_code.error = resources.getString(R.string.text_coupon_error)
            }
        }

    }

    companion object {
        fun newInstance(
            list: List<CouponData>,
            listener: OnSelectCouponListener
        ): PromoBottomSheetFragment {
            return PromoBottomSheetFragment(list, listener)
        }
    }
}

interface OnSelectCouponListener {
    fun onSelect(couponCode: String)
}

data class CouponModelInfo(
    val data: List<BasketItemObject>,
    val meta: BasketMetaObject
)

class CouponModel(
    val data: List<CouponFromServer>,
    val status: String?,
    val links: Links
)

data class Links(
    val self: String
)

data class CouponFromServer(
    val name: String?,
    val coupon: String?,
    val start: String?,
    val end: String?
)

data class CouponData(
    val id: Int,
    var isSelect: Boolean = false,
    var promoCode: String? = "",
    var promoName: String? = "",
    var date: String? = ""
) : AbstractItem<CouponData.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<CouponData>(itemView) {
        override fun bindView(item: CouponData, payloads: List<Any>) {
            itemView.selectedCoupon.isVisible = item.isSelect
            itemView.message.text = item.date
            itemView.shopName.text = item.promoName
        }

        override fun unbindView(item: CouponData) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_coupon_data
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}