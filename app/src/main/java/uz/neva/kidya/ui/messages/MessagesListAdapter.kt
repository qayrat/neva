package uz.neva.kidya.ui.messages

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.message_list_item.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.MessagesObject

class MessagesListAdapter (): PagingDataAdapter<MessagesObject,MessagesListAdapter.ViewHolder>(MESSSAGE){

    var messagesList:List<MessagesObject>? = null

//    fun updateList(messages: List<Message>) {
//        this.messagesList = messages
//        notifyDataSetChanged()
//    }

    var onItemClick: ((MessagesObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.message_list_item, parent, false)
    )

 //   override fun getItemCount()=messagesList?.size?:0


    override fun onBindViewHolder(holder: MessagesListAdapter.ViewHolder, position: Int) {
        holder.bindData(getItem(position) as MessagesObject)
        // holder.collapse.setOnClickListener(this)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val title=itemView.brandName
        val date=itemView.messageDate
        val content=itemView.messageContent

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(getItem(adapterPosition) as MessagesObject)
            }
        }

        fun bindData(messages: MessagesObject) {
            title.text=messages.suppiler
            date.text=messages.last_date
            content.text=messages.last_message

        }

    }

    companion object{
        val MESSSAGE=object : DiffUtil.ItemCallback<MessagesObject>(){
            override fun areItemsTheSame(
                oldItem: MessagesObject,
                newItem: MessagesObject
            ): Boolean = oldItem.id == newItem.id


            override fun areContentsTheSame(
                oldItem: MessagesObject,
                newItem: MessagesObject
            ): Boolean =
                oldItem == newItem

        }
    }
}