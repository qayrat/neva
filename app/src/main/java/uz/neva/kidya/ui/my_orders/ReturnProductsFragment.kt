package uz.neva.kidya.ui.my_orders

import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_return_products.*
import kotlinx.android.synthetic.main.fragment_return_products.returnProduct
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.network.dto.SelectedOrderDetails
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.changeUiStateEnabled
//import uz.usoft.kidya.utils.showSnackbar
//import uz.usoft.kidya.utils.showSnackbarWithMargin

class ReturnProductsFragment : Fragment() {

    private val profileViewModel: ProfileViewModel by viewModel()
    private lateinit var order:SelectedOrderDetails
    private lateinit var returnOrderItemListAdapter: ReturnOrderItemListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            order=it.getSerializable("selectedOrder") as SelectedOrderDetails
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_return_products, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        returnProductOrderNumber.text=order.data[0].id

        returnOrderItemListAdapter= ReturnOrderItemListAdapter(requireContext())

        returnOrderItemsRecycler.layoutManager=LinearLayoutManager(requireContext())
        returnOrderItemsRecycler.adapter=returnOrderItemListAdapter

        returnOrderItemListAdapter.updateList(order.products as ArrayList<BasketItemObject>)

        returnProductBack.setOnClickListener {
            findNavController().popBackStack()
        }

        profileViewModel.returnProductResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                            changeUiStateEnabled(true,progressIndicatorReturn,returnProduct)
                    }
                    is Resource.Success -> {
//                        showSnackbarWithMargin("Оформлен")
                        changeUiStateEnabled(false,progressIndicatorReturn,returnProduct)
                    }
                    is Resource.GenericError -> {

                        changeUiStateEnabled(false,progressIndicatorReturn,returnProduct)
//                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        changeUiStateEnabled(false,progressIndicatorReturn,returnProduct)
//                        resource.exception.message?.let { it1 -> showSnackbarWithMargin(it1) }
                    }
                }
            }
        })
        returnProduct.setOnClickListener {
            profileViewModel.getReturnProductResponse(PrefManager.getUserID(requireContext()),order.data[0].id,returnOrderItemListAdapter.getReturnProductList())
            Log.i("return", returnOrderItemListAdapter.getReturnProductList().toString())
        }


    }
}