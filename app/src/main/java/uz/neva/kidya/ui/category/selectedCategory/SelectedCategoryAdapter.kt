package uz.neva.kidya.ui.category.selectedCategory

import android.annotation.SuppressLint
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import kotlinx.android.synthetic.main.recommended_product_item.view.*
import kotlinx.android.synthetic.main.selected_category_item.view.*
import timber.log.Timber
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.utils.ClickEvents
import uz.neva.kidya.utils.show


class SelectedCategoryAdapter(val context: Context) :
    PagingDataAdapter<ProductObject, SelectedCategoryAdapter.ViewHolder>(
        PRODUCTS
    ) {
    var selectedCategory: List<ProductObject>? = null

    var onItemClick: ((ProductObject) -> Unit)? = null
    var click: ((ClickEvents) -> (Unit))? = null

    fun update(int: Int, fav: Boolean) {
        (getItem(int) as ProductObject).fields.in_favourites = if (fav) "Y" else "N"
        notifyItemChanged(int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.selected_category_item, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(getItem(position) as ProductObject)
        getItem(position)

    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.selectedCategoryItemImage
        private val itemItemName = itemView.selectedCategoryItemName

        //        private var itemRating=itemView.selectedCategoryItemRatingBar
        private val itemPrice = itemView.selectedCategoryItemPrice
        private val itemOldPrice = itemView.selectedCategoryItemOldPrice
        private val itemBrand = itemView.selectedCategoryItemBrand
        private val itemIsFav = itemView.isFavourite
        private val itemTag = itemView.tagback


        init {
            itemView.setOnClickListener {
                click?.invoke(ClickEvents.onItemClick((getItem(absoluteAdapterPosition) as ProductObject)))
            }
            itemView.isFavourite.setOnClickListener {
                click?.invoke(
                    ClickEvents.onAddToFav(
                        (getItem(absoluteAdapterPosition) as ProductObject),
                        absoluteAdapterPosition
                    )
                )
            }
        }

        @SuppressLint("SetTextI18n")
        fun bindData(selectedCategory: ProductObject) {
            if (selectedCategory.fields?.preview_picture != null && selectedCategory.fields.preview_picture != "")
                if (selectedCategory.fields.preview_picture[0] == '/') {
                    Glide.with(context).load(Const.BASE_URL + selectedCategory.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(250, 250)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
                } else {
                    Glide.with(context).load(selectedCategory.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(250, 250)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
                }
            else
                itemImageLink.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_broken_image_24))


//            itemRating.rating=selectedCategory.fields.rating?:0f
            itemTag.isVisible = selectedCategory.fields?.percent != null
            itemItemName.text = selectedCategory.fields?.name
            itemPrice.text =
                "${(selectedCategory.fields?.catalog_price_1 ?: "0").replace(",", " ")} сум"
            itemOldPrice.isVisible =
                selectedCategory.fields?.percent != null && selectedCategory.fields.percent != ""

            if (selectedCategory.fields?.percent != null) {
                itemOldPrice.text =
                    "${(selectedCategory.fields.old_price ?: "0").replace(",", " ")} сум"
                itemPrice.text =
                    "${(selectedCategory.fields.catalog_price_1) ?: "0".replace(",", " ")} сум"
                itemTag.text = "-${selectedCategory.fields.percent ?: ""}%"
            }

            for (item in selectedCategory.params) {

                when (item.name) {
                    "Поставщик" -> {

                        itemBrand.text = item.value[0]
                    }
                    "Цена" -> {
                        if (itemTag.visibility == View.GONE)
                            itemPrice.text = "${item.value[0].replace(",", " ")} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(context).load(item.value[0])
                                .thumbnail(0.2f)
                                .override(250, 250)
                                .placeholder(R.drawable.placeholder_neva)
                                .into(itemImageLink)
                    }

                }
            }


            if (selectedCategory.fields?.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_red_favourite)
            } else
                itemIsFav.setImageResource(R.drawable.ic_favourite)

        }
    }

    companion object {
        val PRODUCTS = object : DiffUtil.ItemCallback<ProductObject>() {
            override fun areItemsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean = oldItem.fields?.id == newItem.fields?.id

            override fun areContentsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean =
                oldItem == newItem

        }
    }
}

class RecommendedProductAdapter(val context: Context) :
    PagingDataAdapter<ProductObject, RecommendedProductAdapter.ViewHolder>(
        PRODUCTS
    ) {
    var rselectedCategory: List<ProductObject>? = null

    var ronItemClick: ((ProductObject) -> Unit)? = null
    var rclick: ((ClickEvents) -> (Unit))? = null

    fun update(int: Int, fav: Boolean) {
        (getItem(int) as ProductObject).fields?.in_favourites = if (fav) "Y" else "N"
        notifyItemChanged(int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context)
            .inflate(R.layout.recommended_product_item, parent, false)
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(getItem(position) as ProductObject)
        getItem(position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.rselectedCategoryItemImage
        private val itemItemName = itemView.rselectedCategoryItemName

        //        private var itemRating=itemView.selectedCategoryItemRatingBar
        private val itemPrice = itemView.rselectedCategoryItemPrice
        private val itemOldPrice = itemView.rselectedCategoryItemOldPrice
        private val itemBrand = itemView.rselectedCategoryItemBrand
        private val itemIsFav = itemView.risFavourite
        private val itemTag = itemView.rtagback


        init {
            itemView.setOnClickListener {
                rclick?.invoke(ClickEvents.onItemClick((getItem(absoluteAdapterPosition) as ProductObject)))
            }
            itemView.risFavourite.setOnClickListener {
                rclick?.invoke(
                    ClickEvents.onAddToFav(
                        (getItem(absoluteAdapterPosition) as ProductObject),
                        absoluteAdapterPosition
                    )
                )
            }
        }

        @SuppressLint("SetTextI18n")
        fun bindData(rselectedCategory: ProductObject) {
            Timber.d(rselectedCategory.fields?.id)
            if (rselectedCategory.fields?.preview_picture != null && rselectedCategory.fields.preview_picture != "")
                if (rselectedCategory.fields.preview_picture[0] == '/') {
                    Glide.with(context)
                        .load(Const.BASE_URL + rselectedCategory.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(250, 250)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
                } else {
                    Glide.with(context).load(rselectedCategory.fields.preview_picture)
                            .thumbnail(0.2f)
                            .override(250, 250)
                            .placeholder(R.drawable.placeholder_neva)
                            .into(itemImageLink)
                }
            else
                itemImageLink.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_broken_image_24))


//            itemRating.rating=selectedCategory.fields.rating?:0f
            itemTag.visibility = View.GONE
            itemItemName.text = rselectedCategory.fields?.name
            itemPrice.text =
                "${rselectedCategory.fields?.catalog_price_1 ?: "0".replace(",", " ")} сум"

            if (rselectedCategory.fields?.percent != null) {
                itemOldPrice.show()
                itemOldPrice.text =
                    "${rselectedCategory.fields.old_price ?: "0".replace(",", " ")} сум"
                itemPrice.text =
                    "${rselectedCategory.fields.catalog_price_1 ?: "".replace(",", " ")} сум"
                itemTag.visibility = View.VISIBLE
                itemTag.text = "-${rselectedCategory.fields.percent ?: ""}% off"
            }

            for (item in rselectedCategory.params) {

                when (item.name) {
                    "Поставщик" -> {

                        itemBrand.text = item.value[0]
                    }
                    "Цена" -> {
                        if (itemTag.visibility == View.GONE)
                            itemPrice.text = "${item.value[0].replace(",", " ")} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(context).load(item.value[0])
                                .thumbnail(0.2f)
                                .override(250, 250)
                                .placeholder(R.drawable.placeholder_neva)
                                .into(itemImageLink)
                    }

                }
            }


            if (rselectedCategory.fields?.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_favorited)
            } else
                itemIsFav.setImageResource(R.drawable.ic_favourite)

        }
    }

    companion object {
        val PRODUCTS = object : DiffUtil.ItemCallback<ProductObject>() {
            override fun areItemsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean = oldItem.fields?.id == newItem.fields?.id

            override fun areContentsTheSame(
                oldItem: ProductObject,
                newItem: ProductObject
            ): Boolean =
                oldItem == newItem

        }
    }
}