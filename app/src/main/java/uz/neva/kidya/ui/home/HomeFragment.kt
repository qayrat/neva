package uz.neva.kidya.ui.home

import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.graphics.drawable.Drawable
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.os.Handler
import android.provider.Settings
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions
import com.bumptech.glide.request.RequestListener
import com.bumptech.glide.request.target.Target
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.coupon_view.view.*
import kotlinx.android.synthetic.main.fragment_collection_object.*
import kotlinx.android.synthetic.main.fragment_landind_page.*
import kotlinx.android.synthetic.main.fragment_landind_page.indicator
import kotlinx.android.synthetic.main.fragment_selected_item.*
import kotlinx.android.synthetic.main.item_shop.view.*
import kotlinx.android.synthetic.main.item_sub_category.view.*
import kotlinx.android.synthetic.main.second_banner_fragment_collection_object.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import retrofit2.Call
import retrofit2.Response
import timber.log.Timber
import uz.neva.kidya.*
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.ContactService
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.*
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.DeviceData
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.network.dto.social.SocialResponse
import uz.neva.kidya.ui.adapter.SelectedCategoryAdapter2
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.selectedCategory.RecommendedProductAdapter
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.*

class LandindPage : Fragment() {

    val handler = Handler()
    lateinit var runnable: Runnable
    var popularListList: HashMap<String, Boolean> = HashMap()
    private val bannerList = ArrayList<BannerObject>()
    private var flashSaleList = ArrayList<FlashSaleProduct>()
    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private val homeViewModel: HomeViewModel by viewModel()
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private lateinit var imageCollectionAdapter: TopBannerAdapter
    private lateinit var viewPager: ViewPager2
    private lateinit var discountitemadapter: SelectedCategoryAdapter
    private lateinit var dealsitemadapter: SelectedCategoryAdapter2
    private lateinit var recommendeditemadapter: RecommendedProductAdapter
    private var homeHitsCount = ArrayList<ProductObject>()
    private var firstOpenApp = 0
    private var banner2Image = ""
    private lateinit var categoryitemadapter: PopularCategoryListAdapter
    private val updateList = ArrayList<CategoriesObject>()
    var updateAdapter = ""

    private val fleshSaleItemAdapter = ItemAdapter<FlashSaleProduct>()
    private val fleshSaleFastAdapter = FastAdapter.with(fleshSaleItemAdapter)
            .addEventHook(object : ClickEventHook<FlashSaleProduct>() {
                override fun onClick(
                        v: View,
                        position: Int,
                        fastAdapter: FastAdapter<FlashSaleProduct>,
                        item: FlashSaleProduct
                ) {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", item.fields.iblock_section_id)
                    bundle.putString("product_id", item.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }

                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                    return viewHolder.itemView
                }
            })
    private val itemAdapter = ItemAdapter<ShopInfoObject>()
    private val fastAdapter =
            FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<ShopInfoObject>() {
                override fun onClick(
                        v: View,
                        position: Int,
                        fastAdapter: FastAdapter<ShopInfoObject>,
                        item: ShopInfoObject
                ) {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", item.id.toString())
                    bundle.putString("section_name", item.title)
                    findNavController().navigate(R.id.selectedShopFragment, bundle)
                }

                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                    return viewHolder.itemView
                }

            })

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment

        return inflater.inflate(R.layout.fragment_landind_page, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        homeViewModel.getBanners()
        categoryitemadapter = PopularCategoryListAdapter(requireContext())
        imageCollectionAdapter = TopBannerAdapter(this)
        getFeedPostsDeals()
        getFeedPostsPopularShoes()
        categoriesViewModel.getPopularCategoriesResponse(width = 120, height = 120)
        homeViewModel.getShopInfo(width = 288, height = 224)
        homeViewModel.getHomeDealsCount(PrefManager.getUserID(requireContext()))
        getFeedPostsDiscount()
        homeViewModel.getHomeBanner()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bottomNavigationView: BottomNavigationView = requireActivity().findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.action_home

        categoriesViewModel.getFlashSaleProduct()

        viewPager = view.findViewById<ViewPager2>(R.id.pager)

        if (banner2Image != "") {
            magic_friday_banner.visibility = View.VISIBLE
            loadImage(banner2Image)
        } else {
            magic_friday_banner.visibility = View.GONE
            discounts.text = getString(R.string.text_discount)
        }
        homeViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Home")
        ))

        if (PrefManager.getIsInstallApp(requireContext()) == 1) {
            createAlertDialog()
//            Timber.d("getRadioVersion: ${android.os.Build.DEVICE}")
//            Timber.d("SECURITY_PATCH: ${android.os.Build.VERSION.RELEASE}")
            val id = Settings.Secure.getString(requireContext().contentResolver, Settings.Secure.ANDROID_ID)
//            val deviceName = "${android.os.Build.BRAND} ${android.os.Build.DEVICE}" +
//                    " ${android.os.Build.MODEL} Android ${android.os.Build.VERSION.RELEASE}" +
//                    " deviceID=${id}"
            val deviceName = "Android ${android.os.Build.VERSION.RELEASE}"
            val deviceData = AnalyticsModel(
                    entity = "analytics",
                    method = "setEvent",
                    data = DeviceData(type = "device", name = deviceName)
            )
            homeViewModel.putAnalytics(deviceData)
            PrefManager.isInstallApp(requireContext(), 2)
        }

        var imglist: MutableList<String> = mutableListOf()
        var secondImgList: MutableList<String> = mutableListOf()
        topLay.setOnClickListener {
            hideKeyboard()
        }


        ll_see_more2.setOnClickListener {
            findNavController().navigate(R.id.action_nav_home_to_nav_category)
        }

        ll_best_seller_see_more.setOnClickListener {
            findNavController().navigate(R.id.action_nav_home_to_nav_deals)
        }

        ll_discount_see_more.setOnClickListener {
            findNavController().navigate(R.id.action_nav_home_to_nav_discount)
        }

        ll_recommended_see_more.setOnClickListener {
            val action = LandindPageDirections.actionNavHomeToNavRecommended()
            findNavController().navigate(action)
        }

        magic_friday_banner.setOnClickListener {
            findNavController().navigate(R.id.action_nav_home_to_nav_discount)
        }

        retry_btn.setOnClickListener {
            home_page.visibility = View.GONE
            error_message.visibility = View.GONE
            progressbar.visibility = View.VISIBLE
            getFeedPostsDeals()
            getFeedPostsPopularShoes()
            categoriesViewModel.getFlashSaleProduct()
            categoriesViewModel.getPopularCategoriesResponse(width = 120, height = 120)
            homeViewModel.getShopInfo(width = 288, height = 224)
            homeViewModel.getBanners()
            homeViewModel.getHomeDealsCount(PrefManager.getUserID(requireContext()))
            getFeedPostsDiscount()
            homeViewModel.getHomeBanner()
            recommendedRecyclerView.adapter = recommendeditemadapter
            discountsRecyclerView.adapter = discountitemadapter
            viewPager.adapter = imageCollectionAdapter
        }

        reverseInt()

        flash_sale_list.adapter = fleshSaleFastAdapter
        dealsRecyclerView.adapter = dealsitemadapter
        recommendedRecyclerView.adapter = recommendeditemadapter
        discountsRecyclerView.adapter = discountitemadapter
        popularcategoryRecyclerView.adapter = categoryitemadapter
        viewPager.adapter = imageCollectionAdapter

        indicator.setupWithViewPager(viewPager)
        indicator.setSliderColor(
                Color.parseColor("#FFCDE9FB"),
                ContextCompat.getColor(requireContext(), R.color.menu_selected_color)
        )

        for (item in popularListList) {
            val title = view.findViewWithTag<TextView>(item.key)
            val recycler = view.findViewWithTag<RecyclerView>("${item.key}Recycler")
            val arrow = view.findViewWithTag<ImageView>("${item.key}Arrow")
            if (item.value) {
                title?.show()
                recycler?.show()
                arrow?.show()
                getFeedPostsDealsType(item.key)
            } else {
                title?.hide()
                recycler?.hide()
                arrow?.hide()
            }
        }

        categoriesViewModel.fleshSaleProductResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        flashSaleList = resource.data.data as ArrayList<FlashSaleProduct>
                        flash_sale_container.isVisible = flashSaleList.isNotEmpty()
                        fleshSaleItemAdapter.apply {
                            clear()
                            add(flashSaleList)
                        }
                        val time = resource.data.diff
                        if (time != null) {
                            val millisecond =
                                    ((time.hour * 60 + time.minute) * 60 + time.second) * 1000
                            countDown.start(millisecond.toLong())

                        }
                    }
                    else -> {

                    }
                }

            }
        })

        categoriesViewModel.popularCategoriesResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        changeUiStateEnabled(
                                true,
                                progressIndicatorCategory,
                                progressIndicatorCategory
                        )
                    }
                    is Resource.Success -> {
                        progressbar.visibility = View.GONE
                        error_message.visibility = View.GONE
                        home_page.visibility = View.VISIBLE
                        changeUiStateEnabled(
                                false,
                                progressIndicatorCategory,
                                progressIndicatorCategory
                        )
                        updateList.clear()
                        for (i in 0 until 6) {
                            updateList.add(resource.data.data[i])
                        }
                        categoryitemadapter.updateList(updateList)
                        categoryitemadapter.notifyDataSetChanged()

                    }
                    is Resource.GenericError -> {
                        changeUiStateEnabled(
                                false,
                                progressIndicatorCategory,
                                progressIndicatorCategory
                        )
                    }
                    is Resource.Error -> {
                        progressbar.visibility = View.GONE
                        home_page.visibility = View.GONE
                        error_message.visibility = View.VISIBLE
                        changeUiStateEnabled(
                                false,
                                progressIndicatorCategory,
                                progressIndicatorCategory
                        )
                    }
                }
            }
        })


        runnable = object : Runnable {
            override fun run() {
                if (viewPager.currentItem + 1 != imageCollectionAdapter.itemCount) {
                    viewPager.setCurrentItem(viewPager.currentItem + 1, true)
                } else {
                    viewPager.setCurrentItem(0, true)
                }
                handler.postDelayed(this, 5000)
            }
        }
        handler.postDelayed(runnable, 5000)


        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()

        discountsRecyclerView.layoutManager =
                GridLayoutManager(requireContext(), 2)
        discountsRecyclerView.addItemDecoration(SpacesItemDecoration(marginpixels, true, 2))

        flash_sale_list.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        dealsRecyclerView.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        val mLayoutManager = GridLayoutManager(requireContext(), 2)
        recommendedRecyclerView.layoutManager = mLayoutManager
        recommendedRecyclerView.addItemDecoration(SpacesItemDecoration(marginpixels, true, 2))


        newsRecyclerView.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        newsRecyclerView.adapter = fastAdapter


        secondindicator.attachToRecyclerView(newsRecyclerView)

        popularcategoryRecyclerView.addItemDecoration(SpacesItemDecoration(0, true, 3))
        popularcategoryRecyclerView.layoutManager =
                GridLayoutManager(requireContext(), 3)

        viewHistoryRecyclerView.layoutManager =
                LinearLayoutManager(requireContext())

        homeViewModel.getShopResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        itemAdapter.clear()
                        itemAdapter.add(resource.data)
                    }
                    is Resource.Error -> {

                    }
                    is Resource.GenericError -> {

                    }
                }
            }
        })

        homeViewModel.getBannersResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        shimmer_view_container.stopShimmer()
                        shimmer_view_container.visibility = View.GONE
                        topLay.visibility = View.VISIBLE

                        imglist.clear()
                        secondImgList.clear()
                        val list = resource.data.data as ArrayList<BannerObject>

                        imageCollectionAdapter.updateImageList(list)
                        bannerList.addAll(list)
                        indicator.setupWithViewPager(viewPager)
                        indicator.setSliderColor(
                                Color.parseColor("#FFCDE9FB"),
                                ContextCompat.getColor(requireContext(), R.color.menu_selected_color)
                        )

                    }
                    is Resource.GenericError -> {
                    }
                    is Resource.Error -> {
                        Timber.d(resource.exception.localizedMessage)
                    }

                }

            }
        })

        if (flashSaleList.isEmpty()) {
            flash_sale_container.visibility = View.GONE
        }

        if (bannerList.isNotEmpty()) {
            shimmer_view_container.stopShimmer()
            shimmer_view_container.visibility = View.GONE
            topLay.visibility = View.VISIBLE
            progressbar.visibility = View.GONE
            home_page.visibility = View.VISIBLE
            error_message.visibility = View.GONE
        } else {
            progressbar.visibility = View.GONE
            home_page.visibility = View.GONE
            error_message.visibility = View.VISIBLE
        }

        if (updateList.isNotEmpty()) {
            shimmer_view_container.stopShimmer()
            shimmer_view_container.visibility = View.GONE
            topLay.visibility = View.VISIBLE
            progressbar.visibility = View.GONE
            home_page.visibility = View.VISIBLE
            error_message.visibility = View.GONE
        } else {
            progressbar.visibility = View.GONE
            home_page.visibility = View.GONE
            error_message.visibility = View.VISIBLE
        }

        if (firstOpenApp == 0) {
            progressbar.visibility = View.VISIBLE
            home_page.visibility = View.GONE
            error_message.visibility = View.GONE
        }
        firstOpenApp = 1

        if (homeHitsCount.isEmpty()) {
            ll_bestseller.visibility = View.GONE
            divide_hol_magic_friday.visibility = View.GONE
        } else {
            ll_bestseller.visibility = View.VISIBLE
            divide_hol_magic_friday.visibility = View.VISIBLE
        }

        discountitemadapter.click = {

            when (it) {
                is ClickEvents.onAddToFav -> {
                    updateAdapter = "sale"
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {
                }
            }
        }



        dealsitemadapter.click = {

            when (it) {
                is ClickEvents.onAddToFav -> {

                    updateAdapter = "deal"
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {

                }
            }
        }


        recommendeditemadapter.rclick = {

            when (it) {
                is ClickEvents.onAddToFav -> {

                    updateAdapter = "shoes"
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {
                }
            }
        }

        categoryitemadapter?.onItemClick = {
            if (!it.has_sub) {
                Log.i("logddddddddd", "you shouldn be here")
                val bundle: Bundle = Bundle()
                bundle.putString("section_id", it.id)
                bundle.putString("section_name", it.name)
                findNavController().navigate(R.id.nav_selectedCategoryFragment, bundle)
            } else {
                val bundle: Bundle = Bundle()
                bundle.putString("section_id", it.id)
                bundle.putString("section_name", it.name)
                findNavController().navigate(R.id.nav_subCategory, bundle)
            }
        }


        if (PrefManager.getUserID(requireContext()).isNotEmpty()) {
            homeViewModel.getBasketResponse(PrefManager.getUserID(requireContext()))
        }

        homeViewModel.getBasketResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        (requireActivity() as MainActivity).updateBadge(resource.data.data.size)
                    }
                    is Resource.GenericError -> {

                    }
                    is Resource.Error -> {

                    }
                }
            }
        })

        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        blockerView.show()
                    }
                    is Resource.Success -> {
                        blockerView.hide()

                        when (updateAdapter) {
                            "sale" -> {
                                discountitemadapter.update(
                                        resource.data.updatedPosition,
                                        resource.data.updatedStatus
                                )
                                getFeedPostsDeals()
                                getFeedPostsPopularShoes()
                            }
                            "deal" -> {
                                dealsitemadapter.update(
                                        resource.data.updatedPosition,
                                        resource.data.updatedStatus
                                )
                                getFeedPostsDiscount()
                                getFeedPostsPopularShoes()
                            }
                            "clothes" -> {
                                getFeedPostsDeals()
                                getFeedPostsDiscount()
                                getFeedPostsPopularShoes()
                            }
                            "shoes" -> {
                                recommendeditemadapter.update(
                                        resource.data.updatedPosition,
                                        resource.data.updatedStatus
                                )
                                getFeedPostsDeals()
                                getFeedPostsDiscount()
                            }
                            "food" -> {
                                getFeedPostsDeals()
                                getFeedPostsDiscount()
                                getFeedPostsPopularShoes()
                            }
                            "toys" -> {
                                getFeedPostsDeals()
                                getFeedPostsDiscount()
                                getFeedPostsPopularShoes()
                            }
                            "view" -> {
                                getFeedPostsDeals()
                                getFeedPostsDiscount()
                            }
                        }

                    }
                    is Resource.GenericError -> {
                        blockerView.hide()
                    }
                    is Resource.Error -> {
                        blockerView.hide()
                    }
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {
            discountitemadapter.loadStateFlow.collectLatest { loadStates ->
                if (loadStates.refresh is LoadState.Loading)
                    progressIndicatorDiscounts.show()
                else
                    progressIndicatorDiscounts.hide()
            }
            dealsitemadapter.loadStateFlow.collectLatest { loadStates ->
                if (loadStates.refresh is LoadState.Loading)
                    progressIndicatorDeals.show()
                else
                    progressIndicatorDeals.hide()
            }

        }

        val response = ContactService.create().getContacts()
        response.enqueue(object : retrofit2.Callback<SocialResponse> {
            override fun onResponse(
                    call: Call<SocialResponse>,
                    response: Response<SocialResponse>
            ) {
                val data = response.body()!!.socialNetworks
                var telegram = ""
                if (response.isSuccessful) {
                    for (i in 0 until data.size) {
                        if (data[i].name == "Telegram") {
                            telegram = data[i].nickname
                        }
                    }
                    val phone = response.body()!!.phone
                    PrefManager.savePhoneSupport(requireContext(), phone)
                    PrefManager.saveTelegramLink(requireContext(), telegram)
                }
            }

            override fun onFailure(call: Call<SocialResponse>, t: Throwable) {
                Timber.d(t.localizedMessage)
            }
        })

        homeViewModel.homeBanner2.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        banner2Image = Const.BASE_URL + resource.data.data.image
                        loadImage(banner2Image)
                    }
                    is Resource.Error -> {
                        banner2Image = ""
                        magic_friday_banner.visibility = View.GONE
                        discounts.text = getString(R.string.text_discount)
                    }
                    else -> {
                    }
                }
            }
        })

        homeViewModel.homeHitsCount.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                        homeHitsCount = resource.data.data as ArrayList<ProductObject>
                        if (resource.data.data.isEmpty()) {
                            ll_bestseller.visibility = View.GONE
                            divide_hol_magic_friday.visibility = View.GONE
                        } else {
                            ll_bestseller.visibility = View.VISIBLE
                            divide_hol_magic_friday.visibility = View.VISIBLE
                        }
                    }
                    is Resource.Error -> {
                    }
                    else -> {
                    }
                }
            }
        })

        homeViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> { }
                    is Resource.GenericError -> { }
                    is Resource.Error -> { }
                }
            }
        })
    }

    private fun loadImage(imageUrl: String) {
        Glide.with(requireContext()).load(imageUrl)
                .listener(object : RequestListener<Drawable> {
                    override fun onLoadFailed(
                            e: GlideException?,
                            model: Any?,
                            target: Target<Drawable>?,
                            isFirstResource: Boolean
                    ): Boolean {
                        magic_friday_banner.visibility = View.GONE
                        discounts.text = getString(R.string.text_discount)
                        return false
                    }

                    override fun onResourceReady(
                            resource: Drawable?,
                            model: Any?,
                            target: Target<Drawable>?,
                            dataSource: DataSource?,
                            isFirstResource: Boolean
                    ): Boolean {
                        return false
                    }
                })
                .into(magic_friday_banner)
    }


    private fun createAlertDialog() {
        val builder = AlertDialog.Builder(requireContext())
        val view = layoutInflater.inflate(R.layout.coupon_view, null)
        builder.setView(view)

        val dialog = builder.create()
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        view.navigate_to_login.setOnClickListener {
            dialog.dismiss()
            findNavController().navigate(R.id.nav_login)
        }
        dialog.show()
    }

    private fun reverseInt() {
        var num = 1534236468
        var reversed = 0

        while (num != 0) {
            val digit = num % 10
            reversed = reversed * 10 + digit
            num /= 10
        }

        println("Char reverse: $reversed")
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).bottom_navigation.show()
        shimmer_view_container.startShimmer()
    }

    override fun onPause() {
        super.onPause()
        shimmer_view_container.stopShimmer()
        shimmer_view_container.visibility = View.GONE
        topLay.visibility = View.VISIBLE
        imageCollectionAdapter = TopBannerAdapter(this)
        imageCollectionAdapter.updateImageList(bannerList)
        handler.removeCallbacks(runnable)

    }

    companion object {
        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                return Html.fromHtml(source)
            }
        }

    }

    private fun getFeedPostsDiscount() {
        lifecycleScope.launch {
            categoriesViewModel.getSaleProductsResponse(
                    PrefManager.getUserID(requireContext()), isHome = true
            ).collect { product ->

                discountitemadapter = SelectedCategoryAdapter(requireContext())
                discountitemadapter.submitData(product)

                return@collect
            }
        }

    }


    private fun getFeedPostsDeals() {
        lifecycleScope.launch {
            try {
                homeViewModel.getHomePageData(
                        PrefManager.getUserID(requireContext()), true
                ).collect { product ->
                    dealsitemadapter = SelectedCategoryAdapter2(requireContext())
                    dealsitemadapter.submitData(product)

                    return@collect
                }
            } catch (e: Exception) {

            }

        }
    }

    private fun getFeedPostsDealsType(type: String) {
        lifecycleScope.launch {
            categoriesViewModel.getDealsProductsResponse(
                    PrefManager.getUserID(requireContext()),
                    type = type,
                    isHome = true
            ).collect { product ->
                when (type) {
                    "hit" -> {

                    }
                    "top_clothes" -> {

                    }
                    "top_shoes" -> {
                        recommendeditemadapter.submitData(product)
                    }
                    "top_food" -> {

                    }
                    "top_toy" -> {

                    }
                }

                return@collect
            }
        }
    }

    private fun getFeedPostsPopularShoes() {
        lifecycleScope.launch {
            homeViewModel.getHomeRecommendedData(
                    PrefManager.getUserID(requireContext()), isHome = true
            ).collect { product ->

                recommendeditemadapter = RecommendedProductAdapter(requireContext())
                recommendeditemadapter.submitData(product)

                return@collect
            }
        }
    }

    fun addToFavs(product: ProductObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            showSnackbarAuth(resources.getString(R.string.text_need_auth))
        } else {
            if (product.fields.in_favourites != "Y") {
                checkOutViewModel.getAddToFavsResponse(
                        PrefManager.getUserID(requireContext()),
                        "favs",
                        product.fields.id,
                        "add",
                        int,
                        true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                        PrefManager.getUserID(requireContext()),
                        "favs",
                        product.fields.id,
                        "delete",
                        int,
                        false
                )
            }
        }
    }

}

class TopBannerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    var fragmentImageList = ArrayList<Fragment>()

    fun updateImageList(bannerList: ArrayList<BannerObject>) {
        bannerList.forEach {
            if (it.type == "Верхний ")
                fragmentImageList.add(TopBannerObjectFragment.newInstance(it))

        }
        notifyDataSetChanged()
    }

    override fun getItemCount(): Int = fragmentImageList.size

    override fun createFragment(position: Int): Fragment {
        // Return a NEW fragment instance in createFragment(int)
        return fragmentImageList[position]
    }


}

// Instances of this class are fragments representing a single
// object in our collection.
class TopBannerObjectFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.fragment_collection_object, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var banner: BannerObject? = null

        arguments?.let {
            banner = it.getSerializable("banner") as BannerObject
            Glide.with(requireContext()).load("${Const.BASE_URL}${banner?.preview_picture}")
                    .thumbnail(0.2f)
                    .placeholder(R.drawable.placeholder_neva)
                    .into(imageView3)
        }
        imageView3.setOnClickListener {
            when (banner?.entity) {
                "product" -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", banner?.entity_id)
                    bundle.putString("product_id", banner?.entity_id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                "category" -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", banner?.section_id)
                    bundle.putString("section_name", banner?.name)
                    findNavController().navigate(R.id.nav_subCategory, bundle)

                }
                "subcategory" -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", banner?.entity_id)
                    bundle.putString("section_name", banner?.name)
                    findNavController().navigate(R.id.subCategoryProductFragment, bundle)
                }
                "shop" -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", banner?.entity_id)
                    bundle.putString("section_name", banner?.name)
                    findNavController().navigate(R.id.selectedShopFragment, bundle)
                }
                else -> {
                    if (banner != null) {
                        if (banner!!.link_to != null) {
                            if (banner!!.link_to!!.isNotEmpty()) {
                                val intent = Intent(Intent.ACTION_VIEW, Uri.parse(banner?.link_to))
                                startActivity(intent)
                            }
                        }
                    }
                }
            }
        }
    }

    companion object {
        fun newInstance(banner: BannerObject): TopBannerObjectFragment {
            val fragment = TopBannerObjectFragment()
            fragment.arguments = Bundle().apply {
                // Our object is just an integer :-P
                putSerializable("banner", banner)

            }
            return fragment
        }
    }
}


class SecondBannerAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    var fragmentImageList = ArrayList<Fragment>()

    fun updateImageList(bannerList: ArrayList<BannerObject>) {
        bannerList.forEach {
            if (it.type == "Нижний ")
                fragmentImageList.add(SecondBannerObjectFragment.newInstance(it))
        }
        notifyDataSetChanged()
    }


    override fun getItemCount(): Int = fragmentImageList.size

    override fun createFragment(position: Int): Fragment {
        // Return a NEW fragment instance in createFragment(int)
        return fragmentImageList[position]
    }
}

class SecondBannerObjectFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.second_banner_fragment_collection_object, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        var banner: BannerObject? = null

        arguments?.let {
            banner = it.getSerializable("banner") as BannerObject
            Glide.with(requireContext()).load("${Const.BASE_URL}${banner?.preview_picture}")
                    .into(secondBanner)
        }
        secondBanner.setOnClickListener {

        }
    }

    companion object {
        fun newInstance(banner: BannerObject): SecondBannerObjectFragment {
            val fragment = SecondBannerObjectFragment()
            fragment.arguments = Bundle().apply {
                // Our object is just an integer :-P
                putSerializable("banner", banner)
            }
            return fragment
        }
    }
}

class secondImageCollectionAdapter(fragment: Fragment) : FragmentStateAdapter(fragment) {

    var fragmentImageList = ArrayList<Fragment>()

    fun updateImageList(imageList: ArrayList<String>) {
        fragmentImageList.clear()
        imageList.forEach {
            fragmentImageList.add(secondImageObjectFragment.newInstance(it))
        }
        this.notifyDataSetChanged()
    }

    override fun getItemCount(): Int = fragmentImageList.size

    override fun createFragment(position: Int): Fragment {
        // Return a NEW fragment instance in createFragment(int)
        return fragmentImageList[position]
    }
}

private const val ARG_IMG_LINK = "imageLink"


class secondImageObjectFragment : Fragment() {
    override fun onCreateView(
            inflater: LayoutInflater,
            container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View {
        return inflater.inflate(R.layout.second_fragment_collection_object, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        arguments?.let {
            var imageLink = it.getString(ARG_IMG_LINK)
            Glide.with(requireContext()).load(imageLink).override(
                    1000,
                    1000
            ).placeholder(R.drawable.ic_baseline_cloud_download_24).into(imageView3)

        }
    }

    companion object {
        fun newInstance(imageLink: String): secondImageObjectFragment {
            val fragment = secondImageObjectFragment()
            fragment.arguments = Bundle().apply {
                // Our object is just an integer :-P
                putString(ARG_IMG_LINK, imageLink)
            }
            return fragment
        }
    }
}

data class SelectedShop(
        var name: Any?,
        var data: List<ProductObject3>,
        var catalogs: List<Catalogs>? = null,
        var pagination: PaginationObject
)

data class Catalogs(
        var id: Int,
        var name: String?,
        var isSelect: Boolean = false
) : AbstractItem<Catalogs.CatalogsVH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class CatalogsVH(itemView: View) : FastAdapter.ViewHolder<Catalogs>(itemView) {
        override fun bindView(item: Catalogs, payloads: List<Any>) {
            itemView.subCategoryName.text = item.name
        }

        override fun unbindView(item: Catalogs) {
        }

    }

    override val layoutRes: Int
        get() = R.layout.item_sub_category
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): CatalogsVH = CatalogsVH(v)
}

data class ShopInfoObject(
        val id: Int,
        val title: String? = "",
        val photo: String? = null
) : AbstractItem<ShopInfoObject.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<ShopInfoObject>(itemView) {
        override fun bindView(item: ShopInfoObject, payloads: List<Any>) {
            itemView.textView27.text = item.title
            Glide.with(itemView.context)
                    .load(Const.BASE_URL + item.photo)
                    .thumbnail(0.2f)
                    .override(250, 250)
                    .placeholder(R.drawable.placeholder_neva)
                    .transition(DrawableTransitionOptions.withCrossFade())
                    .into(itemView.imageView)
        }

        override fun unbindView(item: ShopInfoObject) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_shop
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}
