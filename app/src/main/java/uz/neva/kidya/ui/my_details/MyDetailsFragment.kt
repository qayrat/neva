package uz.neva.kidya.ui.my_details

import android.app.DatePickerDialog
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import kotlinx.android.synthetic.main.fragment_my_details.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.MockData
import uz.neva.kidya.utils.*
import java.util.*

var count = 0
private val c: Calendar = Calendar.getInstance()
val year = c.get(Calendar.YEAR)
val month = c.get(Calendar.MONTH)
val day = c.get(Calendar.DAY_OF_MONTH)

class MyDetailsFragment : Fragment(), AddChildDialog.NoticeDialogListener {

    private val profileViewModel: ProfileViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_details, container, false)

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        citySpinner.isEnabled = false
        districtSpinner.isEnabled = false
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.gender_array,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
            genderspinner.adapter = adapter
        }

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.district,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
            districtSpinner.adapter = adapter
        }

        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.city,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
            citySpinner.adapter = adapter
        }

        addChildButton.setOnClickListener {
            val newDialog = AddChildDialog(this)
            val fm = requireActivity().supportFragmentManager
            newDialog.show(fm, "success")
            newDialog.isCancelable = true
            // fragmentManager?.let { it1 -> newDialog.show(it1,"addChild") }
        }

        val dpd = DatePickerDialog(
            requireContext(), R.style.datePicker,
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->

                var monthName = ""
                // Display Selected date in textbox
                when (monthOfYear) {
                    1 -> monthName = "Января"
                    2 -> monthName = "Февраля"
                    3 -> monthName = "Марта"
                    4 -> monthName = "Апреля"
                    5 -> monthName = "Майа"
                    6 -> monthName = "Июня"
                    7 -> monthName = "Июля"
                    8 -> monthName = "Августа"
                    9 -> monthName = "Сентября"
                    10 -> monthName = "Октября"
                    11 -> monthName = "Ноября"
                    12 -> monthName = "Декабря"
                }
                DOBNumber.text = "$dayOfMonth $monthName $year"

            }, year, month, day
        )
        DOBNumber.setOnClickListener {
            dpd.show()
        }

        phoneNumber.formatPhoneMask()
        myDetailsBack.setOnClickListener {
            findNavController().popBackStack()
            findNavController().navigate(R.id.nav_home)
        }

        profileViewModel.getProfile(PrefManager.getUserID(requireContext()))
        profileViewModel.getProfileResponse.observe(
            viewLifecycleOwner,
            androidx.lifecycle.Observer {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {
                        is Resource.Loading -> {

                        }
                        is Resource.Success -> {

                            myDetailsName.setText(resource.data.data.name)
                            myDetailsSurname.setText(resource.data.data.last_name)

                            PrefManager.saveName(
                                requireContext(),
                                resource.data.data.name + " " + resource.data.data.last_name
                            )
                            PrefManager.savePhone(requireContext(), "+" + resource.data.data.phone)
                            phoneNumber.setText(resource.data.data.phone)
                            PrefManager.saveCity(requireContext(), resource.data.data.address ?: "")
                            PrefManager.saveDistrict(
                                requireContext(),
                                resource.data.data.district ?: ""
                            )
                            PrefManager.saveStreet(
                                requireContext(),
                                resource.data.data.street ?: ""
                            )
                            addressStreet.setText(resource.data.data.street)
                            if (resource.data.data.district != null) {
                                if (resource.data.data.district.isNotBlank()) {
                                    val region = resources.getStringArray(R.array.district)
                                    districtSpinner.setSelection(region.indexOf(resource.data.data.district))
                                }
                            }
                            if (resource.data.data.address!=null){
                                if (resource.data.data.address.isNotBlank()) {
                                    val city = resources.getStringArray(R.array.city)
                                    citySpinner.setSelection(city.indexOf(resource.data.data.address))
                                }
                            }
                        }
                        is Resource.GenericError -> {
//                      showSnackbarWithMargin(
//                          resource.errorResponse.jsonResponse.getString(
//                              "error"
//                          )
//                      )
                        }
                        is Resource.Error -> {
//                       resource.exception.message?.let { it1 -> showSnackbar(it1) }
                        }

                    }
                }
            })

//        edit.setOnClickListener {
//            when (edit.text) {
//                "Изменить" -> {
//                    edit.text = "Сохранить"
//                    blockerViewTop.hide()
//                    blockerViewBottom.hide()
//                    myDetailsName.isEnabled = true
//                    myDetailsSurname.isEnabled = true
//                    districtSpinner.isEnabled = true
//                    citySpinner.isEnabled = true
//                    phoneNumber.isEnabled = true
//                    addressStreet.isEnabled = true
//                }
//                "Сохранить" -> {
//
//                    blockerViewTop.show()
//                    blockerViewBottom.show()
//                    edit.text = "Изменить"
//                    myDetailsName.isEnabled = false
//                    myDetailsSurname.isEnabled = false
//                    districtSpinner.isEnabled = false
//                    citySpinner.isEnabled = false
//                    phoneNumber.isEnabled = false
//                    addressStreet.isEnabled = false
//                    profileViewModel.updateProfile(
//                        PrefManager.getUserID(requireContext()),
//                        myDetailsName.text.toString(),
//                        myDetailsSurname.text.toString(),
//                        phoneNumber.getMaskedPhoneWithoutSpace(),
//                        citySpinner.selectedItem.toString(),
//                        districtSpinner.selectedItem.toString(),
//                        addressStreet.text.toString()
//                    )
//
//                    profileViewModel.updateProfileResponse.observe(
//                        viewLifecycleOwner,
//                        androidx.lifecycle.Observer {
//                            it.getContentIfNotHandled()?.let { resource ->
//                                when (resource) {
//                                    is Resource.Loading -> {
//                                        changeUiStateEnabled(true, progressIndicatorProfile, edit)
//                                    }
//                                    is Resource.Success -> {
//
//                                        changeUiStateEnabled(false, progressIndicatorProfile, edit)
//                                        PrefManager.saveName(
//                                            requireContext(),
//                                            resource.data.data.name + " " + resource.data.data.last_name
//                                        )
//                                        PrefManager.savePhone(
//                                            requireContext(),
//                                            "+" + resource.data.data.phone
//                                        )
//                                        PrefManager.saveCity(
//                                            requireContext(),
//                                            resource.data.data.address?:""
//                                        )
//                                        PrefManager.saveDistrict(
//                                            requireContext(),
//                                            resource.data.data.district?:""
//                                        )
//                                        PrefManager.saveStreet(
//                                            requireContext(),
//                                            resource.data.data.street?:""
//                                        )
//
//
//                                    }
//                                    is Resource.GenericError -> {
//
//                                        changeUiStateEnabled(false, progressIndicatorProfile, edit)
////                                    showSnackbarWithMargin(
////                                        resource.errorResponse.jsonResponse.getString(
////                                            "error"
////                                        )
////                                    )
//                                    }
//                                    is Resource.Error -> {
//
//                                        changeUiStateEnabled(false, progressIndicatorProfile, edit)
//                                    }
//
//                                }
//                            }
//                        })
//                }
//            }
//        }

//        addchild("Руслан", "Мальчик", Date(2010, 9, 9))
//        addchild("Рушана", "Девочка", Date(2014, 9, 9))
//        addchild("Мия", "Девочка", Date(2019, 9, 9))
    }


    @RequiresApi(Build.VERSION_CODES.O)
    fun addchild(name: String, sex: String, dob: Date) {
        val scale = resources.displayMetrics.density
        val paddingpixels = (4 * scale + 0.5f).toInt()
        val marginpixels = (16 * scale + 0.5f).toInt()

        val paramsForLayout = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.MATCH_PARENT,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            1.0f
        )

        val params = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val paramsForRemove = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val paramsForAge = LinearLayout.LayoutParams(
            LinearLayout.LayoutParams.WRAP_CONTENT,
            LinearLayout.LayoutParams.WRAP_CONTENT,
            1.0f,
        )

        params.setMargins(marginpixels, 0, 0, 0)
        paramsForAge.setMargins(0, 0, marginpixels, 0)
        paramsForAge.gravity = Gravity.CENTER
        paramsForRemove.setMargins(0, 0, marginpixels, 0)
        paramsForRemove.gravity = Gravity.CENTER

        val linearLayout: LinearLayout = childlist
        val childLinearLayout: LinearLayout = LinearLayout(requireContext())
        val childLinearLayoutAgeSex: LinearLayout = LinearLayout(requireContext())
        val childName = TextView(requireContext())
        val childSex = TextView(requireContext())
        val childAge = TextView(requireContext())
        val remove = ImageView(requireContext())

        childLinearLayout.orientation = LinearLayout.HORIZONTAL

        if (count % 2 == 0) {
            childLinearLayout.background =
                resources.getDrawable(R.drawable.child_border)
        }

        childLinearLayout.setPadding(paddingpixels, paddingpixels, paddingpixels, paddingpixels)
        childLinearLayout.layoutParams = paramsForLayout

        childName.text = name
        childSex.text = sex
        childAge.text = ((Calendar.getInstance().get(Calendar.YEAR) - dob.year).toString() + " лет")
        childAge.textSize = 20.0f

        childName.setTextColor(ContextCompat.getColor(requireContext(), R.color.text_selector))
        childSex.setTextColor(ContextCompat.getColor(requireContext(), R.color.text_selector))
        childAge.setTextColor(ContextCompat.getColor(requireContext(), R.color.text_selector))

        childName.typeface = resources.getFont(R.font.tt_nunito_bold)
        childSex.typeface = resources.getFont(R.font.tt_nunito_bold)
        childAge.typeface = resources.getFont(R.font.tt_nunito_bold)

        childAge.layoutParams = paramsForAge
        childAge.textAlignment = TextView.TEXT_ALIGNMENT_TEXT_END
        childName.layoutParams = params
        childSex.layoutParams = params
        remove.layoutParams = paramsForRemove

        childAge.gravity = Gravity.CENTER_VERTICAL

        remove.setImageResource(R.drawable.ic_close)
        remove.setOnClickListener {
            linearLayout.removeViewAt(linearLayout.indexOfChild(childLinearLayout))
        }

        childLinearLayoutAgeSex.orientation = LinearLayout.VERTICAL
        childLinearLayoutAgeSex.addView(childName)
        childLinearLayoutAgeSex.addView(childSex)

        childLinearLayout.addView(childLinearLayoutAgeSex)
        childLinearLayout.addView(childAge)
        childLinearLayout.addView(remove)


        linearLayout.addView(childLinearLayout)
        Log.i("count", count.toString())

        count = count.inc()

    }

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onChildDataAdded(child: MockData.Child) {
        addchild(child.name, child.sex, child.age)
    }
}