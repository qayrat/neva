package uz.neva.kidya.ui.category.filter

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_filter_dialog.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.MockData
import uz.neva.kidya.ui.category.MaterialListAdapter
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.selectedProduct.ColorListAdapter
import kotlin.collections.ArrayList

class FilterDialog(var listener: NoticeDialogListener,val sectionId:String,val sale:Boolean=false) : DialogFragment(){
    private lateinit var colorListAdapter: ColorListAdapter
    private lateinit var sizeListAdapter: MaterialListAdapter
    private lateinit var materialListAdapter: MaterialListAdapter

    private val filtersViewModel: FilterViewModel by viewModel()

    interface NoticeDialogListener {
        fun onFilterApplied(color: ArrayList<String>,supplier:ArrayList<String>,from:String,to:String,size:ArrayList<String>,hit:String="")
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dialog?.requestWindowFeature(Window.FEATURE_NO_TITLE)
        return inflater.inflate(R.layout.fragment_filter_dialog, container, false)
    }

    override fun onStart() {
        super.onStart()

        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        colorListAdapter= ColorListAdapter(true)
        sizeListAdapter= MaterialListAdapter()
        materialListAdapter= MaterialListAdapter()

        //filterColorRecycler.layoutManager= LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        filterColorRecycler.layoutManager= GridLayoutManager(requireContext(),5)
        //filterSizeRecycler.layoutManager= LinearLayoutManager(requireContext(),LinearLayoutManager.HORIZONTAL,false)
        filterSizeRecycler.layoutManager= GridLayoutManager(requireContext(),3,GridLayoutManager.HORIZONTAL,false)
        //filterSizeRecycler.layoutManager= GridLayoutManager(requireContext(),2)
        filterSupplierRecycler.layoutManager= GridLayoutManager(requireContext(),3,GridLayoutManager.HORIZONTAL,false)
        //filterSupplierRecycler.layoutManager= GridLayoutManager(requireContext(),2)
      //  filterMaterialRecycler.layoutManager=GridLayoutManager(requireContext(),2)


        filterColorRecycler.adapter=colorListAdapter
        filterSizeRecycler.adapter=sizeListAdapter
        filterSupplierRecycler.adapter=materialListAdapter

        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()

        filterColorRecycler.addItemDecoration(SpacesItemDecoration(marginpixels,true,5))
        filterSizeRecycler.addItemDecoration(SpacesItemDecoration(marginpixels,false,grid= true,span = 3))
        filterSupplierRecycler.addItemDecoration(SpacesItemDecoration(marginpixels,false,grid= true,span = 3))

        filterClose.setOnClickListener{
            this.dismiss()
        }

        applyFilterButton.setOnClickListener{

            Log.i("filter",materialListAdapter.getSelectedSupplier().toString())
            listener.onFilterApplied(colorListAdapter.getSelectedColors(),materialListAdapter.getSelectedSupplier(),priceSlider.values[0].toString(),priceSlider.values[1].toString(),sizeListAdapter.getSelectedSupplier(),if(propertyHit.isChecked) "Да" else "")

            this.dismiss()
        }

        if(sale)
        {
            filtersViewModel.getFiltersSale()
        }
        else
        filtersViewModel.getFilters(sectionId)
        priceSlider.addOnChangeListener { slider, value, fromUser ->
            // Responds to when slider's value is changed
            var fromvalue:String=slider.values[0].toString().removeSuffix(".0")
            var tovalue=slider.values[1].toString().removeSuffix(".0")

            priceValueFrom.text="От: ${fromvalue.reversed().chunked(3).joinToString(separator = " ").reversed()}"
            priceValueTo.text="До: ${tovalue.reversed().chunked(3).joinToString(separator = " ").reversed()}"

        }
        filtersViewModel.getFiltersResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {

                        for (filter in resource.data.data)
                            when (filter.name) {
                                "Цвет" -> {
//                                    var list = ArrayList<MockData.Color>()
//
//                                    for (color in filter.values)
//                                        list.add(MockData.Color("#${color}", false))
//
//                                    colorListAdapter.updateList(list)

                                }

                                "Поставщик" -> {
                                    var list = ArrayList<MockData.Material>()

                                    for (size in filter.values)
                                        list.add(MockData.Material(size, false))

                                    materialListAdapter.updateList(list)
                                    if (materialListAdapter.itemCount < 6) {
                                        filterSupplierRecycler.layoutManager = LinearLayoutManager(
                                            requireContext(),
                                            LinearLayoutManager.HORIZONTAL,
                                            false
                                        )
                                        // filterSupplierRecycler.addItemDecoration(SpacesItemDecoration(marginpixels,false,grid= false,span = 3))
                                    }
                                }

                                "Цена" -> {
                                    var list = ArrayList<Float>()

                                        var priceFrom=filter.values[0].toFloat()

                                            if(priceFrom%50000.0 > 0){
                                                priceFrom=(priceFrom-priceFrom%50000.0).toFloat()
                                            }

                                        var priceTo=filter.values[1].toFloat()

                                        if(priceTo%50000.0 > 0){
                                            priceTo=(priceTo+(50000.0-priceTo%50000.0)).toFloat()
                                        }

                                    if(priceFrom==priceTo&&priceFrom>50000.0)
                                        priceTo+=50000.0f

                                        list.add(priceFrom)
                                        list.add(priceTo)

                                    priceSlider.valueFrom=priceFrom
                                    priceSlider.valueTo=priceTo
                                    priceSlider.values = list
                                }

                                "Размер" -> {
                                    var list = ArrayList<MockData.Material>()

                                    for (size in filter.values)

                                        list.add(MockData.Material(size, false))

                                    sizeListAdapter.updateList(list)
                                    if (sizeListAdapter.itemCount < 6) {
                                        filterSizeRecycler.layoutManager = LinearLayoutManager(
                                            requireContext(),
                                            LinearLayoutManager.HORIZONTAL,
                                            false
                                        )
                                        // filterSizeRecycler.addItemDecoration(SpacesItemDecoration(marginpixels,false,grid= false,span = 3))
                                    }
                                }

                            }


                        priceValueFrom.text = "От: ${priceSlider.values[0].toString().removeSuffix(".0").reversed().chunked(3).joinToString(separator = " ").reversed()}"
                        priceValueTo.text = "До: ${priceSlider.values[1].toString().removeSuffix(".0").reversed().chunked(3).joinToString(separator = " ").reversed()}"

                    }
                    is Resource.GenericError -> {


//                        showSnackbarWithMargin(
//                            resource.errorResponse.jsonResponse.getString(
//                                "error"
//                            )
//                        )
                    }
                    is Resource.Error -> {

//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })
    }
}