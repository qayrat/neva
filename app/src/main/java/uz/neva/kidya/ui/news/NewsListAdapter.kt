package uz.neva.kidya.ui.news


import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.target.Target.SIZE_ORIGINAL
import kotlinx.android.synthetic.main.news_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.NewsObject
import kotlin.collections.ArrayList

class NewsListAdapter(val context: Context) :
    PagingDataAdapter<NewsObject, NewsListAdapter.ViewHolder>(NEWS) {

    var news: ArrayList<NewsObject>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.news_item, parent, false)
    )


    var onItemClick: ((NewsObject) -> Unit)? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: NewsListAdapter.ViewHolder, position: Int) {
        holder.bindData(getItem(position) as NewsObject)
        getItem(position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.newsArticleImg
        private val itemTitle = itemView.newsTitle
        private val itemDate = itemView.newsDate

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(getItem(position) as NewsObject)
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun bindData(news: NewsObject) {
            itemTitle.text = news.name
            //val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
            if (news.date != null) {
                //val date = LocalDate.parse(news.date, formatter)
                // val monthsru = context.resources.getStringArray(R.array.months_ru)
                itemDate.text = news.date
            }
            if (news.detail_picture != null)
                if (news.detail_picture[0] == '/')
                    Glide.with(context).load("${Const.BASE_URL}${news.detail_picture}")
                        .override(SIZE_ORIGINAL, SIZE_ORIGINAL).into(itemImageLink)
                else
                    Glide.with(context).load(news.detail_picture)
                        .override(SIZE_ORIGINAL, SIZE_ORIGINAL).into(itemImageLink)
        }
    }

    companion object {
        val NEWS = object : DiffUtil.ItemCallback<NewsObject>() {
            override fun areItemsTheSame(
                oldItem: NewsObject,
                newItem: NewsObject
            ): Boolean = oldItem.id == newItem.id


            override fun areContentsTheSame(
                oldItem: NewsObject,
                newItem: NewsObject
            ): Boolean =
                oldItem == newItem

        }
    }
}