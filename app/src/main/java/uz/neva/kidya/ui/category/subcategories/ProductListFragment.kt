package uz.neva.kidya.ui.category.subcategories

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_product_list.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.ProductSortObject
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.ui.category.subcategories.adapter.CategorySortAdapter
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.*

class ProductListFragment(
    var subid: Int,
    var categoryId: String,
    private val categoriesViewModel: CategoriesViewModel
) :
    Fragment(R.layout.fragment_product_list), CategorySortAdapter.OnItemClickListener {

    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private lateinit var categoryAdapter2: SelectedCategoryAdapter
    private lateinit var categoryAdapter: CategorySortAdapter
    var updateAdapter = ""
    private var scrollPosition: Int = 0

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        categoryAdapter2 = SelectedCategoryAdapter(requireContext())

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        getFeedPosts(subid.toString())
        setupRecyclerView()

        product_list.adapter = categoryAdapter2

        categoriesViewModel.isSuccess.observe(viewLifecycleOwner, { isHasProduct ->
            shimmer_product_container.stopShimmer()
            shimmer_product_container.visibility = View.GONE
            product_list.isVisible = isHasProduct
            empty_page.isVisible = !isHasProduct
        })

        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {

                        when (updateAdapter) {
                            "deal" -> {
                                categoryAdapter2.update(
                                    resource.data.updatedPosition,
                                    resource.data.updatedStatus
                                )
                            }
                        }

                        if (resource.data.updatedStatus)
                            showSnackbarWithMargin(resources.getString(R.string.text_added_snackbar_message))
                        else
                            showSnackbarWithMargin(resources.getString(R.string.text_delete_snackbar_message))
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(
                            resource.errorResponse.jsonResponse.getString(
                                "error"
                            )
                        )
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        categoryAdapter2?.click = {

            when (it) {
                is ClickEvents.onAddToFav -> {

                    updateAdapter = "deal"
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {

                }
            }
        }

        categoriesViewModel.categorySort.observe(viewLifecycleOwner, Observer {
            Timber.d("${it.sectionId}, ${it.sort}")
            getFeedPostsSort(it.sectionId, it.sort)
        })
    }

    private fun getFeedPostsSort(section_id: String, sort: String) {
        try {
            lifecycleScope.launch {
                categoriesViewModel.getCategoryFilterResult(
                    PrefManager.getUserID(requireContext()),
                    section_id,
                    sort
                ).collect { product ->
                    categoryAdapter = CategorySortAdapter(this@ProductListFragment)
                    product_list.adapter = categoryAdapter
                    categoryAdapter.submitData(viewLifecycleOwner.lifecycle, product)
                    return@collect
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }
    }

    private fun setupRecyclerView() {
        product_list.layoutManager =
            GridLayoutManager(requireContext(), 2, LinearLayoutManager.VERTICAL, false)
        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()
        product_list.addItemDecoration(SpacesItemDecoration(marginpixels))
    }

    fun addToFavs(product: ProductObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            showSnackbarAuth(resources.getString(R.string.text_need_auth))
        } else {
            if (product.fields.in_favourites != "Y") {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "add",
                    int,
                    true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "delete",
                    int,
                    false
                )
            }
        }
    }

    private fun addToSortFavs(product: ProductSortObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            showSnackbarAuth(resources.getString(R.string.text_need_auth))
        } else {
            if (product.fields?.in_favourites != "Y") {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields?.id ?: "",
                    "add",
                    int,
                    true
                )
            } else {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "delete",
                    int,
                    false
                )
            }
        }
    }

    private fun getFeedPosts(section_id: String) {
        if (section_id != "-1") {
            lifecycleScope.launch {
                categoriesViewModel.getSelectedCategoryProductsResponse(
                    section_id,
                    PrefManager.getUserID(requireContext())
                ).collect { product ->
                    categoryAdapter2.submitData(product)
                    return@collect
                }
            }
            categoryAdapter2.addLoadStateListener { loadState ->
                if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached && categoryAdapter2.itemCount < 1) {
                    categoriesViewModel.setData(false)
                } else {
                    if (categoryAdapter2.itemCount > 0) {
                        categoriesViewModel.setData(true)
                    }
                }
            }
        } else {
            lifecycleScope.launch {
                categoriesViewModel.getAllSelectedCategoryProductsResponse(
                    categoryId,
                    PrefManager.getUserID(requireContext())
                ).collect { product ->
                    categoryAdapter2.submitData(product)
                    return@collect
                }
            }
            categoryAdapter2.addLoadStateListener { loadState ->
                if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached && categoryAdapter2.itemCount < 1) {
                    categoriesViewModel.setData(false)
                } else {
                    if (categoryAdapter2.itemCount > 0) {
                        categoriesViewModel.setData(true)
                    }
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        shimmer_product_container.startShimmer()
    }

    override fun onPause() {
        shimmer_product_container.stopShimmer()
        super.onPause()
    }

    override fun onClick(data: ProductSortObject) {
        val bundle = Bundle()
        bundle.putString("section_id", data.fields?.iblock_section_id)
        bundle.putString("product_id", data.fields?.id)
        findNavController().navigate(R.id.nav_selectedItem, bundle)
    }

    override fun onFavourite(data: ProductSortObject, index: Int) {
        updateAdapter = "deal"
        addToSortFavs(data, index)
    }
}