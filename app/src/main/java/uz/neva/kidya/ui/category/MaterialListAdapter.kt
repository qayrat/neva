package uz.neva.kidya.ui.category

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.material_item.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.MockData

class MaterialListAdapter(): RecyclerView.Adapter<MaterialListAdapter.ViewHolder>(){

    var material: List<MockData.Material>?= MockData.getMaterial()

    var selected:ArrayList<String> = arrayListOf()


    fun updateList(size: List<MockData.Material>) {
        this.material = size
        notifyDataSetChanged()

    }

    var onItemClick: ((MockData.Material) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.material_item, parent, false)
    )

    fun getSelectedSupplier():ArrayList<String>{
        return selected
    }

    override fun getItemCount()=material?.size?:0

    override fun onBindViewHolder(holder: MaterialListAdapter.ViewHolder, position: Int) {
        holder.bindData(material!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener  {
        val materialisPicked=itemView.matIsPicked
        val materialName=itemView.materialName

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(material!![adapterPosition])
            }
        }

        fun bindData(mat: MockData.Material)
        {
            materialisPicked.setImageResource(R.drawable.ic_check_unchecked)
            materialisPicked.setOnClickListener(this)
            materialName.text=mat.material
            materialName.setOnClickListener(this)

        }

        override fun onClick(p0: View?) {

            if (!material!![adapterPosition].isPicked) {
                    Log.i("color", "color picked")
                    material!![adapterPosition].isPicked = true
                    materialisPicked.setImageResource(R.drawable.ic_check_checked)
                    selected.add(material!![adapterPosition].material)
                } else {
                    Log.i("color", "color unpicked")
                    material!![adapterPosition].isPicked = false
                    materialisPicked.setImageResource(R.drawable.ic_check_unchecked)
                    selected.remove(material!![adapterPosition].material)
                }

//            if (selected == "" || material!![adapterPosition].material == selected) {
//                if (!material!![adapterPosition].isPicked) {
//                    Log.i("color", "color picked")
//                    material!![adapterPosition].isPicked = true
//                    materialisPicked.setImageResource(R.drawable.ic_check_checked)
//                    selected=material!![adapterPosition].material
//                } else {
//                    Log.i("color", "color unpicked")
//                    material!![adapterPosition].isPicked = false
//                    materialisPicked.setImageResource(R.drawable.ic_check_unchecked)
//                    selected=""
//                }
//            }
        }
    }
}