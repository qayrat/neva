package uz.neva.kidya.ui.favourites

import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_category.*
import kotlinx.android.synthetic.main.fragment_favourites.*
import kotlinx.android.synthetic.main.fragment_favourites.blockerView
import kotlinx.android.synthetic.main.fragment_selected_category.*
import kotlinx.android.synthetic.main.fragment_selected_item.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.selectedProduct.SelectedItemFragment
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.*

class FavouritesFragment : Fragment() {

    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private val checkOutViewModel: CheckOutViewModel by viewModel()

    private lateinit var categoryAdapter: SelectedCategoryAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_favourites, container, false)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        getFeedPosts()

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val bottomNavigationView: BottomNavigationView = requireActivity().findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.action_heart

        favouritesRecyclerView.layoutManager =
            GridLayoutManager(requireContext(), 2, LinearLayoutManager.VERTICAL, false)
        favouritesRecyclerView.adapter = categoryAdapter

        favouritesRecyclerView.addItemDecoration(
            SpacesItemDecoration(
                resources.getDimension(R.dimen.fab_margin_16).toInt()
            )
        )

        categoriesViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Favourites")
        ))

        categoriesViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        Timber.d("${resource.data.status}, ${resource.data.data}")
                    }
                    is Resource.GenericError -> {
                        Timber.d(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
                        Timber.d(resource.exception.localizedMessage)
                    }
                }
            }
        })

        categoryAdapter?.click = {
            when (it) {
                is ClickEvents.onAddToFav -> {

                    Log.i("fav", "fagment recived index ${it.index}")
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {

                }
            }
            categoryAdapter.notifyDataSetChanged()
        }

        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        blockerView.show()
                    }
                    is Resource.Success -> {
                        blockerView.hide()

                        categoryAdapter.update(
                            resource.data.updatedPosition,
                            resource.data.updatedStatus
                        )
                        categoryAdapter.notifyDataSetChanged()

                    }
                    is Resource.GenericError -> {
                        blockerView.hide()
                        changeUiStateEnabled(
                            false,
                            progressIndicatorAddToCart,
                            itemFavourite
                        )
                    }
                    is Resource.Error -> {
                        blockerView.hide()
                    }
                }
            }
        })

        viewLifecycleOwner.lifecycleScope.launch {
            categoryAdapter.loadStateFlow.collectLatest { loadStates ->
                if (!(loadStates.refresh is LoadState.Loading)) {
                    shimmer_view_favourite_container.stopShimmer()
                    shimmer_view_favourite_container.visibility = View.GONE
                    favouriteMain.visibility = View.VISIBLE
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).bottom_navigation.show()
        shimmer_view_favourite_container.startShimmer()
    }

    override fun onPause() {
        shimmer_view_favourite_container.stopShimmer()
        super.onPause()
    }

    companion object {
        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                return Html.fromHtml(source)
            }
        }

    }

    private fun getFeedPosts() {
        lifecycleScope.launch {
            categoriesViewModel.getFavProductsResponse(PrefManager.getUserID(requireContext()))
                .collect { product ->

                    categoryAdapter = SelectedCategoryAdapter(requireContext())
                    categoryAdapter.submitData(product)

                    return@collect
                }
        }
    }

    fun addToFavs(product: ProductObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
//            showSnackbarAuth("вам необходимо")
        } else {
            if (product.fields.in_favourites != "Y") {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "add",
                    int,
                    true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "delete",
                    int,
                    false
                )

            }
        }
    }

}
