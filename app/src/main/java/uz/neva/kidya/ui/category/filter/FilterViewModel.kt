package uz.neva.kidya.ui.category.filter

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.Filters
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event

class FilterViewModel constructor(private val repository: KidyaRepository): ViewModel(){

    val getFiltersResponse= MutableLiveData<Event<Resource<Filters>>>()

    fun getFilters(section_id:String) {
        viewModelScope.launch {
            repository.getFilters(section_id).onEach{
                getFiltersResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getFiltersSale() {
        viewModelScope.launch {
            repository.getFiltersSale().onEach{
                getFiltersResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }
}