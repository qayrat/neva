package uz.neva.kidya.ui.category.selectedProduct

import android.content.res.ColorStateList
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.bumptech.glide.request.RequestOptions
import kotlinx.android.synthetic.main.clolor_item.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.MockData

class ColorListAdapter(val inFilter: Boolean = false) :
    RecyclerView.Adapter<ColorListAdapter.ViewHolder>() {

    lateinit var color: List<MockData.Color>
    var selected: String = ""
    var selectedPosition: Int = -1
    var selectedList: ArrayList<String> = arrayListOf()
    fun updateList(color: List<MockData.Color>) {
        this.color = color
        notifyDataSetChanged()
    }

    var onItemClick: ((MockData.Color) -> Unit)? = null

    fun getSelectedColor(): String {
        return selected
    }

    fun cancelChoice() {
        for (item in color)
            item.isPicked = false
        selected = ""
        notifyDataSetChanged()
    }

    fun getSelectedColors(): ArrayList<String> {
        return selectedList
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.clolor_item, parent, false)
    )

    override fun getItemCount() = color.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(color!![position])

    }

    fun defaultSelect()
    {
        if(selectedPosition==-1 &&color?.size!=0)
        {
            color?.get(0)?.isPicked =true
            selected=color?.get(0)?.hex.toString()
            selectedPosition=0
            notifyDataSetChanged()
        }
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener  {
        val outerCircle=itemView.colorOuter
        val innerCircle=itemView.colorInner

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(color!![adapterPosition])
            }
        }

        fun bindData(color:MockData.Color)
        {

            outerCircle.imageTintList= ColorStateList.valueOf(Color.parseColor(color.hex))
            outerCircle.setOnClickListener(this)
            innerCircle.imageTintList= ColorStateList.valueOf(Color.parseColor(color.hex))
            innerCircle.setOnClickListener(this)


            if(color.isPicked)
                outerCircle.imageTintList = null
            else
                outerCircle.imageTintList = ColorStateList.valueOf(Color.parseColor(color.hex))
        }



        override fun onClick(p0: View?) {
            if(inFilter)
            {
                if (!color!![absoluteAdapterPosition].isPicked) {
                    Log.i("color", "color picked")
                    color!![absoluteAdapterPosition].isPicked = true
                    outerCircle.imageTintList = null
                    selectedList.add(color!![absoluteAdapterPosition].hex)
                } else {
                    Log.i("color", "color unpicked")
                    color!![absoluteAdapterPosition].isPicked = false
                    outerCircle.imageTintList =
                        ColorStateList.valueOf(Color.parseColor(color!![absoluteAdapterPosition].hex))
                    selectedList.remove(color!![absoluteAdapterPosition].hex)
                }
            }
            else{
                if (!color!![absoluteAdapterPosition].isPicked) {
                    if(selectedPosition!=-1)
                    {
                        color!![selectedPosition].isPicked = false
                        selected=""
                    }
                    color!![absoluteAdapterPosition].isPicked = true
                    outerCircle.imageTintList = null
                    selected=color!![absoluteAdapterPosition].hex

                    Log.i("color", "size picked $selected")
                    selectedPosition=absoluteAdapterPosition

                    onItemClick?.invoke(color!![absoluteAdapterPosition])

                } else {
                    Log.i("color", "size unpicked")
                    color!![absoluteAdapterPosition].isPicked = false
                    outerCircle.imageTintList =
                        ColorStateList.valueOf(Color.parseColor(color!![absoluteAdapterPosition].hex))
                    selected=""

                    onItemClick?.invoke(color!![absoluteAdapterPosition])
                }

                notifyDataSetChanged()
            }

        }
    }
}