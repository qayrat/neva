package uz.neva.kidya.ui.brandShop.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.selected_category_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.ProductObject

class BrandShopPagingAdapter(
        private val fragment: Fragment
) : PagingDataAdapter<ProductObject, BrandShopPagingAdapter.ViewHolder>(PRODUCTS) {

    private val listener = fragment as OnItemClickListener
    private val layoutInflater = LayoutInflater.from(fragment.requireContext())

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = layoutInflater.inflate(R.layout.selected_category_item, parent, false)
        return ViewHolder(view, listener)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(getItem(position) as ProductObject)
    }

    fun update(int: Int, fav: Boolean) {
        (getItem(int) as ProductObject).fields.in_favourites = if (fav) "Y" else "N"
        notifyItemChanged(int)
    }

    inner class ViewHolder(
            itemView: View,
            listener: OnItemClickListener
    ) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.selectedCategoryItemImage
        private val itemItemName = itemView.selectedCategoryItemName
        private val itemPrice = itemView.selectedCategoryItemPrice
        private val itemOldPrice = itemView.selectedCategoryItemOldPrice
        private val itemBrand = itemView.selectedCategoryItemBrand
        private val itemIsFav = itemView.isFavourite
        private val itemTag = itemView.tagback

        init {
            itemView.setOnClickListener {
                listener.onClick((getItem(absoluteAdapterPosition) as ProductObject))
            }
            itemView.isFavourite.setOnClickListener {
                listener.onFavourite(
                        (getItem(absoluteAdapterPosition) as ProductObject),
                        absoluteAdapterPosition
                )
            }
        }

        fun bindData(item: ProductObject) {
            if (item.fields.preview_picture != null) {
                var imageUrl = ""
                imageUrl = if (item.fields.preview_picture[0] == '/') {
                    Const.BASE_URL + item.fields.preview_picture
                } else {
                    item.fields.preview_picture
                }
                Glide.with(fragment).load(imageUrl)
                        .thumbnail(0.2f)
                        .override(250, 250)
                        .placeholder(R.drawable.placeholder_neva)
                        .into(itemImageLink)
            }

            itemTag.isVisible = item.fields.percent != null
            itemItemName.text = item.fields.name
            itemPrice.text =
                    "${(item.fields?.catalog_price_1 ?: "0").replace(",", " ")} сум"
            itemOldPrice.isVisible =
                    item.fields?.percent != null && item.fields.percent != ""

            if (item.fields?.percent != null) {
                itemOldPrice.text =
                        "${(item.fields.old_price ?: "0").replace(",", " ")} сум"
                itemPrice.text =
                        "${(item.fields.catalog_price_1) ?: "0".replace(",", " ")} сум"
                itemTag.text = "-${item.fields.percent ?: ""}%"
            }

            for (i in item.params) {
                when (i.name) {
                    "Поставщик" -> {

                        itemBrand.text = i.value[0]
                    }
                    "Цена" -> {
                        if (itemTag.visibility == View.GONE)
                            itemPrice.text = "${i.value[0].replace(",", " ")} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(fragment).load(i.value[0])
                                .thumbnail(0.2f)
                                .override(250, 250)
                                .placeholder(R.drawable.placeholder_neva)
                                .into(itemImageLink)
                    }
                }
            }

            if (item.fields.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_red_favourite)
            } else {
                itemIsFav.setImageResource(R.drawable.ic_favourite)
            }
        }
    }

    companion object {
        val PRODUCTS = object : DiffUtil.ItemCallback<ProductObject>() {
            override fun areItemsTheSame(
                    oldItem: ProductObject,
                    newItem: ProductObject
            ): Boolean = oldItem.fields.id == newItem.fields.id

            override fun areContentsTheSame(
                    oldItem: ProductObject,
                    newItem: ProductObject
            ): Boolean =
                    oldItem == newItem

        }
    }

    interface OnItemClickListener {
        fun onClick(data: ProductObject)
        fun onFavourite(data: ProductObject, index: Int)
    }
}