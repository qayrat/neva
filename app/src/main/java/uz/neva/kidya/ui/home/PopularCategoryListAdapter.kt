package uz.neva.kidya.ui.home

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.yariksoffice.lingver.Lingver
import kotlinx.android.synthetic.main.category_item.view.*
import timber.log.Timber
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.dto.CategoriesObject


class PopularCategoryListAdapter(val context: Context) :
    RecyclerView.Adapter<PopularCategoryListAdapter.ViewHolder>() {
    var category: ArrayList<CategoriesObject> = arrayListOf<CategoriesObject>()
    fun updateList(category: ArrayList<CategoriesObject>) {
        this.category = category
        notifyDataSetChanged()
    }


    var onItemClick: ((CategoriesObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.category_item, parent, false)
    )

    override fun getItemCount() = category?.size ?: 0

    override fun onBindViewHolder(holder: PopularCategoryListAdapter.ViewHolder, position: Int) {
        holder.bindData(category!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.categoryImage
        private val name = itemView.categoryName

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(category!![absoluteAdapterPosition])
            }
        }

        fun bindData(category: CategoriesObject) {
            Timber.d(Lingver.getInstance().getLanguage())
            when (Lingver.getInstance().getLanguage()) {
                "ru" -> {
                    name.text = category.name
                }
                "en" -> {
                    name.text = category.name_en
                }
                "uz" -> {
                    if (category.name_uz != null) {
                        name.text = category.name_uz
                    } else {
                        name.text = category.name
                    }
                }
                else -> {
                    name.text = category.name
                }
            }
            if (category.picture != null)
                Glide.with(context)
                    .load(Const.BASE_URL + category.picture)
                    .thumbnail(0.2f)
                    .override(200, 200)
                    .placeholder(R.drawable.placeholder_neva)
                    .into(itemImageLink)
            else
                itemImageLink.setImageDrawable(context.resources.getDrawable(R.drawable.ic_baseline_broken_image_24))
        }
    }

}