package uz.neva.kidya.ui.category.subcategories

import android.annotation.SuppressLint
import android.app.AlertDialog
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.util.Log
import android.view.*
import android.view.ViewGroup.MarginLayoutParams
import android.widget.*
import androidx.annotation.MenuRes
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import com.google.android.material.tabs.TabLayout
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_sub_category.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.CategoriesObject
import uz.neva.kidya.network.dto.ProductObjectNew
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.filter.FilterDialog
import uz.neva.kidya.ui.category.subcategories.adapter.SelectedCategoryNewAdapter
import uz.neva.kidya.ui.category.subcategories.adapter.SubCategoryLoadStateAdapter
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.*

class SubCategoryFragment : Fragment(), FilterDialog.NoticeDialogListener,
        SelectedCategoryNewAdapter.OnItemClickListener {

    var sectionName = ""
    var sectionId = ""
    var subSectionId = ""
    var sortText = ""
    var totalCount = ""
    val categoriesViewModel: CategoriesViewModel by viewModel()
    private var subCategoryList = ArrayList<CategoriesObject>()
    private var menuSortSelectId = -1
    private var onPause = false
    private var firstCreated = false

    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private lateinit var categoryAdapter: SelectedCategoryNewAdapter
    var updateAdapter = ""
    private val navController by lazy(LazyThreadSafetyMode.NONE) {
        Navigation.findNavController(requireActivity(), R.id.nav_host_fragment)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).bottom_navigation.hide()
        arguments?.let {
            sectionName = it.getString("section_name").toString()
            sectionId = it.getString("section_id").toString()
        }
        subSectionId = sectionId
        categoriesViewModel.getSubCategories(sectionId)
        requireActivity().window.setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
        )

        categoryAdapter = SelectedCategoryNewAdapter(this)
        totalCount = "${resources.getString(R.string.text_result)}:"
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_sub_category, container, false)
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoriesViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Sub Category")
        ))

        categoriesViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> { }
                    is Resource.GenericError -> { }
                    is Resource.Error -> { }
                }
            }
        })

        selectedCategoryName.text = totalCount
        subCatName.text = sectionName
        setupRecyclerView()

        categoriesViewModel.isSuccess.observe(viewLifecycleOwner, { isHasProduct ->
//            shimmer_product_container.stopShimmer()
//            shimmer_product_container.visibility = View.GONE
//            product_list.isVisible = isHasProduct
//            empty_page.isVisible = !isHasProduct
        })

        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {

                        when (updateAdapter) {
                            "deal" -> {
                                categoryAdapter.update(
                                        resource.data.updatedPosition,
                                        resource.data.updatedStatus
                                )
                            }
                        }

                        if (resource.data.updatedStatus)
                            showSnackbarWithMargin(resources.getString(R.string.text_added_snackbar_message))
                        else
                            showSnackbarWithMargin(resources.getString(R.string.text_delete_snackbar_message))
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(
                                resource.errorResponse.jsonResponse.getString(
                                        "error"
                                )
                        )
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        searchItem.setOnClickListener {
            findNavController().navigate(R.id.nav_searchResult)
        }

        filterLay.setOnClickListener {
            showMenu(it, R.menu.category_sort_menu)
        }

        categoriesViewModel.tab.observe(viewLifecycleOwner, { tab ->
            tab?.let {
                sub_category_tab_layout.selectTab(it)
            }
        })

        action_up.setOnClickListener {
//            NestedScrollView.smoothScrollTo(0, 0)
        }

        if (subCategoryList.isNotEmpty()) {
            for (i in subCategoryList.indices) {
                sub_category_tab_layout.addTab(sub_category_tab_layout.newTab().setText(subCategoryList[i].name))
                val tab =
                        (sub_category_tab_layout.getChildAt(0) as ViewGroup).getChildAt(i)
                val p = tab.layoutParams as MarginLayoutParams
                p.setMargins(6, 4, 6, 4)
                tab.requestLayout()
            }
        }

        subCategoryBack.setOnClickListener {
            findNavController().popBackStack()
        }

        sub_category_tab_layout.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab?) {
                tab?.let {
                    val id = if (subCategoryList[it.position].id == "-1") {
                        sectionId
                    } else {
                        subCategoryList[it.position].id
                    }
                    subSectionId = id
                    if (!onPause) {
                        shimmer_product_container.visibility = View.VISIBLE
                        shimmer_product_container.startShimmer()
                        product_list.isVisible = false
                        empty_page.isVisible = false
                        categoriesViewModel.getProductTotalCount(subSectionId)
                        getFeedPosts(subSectionId, sortText)
                        product_list.smoothScrollToPosition(0)
                        categoryAdapter.notifyDataSetChanged()
                    } else {
                        onPause = false
                    }
                }
                categoriesViewModel.tab.value = tab
            }

            override fun onTabUnselected(tab: TabLayout.Tab?) {
            }

            override fun onTabReselected(tab: TabLayout.Tab?) {
            }

        })

        categoriesViewModel.totalCount.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        val total = resource.data.pagination?.total_count
                        ll_sort_.isVisible = total != null && total != 0
                        if (total != null && total != 0) {
                            selectedCategoryName.text =
                                    "${resources.getString(R.string.text_result)}: ${total}"
                            totalCount = "${resources.getString(R.string.text_result)}: ${total}"
                            shimmer_product_container.stopShimmer()
                            shimmer_product_container.visibility = View.GONE
                            product_list.isVisible = true
                            empty_page.isVisible = false
                        } else {
                            shimmer_product_container.stopShimmer()
                            shimmer_product_container.visibility = View.GONE
                            product_list.isVisible = false
                            empty_page.isVisible = true
                        }
                    }
                    else -> {
                        Log.d(TAG, "onViewCreated: $resource")
                    }
                }
            }
        })

        categoriesViewModel.subCategoriesResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        val subCategoryList = resource.data.data

                        this.subCategoryList.addAll(subCategoryList)
                        for (i in subCategoryList.indices) {
                            sub_category_tab_layout.addTab(sub_category_tab_layout.newTab().setText(subCategoryList[i].name))
                        }

                        for (i in 0 until sub_category_tab_layout.tabCount) {
                            val tab =
                                    (sub_category_tab_layout.getChildAt(0) as ViewGroup).getChildAt(i)
                            val p = tab.layoutParams as MarginLayoutParams
                            p.setMargins(6, 4, 6, 4)
                            tab.requestLayout()
                        }
                        shimmer_product_tab.stopShimmer()
                        shimmer_product_tab.visibility = View.GONE
                        sub_category_tab_layout.visibility = View.VISIBLE
                    }
                    is Resource.GenericError -> {

                    }
                    is Resource.Error -> {

                    }
                }
            }
        })
    }

    private fun showMenu(v: View, @MenuRes menuRes: Int) {
        val popup = PopupMenu(requireContext(), v)
        popup.menuInflater.inflate(menuRes, popup.menu)
        if (menuSortSelectId != -1) {
            popup.menu.getItem(menuSortSelectId).isChecked = true
        }
        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when (menuItem.itemId) {
                R.id.action_update -> {
                    actionMenuSort("id:desc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 0
                    }
                }
                R.id.action_price_increase -> {
                    actionMenuSort("price:asc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 1
                    }
                }
                R.id.action_price_reduction -> {
                    actionMenuSort("price:desc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 2
                    }
                }
                R.id.action_rating -> {
                    actionMenuSort("id:asc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 3
                    }
                }
            }
            false
        }
        popup.setOnDismissListener {
        }
        popup.show()
    }

    private fun actionMenuSort(sort: String) {
        Timber.d("actionMenuSort")
        getFeedPosts(subSectionId, sort)
        sortText = sort
//        shimmer_product_container.visibility = View.VISIBLE
//        shimmer_product_container.startShimmer()
//        product_list.visibility = View.GONE
//        empty_page.visibility = View.GONE
        product_list.smoothScrollToPosition(0)
        categoryAdapter.notifyDataSetChanged()
    }

    companion object {
        val TAG = "SUBCATEGORYFRAGMENT"

        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            return if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                Html.fromHtml(source)
            }
        }

    }

    override fun onFilterApplied(
            color: ArrayList<String>,
            supplier: ArrayList<String>,
            from: String,
            to: String,
            size: ArrayList<String>,
            hit: String
    ) {
        Log.i("filter", "color $color, supplier $supplier")
        val properties = HashMap<String, String>()
        if (color.isNotEmpty())
            for (index in 0 until color.size)
                properties["property_color[$index]"] = color[index].substring(1)
        if (supplier.isNotEmpty())
            for (index in 0 until supplier.size)
                properties["property_suppiler[$index]"] = supplier[index]
        properties["property_base_from"] = from
        properties["property_base_to"] = to
        if (size.isNotEmpty())
            for (index in 0 until size.size)
                properties["property_razmer[$index]"] = size[index]
        if (hit.isNotBlank())
            properties["property_hit"] = hit

    }

    private fun setupRecyclerView() {
        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()
        val gridLayoutManager =
                GridLayoutManager(requireContext(), 2, LinearLayoutManager.VERTICAL, false)
        val footerAdapter = SubCategoryLoadStateAdapter { categoryAdapter.retry() }
        product_list.apply {
            layoutManager = gridLayoutManager
            addItemDecoration(SpacesItemDecoration(marginpixels))
            adapter = categoryAdapter.withLoadStateFooter(
                    footer = footerAdapter
            )
            gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == categoryAdapter.itemCount && footerAdapter.itemCount > 0) {
                        2
                    } else {
                        1
                    }
                }
            }
        }
    }

    fun addToFavs(product: ProductObjectNew, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            showSnackbarAuth(resources.getString(R.string.text_need_auth))
        } else {
            if (product.fields?.in_favourites != "Y") {
                val productId = product.fields?.id ?: ""
                checkOutViewModel.getAddToFavsResponse(
                        PrefManager.getUserID(requireContext()),
                        "favs",
                        productId,
                        "add",
                        int,
                        true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                        PrefManager.getUserID(requireContext()),
                        "favs",
                        product.fields.id,
                        "delete",
                        int,
                        false
                )
            }
        }
    }

    private fun getFeedPosts(section_id: String, sort: String) {
        lifecycleScope.launch {
            categoriesViewModel.getAllSelectedCategoryProductsResponseNew(
                    section_id,
                    PrefManager.getUserID(requireContext()),
                    true,
                    sort
            ).collect { product ->
                categoryAdapter.submitData(product)
                return@collect
            }
        }
        categoryAdapter.addLoadStateListener { loadState ->
            if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached && categoryAdapter.itemCount < 1) {
                categoriesViewModel.setData(false)
            } else {
                if (categoryAdapter.itemCount > 0) {
                    categoriesViewModel.setData(true)
                    categoryAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).bottom_navigation.hide()
        shimmer_product_container.startShimmer()
        if (!firstCreated) {
            shimmer_product_tab.startShimmer()
            firstCreated = true
        } else {
            shimmer_product_tab.stopShimmer()
            shimmer_product_tab.visibility = View.GONE
            sub_category_tab_layout.visibility = View.VISIBLE
        }
    }

    override fun onPause() {
        super.onPause()
        onPause = true
        shimmer_product_container.stopShimmer()
        shimmer_product_tab.stopShimmer()
    }

    override fun onClick(data: ProductObjectNew) {
        val bundle = Bundle()
        bundle.putString("section_id", data.fields?.iblock_section_id)
        bundle.putString("product_id", data.fields?.id)
        findNavController().navigate(R.id.nav_selectedItem, bundle)
    }

    override fun onFavourite(data: ProductObjectNew, index: Int) {
        updateAdapter = "deal"
        addToFavs(data, index)
    }
}

class SortDialogFragment(val sortListener: OnSortDialogClick? = null) : DialogFragment() {


    override fun onStart() {
        super.onStart()
        val width = resources.getDimensionPixelSize(R.dimen.popup_width)
        val height = resources.getDimensionPixelSize(R.dimen.popup_height)
        dialog!!.window!!.setLayout(width, height)
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val builder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.custom_sort_layout, null)

        builder.setView(dialogView)

        val dialogD = builder.create()
        dialogD.window?.attributes?.windowAnimations = R.style.SortDialogAnimation;
        val wmlp = dialogD.window?.attributes
        wmlp?.gravity = Gravity.END

        dialogView.apply {
            findViewById<TextView>(R.id.new_product).setOnClickListener {
                sortListener?.newProduct()
                dialog?.dismiss()
            }

            findViewById<TextView>(R.id.price_up).setOnClickListener {
                sortListener?.priceUp()
                dialog?.dismiss()
            }

            findViewById<TextView>(R.id.price_down).setOnClickListener {
                sortListener?.priceDown()
                dialog?.dismiss()
            }

            findViewById<TextView>(R.id.price_review).setOnClickListener {
                sortListener?.priceReview()
                dialog?.dismiss()
            }
        }

        return dialogD
    }
}

interface OnSortDialogClick {
    fun newProduct()
    fun priceUp()
    fun priceDown()
    fun priceReview()
}