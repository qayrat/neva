package uz.neva.kidya.ui.category

import android.graphics.Rect
import android.os.Build
import android.os.Bundle
import android.text.Html
import android.text.Spanned
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.recyclerview.widget.RecyclerView.ItemDecoration
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.bumptech.glide.Glide
import com.google.android.material.bottomnavigation.BottomNavigationView
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_category.*
import kotlinx.android.synthetic.main.fragment_category.indicator
import kotlinx.android.synthetic.main.fragment_collection_object.*
import kotlinx.android.synthetic.main.fragment_landind_page.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.Const
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.category.subcategories.SubCategoryFragment
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show


class CategoryFragment : Fragment() {

    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private lateinit var categoryAdapter: CategoryListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        categoryAdapter = CategoryListAdapter(requireContext())
        getFeedPosts()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_category, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        val bottomNavigationView: BottomNavigationView = requireActivity().findViewById(R.id.bottom_navigation)
        bottomNavigationView.selectedItemId = R.id.action_grid
//        val scale = resources.displayMetrics.density
//        val marginpixels = (16 * scale + 0.5f).toInt()

        categoryRecyclerView.addItemDecoration(SpacesItemDecoration(0, true, 3))
        categoryRecyclerView.layoutManager =
            GridLayoutManager(requireContext(), 3)

        categoryRecyclerView.adapter = categoryAdapter

        //getFeedPosts()

        categoriesViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Categories")
        ))

        categoryAdapter?.onItemClick = { it, view ->

            val bundle = Bundle()
            bundle.putString("section_id", it.id)
            bundle.putString("section_name", it.name)
            findNavController().navigate(R.id.nav_subCategory, bundle)

        }


        viewLifecycleOwner.lifecycleScope.launch {
            categoryAdapter.loadStateFlow.collectLatest { loadStates ->
                if (loadStates.refresh is LoadState.Loading)
                    progressIndicatorCategories.show()
                else {
                    progressIndicatorCategories.hide()
                    shimmer_view_category_container.stopShimmer()
                    shimmer_view_category_container.visibility = View.GONE
                    categoryAllMain.visibility = View.VISIBLE
                }
            }
        }

        categoriesViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        Timber.d("${resource.data.status}, ${resource.data.data}")
                    }
                    is Resource.GenericError -> {
                        Timber.d(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
                        Timber.d(resource.exception.localizedMessage)
                    }
                }
            }
        })
    }

    private fun getFeedPosts() {
        lifecycleScope.launch {
            categoriesViewModel.getCategoriesResponse().collect { categories ->
                // categoryRecyclerView.adapter = categoryAdapter
                categoryAdapter.submitData(categories)

                return@collect
            }
        }
    }

    override fun onResume() {
        super.onResume()
        shimmer_view_category_container.startShimmer()
        (requireActivity() as MainActivity).bottom_navigation.show()
    }

    override fun onPause() {
        shimmer_view_category_container.stopShimmer()
        super.onPause()
    }


    companion object {
        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                return Html.fromHtml(source)
            }
        }

    }
}

class SpacesItemDecoration(
    private val space: Int,
    private val vertical: Boolean = true,
    private val span: Int = 2,
    private val grid: Boolean = false,
) : ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        if (vertical) {
            var itemposition = parent.getChildLayoutPosition(view)
            var column = itemposition % span

            outRect.left = space - column * space / span
            outRect.right = (column + 1) * space / span
            outRect.bottom = space

            if (itemposition < span) {
                outRect.top = space
            } else {
                outRect.top = 0
            }
        } else
            if (grid) {
                var itemposition = parent.getChildLayoutPosition(view)
                var column = itemposition % span

                outRect.top = space//-column* space/span
                outRect.right = space//(column+1)* space/span
                outRect.bottom = space

                if (itemposition < span) {
                    outRect.left = 0
                } else {
                    outRect.left = space
                }
            } else {
                var itemposition = parent.getChildLayoutPosition(view)

                outRect.top = space
                outRect.bottom = space

                if (itemposition == 0) {
                    outRect.left = 0
                } else {
                    outRect.left = space
                }
            }

    }
}

class ReverseSpacesItemDecoration(private val space: Int, private val span: Int = 1) :
    ItemDecoration() {
    override fun getItemOffsets(
        outRect: Rect,
        view: View,
        parent: RecyclerView,
        state: RecyclerView.State
    ) {
        super.getItemOffsets(outRect, view, parent, state)

        var itemposition = parent.getChildLayoutPosition(view)
        var column = itemposition % span

        outRect.left = space - column * space / span
        outRect.right = (column + 1) * space / span
        outRect.top = space

        if (itemposition < span) {
            outRect.bottom = space
        } else {
            outRect.bottom = 0
        }

    }
}
