package uz.neva.kidya.ui.category.subcategories.adapter

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.selected_category_item.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.ProductSortObject

class CategorySortAdapter(private val fragment: Fragment) :
    PagingDataAdapter<ProductSortObject, CategorySortAdapter.ViewHolder>(
        PRODUCTS
    ) {
    var selectedCategory: List<ProductSortObject>? = null

    private val listener = fragment as OnItemClickListener

    fun update(int: Int, fav: Boolean) {
        (getItem(int) as ProductSortObject).fields?.in_favourites = if (fav) "Y" else "N"
        notifyItemChanged(int)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.selected_category_item, parent, false),
        listener
    )

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(getItem(position) as ProductSortObject)
        getItem(position)

    }

    inner class ViewHolder(itemView: View, private val listener: OnItemClickListener) :
        RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.selectedCategoryItemImage
        private val itemItemName = itemView.selectedCategoryItemName

        //        private var itemRating=itemView.selectedCategoryItemRatingBar
        private val itemPrice = itemView.selectedCategoryItemPrice
        private val itemOldPrice = itemView.selectedCategoryItemOldPrice
        private val itemBrand = itemView.selectedCategoryItemBrand
        private val itemIsFav = itemView.isFavourite
        private val itemTag = itemView.tagback


        init {
            itemView.setOnClickListener {
                listener.onClick((getItem(absoluteAdapterPosition) as ProductSortObject))
            }
            itemView.isFavourite.setOnClickListener {
                listener.onFavourite(
                    (getItem(absoluteAdapterPosition) as ProductSortObject),
                    absoluteAdapterPosition
                )
            }
        }

        @SuppressLint("SetTextI18n")
        fun bindData(selectedCategory: ProductSortObject) {
            if (selectedCategory.fields?.preview_picture != null && selectedCategory.fields.preview_picture != "")
                if (selectedCategory.fields.preview_picture[0] == '/') {
                    Glide.with(fragment)
                        .load(Const.BASE_URL + selectedCategory.fields.preview_picture)
                        .override(
                            500, 500
                        ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                        .into(itemImageLink)
                } else {
                    Glide.with(fragment).load(selectedCategory.fields.preview_picture)
                        .override(
                            500,
                            500
                        ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                        .into(itemImageLink)
                }
            else
                itemImageLink.setImageDrawable(fragment.resources.getDrawable(R.drawable.ic_baseline_broken_image_24))


//            itemRating.rating=selectedCategory.fields.rating?:0f
            itemTag.isVisible = selectedCategory.fields?.percent != null
            itemItemName.text = selectedCategory.fields?.name
            itemPrice.text =
                "${(selectedCategory.fields?.catalog_price_1 ?: "0").replace(",", " ")} сум"
            itemOldPrice.isVisible =
                selectedCategory.fields?.percent != null && selectedCategory.fields.percent != ""

            if (selectedCategory.fields?.percent != null) {
                itemOldPrice.text =
                    "${(selectedCategory.fields.old_price ?: "0").replace(",", " ")} сум"
                itemPrice.text =
                    "${(selectedCategory.fields.catalog_price_1) ?: "0".replace(",", " ")} сум"
                itemTag.text = "-${selectedCategory.fields.percent ?: ""}%"
            }

            for (item in selectedCategory.params) {

                when (item.name) {
                    "Поставщик" -> {

                        itemBrand.text = item.value[0]
                    }
                    "Цена" -> {
                        if (itemTag.visibility == View.GONE)
                            itemPrice.text = "${item.value[0].replace(",", " ")} сум"
                    }
                    "Путь на картинку" -> {
                        Glide.with(fragment).load(item.value[0])
                            .override(
                                500,
                                500
                            ).placeholder(R.drawable.ic_baseline_cloud_download_24)
                            .into(itemImageLink)
                    }

                }
            }


            if (selectedCategory.fields?.in_favourites == "Y") {
                itemIsFav.setImageResource(R.drawable.ic_red_favourite)
            } else
                itemIsFav.setImageResource(R.drawable.ic_favourite)

        }
    }

    companion object {
        val PRODUCTS = object : DiffUtil.ItemCallback<ProductSortObject>() {
            override fun areItemsTheSame(
                oldItem: ProductSortObject,
                newItem: ProductSortObject
            ): Boolean = oldItem.fields?.id == newItem.fields?.id

            override fun areContentsTheSame(
                oldItem: ProductSortObject,
                newItem: ProductSortObject
            ): Boolean =
                oldItem == newItem

        }
    }

    interface OnItemClickListener {
        fun onClick(data: ProductSortObject)
        fun onFavourite(data: ProductSortObject, index: Int)
    }
}