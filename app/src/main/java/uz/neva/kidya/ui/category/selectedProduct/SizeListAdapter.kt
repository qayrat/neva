package uz.neva.kidya.ui.category.selectedProduct


import android.content.res.ColorStateList
import android.graphics.Color
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.size_item.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.MockData
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show

class SizeListAdapter() : RecyclerView.Adapter<SizeListAdapter.ViewHolder>() {

    var size: List<MockData.Size>? = MockData.getSizes()
    var selected: String = ""
    var selectedPosition: Int = -1

    fun updateList(size: List<MockData.Size>) {
        this.size = size
        notifyDataSetChanged()
    }

    fun disableSizes(list: ArrayList<String>) {
        for (i in 0 until size!!.size) {
            size!![i].isAvailable = list.contains(size!![i].size)
        }
        notifyDataSetChanged()
    }

    fun getSelectedSize(): String {
        return selected
    }

    fun cancelChoice() {
        for (item in size!!)
            item.isPicked = false
        selected = ""
        notifyDataSetChanged()
    }

    var onItemClick: ((MockData.Size) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.size_item, parent, false)
    )

    override fun getItemCount() = size?.size ?: 0

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(size!![position])
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView),
        View.OnClickListener {
        val outerCircle = itemView.sizeOuter
        val sizeDisabled = itemView.sizeDisabled
        val sizeText = itemView.sizeText
        val sizeFrameLayout = itemView.sizeFrameLayout

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(size!![adapterPosition])
            }
        }

        fun bindData(size: MockData.Size) {
            outerCircle.imageTintList = ColorStateList.valueOf(Color.WHITE)
            sizeFrameLayout.isSelected = size.isPicked
            if (size.isPicked) {
                sizeText.setTextColor(ColorStateList.valueOf(Color.WHITE))
                outerCircle.imageTintList = null
            } else {
                sizeText.setTextColor(ColorStateList.valueOf(Color.parseColor("#87B2BF")))
                outerCircle.imageTintList = ColorStateList.valueOf(Color.WHITE)
            }

            if (size.isAvailable)
                sizeDisabled.hide()
            else
                sizeDisabled.show()

            outerCircle.setOnClickListener(this)
            sizeText.text = size.size
            if (sizeText.text.length > 4) {
                sizeText.textSize = 12f
            }
            sizeText.setOnClickListener(this)
        }

        override fun onClick(p0: View?) {

            if (!size!![adapterPosition].isPicked) {
                if (size!![adapterPosition].isAvailable) {
                    if (selectedPosition != -1) {
                        size!![selectedPosition].isPicked = false
                        selected = ""
                    }
                    size!![adapterPosition].isPicked = true
                    outerCircle.imageTintList = null
                    selected = size!![adapterPosition].size

                    Log.i("color", "size picked $selected")
                    selectedPosition = adapterPosition

                    onItemClick?.invoke(size!![adapterPosition])
                }
            } else {
                Log.i("color", "size unpicked")
                size!![adapterPosition].isPicked = false
                outerCircle.imageTintList = ColorStateList.valueOf(Color.WHITE)
                selected = ""

                onItemClick?.invoke(size!![adapterPosition])
            }
            notifyDataSetChanged()
        }
    }
}