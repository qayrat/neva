package uz.neva.kidya.ui.home


import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import kotlinx.android.synthetic.main.item_news.view.*
import uz.neva.kidya.Const
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.NewsObject
import kotlin.collections.ArrayList

class FeaturedNewsListAdapter(val context: Context) :
    PagingDataAdapter<NewsObject, FeaturedNewsListAdapter.ViewHolder>(
        NEWS
    ) {

    var news: ArrayList<NewsObject>? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) = ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_news, parent, false)
    )

    var onItemClick: ((NewsObject) -> Unit)? = null

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.bindData(getItem(position) as NewsObject)
        getItem(position)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val itemImageLink = itemView.newsImage
        private val itemTitle = itemView.description
//        private val itemDate = itemView.newsItemDate

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(getItem(position) as NewsObject)
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        fun bindData(news: NewsObject) {
            itemTitle.text = news.name
//            itemDate.text = news.date
//            val formatter = DateTimeFormatter.ofPattern("dd.MM.yyyy HH:mm:ss")
//            if(news.date!=null)
//            {
//            val date = LocalDate.parse(news.date, formatter)
//                val monthsru = context.resources.getStringArray(R.array.months_ru)
//                itemDate.text = "${date.dayOfMonth} ${monthsru[date.monthValue-1]}"
//            }
            if (!news.preview_picture.isNullOrBlank())
                if (news.preview_picture[0] == '/') {
                    Glide.with(context).load("${Const.BASE_URL}${news.preview_picture}")
                        .override(
                            300, 300
                        ).into(itemImageLink)
                } else
                    Glide.with(context).load(news.preview_picture)
                        .override(
                            300, 300
                        ).into(itemImageLink)
        }
    }

    companion object {
        val NEWS = object : DiffUtil.ItemCallback<NewsObject>() {
            override fun areItemsTheSame(
                oldItem: NewsObject,
                newItem: NewsObject
            ): Boolean = oldItem.id == newItem.id


            override fun areContentsTheSame(
                oldItem: NewsObject,
                newItem: NewsObject
            ): Boolean =
                oldItem == newItem

        }
    }
}