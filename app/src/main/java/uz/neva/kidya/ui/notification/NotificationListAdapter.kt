package uz.neva.kidya.ui.notification

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.notification_item.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.NotificationObject

class NotificationListAdapter(): RecyclerView.Adapter<NotificationListAdapter.ViewHolder>()  {

    var notifications:List<NotificationObject>? = arrayListOf()

    fun updateList(notifications: List<NotificationObject>) {
        this.notifications = notifications
        notifyDataSetChanged()
    }

    var onItemClick: ((NotificationObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.notification_item, parent, false)
    )

    override fun getItemCount()=notifications?.size?:0


    override fun onBindViewHolder(holder: NotificationListAdapter.ViewHolder, position: Int) {
        holder.bindData(notifications!![position])
       // holder.collapse.setOnClickListener(this)
    }



    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener  {
        val title=itemView.notificationTitle
        val date=itemView.notificationDate
        val content=itemView.notificationContent
        var collapse=itemView.collapse

        override fun onClick(v: View?) {
            when(v?.id){
                R.id.collapse -> {
                    Log.d("clicked", "collapse clicked on item$adapterPosition")
                    if (notifications!![adapterPosition].collapsed) {
                        content.maxLines = 2
                        notifications!![adapterPosition].collapsed=false
                        collapse.setImageResource(R.drawable.ic_arrow_circle_down)
                    }
                    else {
                        content.maxLines = 100
                        notifications!![adapterPosition].collapsed = true
                        collapse.setImageResource(R.drawable.ic_arrow_circle_up)
                    }
                }
            }
        }


        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(notifications!![adapterPosition])
            }
        }

        fun bindData(notifications: NotificationObject) {
            title.text=notifications.text
            date.text=notifications.date
            content.text=notifications.preview_text
           // collapse.setOnClickListener(this)
        }

    }
}