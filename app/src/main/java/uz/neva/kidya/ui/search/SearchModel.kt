package uz.neva.kidya.ui.search

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.item_seach_history.view.*
import kotlinx.android.synthetic.main.live_search_layout.view.*
import uz.neva.kidya.R

data class SearchHistoryModel(
    val history: List<SearchHistory>? = null,
    val tags: List<String>? = null
)

class SearchHistory(
    val id: Int,
    val text: String
) : AbstractItem<SearchHistory.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<SearchHistory>(itemView) {
        override fun bindView(item: SearchHistory, payloads: List<Any>) {
            itemView.historyText.text = item.text
        }

        override fun unbindView(item: SearchHistory) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_seach_history
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)

    class DeleteOneHistoryItemClickEvent(private val listener: DeleteHistoryItemListener? = null) :
        ClickEventHook<SearchHistory>() {

        override fun onBindMany(viewHolder: RecyclerView.ViewHolder): List<View>? {
            if (viewHolder is VH) {
                return listOf(viewHolder.itemView.close)
            }
            return super.onBindMany(viewHolder)
        }

        override fun onClick(
            v: View,
            position: Int,
            fastAdapter: FastAdapter<SearchHistory>,
            item: SearchHistory
        ) {
            listener?.onDeleteItem(item)
        }
    }
}

interface DeleteHistoryItemListener {
    fun onDeleteItem(item: SearchHistory)
}

class LiveSearchResponse(val data: List<String>)

data class LiveSearchModel(
    val id: Int,
    val string: String
) : AbstractItem<LiveSearchModel.LiveViewHolder>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class LiveViewHolder(itemView: View) : FastAdapter.ViewHolder<LiveSearchModel>(itemView) {
        override fun bindView(item: LiveSearchModel, payloads: List<Any>) {
            itemView.liveSearch.text = item.string
        }

        override fun unbindView(item: LiveSearchModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.live_search_layout
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): LiveViewHolder = LiveViewHolder(v)
}