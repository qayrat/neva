package uz.neva.kidya.ui.category.selectedProduct

import android.os.Bundle
import android.view.View
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import kotlinx.android.synthetic.main.fragment_review_pager.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.AddReviewFragment
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.utils.showSnackbarAuth

class ReviewPagerFragment(val productId: String) : Fragment(R.layout.fragment_review_pager) {

    private val itemReviewImageAdapter = ItemAdapter<ReviewImageModel>()
    private val itemReviewImageFastAdapter = FastAdapter.with(itemReviewImageAdapter)
    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private lateinit var reviewsAdapter: ReviewsAdapter
    private val itemReviewAdapter = ItemAdapter<ReviewModel>()
    private val itemReviewFastAdapter = FastAdapter.with(itemReviewAdapter)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        //ReviewImageList
        reviewImageList.layoutManager =
            LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        reviewImageList.adapter = itemReviewImageFastAdapter

        //ReviewList
        reviewsAdapter = ReviewsAdapter()
        reviewList.layoutManager = LinearLayoutManager(requireContext())
        reviewList.adapter = itemReviewFastAdapter

        categoriesViewModel.getReview(productId)

        categoriesViewModel.getSelectedProductReviewsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        if (resource.data.status == "OK") {
                            val data = resource.data.data
                            if (data != null) {
                                itemReviewAdapter.clear()
                                for (i in 0 until data.size) {
                                    itemReviewAdapter.add(
                                        ReviewModel(
                                            id = data[i].id.toInt(),
                                            rating = data[i].rating,
                                            color = null,
                                            size = null,
                                            data[i].text,
                                            reviewPeople = data[i].user_name,
                                            reviewDate = data[i].date
                                        )
                                    )
                                }
                                review_image.isVisible = resource.data.data.isEmpty()
                                photo_review_2.isVisible = resource.data.data.isEmpty()
                                photo_review.isVisible = resource.data.data.isNotEmpty()
                                photo_review.text = "${data.size} ${resources.getString(R.string.text_count_review)}"
                            }

                        }
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 ->
//                            showSnackbar(it1)
//                        }
                    }
                }
            }
        })

        add_review.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isEmpty())
                showSnackbarAuth("вам необходимо")
            else {
                val bundle = Bundle()
                bundle.putString("product_id", productId)
                findNavController().navigate(R.id.addReviewFragment, bundle)
            }
        }
    }
}