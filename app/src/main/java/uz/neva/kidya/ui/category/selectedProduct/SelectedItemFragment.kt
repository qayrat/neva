package uz.neva.kidya.ui.category.selectedProduct

import android.annotation.SuppressLint
import android.content.Intent
import android.graphics.drawable.GradientDrawable
import android.os.Build
import android.os.Bundle
import android.util.DisplayMetrics
import android.util.Log
import android.view.*
import android.widget.LinearLayout
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.core.content.res.ResourcesCompat
import androidx.core.text.HtmlCompat
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager2.adapter.FragmentStateAdapter
import androidx.viewpager2.widget.ViewPager2
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.google.android.material.tabs.TabLayoutMediator
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_selected_item.*
import kotlinx.android.synthetic.main.fragment_selected_item.linearLayout
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.BuildConfig
import uz.neva.kidya.Const
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.*
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewProductData
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.*
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.ArrayList

class SelectedItemFragment : Fragment() {
    var paysytem = 2
    var delivery = 2
    private var width = 0
    private var height = 0
    private var onPause = false
    private var frequentlyList: List<ProductObject2>? = null
    private var reviewsList: ArrayList<ReviewObject>? = null
    private var startTime = ""
    private var finishTime = ""
    private var productId = ""
    private var sectionId = ""
    private var supplierId = ""
    private var supplierName = ""
    var selectedSize = ""
    var selectedColor = ""
    private var product: ProductObject? = null
    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private lateinit var imageCollectionAdapter: ImagesAdapter
    private lateinit var viewPager: ViewPager2
    private lateinit var colorListAdapter: ColorListAdapter
    private lateinit var sizeListAdapter: SizeListAdapter
    private lateinit var reviewsAdapter: ReviewsAdapter
    private lateinit var offersList: ArrayList<OfferObject>
    private lateinit var pagerAdapter: CollectionAdapter
    val imglist: ArrayList<String> = ArrayList()
    var selectedProductImage = ""
    private val itemAdapter = ItemAdapter<ProductObject2>()
    private val itemAdapter2 = ItemAdapter<ProductObject3>()
    private val fastAdapter2 =
            FastAdapter.with(itemAdapter2).addEventHook(object : ClickEventHook<ProductObject3>() {
                override fun onClick(
                        v: View,
                        position: Int,
                        fastAdapter: FastAdapter<ProductObject3>,
                        item: ProductObject3
                ) {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", item.fields.iblock_section_id)
                    bundle.putString("product_id", item.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }

                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                    return viewHolder.itemView
                }
            })
    private val fastAdapter =
            FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<ProductObject2>() {
                override fun onClick(
                        v: View,
                        position: Int,
                        fastAdapter: FastAdapter<ProductObject2>,
                        item: ProductObject2
                ) {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", item.fields.iblock_section_id)
                    bundle.putString("product_id", item.fields.id)
                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }

                override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                    return viewHolder.itemView
                }

            })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).bottom_navigation.hide()
        arguments?.let {
            sectionId = it.getString("section_id").toString()
            productId = it.getString("product_id").toString()
        }

        val displayMetrics = DisplayMetrics()
        requireActivity().windowManager.defaultDisplay?.getRealMetrics(displayMetrics)

        width = displayMetrics.widthPixels
        height = 280
        categoriesViewModel.getSelectedFrequentlyProducts(productId)
        categoriesViewModel.getSelectedMoreProducts(productId)

        imageCollectionAdapter = ImagesAdapter(requireContext())
        reviewsAdapter = ReviewsAdapter()
        reviewsList = ArrayList()
        startTime = SimpleDateFormat("yyyy-mm-dd, HH:mm:ss", Locale.getDefault()).format(Date())
    }

    override fun onCreateView(
            inflater: LayoutInflater, container: ViewGroup?,
            savedInstanceState: Bundle?
    ): View? {
        buyNow
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selected_item, container, false)
    }


    @RequiresApi(Build.VERSION_CODES.O)
    @SuppressLint("UseCompatLoadingForDrawables", "CommitPrefEdits", "SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        requireActivity().toolbar.setNavigationIcon(R.drawable.icon_back)
        requireActivity().toolbar.setNavigationOnClickListener {
            findNavController().popBackStack()
        }

        categoriesViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> { }
                    is Resource.GenericError -> { }
                    is Resource.Error -> { }
                }
            }
        })

        categoriesViewModel.getSelectedProductResponse(
            PrefManager.getUserID(requireContext()),
            productId,
            width = width,
            height = height
        )
        if (product != null) {
            pagerAdapter = CollectionAdapter(this, product!!)
            pager.adapter = pagerAdapter
            pager.isUserInputEnabled = false
            TabLayoutMediator(tab_layout, pager) { tab, position ->
                tab.text = when (position) {
                    0 -> resources.getString(R.string.text_material_title)
                    1 -> resources.getString(R.string.text_faq)
                    2 -> resources.getString(R.string.text_reviews)
                    else -> ""
                }
            }.attach()
        }

        selectedItemBrand.setOnClickListener {
            if (product != null) {
                val bundle = Bundle()
                bundle.putString("section_id", product!!.fields.suppiler_id)
                bundle.putString("section_name", product!!.fields.suppiler_name)
                findNavController().navigate(R.id.selectedShopFragment, bundle)
            }
        }

        swipeRefreshLayout.setOnRefreshListener {
            categoriesViewModel.getSelectedProductResponse(
                    PrefManager.getUserID(requireContext()),
                    productId,
                    width = width,
                    height = height
            )
            categoriesViewModel.getSelectedFrequentlyProducts(productId)
            categoriesViewModel.getSelectedMoreProducts(productId)
        }

        var quant = 1

        //imageCollectionAdapter = secondImageCollectionAdapter(this)
        viewPager = view.findViewById(R.id.selectedItemPager)
        viewPager.adapter = imageCollectionAdapter
        viewPager.offscreenPageLimit = 1

        viewPager.registerOnPageChangeCallback(object : ViewPager2.OnPageChangeCallback() {
            override fun onPageSelected(position: Int) {
                super.onPageSelected(position)
                count.text = "${position + 1}/${imageCollectionAdapter.images?.size}"
                selectedProductImage = imageCollectionAdapter.images?.get(position) ?: ""
            }
        })

        val root = tab_layout.getChildAt(0)
        if (root is LinearLayout) {
            (root as LinearLayout).showDividers = LinearLayout.SHOW_DIVIDER_MIDDLE
            val drawable = GradientDrawable()
            drawable.setColor(resources.getColor(R.color.text_color_faded))
            drawable.setSize(2, 1)
            (root as LinearLayout).dividerPadding = 10
            (root as LinearLayout).dividerDrawable = drawable
        }

        linearLayout.setOnClickListener {
            findNavController().navigate(R.id.nav_searchResult)
        }



        colorListAdapter = ColorListAdapter()
        colorRecycler.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        colorRecycler.adapter = colorListAdapter

        sizeListAdapter = SizeListAdapter()
        sizeRecycler.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        sizeRecycler.adapter = sizeListAdapter

        selectedItemRatingBar.rating = 5.0f

        reviewsRecycler.layoutManager = LinearLayoutManager(requireContext())
        reviewsRecycler.adapter = reviewsAdapter

        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()


        //        alsoLikeAdapter = SelectedCategoryAdapter2(requireContext())
//        list_also_like.layoutManager =
//        LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

        share.setOnClickListener {
            try {
                val shareIntent = Intent(Intent.ACTION_SEND)
                shareIntent.type = "text/plain"
                shareIntent.putExtra(Intent.EXTRA_SUBJECT, "NEVA")
                var shareMessage = "\nLet me recommend you this application\n\n"
                shareMessage =
                        "${shareMessage}https://play.google.com/store/apps/details?id=${BuildConfig.APPLICATION_ID}".trimIndent()
                shareIntent.putExtra(Intent.EXTRA_TEXT, shareMessage)
                startActivity(Intent.createChooser(shareIntent, "choose one"))
            } catch (e: Exception) {
                showSnackbarWithMargin(e.toString())
            }
        }

        list_frequently.layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)
        list_frequently.adapter = fastAdapter

        list_recommended.layoutManager =
                GridLayoutManager(requireContext(), 2)
        list_recommended.addItemDecoration(SpacesItemDecoration(marginpixels, true, 2))
        list_recommended.adapter = fastAdapter2

        colorRecycler.addItemDecoration(SpacesItemDecoration(marginpixels, false))
        sizeRecycler.addItemDecoration(SpacesItemDecoration(marginpixels, false, 1))

        selectedItemBack.setOnClickListener {
            findNavController().popBackStack()
        }

        itemBug.setOnClickListener {
            findNavController().navigate(R.id.nav_checkOut)
        }
        countBug.setOnClickListener {
            findNavController().navigate(R.id.nav_checkOut)
        }
        itemHome.setOnClickListener {
            findNavController().navigate(R.id.nav_home)
        }
        countBug.isVisible = PrefUtils.getProductListCount() > 0
        countBug.text = PrefUtils.getProductListCount().toString()

        selectedItemAddToCart.setOnClickListener {

            selectedSize = sizeListAdapter.getSelectedSize()
            selectedColor = (colorListAdapter.getSelectedColor()).removePrefix("#")
            var isValidOffer = false
            var offerId = ""
            for (offers in offersList) {
                if (offers.razmer == selectedSize) {
                    isValidOffer = true
                    offerId = offers.id
                    break
                }
            }
            if (PrefManager.getUserID(requireContext()).isEmpty()) {
                product?.let {
                    if (isValidOffer) {
                        val basketProduct = BasketItemObject(
                                offerId,
                                quantity.text.toString(),
                                it.fields.name,
                                selectedProductImage,
                                it.fields.catalog_price_1?.replace(",", " "),
                                suppiler = null,
                                color = selectedColor,
                                selectedSize,
                                false,
                                it.params
                        )
                        PrefUtils.setProduct(basketProduct)
                        showSnackbarWithMargin(resources.getString(R.string.text_added_snackbar_message))
                        countBug.isVisible = PrefUtils.getProductListCount() > 0
                        countBug.text = PrefUtils.getProductListCount().toString()

                        var s = 0
                        PrefUtils.getProduct().forEach { e -> s += e.quantity.toInt() }
                        (requireActivity() as MainActivity).updateBadge(s)

                    } else {
                        showSnackbarWithMargin(resources.getString(R.string.text_select_color_or_size))
                    }
                }
            } else {
                if (isValidOffer){
                    selectedItemAddToCart.visibility = View.GONE
                    addCart_progressbar.visibility = View.VISIBLE
                    checkOutViewModel.getAddToBasketResponse(
                            PrefManager.getUserID(requireContext()),
                            "basket",
                            quantity.text.toString(),
                            offerId,
                            "add",
                            selectedColor,
                            selectedSize
                    )
                } else {
                    showSnackbarWithMargin(resources.getString(R.string.text_select_color_or_size))
                }
            }
        }

        checkOutViewModel.addToBasketResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        changeUiStateEnabled(
                                true,
                                progressIndicatorAddToCart,
                                selectedItemAddToCart
                        )
                    }
                    is Resource.Success -> {
                        changeUiStateEnabled(
                                false,
                                progressIndicatorAddToCart,
                                selectedItemAddToCart
                        )
                        showMessageDialog(resources.getString(R.string.text_added_snackbar_message))
                        countBug.isVisible = resource.data.meta.count != 0
                        countBug.text = resource.data.meta.count!!.toString()
                        (requireActivity() as MainActivity).updateBadge(resource.data.meta.count!!.toInt())
                        selectedItemAddToCart.visibility = View.VISIBLE
                        addCart_progressbar.visibility = View.GONE
                    }
                    is Resource.GenericError -> {
                        changeUiStateEnabled(
                                false,
                                progressIndicatorAddToCart,
                                selectedItemAddToCart
                        )
                        selectedItemAddToCart.visibility = View.VISIBLE
                        addCart_progressbar.visibility = View.GONE
                        showMessageDialog(resource.errorResponse.jsonResponse.getString("error"))
//                        showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
                    }
                    is Resource.Error -> {
                        changeUiStateEnabled(
                                false,
                                progressIndicatorAddToCart,
                                selectedItemAddToCart
                        )
                        selectedItemAddToCart.visibility = View.VISIBLE
                        addCart_progressbar.visibility = View.GONE
                        resource.exception.message?.let { it1 -> showMessageDialog(it1) }
//                        showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
                    }
                }
            }
        })

        buyNow.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isEmpty()) {
                selectedSize = sizeListAdapter.getSelectedSize()
                selectedColor = (colorListAdapter.getSelectedColor()).removePrefix("#")
                var isValidOffer = false
                var offerId = ""
                for (offers in offersList) {
                    if (offers.razmer == selectedSize) {
                        isValidOffer = true
                        offerId = offers.id
                        break
                    }
                }
                product?.let {
                    if (isValidOffer) {
                        val basketProduct = BasketItemObject(
                                offerId,
                                quantity.text.toString(),
                                it.fields.name,
                                selectedProductImage,
                                it.fields.catalog_price_1?.replace(",", " "),
                                suppiler = null,
                                color = selectedColor,
                                selectedSize,
                                false
                        )
                        PrefUtils.setProductForBuyNow(basketProduct)
                        val bundle = Bundle()
                        bundle.putString("buyNow", "true")
                        findNavController().navigate(R.id.nav_login, bundle)
                    } else {
                        showSnackbarWithMargin(resources.getString(R.string.text_select_color_or_size))
                    }
                }
            } else {
                selectedSize = sizeListAdapter.getSelectedSize()
                selectedColor = (colorListAdapter.getSelectedColor()).removePrefix("#")
                var isValidOffer = false
                var offerId = ""
                for (offers in offersList) {
                    if (offers.razmer == selectedSize) {
                        isValidOffer = true
                        offerId = offers.id
                        break
                    }
                }

                if (isValidOffer) {
                    buyNow.visibility = View.GONE
                    buyNow_progressbar.visibility = View.VISIBLE
                    checkOutViewModel.getAddToBasketBuyNowResponse(
                            PrefManager.getUserID(requireContext()),
                            "basket",
                            quantity.text.toString(),
                            offerId,
                            "add",
                            selectedColor,
                            selectedSize
                    )
                } else
                    showSnackbarWithMargin(resources.getString(R.string.text_select_color_or_size))

            }
        }

        checkOutViewModel.addToBasketBuyNowResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(
//                            true,
//                            progressIndicatorAddToCart,
//                            selectedItemAddToCart
//                        )
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(
//                            false,
//                            progressIndicatorAddToCart,
//                            selectedItemAddToCart
//                        )
//                        showSnackbarWithMargin("Добавлена")
//                        countBug.text = resource.data.meta.count!!.toString()
//                        (requireActivity() as MainActivity).updateBadge(resource.data.meta.count!!.toInt())


                        checkOutViewModel.getGetCheckOutResponse(
                                PrefManager.getUserID(requireContext()),
                        )
                    }
                    is Resource.GenericError -> {
                        changeUiStateEnabled(
                                false,
                                progressIndicatorAddToCart,
                                selectedItemAddToCart
                        )
                        buyNow.visibility = View.VISIBLE
                        buyNow_progressbar.visibility = View.GONE
                        showMessageDialog(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        changeUiStateEnabled(
                                false,
                                progressIndicatorAddToCart,
                                selectedItemAddToCart
                        )
                        buyNow.visibility = View.VISIBLE
                        buyNow_progressbar.visibility = View.GONE
                        resource.exception.message?.let { it1 -> showMessageDialog(it1) }
//                        showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
                    }
                }
            }
        })

        checkOutViewModel.getCheckOutResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(true, progressIndicatorCheckOut, checkOutButton)
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
                        showSnackbarWithMargin(resources.getString(R.string.text_checkout))
//                        (requireActivity() as MainActivity).updateBadge(0)
                        PrefManager.saveOrderID(requireContext(), resource.data.order_id.toInt())
                        findNavController().navigate(R.id.shipmentDetailsFragment)
                    }
                    is Resource.GenericError -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
                        showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
                    }
                    is Resource.Error -> {
//                        changeUiStateEnabled(false, progressIndicatorCheckOut, checkOutButton)
                        showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })



        categoriesViewModel.selectedProductResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        shimmer_descriptions.stopShimmer()
                        shimmer_descriptions.visibility = View.GONE
                        descriptions_layout.visibility = View.VISIBLE
                        main_ll.visibility = View.VISIBLE


                        product = resource.data.product

                        if (product != null) {
                            pagerAdapter = CollectionAdapter(this, product!!)
                        }
                        pager.adapter = pagerAdapter
                        pager.isUserInputEnabled = false
                        TabLayoutMediator(tab_layout, pager) { tab, position ->
                            tab.text = when (position) {
                                0 -> resources.getString(R.string.text_material_title)
                                1 -> resources.getString(R.string.text_faq)
                                2 -> resources.getString(R.string.text_reviews)
                                else -> ""
                            }
                        }.attach()

                        supplierId = resource.data.product.fields.suppiler_id.toString()
                        selectedItemRatingBar.rating = 5.0f
                        selectedItemName.text = resource.data.product.fields.name

                        offersList = resource.data.product.offers as ArrayList<OfferObject>

                        if (resource.data.product.fields.preview_text != null)
                            selectedItemDescription.text = HtmlCompat.fromHtml(
                                    resource.data.product.fields.preview_text,
                                    HtmlCompat.FROM_HTML_MODE_LEGACY
                            )
                        else
                            selectedItemDescription.hide()
                        if (resource.data.product.fields.in_favourites == "Y") {
                            itemFavourite.setImageResource(R.drawable.ic_heart_filled)
                            itemFavourite.tag = "fav"
                        }
                        if (resource.data.product.fields.catalog_price_1 != null) {
                            // selectedItemPrice.formatCurrencyMask((resource.data.data[0].fields.catalog_price_1)!!.toFloat())
                            selectedItemPrice.text =
                                    "${
                                        (resource.data.product.fields.catalog_price_1).replace(
                                                ",",
                                                " "
                                        )
                                    } сум"


                        } else
                            selectedItemPrice.formatCurrencyMask(0.0f)

                        if (resource.data.product.fields.percent != null) {
                            selectedItemPrice.text =
                                    "${
                                        resource.data.product.fields.catalog_price_1?.replace(
                                                ",",
                                                " "
                                        )
                                    } сум"
                            if (resource.data.product.fields.old_price != null && resource.data.product.fields.old_price != "") {
                                selectedItemOldPrice.text =
                                    "${resource.data.product.fields.old_price} сум"
                                selectedItemOldPrice.visibility = View.VISIBLE
                                selectedItemTag.visibility = View.VISIBLE
                                selectedItemTag.text = "-" + resource.data.product.fields.percent + "%"
                            }
                        }

                        for (item in resource.data.product.params) {

                            when (item.name) {
                                "Поставщик" -> {
                                    selectedItemBrand.text = item.value[0].toString()
                                    supplierName = item.value[0].toString()
                                }
                                "Картинки" -> {
                                    imglist.clear()
                                    for (image in item.value.indices) {
                                        imglist.add("${Const.BASE_URL}${item.value[image]}")
                                    }
                                    imageCollectionAdapter.updateList(imglist)
                                    indicator.setupWithViewPager(viewPager)
                                }
                                "Цвет" -> {
                                    var list = ArrayList<MockData.Color>()

                                    for (color in item.value)
                                        list.add(MockData.Color("#${color}", false))
                                    colorLay.visibility = View.VISIBLE
                                    colorListAdapter.updateList(list)
                                    colorListAdapter.defaultSelect()
                                }
                                "Размер" -> {
                                    var list = ArrayList<MockData.Size>()

                                    for (size in item.value)
                                        list.add(MockData.Size(size, false, true))

                                    sizeLay.visibility = View.VISIBLE
                                    sizeListAdapter.updateList(list)

                                    updateSizesAndImages()
                                }
                                "Цена со скидкой" -> {
                                    selectedItemPrice.text = "${item.value[0]} сум"
                                    selectedItemOldPrice.text = "${resource.data.product.fields.catalog_price_1?.replace(",", " ")} сум"
                                    selectedItemOldPrice.visibility = View.VISIBLE

                                }
                                else -> {
                                    selectedItemDetailsLayout.addView(
                                            createParam(
                                                    item.name,
                                                    item.value[0].toString()
                                            )
                                    )
                                }

                            }
                        }
                    }
                    is Resource.GenericError -> {
                        shimmer_descriptions.stopShimmer()
                        shimmer_descriptions.visibility = View.GONE
                        descriptions_layout.visibility = View.VISIBLE
//                        main_ll.visibility = View.VISIBLE
                        showSnackbarWithMargin(getString(R.string.update_profile_something_wrong))
                    }
                    is Resource.Error -> {
                        shimmer_descriptions.stopShimmer()
                        shimmer_descriptions.visibility = View.GONE
                        descriptions_layout.visibility = View.VISIBLE
//                        showSnackbar(resource.exception.message.getString("Error on server"))
                        showSnackbarWithMargin(getString(R.string.update_profile_something_wrong))
//                        resource.exception.message?.let { it1 ->
//                            showSnackbar("Error on server: $it1")
//                        }
                    }
                }
                swipeRefreshLayout.isRefreshing = resource is Resource.Loading
            }
        })

        categoriesViewModel.selectedFrequentlyProductResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        frequentlyList = resource.data.frequently
                        frequentlyList?.let { list ->
                            ll_frequently.isVisible = list.isNotEmpty()
                            if (frequentlyList != null) {
                                itemAdapter.add(frequentlyList!!)
                            }
                        }
                    }

                    is Resource.GenericError -> {
//                        main_ll.visibility = View.VISIBLE
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("Error on server"))
                    }
                    is Resource.Error -> {
//                        showSnackbar(resource.exception.message.getString("Error on server"))
//                        resource.exception.message?.let { it1 ->
//                            showSnackbar("Error on server: $it1")
//                        }
                    }
                }
            }
        })

        categoriesViewModel.selectedMoreProductResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        shimmer_recommended.stopShimmer()
                        shimmer_recommended.visibility = View.GONE

                        val list = ArrayList<ProductObject3>()
                        resource.data.more_products.forEach { (t, u) ->
                            list.add(u)
                        }
                        ll_recommended.isVisible = list.isNotEmpty()
                        itemAdapter2.add(list)
                    }

                    is Resource.GenericError -> {
                        shimmer_recommended.stopShimmer()
                        shimmer_recommended.visibility = View.GONE
                        ll_recommended.visibility = View.GONE
//                        main_ll.visibility = View.VISIBLE
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("Error on server"))
                    }
                    is Resource.Error -> {
                        shimmer_recommended.stopShimmer()
                        shimmer_recommended.visibility = View.GONE
                        ll_recommended.visibility = View.GONE
//                        showSnackbar(resource.exception.message.getString("Error on server"))
//                        resource.exception.message?.let { it1 ->
//                            showSnackbar("Error on server: $it1")
//                        }
                    }
                }
            }
        })


        //imageCollectionAdapter.updateImageList(imglist)
        // indicator.setupWithViewPager(viewPager)
        indicator.setSliderColor(
                ContextCompat.getColor(requireContext(), R.color.indicator),
                ContextCompat.getColor(requireContext(), R.color.item_color)
        )

        //selectedItemFeedback.hide()
        // seklectedItemMessageBrand.hide()
        selectedItemFeedback.setOnClickListener {
            showSelectedItemFeedback.visibility = View.VISIBLE
            selectedItemDetailsLayout.visibility = View.GONE
            selectedItemFeedback.setBackgroundColor(
                    ContextCompat.getColor(
                            requireContext(),
                            R.color.transparent
                    )
            )
            selectedItemDetails.setBackgroundColor(
                    ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                    )
            )
            categoriesViewModel.getReviews(productId)
        }




        categoriesViewModel.selectedProductReviewsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        if (resource.data.data != null)
                            reviewsList = resource.data.data as ArrayList<ReviewObject>
                        reviewsAdapter.updateList(reviewsList!!)
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 ->
//                            showSnackbar(it1)
//                        }
                    }
                }
            }
        })


        colorListAdapter.onItemClick = {
            if (!updateOffer()) {
                //colorListAdapter.cancelChoice()
                //   showSnackbarWithMargin("not valid")
            }
        }


        sizeListAdapter.onItemClick = {
            if (!updateOffer()) {

                //sizeListAdapter.cancelChoice()
                // showSnackbarWithMargin("not valid")
            }
        }

        selectedItemDetails.setOnClickListener {
            showSelectedItemFeedback.visibility = View.GONE
            selectedItemDetailsLayout.visibility = View.VISIBLE
            selectedItemFeedback.setBackgroundColor(
                    ContextCompat.getColor(
                            requireContext(),
                            R.color.white
                    )
            )
            selectedItemDetails.setBackgroundColor(
                    ContextCompat.getColor(
                            requireContext(),
                            R.color.transparent
                    )
            )
        }

        incquantity.setOnClickListener {
            quant = quant.inc()
            quantity.text = "$quant"
        }
        decquantity.setOnClickListener {
            if (quant > 1) {
                quant = quant.dec()
            }
            quantity.text = "$quant"
        }

        itemFavourite.setOnClickListener {
            if (PrefManager.getUserID(requireContext()).isEmpty()) {
                showSnackbarAuth(resources.getString(R.string.text_need_auth))
            } else {
                if (itemFavourite.tag == "notfav") {
                    checkOutViewModel.getAddToFavsResponse(
                            PrefManager.getUserID(requireContext()),
                            "favs",
                            productId,
                            "add"
                    )
                    checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
                        it.getContentIfNotHandled()?.let { resource ->
                            when (resource) {
                                is Resource.Loading -> {
                                    changeUiStateEnabled(
                                            true,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                }
                                is Resource.Success -> {
                                    changeUiStateEnabled(
                                            false,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                    itemFavourite.setImageResource(R.drawable.ic_heart_filled)
                                    itemFavourite.tag = "fav"
                                    showSnackbarWithMargin(resources.getString(R.string.text_added_snackbar_message))
                                }
                                is Resource.GenericError -> {
                                    changeUiStateEnabled(
                                            false,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                    showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
//                                    showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                                }
                                is Resource.Error -> {
                                    changeUiStateEnabled(
                                            false,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                    showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
//                                    resource.exception.message?.let { it1 -> showSnackbar(it1) }
                                }
                            }
                        }
                    })
                } else {
                    checkOutViewModel.getDeleteFromFavsResponse(
                            PrefManager.getUserID(requireContext()),
                            "favs",
                            productId,
                            "delete"
                    )
                    checkOutViewModel.deleteFromFavsResponse.observe(viewLifecycleOwner, Observer {
                        it.getContentIfNotHandled()?.let { resource ->
                            when (resource) {
                                is Resource.Loading -> {
                                    changeUiStateEnabled(
                                            true,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                }
                                is Resource.Success -> {
                                    changeUiStateEnabled(
                                            false,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                    itemFavourite.setImageResource(R.drawable.ic_favourite)
                                    itemFavourite.tag = "notfav"
                                    showSnackbarWithMargin(resources.getString(R.string.text_delete_snackbar_message))
                                }
                                is Resource.GenericError -> {
                                    changeUiStateEnabled(
                                            false,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                    showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
//                                    showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                                }
                                is Resource.Error -> {
                                    changeUiStateEnabled(
                                            false,
                                            progressIndicatorAddToCart,
                                            itemFavourite
                                    )
                                    showSnackbarWithMargin(getString(R.string.edit_profile_something_wrong))
//                                    resource.exception.message?.let { it1 -> showSnackbar(it1) }
                                }
                            }
                        }
                    })

                }
            }

        }

        categoriesViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        Timber.d("${resource.data.status}, ${resource.data.data}")
                    }
                    is Resource.GenericError -> {
                        Timber.d(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
                        Timber.d(resource.exception.localizedMessage)
                    }
                }
            }
        })
    }

    private fun showMessageDialog(s: String) {
        MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_App_MaterialAlertDialog)
                .setMessage(s)
                .setPositiveButton(getString(R.string.ok)) {dialog, which ->
                    dialog.dismiss() }
                .show()
    }

    override fun onStart() {
        super.onStart()
        (requireActivity() as MainActivity).toolbar.show()
    }

    fun updateSizesAndImages() {

        val img: ArrayList<String> = ArrayList()

        val availableSizes: ArrayList<String> = ArrayList()
        for (offers in offersList) {
            if (offers.color == (colorListAdapter.getSelectedColor()).removePrefix("#")) {
                availableSizes.add(offers.razmer!!)
            }
        }
        for (offers in offersList) {
            if (offers.color == (colorListAdapter.getSelectedColor()).removePrefix("#")) {
                if (offers.more_photo != null) {
                    if (offers.more_photo[0] != "") {
                        for (i in offers.more_photo) img.add("${Const.BASE_URL}${i}")
                        imageCollectionAdapter.updateList(img)
                        imageCollectionAdapter.notifyDataSetChanged()
                        indicator.setupWithViewPager(viewPager)
                        break
                    }
                    imageCollectionAdapter.updateList(imglist)
                    indicator.setupWithViewPager(viewPager)
                } else {
                    imageCollectionAdapter.updateList(imglist)
                    imageCollectionAdapter.notifyDataSetChanged()
                    indicator.setupWithViewPager(viewPager)
                }

            }
        }
        sizeListAdapter.disableSizes(availableSizes)
    }

    fun updateOffer(): Boolean {

        val selectedSize = sizeListAdapter.getSelectedSize()
        val selectedColor = (colorListAdapter.getSelectedColor()).removePrefix("#")

        var isValidOffer = false
        var offerId = ""
        var price = ""
        val img: ArrayList<String> = ArrayList()

        updateSizesAndImages()


        for (offers in offersList) {
            if (offers.color == selectedColor && offers.razmer == selectedSize) {
                isValidOffer = true
                offerId = offers.id
                price = offers.price.toString()

                selectedItemPrice.text =
                        "${price} сум"
                if (offers.more_photo != null) {
                    if (offers.more_photo[0] != "") {
                        for (i in offers.more_photo)
                            img.add("${Const.BASE_URL}${i}")

                        imageCollectionAdapter.updateList(img)
                        imageCollectionAdapter.notifyDataSetChanged()
                        indicator.setupWithViewPager(viewPager)
                    }
                } else {
                    imageCollectionAdapter.updateList(imglist)
                    imageCollectionAdapter.notifyDataSetChanged()
                    indicator.setupWithViewPager(viewPager)
                }
                return true
            }
        }
        sizeListAdapter.cancelChoice()
        return false

    }

    fun createParam(name: String, value: String): LinearLayout {
        val layout = LinearLayout(requireContext())

        val scale = resources.displayMetrics.density
        val paddingpixels = (4 * scale + 0.5f).toInt()
        val marginpixels = (16 * scale + 0.5f).toInt()

        val paramsForLayout = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f
        )

        val params = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        )

        val paramsName = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f,
        )
        val paramsValue = LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT,
                1.0f,
        )
        paramsForLayout.setMargins(0, 0, marginpixels, 0)
        layout.layoutParams = paramsForLayout
        layout.setPadding(paddingpixels, paddingpixels, paddingpixels, paddingpixels)

        paramsName.gravity = Gravity.START
        paramsValue.gravity = Gravity.END
        val paramName = TextView(requireContext())
        val paramValue = TextView(requireContext())

        paramName.text = name
        val typeface = ResourcesCompat.getFont(requireContext(), R.font.tt_nunito_regular)
        paramName.typeface = typeface
        paramName.textSize = 18.0f
        paramName.setTextColor(ContextCompat.getColor(requireContext(), R.color.text_selector))
        paramName.layoutParams = paramsName


        paramValue.text = value
        paramValue.typeface = ResourcesCompat.getFont(requireContext(), R.font.tt_nunito_regular)
        paramValue.textAlignment = TextView.TEXT_ALIGNMENT_TEXT_END
        paramValue.textSize = 18.0f
        paramValue.setTextColor(ContextCompat.getColor(requireContext(), R.color.text_selector))
        paramValue.layoutParams = paramsValue

        layout.addView(paramName)
        layout.addView(paramValue)
        return layout
    }

//    private fun timeToSeconds(): String {
//        val simpleDateFormat = SimpleDateFormat("hh:mm:ss")
//        var difference: Long = finishTime.time - startTime.time
//        if (difference < 0) {
//            val dateMax: Date = simpleDateFormat.parse("24:00:00")
//            val dateMin: Date = simpleDateFormat.parse("00:00:00")
//            difference = (dateMax.time - startTime.time) + (finishTime.time - dateMin.time)
//        }
//        val days: Int = (difference - (1000*60*60*24)).toInt()
//        val hours: Int = ((difference - (1000*60*60*24*days)) / (1000*60*60)).toInt()
//        val minute: Int = (((difference - (1000*60*60*24*days)) / (1000*60*60*hours)) / (1000*60)).toInt()
//        return "$hours:$minute"
//    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).bottom_navigation.hide()
        if (onPause) {
            shimmer_descriptions.stopShimmer()
            shimmer_descriptions.visibility = View.GONE
            descriptions_layout.visibility = View.VISIBLE

            shimmer_recommended.stopShimmer()
            shimmer_recommended.visibility = View.GONE
            ll_recommended.visibility = View.VISIBLE

            if (frequentlyList != null) {
                ll_frequently.isVisible = frequentlyList!!.isNotEmpty()
            }
            if (reviewsList!!.isNotEmpty()) {
                showSelectedItemFeedback.visibility = View.VISIBLE
            } else {
                showSelectedItemFeedback.visibility = View.GONE
            }
        } else {
            shimmer_descriptions.startShimmer()
            shimmer_recommended.startShimmer()
        }
    }

    override fun onPause() {
        super.onPause()
        onPause = true
        shimmer_recommended.stopShimmer()
        shimmer_descriptions.stopShimmer()
    }

    override fun onDestroyView() {
        super.onDestroyView()
        finishTime = SimpleDateFormat("yyyy-mm-dd, HH:mm:ss", Locale.getDefault()).format(Date())
        categoriesViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewProductData(
                        type = "view_product",
                        product = productId.toInt(),
                        dateto = startTime,
                        datefrom = finishTime)
        ))
    }
}

class CollectionAdapter(fragment: Fragment, val product: ProductObject) :
        FragmentStateAdapter(fragment) {
    override fun getItemCount(): Int = 3

    override fun createFragment(position: Int): Fragment {
        return when (position) {
            0 -> DetailsFragment(product)
            1 -> QuestionAnswerFragment(product)
            2 -> ReviewPagerFragment(product.fields.id)
            else -> Fragment()
        }
    }

}
