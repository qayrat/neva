package uz.neva.kidya.ui.my_orders

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_selected_order.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.BasketItemObject
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.formatPhoneMask
//import uz.usoft.kidya.utils.showSnackbarWithMargin


class SelectedOrderFragment : Fragment() {

    var orderId=""

    var statuses:HashMap<String, String> = HashMap()

    val orderBundle=Bundle()
    private lateinit var selectedOrderItemListAdapter: SelectedOrderItemListAdapter
    private val profileViewModel: ProfileViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            orderId=it.getString("order_id").toString()
        }
        statuses=(requireActivity() as MainActivity).statuses
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selected_order, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        selectedOrderBack.setOnClickListener{
            findNavController().popBackStack()
            //findNavController().navigate(R.id.nav_myOrders)
        }
        selectedOrderItemListAdapter=SelectedOrderItemListAdapter(requireContext())
        orderItemsRecycler.layoutManager=LinearLayoutManager(requireContext())
        orderItemsRecycler.adapter=selectedOrderItemListAdapter
        feedBackPhone.formatPhoneMask()
        profileViewModel.getGetOrderDetailsResponse(
            PrefManager.getUserID(requireContext()),
            orderId
        )
        profileViewModel.getOrderDetailsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {

                        orderBundle.putSerializable("selectedOrder",resource.data )


                        //adapter.updateList(resource.data.data)
                        selectedOrderNumber.text = resource.data.data[0].id
                        selectedOrderPrice.text = "${resource.data.data[0].price} сум"
                        selectedOrderDeliveryFee.text =
                            "${resource.data.data[0].price_delivery} сум"
                        selectedOrderTotal.text = "${resource.data.data[0].total_price} сум"
                        selectedOrderTimeline.text = resource.data.data[0].status_name
                        if (resource.data.data[0].status_id == "N")
                            makePayment.visibility = View.VISIBLE
                        else
                            makePayment.visibility = View.GONE
                        //selectedOrderStatus.text=resource.data.paysystem?.name
                        selectedOrderItemListAdapter.updateList(resource.data.products as ArrayList<BasketItemObject>)
                        selectedOrderStatus.text = resource.data.paysystem
                        feedBackName.setText(resource.data.props.fio)
                        feedBackPhone.setText(resource.data.props.phone)
                        if (resource.data.props.region.isNotBlank()) {
                            val region = resources.getStringArray(R.array.district)
                            FeedbackAddressSpinnerOne.setSelection(region.indexOf(resource.data.props.region))
                        }
                        if (resource.data.props.city.isNotBlank()) {
                            val city = resources.getStringArray(R.array.city)
                            FeedbackAddressSpinnerOneTwo.setSelection(city.indexOf(resource.data.props.city))
                        }
                        //FeedbackAddressSpinnerOne.setText(resource.data.props.phone)
                        feedBackAddress.setText(resource.data.props.street)
                        feedBackComment.setText(resource.data.props.comment)
                    }
                    is Resource.GenericError -> {
//                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbarWithMargin(it1) }
                    }
                }
            }
        })
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.district,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
            FeedbackAddressSpinnerOne.adapter = adapter
        }
        ArrayAdapter.createFromResource(
            requireContext(),
            R.array.city,
            R.layout.spinner_lay
        ).also { adapter ->
            // Specify the layout to use when the list of choices appears
            adapter.setDropDownViewResource(R.layout.spinner_dropdown_lay)
            // Apply the adapter to the spinner
            FeedbackAddressSpinnerOneTwo.adapter = adapter
        }
        makePayment.setOnClickListener {
            profileViewModel.getMakePaymentResponse(orderId)
            profileViewModel.makePaymentResponse.observe(viewLifecycleOwner, Observer {
                it.getContentIfNotHandled()?.let { resource ->
                    when (resource) {
                        is Resource.Loading -> {
                            makePayment.isEnabled=false
                        }
                        is Resource.Success -> {
                            makePayment.isEnabled=true

                        val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse(resource.data.data))
                        startActivity(browserIntent)
                        }
                        is Resource.GenericError -> {
                            makePayment.isEnabled=true
//                            showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                        }
                        is Resource.Error -> {
                            makePayment.isEnabled=true
//                            resource.exception.message?.let { it1 -> showSnackbarWithMargin(it1) }
                        }
                    }
                }
            })
//
//            val browserIntent = Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"))
//            startActivity(browserIntent)
        }
        leaveFeedback.setOnClickListener{
            val newDialog= FeedbackDialogFragment()
            val fm = requireActivity().supportFragmentManager
            newDialog.show(fm, "success")
            newDialog.isCancelable = true
        }

        returnProduct.setOnClickListener {

            findNavController().navigate(R.id.action_nav_selectedOrder_to_nav_returnProducts,orderBundle)
        }
    }
}