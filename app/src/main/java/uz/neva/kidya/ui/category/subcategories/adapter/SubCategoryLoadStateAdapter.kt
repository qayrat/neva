package uz.neva.kidya.ui.category.subcategories.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.view.isVisible
import androidx.paging.LoadState
import androidx.paging.LoadStateAdapter
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.load_state_view.view.*
import uz.neva.kidya.R

class SubCategoryLoadStateAdapter(
        private val retry: () -> Unit
) : LoadStateAdapter<SubCategoryLoadStateAdapter.LoadStateViewHolder>() {
    override fun onBindViewHolder(holder: LoadStateViewHolder, loadState: LoadState) {
        val progress = holder.itemView.load_state_progress
        val btnRetry =  holder.itemView.load_state_retry
        val txtErrorMessage = holder.itemView.load_state_errorMessage

        btnRetry.isVisible = loadState !is LoadState.Loading
        txtErrorMessage.isVisible = loadState !is LoadState.Loading
        progress.isVisible = loadState is LoadState.Loading

        btnRetry.setOnClickListener {
            retry.invoke()
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, loadState: LoadState): LoadStateViewHolder {
        val view = LayoutInflater.from(parent.context)
                .inflate(R.layout.load_state_view, parent, false)
        return LoadStateViewHolder(view)
    }

    class LoadStateViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView)
}