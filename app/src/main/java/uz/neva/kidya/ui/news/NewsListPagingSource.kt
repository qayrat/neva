package uz.neva.kidya.ui.news

import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.dto.NewsObject
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class NewsListPagingSource (private val api:KidyaApi,val isHomeNews:Boolean=false):PagingSource<Int,NewsObject>(){
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, NewsObject> {
        val position =params.key?: STARTING_PAGE_INDEX

        return try {
            val response=if(isHomeNews)api.getNews(position,"6") else api.getNews(position)
            val feedPost=response.data
            LoadResult.Page(
                data=feedPost,
                prevKey = if(position== STARTING_PAGE_INDEX)null else position-1,
                nextKey = when {
                    isHomeNews->{
                        null
                    }
                    response.pagination.total_count==1->{
                        null
                    }
                    feedPost.isEmpty() -> {
                        null
                    }
                    response.pagination.current_page == response.pagination.total_count -> {
                        position + 1

                    }
                    response.pagination.current_page < response.pagination.page_count -> {
                        position + 1
                    }
                    else -> null
                }
            )
        }
        catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, NewsObject>): Int? =null
}