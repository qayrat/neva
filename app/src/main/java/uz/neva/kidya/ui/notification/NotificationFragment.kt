package uz.neva.kidya.ui.notification

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import androidx.fragment.app.Fragment
import androidx.core.view.isVisible
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import com.yariksoffice.lingver.Lingver
import kotlinx.android.synthetic.main.coupon_fragment_layout.view.*
import kotlinx.android.synthetic.main.fragment_check_out.*
import kotlinx.android.synthetic.main.fragment_notification.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.NotificationModel
import uz.neva.kidya.utils.*

//import uz.usoft.kidya.utils.showSnackbar

class NotificationFragment : Fragment(), OnDeleteNotificationItemListener {
    private val itemAdapter = ItemAdapter<NotificationModel>()
    private val fastAdapter =
        FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<NotificationModel>() {
            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<NotificationModel>,
                item: NotificationModel
            ) {
                notificationClickEvent(item)
            }

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView
            }
        })

    private var item: NotificationModel? = null

    private lateinit var adapter: NotificationListAdapter

    private val viewModel: NotificationViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_notification, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter = NotificationListAdapter()
        notificationRecyclerView.layoutManager = LinearLayoutManager(requireContext())
//        notificationRecyclerView.adapter=adapter
        notificationRecyclerView.adapter = fastAdapter


        notificationBack.setOnClickListener {
            findNavController().popBackStack()
        }

        adapter?.onItemClick = {
            viewModel.notificationMarkAsRead(PrefManager.getUserID(requireContext()), it.id)
        }
        readAll.setOnClickListener {
            openAgreeDialog()
        }
        var itemTouchHelper = ItemTouchHelper(SwipeDeleteToNotification(itemAdapter, this))
        itemTouchHelper.attachToRecyclerView(notificationRecyclerView)


        viewModel.getNotifications(PrefManager.getUserID(requireContext()))
        viewModel.notificationMarkAsReadResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(true, blockerView, notificationRecyclerView)
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(false, blockerView, notificationRecyclerView)
                        if (this.item != null) {
                            itemAdapter.remove(itemAdapter.getAdapterPosition(this.item!!))
                        } else {
                            itemAdapter.clear()
                        }
                        viewModel.getNotifications(PrefManager.getUserID(requireContext()))
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
//                        changeUiStateEnabled(false, blockerView, notificationRecyclerView)
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
//                        changeUiStateEnabled(false, blockerView, notificationRecyclerView)
                    }
                }
            }
        })

        viewModel.notificationAllResponse.observe(viewLifecycleOwner, {
            it?.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(true, blockerView, notificationRecyclerView)
                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(false, blockerView, notificationRecyclerView)

                        itemAdapter.clear()

                        viewModel.getNotifications(PrefManager.getUserID(requireContext()))
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
//                        changeUiStateEnabled(false, blockerView, notificationRecyclerView)
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
//                        changeUiStateEnabled(false, blockerView, notificationRecyclerView)
                    }
                }
            }
        })

        viewModel.getNotificationsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        changeUiStateEnabled(
                            true,
                            progressIndicatorNotification,
                            notificationRecyclerView
                        )
                    }
                    is Resource.Success -> {
                        itemAdapter.clear()
                        val list = resource.data.data
                        itemAdapter.add(list)
                        notificationRecyclerView.isVisible = list.isNotEmpty()
                        notificationIsEmpty.isVisible = list.isEmpty()
                        noNotifications.isVisible = list.isEmpty()
//                        adapter.updateList(resource.data.data)
                        changeUiStateEnabled(
                            false,
                            progressIndicatorNotification,
                            notificationRecyclerView
                        )

                        (requireActivity() as MainActivity).updateNotify(list.size)
//                        if (adapter.itemCount==0)
//                            noNotifications.show()
//                        else
//                            noNotifications.hide()
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                        changeUiStateEnabled(
                            false,
                            progressIndicatorNotification,
                            notificationRecyclerView
                        )
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                        changeUiStateEnabled(
                            false,
                            progressIndicatorNotification,
                            notificationRecyclerView
                        )
                    }
                }
            }
        })
    }

    private fun openAgreeDialog() {
        val dialogFragment =
            MaterialAlertDialogBuilder(requireContext(), R.style.MyCustomAlertDialog)
        dialogFragment.apply {
            setTitle(requireActivity().resources.getString(R.string.text_clear_all_title))
            setMessage(requireActivity().resources.getString(R.string.text_clear_all_message))
        }
        dialogFragment.setNegativeButton(R.string.text_cancel) { dialog, which ->
            dialog.dismiss()
        }
        dialogFragment.setPositiveButton("OK") { dialog, which ->
            viewModel.notificationAllRead(PrefManager.getUserID(requireContext()), all = "y")
        }

        dialogFragment.show()
    }

    private fun notificationClickEvent(item: NotificationModel) {
        when (item.notify_type) {
            "seller" -> {
                val bundle: Bundle = Bundle()
                bundle.putString("section_id", item.object_id)
                bundle.putString("section_name", "")
                findNavController().navigate(R.id.selectedShopFragment, bundle)
            }
            "special" -> {
//                val dialog = CouponDialogFragment()
//                if (dialog.isAdded) {
//                    return
//                } else {
//                    dialog.show(
//                        (requireActivity() as MainActivity).supportFragmentManager,
//                        CouponDialogFragment.TAG
//                    )
//                }
            }
            "new_order" -> {
                val bundle = Bundle()
                bundle.putString("order_id", item.object_id)
                findNavController().navigate(R.id.trackOrderFragment, bundle)
            }
            "return" -> {
                val bundle = Bundle()
                bundle.putString("order_id", item.object_id)
                findNavController().navigate(R.id.trackOrderFragment, bundle)
            }
            "coupon" -> {
//                val dialog = CouponDialogFragment()
//                if (dialog.isAdded) {
//                    return
//                } else {
//                    dialog.show(
//                        (requireActivity() as MainActivity).supportFragmentManager,
//                        CouponDialogFragment.TAG
//                    )
//                }
            }
            "order_status" -> {
                val bundle = Bundle()
                bundle.putString("order_id", item.object_id)
                findNavController().navigate(R.id.trackOrderFragment, bundle)
            }
            else -> {

            }
        }

    }



    override fun onSwipeToDelete(item: NotificationModel) {
        viewModel.notificationMarkAsRead(
            PrefManager.getUserID(requireContext()),
            item.id.toString()
        )
        this.item = item
    }

}

class CouponDialogFragment : DialogFragment() {

    companion object {
        const val TAG = "CouponDialogFragment"
    }

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
            WindowManager.LayoutParams.MATCH_PARENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
            DialogFragment.STYLE_NORMAL,
            android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen
        )
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(
            DialogFragment.STYLE_NORMAL,
            android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen
        )

        val builder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.coupon_fragment_layout, null)

        builder.setView(dialogView)

        val couponDialog = builder.create()
        couponDialog.window?.attributes?.windowAnimations = R.style.CouponDialogAnimation;
        val wmlp = couponDialog.window?.attributes

        wmlp?.gravity = Gravity.CENTER_VERTICAL or Gravity.CENTER_HORIZONTAL
        wmlp?.x = 100 //x position

        wmlp?.y = 0 //y position

        dialogView.ok_button.setOnClickListener {
            dialog?.dismiss()
        }

        return couponDialog
    }
}