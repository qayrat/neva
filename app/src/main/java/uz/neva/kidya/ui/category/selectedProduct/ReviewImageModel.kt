package uz.neva.kidya.ui.category.selectedProduct

import android.annotation.SuppressLint
import android.view.View
import androidx.core.view.isVisible
import com.bumptech.glide.Glide
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.item_q_a.view.*
import kotlinx.android.synthetic.main.item_review.view.*
import kotlinx.android.synthetic.main.item_review_image.view.*
import uz.neva.kidya.R

class ReviewImageModel(
    val id: Int,
    val image: Int
) : AbstractItem<ReviewImageModel.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<ReviewImageModel>(itemView) {
        override fun bindView(item: ReviewImageModel, payloads: List<Any>) {
            Glide.with(itemView.context)
                .load(item.image)
                .placeholder(R.drawable.ic_baseline_broken_image_24)
                .into(itemView.reviewImage)
        }

        override fun unbindView(item: ReviewImageModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_review_image
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}

class ReviewModel(
    val id: Int,
    val rating: Float?,
    val color: String?,
    val size: String?,
    val reviewText: String?,
    val reviewPeople: String?,
    val reviewDate: String?=""
) : AbstractItem<ReviewModel.VH>() {

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<ReviewModel>(itemView) {
        @SuppressLint("SetTextI18n")
        override fun bindView(item: ReviewModel, payloads: List<Any>) {
            itemView.apply {
                reviewRatingBar.rating = item.rating?.toFloat()?:0f
                reviewColor.text = "Цвет: ${item.color}  |  Размер: ${item.size}"
                reviewText.text = item.reviewText
                reviewData.text = "${item.reviewPeople} ${item.reviewDate}"
            }
        }

        override fun unbindView(item: ReviewModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_review
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}

data class FAQData(
    val data:List<QuestionAnswerModel>,
    val status:String,
    val error:String,
)

class QuestionAnswerModel(
    val id: Int,
    val question:String,
    val answer:String?,
    val user:String,
    val date:String
):AbstractItem<QuestionAnswerModel.VH>(){
    override var identifier: Long
        get() = id.toLong()
        set(value) {}
    class VH(itemView: View):FastAdapter.ViewHolder<QuestionAnswerModel>(itemView){
        override fun bindView(item: QuestionAnswerModel, payloads: List<Any>) {
            itemView.apply {
                questionText.text = item.question
                answerText.text = item.answer?:"Пока нет ответа"
                ic_answer.isVisible = item.answer!=null
                qaDateText.text = item.date
            }
        }

        override fun unbindView(item: QuestionAnswerModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_q_a
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}