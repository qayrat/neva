package uz.neva.kidya.ui.search

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.text.Editable
import android.text.Html
import android.text.Spanned
import android.text.TextWatcher
import android.util.Log
import android.view.KeyEvent
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.inputmethod.EditorInfo
import android.widget.TextView
import androidx.core.view.isVisible
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.google.android.flexbox.*
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.listeners.ClickEventHook
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_search_result.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.adapter.TagModel
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.utils.*


class SearcgResultFragment : Fragment(), DeleteHistoryItemListener {
    private lateinit var searchHistoryItem: SearchHistory
    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private val itemAdapter = ItemAdapter<SearchHistory>()
    private val fastAdapter =
        FastAdapter.with(itemAdapter).addEventHook(object : ClickEventHook<SearchHistory>() {

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView
            }

            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<SearchHistory>,
                item: SearchHistory
            ) {
                searchView.text?.clear()
                searchView.setText(item.text)
            }

        })

    private val homeViewModel: HomeViewModel by viewModel()
    private val tagItemAdapter = ItemAdapter<TagModel>()
    private val tagFastAdapter =
        FastAdapter.with(tagItemAdapter).addEventHook(object : ClickEventHook<TagModel>() {
            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<TagModel>,
                item: TagModel
            ) {
                searchView.text?.clear()
                searchView.setText(item.tagText)
            }

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView
            }

        })

    var searchValue = ""
    private lateinit var categoryAdapter: SelectedCategoryAdapter
    private val liveSearchItemAdapter = ItemAdapter<LiveSearchModel>()
    private val liveSearchFastAdapter = FastAdapter.with(liveSearchItemAdapter)
        .addEventHook(object : ClickEventHook<LiveSearchModel>() {
            override fun onClick(
                v: View,
                position: Int,
                fastAdapter: FastAdapter<LiveSearchModel>,
                item: LiveSearchModel
            ) {
                searchView.text?.clear()
                searchView.setText(item.string)
                getFeedPosts(item.string)
            }

            override fun onBind(viewHolder: RecyclerView.ViewHolder): View? {
                return viewHolder.itemView
            }
        })

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            searchValue = it.getString("searchValue").toString()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_search_result, container, false)
    }

    @SuppressLint("StringFormatMatches")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoriesViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Search")
        ))
        categoriesViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> { }
                    is Resource.GenericError -> { }
                    is Resource.Error -> { }
                }
            }
        })

        categoryAdapter = SelectedCategoryAdapter(requireContext())

        searchTitle.text = searchValue

        searchRecyclerView.layoutManager =
            GridLayoutManager(requireContext(), 2, LinearLayoutManager.VERTICAL, false)

        historyList.layoutManager = LinearLayoutManager(requireContext())
        historyList.adapter = fastAdapter

        liveSearchRV.layoutManager = LinearLayoutManager(requireContext())
        liveSearchRV.adapter = liveSearchFastAdapter

        fastAdapter.addEventHook(SearchHistory.DeleteOneHistoryItemClickEvent(this))

        val layoutManager = FlexboxLayoutManager(requireContext()).apply {
            flexWrap = FlexWrap.WRAP
            flexDirection = FlexDirection.ROW
            alignItems = AlignItems.STRETCH
        }
        tagList.layoutManager = layoutManager
        tagList.adapter = tagFastAdapter

        homeViewModel.getSearchHistory(PrefManager.getUserID(requireContext()))
        homeViewModel.getSearchResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        itemAdapter.clear()
                        if (resource.data.history != null) {
                            if (resource.data.history.size >= 5) {
                                for (i in 0 until 5) {
                                    itemAdapter.add(resource.data.history[i])
                                }
                            } else {
                                itemAdapter.add(resource.data.history)
                            }
                        }
                        val isHasData = resource.data.history != null

                        some_id.isVisible = isHasData
                        delete.isVisible = isHasData
                        historyList.isVisible = isHasData


                    }
                    else -> {

                    }
                }
            }
        })

        homeViewModel.clearOnlyOneResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        itemAdapter.remove(itemAdapter.getAdapterPosition(searchHistoryItem))
                    }
                    else -> {

                    }
                }
            }
        })

        homeViewModel.getLiveResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        liveSearchItemAdapter.clear()
                        val liveSearchList = resource.data.data
                        liveSearchRV.isVisible = liveSearchList.isNotEmpty()

                        for (i in liveSearchList.indices) {
                            liveSearchItemAdapter.add(LiveSearchModel(i, liveSearchList[i]))
                        }

                    }
                    else -> {

                    }
                }
            }
        })

        categoriesViewModel.productSize.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Success -> {
                        val hasProduct = resource.data.pagination?.total_count!! > 0
                        findProduct.isVisible = hasProduct
                        findProduct.text = getString(
                            R.string.text_find_product,
                            resource.data.pagination.total_count
                        )
                    }
                    else -> {

                    }
                }
            }
        })

        searchRecyclerView.addItemDecoration(
            SpacesItemDecoration(
                resources.getDimension(R.dimen.fab_margin_8).toInt()
            )
        )
        searchView.addTextChangedListener(object : TextWatcher {
            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {

            }

            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {
                if (s != null) {
                    if (s.isNotEmpty() && s.length < 20)
                        homeViewModel.getLiveSearch(s.toString())
                    else
                        liveSearchItemAdapter.clear()
                }
            }

            override fun afterTextChanged(s: Editable?) {

            }

        })

        searchView.setOnEditorActionListener(object : TextView.OnEditorActionListener {
            override fun onEditorAction(v: TextView?, actionId: Int, event: KeyEvent?): Boolean {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    getFeedPosts(v?.text.toString())
                    search_background.visibility = View.GONE
                    search_background_close.visibility = View.VISIBLE
                    return true;
                }
                return false;
            }

        })

        search_background.setOnClickListener {
            getFeedPosts(searchView?.text.toString())
            search_background.visibility = View.GONE
            search_background_close.visibility = View.VISIBLE
        }

        search_background_close.setOnClickListener {
            searchView.setText("")
            search_background_close.visibility = View.GONE
            search_background.visibility = View.VISIBLE
        }

        categoryAdapter.click = {
            when (it) {
                is ClickEvents.onAddToFav -> {

                    Log.i("fav", "fagment recived index ${it.index}")
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)

                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
                else -> {

                }
            }
        }
        delete.setOnClickListener {
            homeViewModel.clearHistory(PrefManager.getUserID(requireContext()))
        }



        favouritesBack.setOnClickListener {
            findNavController().popBackStack()
        }
        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {

                        categoryAdapter.update(
                            resource.data.updatedPosition,
                            resource.data.updatedStatus
                        )
                        if (resource.data.updatedStatus)
                            showSnackbarWithMargin(resources.getString(R.string.text_added_snackbar_message))
                        else
                            showSnackbarWithMargin(resources.getString(R.string.text_delete_snackbar_message))
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(
                            resource.errorResponse.jsonResponse.getString(
                                "error"
                            )
                        )
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })
    }

    companion object {
        @Suppress("DEPRECATION")
        fun fromHtml(source: String): Spanned {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                return Html.fromHtml(source, Html.FROM_HTML_MODE_LEGACY)
            } else {
                return Html.fromHtml(source)
            }
        }

    }


    @SuppressLint("StringFormatInvalid", "StringFormatMatches")
    private fun getFeedPosts(searchValue: String) {
        hideKeyboard()
        some_id.isVisible = false
        delete.isVisible = false
        historyList.isVisible = false


        categoriesViewModel.getSearchProductsSize(
            PrefManager.getUserID(requireContext()),
            searchValue
        )

        lifecycleScope.launch {
            categoriesViewModel.getSearchProductsResponse(
                PrefManager.getUserID(requireContext()),
                searchValue
            ).collect { product ->

                searchRecyclerView.adapter = categoryAdapter
                categoryAdapter.submitData(product)

                return@collect
            }
        }
        categoryAdapter.addLoadStateListener { combinedLoadStates ->
            if (combinedLoadStates.append.endOfPaginationReached) {
                some_id.isVisible = !(categoryAdapter.itemCount >= 1)
                delete.isVisible = !(categoryAdapter.itemCount >= 1)
                historyList.isVisible = !(categoryAdapter.itemCount >= 1)
                ll_empty_list.isVisible = categoryAdapter.itemCount == 0
                liveSearchRV.visibility = View.GONE
                search_value.text = getString(R.string.text_no_product, searchView.text.toString())
            }
        }
    }

    fun addToFavs(product: ProductObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            showSnackbarAuth("вам необходимо")
        } else {
            if (product.fields.in_favourites != "Y") {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "add",
                    int,
                    true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "delete",
                    int,
                    false
                )

            }
        }
    }

    override fun onDeleteItem(item: SearchHistory) {
        homeViewModel.clearOnlyOneHistory(
            PrefManager.getUserID(requireContext()),
            item.id.toString()
        )
        searchHistoryItem = item
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).toolbar.hide()
        (requireActivity() as MainActivity).bottom_navigation.hide()
    }
}