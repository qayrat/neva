package uz.neva.kidya.ui.brandShop

import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.PopupMenu
import androidx.annotation.MenuRes
import androidx.core.view.isVisible
import androidx.fragment.app.Fragment
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_selected_shop.*
import kotlinx.android.synthetic.main.fragment_selected_shop.empty_page
import kotlinx.android.synthetic.main.fragment_selected_shop.filterLay
import kotlinx.android.synthetic.main.fragment_selected_shop.searchItem
import kotlinx.android.synthetic.main.fragment_selected_shop.selectedCategoryName
import kotlinx.android.synthetic.main.fragment_selected_shop.shimmer_product_container
import kotlinx.android.synthetic.main.fragment_selected_shop.subCatName
import kotlinx.android.synthetic.main.fragment_selected_shop.subCategoryBack
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.brandShop.adapter.BrandShopPagingAdapter
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.subcategories.adapter.SubCategoryLoadStateAdapter
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.utils.showSnackbar
import uz.neva.kidya.utils.showSnackbarAuth
import uz.neva.kidya.utils.showSnackbarWithMargin
import java.util.*

class SelectedShopFragment : Fragment(R.layout.fragment_selected_shop),
        BrandShopPagingAdapter.OnItemClickListener {

    private val homeViewModel: HomeViewModel by viewModel()
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private lateinit var shopAdapter : BrandShopPagingAdapter
    var sectionName = ""
    var sectionId = ""
    var page = 1
    var size = 0
    var updateAdapter = ""
    private var sortText = ""
    private var menuSortSelectId = -1
    private var firstCreated = false
    private var totalCount = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        requireActivity().window.setFlags(
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED,
                WindowManager.LayoutParams.FLAG_HARDWARE_ACCELERATED
        )
        arguments?.let {
            sectionName = it.getString("section_name").toString()
            sectionId = it.getString("section_id").toString()
        }
        shopAdapter = BrandShopPagingAdapter(this)
        getShopItems(sectionId, sortText)
        homeViewModel.getBrandShopProductsSize(sectionId)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        homeViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Brand Shops")
        ))
        homeViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> { }
                    is Resource.GenericError -> { }
                    is Resource.Error -> { }
                }
            }
        })

        selectedCategoryName.text = totalCount
        subCatName.text = sectionName
        searchItem.setOnClickListener {
            findNavController().navigate(R.id.nav_searchResult)
        }

        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()

        val gridLayoutManager = GridLayoutManager(requireContext(), 2)
        val footerAdapter = SubCategoryLoadStateAdapter { shopAdapter.retry() }
        selectedCategoryRecyclerView.apply {
            layoutManager = gridLayoutManager
            addItemDecoration(SpacesItemDecoration(marginpixels, true, 2))
            adapter = shopAdapter.withLoadStateFooter(footer = footerAdapter)
            gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
                override fun getSpanSize(position: Int): Int {
                    return if (position == shopAdapter.itemCount && footerAdapter.itemCount > 0) {
                        2
                    } else {
                        1
                    }
                }
            }
        }

        filterLay.setOnClickListener {
            showMenu(it, R.menu.category_sort_menu)
        }

        subCategoryBack.setOnClickListener {
            findNavController().popBackStack()
        }

        homeViewModel.shopProductsSize.observe(viewLifecycleOwner,{
            it.getContentIfNotHandled()?.let { resource->
                when(resource){
                    is Resource.Success-> {
                        val total = resource.data.pagination?.total_count
                        totalCount = "${resources.getString(R.string.text_result)}: $total"
                        selectedCategoryName.text = totalCount
                        if (total != null && total != 0) {
                            shimmer_product_container.stopShimmer()
                            shimmer_product_container.visibility = View.GONE
                            selectedCategoryRecyclerView.isVisible = true
                            empty_page.isVisible = false
                        } else {
                            shimmer_product_container.stopShimmer()
                            shimmer_product_container.visibility = View.GONE
                            selectedCategoryRecyclerView.isVisible = false
                            empty_page.isVisible = true
                        }
                    }
                    else-> { }
                }
            }
        })

        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        when (updateAdapter) {
                            "deal" -> {
                                shopAdapter.update(
                                        resource.data.updatedPosition,
                                        resource.data.updatedStatus
                                )
                            }
                        }

                        if (resource.data.updatedStatus)
                            showSnackbarWithMargin(resources.getString(R.string.text_added_snackbar_message))
                        else
                            showSnackbarWithMargin(resources.getString(R.string.text_delete_snackbar_message))
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(
                                resource.errorResponse.jsonResponse.getString(
                                        "error"
                                )
                        )
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })
    }

    private fun getShopItems(sectionId: String, sort: String) {
        lifecycleScope.launch {
            homeViewModel.getSelectedShop(
                    PrefManager.getUserID(requireContext()),
                    sectionId,
                    sort
            ).collect { products ->
                shopAdapter.submitData(products)
                return@collect
            }
        }
        shopAdapter.addLoadStateListener { loadState ->
            if (loadState.source.refresh is LoadState.NotLoading && loadState.append.endOfPaginationReached && shopAdapter.itemCount < 1) {
            } else {
                if (shopAdapter.itemCount > 0) {
                    shopAdapter.notifyDataSetChanged()
                }
            }
        }
    }

    fun addToFavs(product: ProductObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
            showSnackbarAuth(resources.getString(R.string.text_need_auth))
        } else {
            if (product.fields.in_favourites != "Y") {
                val productId = product.fields.id ?: ""
                checkOutViewModel.getAddToFavsResponse(
                        PrefManager.getUserID(requireContext()),
                        "favs",
                        productId,
                        "add",
                        int,
                        true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                        PrefManager.getUserID(requireContext()),
                        "favs",
                        product.fields.id,
                        "delete",
                        int,
                        false
                )
            }
        }
    }

    private fun showMenu(v: View, @MenuRes menuRes: Int) {
        val popup = PopupMenu(requireContext(), v)
        popup.menuInflater.inflate(menuRes, popup.menu)
        if (menuSortSelectId != -1) {
            popup.menu.getItem(menuSortSelectId).isChecked = true
        }
        popup.setOnMenuItemClickListener { menuItem: MenuItem ->
            when (menuItem.itemId) {
                R.id.action_update -> {
                    actionMenuSort("id:desc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 0
                    }
                }
                R.id.action_price_increase -> {
                    actionMenuSort("price:asc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 1
                    }
                }
                R.id.action_price_reduction -> {
                    actionMenuSort("price:desc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 2
                    }
                }
                R.id.action_rating -> {
                    actionMenuSort("id:asc")
                    if (menuItem.isChecked) {
                        menuItem.isChecked = false
                    } else {
                        menuItem.isChecked = true
                        menuSortSelectId = 3
                    }
                }
            }
            false
        }
        popup.setOnDismissListener {
        }
        popup.show()
    }
    private fun actionMenuSort(sort: String) {
        getShopItems(sectionId, sort)
        sortText = sort
        selectedCategoryRecyclerView.smoothScrollToPosition(0)
        shopAdapter.notifyDataSetChanged()
    }

    override fun onClick(data: ProductObject) {
        val bundle = Bundle()
        bundle.putString("section_id", data.fields.iblock_section_id)
        bundle.putString("product_id", data.fields.id)
        findNavController().navigate(R.id.nav_selectedItem, bundle)
    }

    override fun onFavourite(data: ProductObject, index: Int) {
        updateAdapter = "deal"
        addToFavs(data, index)
    }

    override fun onResume() {
        super.onResume()
        shimmer_product_container.startShimmer()
        shimmer_product_container.visibility = View.VISIBLE
        selectedCategoryRecyclerView.isVisible = false
        empty_page.isVisible = false
        if (firstCreated) {
            shimmer_product_container.stopShimmer()
            shimmer_product_container.visibility = View.GONE
            selectedCategoryRecyclerView.isVisible = true
            empty_page.isVisible = false
        } else {
            firstCreated = true
        }
    }

    override fun onPause() {
        super.onPause()
        shimmer_product_container.stopShimmer()
        shimmer_product_container.visibility = View.GONE
        selectedCategoryRecyclerView.isVisible = true
        empty_page.isVisible = false
    }
}