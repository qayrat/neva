package uz.neva.kidya.ui.category

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import com.google.android.material.tabs.TabLayout
import kotlinx.coroutines.flow.*
import kotlinx.coroutines.launch
import timber.log.Timber
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.*
import uz.neva.kidya.network.dto.categorySort.CategorySortData
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.response.EmptyResponse
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event

class CategoriesViewModel constructor(private val repository: KidyaRepository) : ViewModel() {
    val popularCategoriesResponse = MutableLiveData<Event<Resource<Categories>>>()
    val subCategoriesResponse = MutableLiveData<Event<Resource<Categories>>>()
    val selectedProductResponse = MutableLiveData<Event<Resource<MCatalog>>>()
    val fleshSaleProductResponse = MutableLiveData<Event<Resource<FlashSaleModel>>>()
    val selectedFrequentlyProductResponse = MutableLiveData<Event<Resource<MFrequently>>>()
    val selectedMoreProductResponse = MutableLiveData<Event<Resource<MMoreProduct>>>()
    val productSize = MutableLiveData<Event<Resource<ProductCount>>>()
    val selectedProductReviewsResponse = MutableLiveData<Event<Resource<Reviews>>>()
    val getSelectedProductReviewsResponse = MutableLiveData<Event<Resource<Reviews>>>()
    val isSuccess = MutableLiveData<Boolean>()
    val product = MutableLiveData<PagingData<ProductObject>>()
    val tab = MutableLiveData<TabLayout.Tab>()
    val totalCount = MutableLiveData<Event<Resource<ProductCount>>>()
    private val _categorySort = MutableLiveData<CategorySortData>()
    val categorySort: LiveData<CategorySortData> = _categorySort
    val analyticsResponse = MutableLiveData<Event<Resource<EmptyResponse>>>()

    fun setData(isSuccess: Boolean) {
        this.isSuccess.postValue(isSuccess)
    }

    fun getCategoriesResponse(section_id: String = ""): Flow<PagingData<CategoriesObject>> {
        val newResult: Flow<PagingData<CategoriesObject>> = repository.getCategories(section_id)
            .cachedIn(viewModelScope)
        return newResult
    }

    fun getProductTotalCount(section_id: String){
        viewModelScope.launch {
            repository.getAllProductCount(section_id).onEach {
                totalCount.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getFlashSaleProduct(){
        viewModelScope.launch {
            repository.getFlashSaleProduct().onEach {
                fleshSaleProductResponse.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getPopularCategoriesResponse(width: Int? = null, height: Int? = null) {
        viewModelScope.launch {
            repository.getPopularCategories(width, height).onEach {
                popularCategoriesResponse.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getHomeRecommendedData(
        userId: String,
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getHomeRecommended(userId, isHome = isHome).cachedIn(viewModelScope)
        return newResult
    }

    fun getReviews(productId: String) {
        viewModelScope.launch {
            repository.getReviews(productId).onEach {
                selectedProductReviewsResponse.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getSubCategories(section_id: String) {
        viewModelScope.launch {
            repository.getSubCategory(section_id = section_id).onEach {
                subCategoriesResponse.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun addReviews(productId: String, user_id: String, text: String, rating: Float) {
        viewModelScope.launch {
            repository.addReviews(productId, user_id, text, rating).onEach {
                selectedProductReviewsResponse.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getReview(productId: String) {
        viewModelScope.launch {
            repository.getReview(productId).onEach {
                getSelectedProductReviewsResponse.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }


    fun getSelectedProductResponse(
        user_id: String,
        id: String,
        width: Int? = null,
        height: Int? = null
    ) {
        viewModelScope.launch {
            repository.getSelectedProduct(user_id = user_id, id, width = width, height = height)
                .onEach {
                    selectedProductResponse.value =
                        Event<Resource<MCatalog>>(it as Resource<MCatalog>)
                }.launchIn(viewModelScope)
        }
    }

    fun getSelectedFrequentlyProducts(id: String) {
        viewModelScope.launch {
            repository.getSelectedFrequentlyProducts(id).onEach {
                selectedFrequentlyProductResponse.value =
                    Event<Resource<MFrequently>>(it as Resource<MFrequently>)
            }.launchIn(viewModelScope)
        }
    }
    fun getSearchProductsSize(user_id: String,searchValue: String) {
        viewModelScope.launch {
            repository.getSearchProductsSize(user_id,searchValue).onEach {
                productSize.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getSelectedMoreProducts(id: String) {
        viewModelScope.launch {
            repository.getSelectedMoreProducts(id).onEach {
                selectedMoreProductResponse.value =
                    Event<Resource<MMoreProduct>>(it as Resource<MMoreProduct>)
            }.launchIn(viewModelScope)
        }
    }

    fun getSelectedCategoryProductsResponse(
        section_id: String,
        user_id: String
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getSelectedCategoryProducts(section_id, user_id)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun getAllSelectedCategoryProductsResponse(
        section_id: String,
        user_id: String
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getAllSelectedCategoryProducts(section_id, user_id)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun getAllSelectedCategoryProductsResponseNew(
            section_id: String,
            user_id: String,
            allProduct: Boolean,
            sort: String
    ): Flow<PagingData<ProductObjectNew>> {
        Timber.d(section_id)
        val newResult: Flow<PagingData<ProductObjectNew>> =
                repository.getAllSelectedCategoryProductsNew(section_id, user_id, allProduct, sort)
                        .cachedIn(viewModelScope)
        return newResult
    }

    fun getFilteredResult(
        user_id: String,
        section_id: String,
        sort: String
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getFilteredResult(user_id, section_id, sort)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun getSaleProductsResponse(
        user_id: String,
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getSaleCategoryProducts(user_id, isHome = isHome)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun getDealsProductsResponse(
        user_id: String,
        type: String = "hits",
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getDealsCategoryProducts(user_id, type, isHome = isHome)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun getFavProductsResponse(user_id: String): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> = repository.getFavCategoryProducts(user_id)
            .cachedIn(viewModelScope)
        return newResult
    }

    fun getSearchProductsResponse(
        user_id: String,
        searchValue: String
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getSearchProducts(searchValue, user_id)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun getViewHistoryResponse(
        user_id: String,
        page: Int?,
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getViewHistoryProducts(user_id, isHome = isHome, page)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun categorySortListener(sectionId: String, sort: String) {
        _categorySort.value = CategorySortData(sectionId, sort)
    }

    fun getCategoryFilterResult(
        user_id: String,
        section_id: String,
        sort: String
    ): Flow<PagingData<ProductSortObject>> {
        val newResult: Flow<PagingData<ProductSortObject>> =
            repository.getCategorySortProducts(section_id, user_id, sort)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun putAnalytics(model: AnalyticsModel) {
        viewModelScope.launch {
            repository.putAnalytics(model)
                    .onEach { analyticsResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }
}