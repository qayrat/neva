package uz.neva.kidya.ui.news

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.News
import uz.neva.kidya.network.dto.NewsObject
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event

class NewsViewModel constructor(private val repository:KidyaRepository): ViewModel(){

    val getHomeNewsResponse=MutableLiveData<Event<Resource<News>>>()
    val getSelectedNewsResponse=MutableLiveData<Event<Resource<News>>>()



        fun getNewsRecourse(isHomeNews:Boolean=false) :Flow<PagingData<NewsObject>>{
            val newResult: Flow<PagingData<NewsObject>> = repository.getNews(isHomeNews)
                .cachedIn(viewModelScope)
            return newResult
        }

    fun getSelectedNewsRecourse(id:String) {
        viewModelScope.launch {
            repository.getSelectedNews(id).onEach{
                getSelectedNewsResponse.value= Event(it)
            }.launchIn(viewModelScope)
        }
    }
}