package uz.neva.kidya.ui.log_in

import android.content.Context
import android.os.Bundle
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.redmadrobot.inputmask.MaskedTextChangedListener
import com.redmadrobot.inputmask.helper.AffinityCalculationStrategy
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_log_in.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.SmsFragment
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.utils.*

const val PHONE_NUMBER = "PHONE_NUMBER"

class LogInFragment : Fragment() {
    private val viewModel: LoginViewModel by viewModel()
    private var state = 1
    private var buyNow = ""
    private var number = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            buyNow = it.getString("buyNow") ?: ""
        }
        (requireActivity() as MainActivity).toolbar.hide()
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_log_in, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        phoneNumber.formatPhoneMask(true)
//        setPhoneInputMask()


//        sendCodeAgain.setOnClickListener {
//            if (isPhoneValid()) {
//                viewModel.getLoginFirstStepResponse(
//                    phoneNumber.getMaskedPhoneWithoutSpace().removeRange(0, 3)
//                )
//            } else {
//                Toast.makeText(requireContext(), "WrongNumber", Toast.LENGTH_SHORT)
//            }
//            //textView.visibility=View.GONE
//            //loginOptions.visibility=View.GONE
//        }

        getMessage.setOnClickListener {
            if (isPhoneValid()) {
                getMessage.startAnimation {
                    viewModel.getLoginFirstStepResponse(
                        phoneNumber.getMaskedPhoneWithoutSpace().removeRange(0, 3)
                    )
                }
            } else {
                showSnackbarWithMargin(resources.getString(R.string.text_phone_error))
            }
            //textView.visibility=View.GONE
            //loginOptions.visibility=View.GONE
        }

        viewModel.loginFirstStepResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        getMessage.revertAnimation()
//                        changeUiStateEnabled(true, progressIndicatorPhone, getMessage)
//                        codeLayout.visibility = View.GONE
//                        logIn.visibility = View.GONE

                    }
                    is Resource.Success -> {
                        (requireActivity() as MainActivity).state = 2
                        getMessage.revertAnimation()

                        val bundle = Bundle()
                        bundle.putString(PHONE_NUMBER, phoneNumber.getMaskedPhoneWithoutSpace())
                        bundle.putString("buyNow", buyNow)
                        findNavController().navigate(R.id.smsFragment, bundle)
                    }
                    is Resource.GenericError -> {
                        getMessage.revertAnimation()
//                        changeUiStateEnabled(false, progressIndicatorPhone, getMessage)
                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        getMessage.revertAnimation()
                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
//                        changeUiStateEnabled(false, progressIndicatorPhone, getMessage)
                    }
                }
            }
        })

        logInBack.setOnClickListener {
            findNavController().popBackStack()
        }
//        logIn.setOnClickListener {
//            if (isCodeValid())
//                viewModel.getLoginSecondStepResponse(
//                    phoneNumber.getMaskedPhoneWithoutSpace().removeRange(0, 3),
//                    codeNumber.text.toString(),
//                    PrefManager.getFirebaseToken(requireContext())
//                )
//        }

        viewModel.loginSecondStepResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                        changeUiStateEnabled(true, progressIndicatorCode, logIn)

                    }
                    is Resource.Success -> {
//                        changeUiStateEnabled(false, progressIndicatorCode, logIn)
                        if (resource.data.status == "OK") {
                            PrefManager.saveUserId(requireContext(), resource.data.user_id!!)
                            PrefManager.saveName(
                                requireContext(),
                                resource.data.current_user?.name!! + " " + resource.data.current_user?.last_name!!
                            )
                            PrefManager.savePhone(
                                requireContext(),
                                resource.data.current_user?.phone ?: ""
                            )
                            PrefManager.saveStreet(
                                requireContext(),
                                resource.data.current_user?.street ?: ""
                            )
                            PrefManager.saveDistrict(
                                requireContext(),
                                resource.data.current_user?.district ?: ""
                            )
                            PrefManager.saveCity(
                                requireContext(),
                                resource.data.current_user?.address ?: ""
                            )
                            findNavController().popBackStack()
                        } else {
//                            showSnackbar(resource.data.error!!)
                        }

                    }
                    is Resource.GenericError -> {
//                        changeUiStateEnabled(false, progressIndicatorCode, logIn)
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
//                        changeUiStateEnabled(false, progressIndicatorCode, logIn)
                    }
                }
            }
        })


    }

    private fun isPhoneValid(): Boolean {
        phoneNumber.showErrorIfNotFilled()

        if ((phoneNumber.getMaskedPhoneWithoutSpace().length == 12)) {
            Log.i("add", "true")
            return true
        } else
            phoneNumber.showError()
        return false

    }

//    private fun isCodeValid(): Boolean {
//        codeNumber.showErrorIfNotFilled()
//
//        if ((codeNumber.text.length == 4)) {
//            return true
//        } else
//            codeNumber.showError()
//        return false
//
//    }

    private fun setPhoneInputMask() {
        val listener = MaskedTextChangedListener.installOn(
            phoneNumber,
            "+998 [00] [000]-[00]-[00]",
            emptyList(),
            AffinityCalculationStrategy.PREFIX,
            object : MaskedTextChangedListener.ValueListener {
                override fun onTextChanged(
                    maskFilled: Boolean,
                    extractedValue: String,
                    formattedValue: String
                ) {
                    number = getString(R.string.code_country_uz) + extractedValue
                    if (maskFilled)
                        phoneNumber.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.ic_baseline_check_circle_green,
                            0
                        )
                    else
                        phoneNumber.setCompoundDrawablesWithIntrinsicBounds(
                            0,
                            0,
                            R.drawable.check_stop,
                            0
                        )
                }
            }
        )
    }


}