package uz.neva.kidya.ui.category.selectedProduct

import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import timber.log.Timber
import uz.neva.kidya.KidyaApplication
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.dto.Catalog
import uz.neva.kidya.network.dto.ProductObject
import java.io.IOException
import kotlin.coroutines.coroutineContext

const val STARTING_PAGE_INDEX = 1

class SelectedCategoryProductListPagingSource(
    private val api: KidyaApi,
    val section_id: String = "",
    val user_id: String = "",
    val getFav: Boolean = false,
    val searchValue: String = "",
    val isSearch: Boolean = false,
    val isHistory: Boolean = false,
    val isDeal: Boolean = false,
    val isHome: Boolean = false,
    val isFiltered: Boolean = false,
    val isRecommended: Boolean = false,
    val isDisCount: Boolean = false,
    val popularType: String = "",
    val sort: String = "",
    val isAllProduct: Boolean = false,
    val properties: HashMap<String, String> = HashMap(),
) : PagingSource<Int, ProductObject>() {

    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, ProductObject> {
        val position = params.key ?: STARTING_PAGE_INDEX

        return try {
            val response =
                when {
                    isHistory -> {
                        if (isHome)
                            api.getViewHistoryProducts(user_id, position, count = "6")
                        else
                            api.getViewHistoryProducts(user_id, position)
                    } //get view history
                    getFav -> api.getFavs(user_id = user_id, position)      //getting fav items
                    isSearch -> api.getSearchResult(
                        user_id,
                        searchValue,
                        position
                    ) //showing search results
                    isFiltered -> {
                        api.getFilter(section_id, "true", sort, position)

                    }//get filtered

                    isRecommended -> {
                        if (isHome)
                            api.getHomeRecommended(
                                user_id = user_id,
                                position,
                                count = "4",
                                width = 152,
                                height = 168
                            )//get hot deals
                        else api.getHomeRecommended(
                            user_id = user_id,
                            position,
                            width = 152,
                            height = 168
                        )
                    }
                    isDeal -> {
                        if (isHome)
                            api.getHomeHits(
                                user_id = user_id,
                                position,
                                count = "8",
                                width = 120,
                                height = 144
                            )
                        else
                            api.getHomeHits(user_id = user_id, position, width = 120, height = 144)

                    }
                    isDisCount -> {
                        Timber.d(user_id)
                        if (isHome)
                            api.getHomeDiscount(
                                user_id = user_id,
                                position,
                                count = "8",
                                width = 152,
                                height = 168
                            )
                        else
                            api.getHomeDiscount(
                                user_id = user_id,
                                position,
                                width = 152,
                                height = 168
                            )
                    }
                    isAllProduct -> {
                        api.getAllSelectedCategoryProducts(
                            user_id,
                            section_id = section_id,
                            position = position,
                            all_product = "true"
                        )
                    }
//                    section_id == "" -> {
//                        if (isHome)
//                            api.getSaleCategoryProducts(
//                                user_id,
//                                position,
//                                count = "6"
//                            ) //get sale products
//                        else
//                            api.getSaleCategoryProducts(user_id, position)
//                    }
                    else -> api.getSelectedCategoryProducts(
                        user_id,
                        section_id = section_id,
                        position
                    ) //getting the product itself
                }

            val feedPost = response.data


            LoadResult.Page(
                data = feedPost,
                prevKey = if (position == STARTING_PAGE_INDEX) null else position - 1,
                nextKey = when {
                    isHome -> null
                    response.pagination?.total_count == 1 -> {
                        null
                    }
                    feedPost.isEmpty() -> {
                        null
                    }
                    response.pagination?.current_page == response.pagination?.total_count -> {
                        position + 1

                    }
                    response.pagination?.current_page ?: 0 < response.pagination?.page_count ?: 0 -> {
                        position + 1
                    }
                    else -> null
                }
            )
        } catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, ProductObject>): Int? = null
}