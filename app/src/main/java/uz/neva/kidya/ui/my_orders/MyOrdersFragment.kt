package uz.neva.kidya.ui.my_orders

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_my_orders.*
import uz.neva.kidya.R
import uz.neva.kidya.ui.my_details.ProfileViewModel

import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource

//import uz.usoft.kidya.utils.showSnackbar

class MyOrdersFragment : Fragment() {

    private val profileViewModel: ProfileViewModel by viewModel()
    private lateinit var adapter: MyOrdersListAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_my_orders, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter= MyOrdersListAdapter(requireContext())
        myOrdersRecyclerView.layoutManager= LinearLayoutManager(requireContext())
        myOrdersRecyclerView.adapter=adapter

        profileViewModel.getGetOrderHistoryResponse(PrefManager.getUserID(requireContext()))
        profileViewModel.getOrderHistoryResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        adapter.updateList(resource.data.data)
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        adapter?.onItemClick={
            val bundle:Bundle=Bundle()
            bundle.putString("order_id",it.id)
            findNavController().navigate(R.id.nav_selectedOrder,bundle)
        }

        myOrdersBack.setOnClickListener {
            findNavController().popBackStack()
        }

//        viewLifecycleOwner.lifecycleScope.launch {
//            categoryAdapter.loadStateFlow.collectLatest { loadStates ->
//                if (loadStates.refresh is LoadState.Loading)
//                    progressIndicatorViewHistory.show()
//                else
//                    progressIndicatorViewHistory.hide()
//            }
//        }
    }
}