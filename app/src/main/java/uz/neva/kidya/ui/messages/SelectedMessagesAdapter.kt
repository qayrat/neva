package uz.neva.kidya.ui.messages

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.paging.PagingDataAdapter
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.item_message_from.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.MessagesObject

class SelectedMessagesAdapter (val user_id:String): PagingDataAdapter<MessagesObject,SelectedMessagesAdapter.ViewHolder>(MESSSAGE){

    var messagesList:List<MessagesObject>? = null


    var onItemClick: ((MessagesObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.item_message_from, parent, false)
    )

    //   override fun getItemCount()=messagesList?.size?:0


    override fun onBindViewHolder(holder: SelectedMessagesAdapter.ViewHolder, position: Int) {
        holder.bindData(getItem(position) as MessagesObject)
        // holder.collapse.setOnClickListener(this)
    }

    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val date=itemView.selectedMessageTime
        val content=itemView.selectedMessageContent
        val parentLay=itemView.parentLay
        val card=itemView.cardLay

        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(getItem(adapterPosition) as MessagesObject)
            }
        }

        fun bindData(messages: MessagesObject) {
            date.text=messages.date
            content.text=messages.text
            val layoutParams=ConstraintLayout.LayoutParams(ConstraintLayout.LayoutParams.WRAP_CONTENT,ConstraintLayout.LayoutParams.WRAP_CONTENT)
            if (messages.sender_id==user_id)
            {
                layoutParams.endToEnd=parentLay.id
                card.layoutParams=layoutParams
                card.setCardBackgroundColor(Color.parseColor("#FFFFFF"))
            }
            else {
                layoutParams.startToStart=parentLay.id
                card.layoutParams=layoutParams
                card.setCardBackgroundColor(Color.parseColor("#F4F4F8"))
            }

        }

    }

    companion object{
        val MESSSAGE=object : DiffUtil.ItemCallback<MessagesObject>(){
            override fun areItemsTheSame(
                oldItem: MessagesObject,
                newItem: MessagesObject
            ): Boolean = oldItem.id == newItem.id


            override fun areContentsTheSame(
                oldItem: MessagesObject,
                newItem: MessagesObject
            ): Boolean =
                oldItem == newItem

        }
    }
}