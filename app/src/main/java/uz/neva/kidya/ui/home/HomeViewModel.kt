package uz.neva.kidya.ui.home

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.*
import uz.neva.kidya.network.dto.homeBanner.HomeBannerData
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.response.EmptyResponse
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.ui.category.selectedProduct.FAQData
import uz.neva.kidya.ui.search.LiveSearchResponse
import uz.neva.kidya.ui.search.SearchHistoryModel
import uz.neva.kidya.utils.Event

class HomeViewModel constructor(private val repository: KidyaRepository) : ViewModel() {
    val getBannersResponse = MutableLiveData<Event<Resource<Banner>>>()
    val getShopResponse = MutableLiveData<Event<Resource<List<ShopInfoObject>>>>()
    val getSelectedShopResponse = MutableLiveData<Event<Resource<SelectedShop>>>()
    val getSearchResponse = MutableLiveData<Event<Resource<SearchHistoryModel>>>()
    val getLiveResponse = MutableLiveData<Event<Resource<LiveSearchResponse>>>()
    val clearOnlyOneResponse = MutableLiveData<Event<Resource<SearchHistoryModel>>>()
    val sendFeedbackResponse = MutableLiveData<Event<Resource<Any>>>()
    val getFAQResponse = MutableLiveData<Event<Resource<FAQData>>>()
    val getCardListResponse = MutableLiveData<Event<Resource<CardResource>>>()
    val addCardResponse = MutableLiveData<Event<Resource<CardResource>>>()
    val verifyCardResponse = MutableLiveData<Event<Resource<VerifyCardResource>>>()
    val deleteCardResponse = MutableLiveData<Event<Resource<Any>>>()
    val payCardResponse = MutableLiveData<Event<Resource<PayModel>>>()
    val shopProductsSize = MutableLiveData<Event<Resource<ProductCount>>>()
    val homeBanner2 = MutableLiveData<Event<Resource<HomeBannerData>>>()
    val homeHitsCount = MutableLiveData<Event<Resource<Catalog>>>()
    val getBasketResponse = MutableLiveData<Event<Resource<Basket>>>()
    val analyticsResponse = MutableLiveData<Event<Resource<EmptyResponse>>>()

    fun getCardList(userId: String, token: String) {
        viewModelScope.launch {
            repository.getCardList(userId, token).onEach { getCardListResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getFilteredResult(
        user_id: String,
        section_id: String,
        sort: String
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getFilteredResult(user_id, section_id, sort)
                .cachedIn(viewModelScope)
        return newResult
    }

    fun addCard(
        user_id: String,
        token: String,
        name: String,
        number: String,
        expire: String
    ) {
        viewModelScope.launch {
            repository.addCard(user_id, token, name, number, expire)
                .onEach { addCardResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun verifyCard(token: String, code: String, id_card: String) {
        viewModelScope.launch {
            repository.verifyCard(token, code, id_card)
                .onEach { verifyCardResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun deleteCard(token: String, id_card: String) {
        viewModelScope.launch {
            repository.deleteCard(token, id_card)
                .onEach { deleteCardResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun payCard(
        token: String,
        order_id: String,
        id_card: String,
        fio: String,
        phoneNumber: String,
        region: String,
        location: String? = null,
        street: String
    ) {
        viewModelScope.launch {
            repository.payCard(
                token,
                order_id,
                id_card,
                fio = fio,
                phoneNumber = phoneNumber,
                region = region,
                location = location,
                street = street
            )
                .onEach { payCardResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getBanners() {
        viewModelScope.launch {
            repository.getBanners().onEach { getBannersResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getShopInfo(width: Int? = null, height: Int? = null) {
        viewModelScope.launch {
            repository.getShopInfo(width, height).onEach { getShopResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getSelectedShop(
            user_id: String,
            id: String,
            sort: String
    ) : Flow<PagingData<ProductObject>> {
        return repository.getSelectedShop(user_id, id, sort).cachedIn(viewModelScope)
    }

    fun getSearchHistory(id: String) {
        viewModelScope.launch {
            repository.getSearchHistory(id).onEach { getSearchResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getLiveSearch(value: String) {
        viewModelScope.launch {
            repository.getLiveSearch(value).onEach { getLiveResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun clearHistory(id: String) {
        viewModelScope.launch {
            repository.clearHistory(id).onEach { getSearchResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun clearOnlyOneHistory(userid: String, historyId: String) {
        viewModelScope.launch {
            repository.clearOnlyOneHistory(userid, historyId)
                .onEach { clearOnlyOneResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun sendFeedback(name: String, phone: String, text: String) {
        viewModelScope.launch {
            repository.sendFeedback(name, phone, text)
                .onEach { sendFeedbackResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun getFaq(userId: String? = null) {
        viewModelScope.launch {
            repository.getFaq(userId).onEach { getFAQResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getBrandShopProductsSize(id: String){
        viewModelScope.launch {
            repository.getBrandShopProductsSize(id).onEach {
                shopProductsSize.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getHomePageData(userId: String, isHome: Boolean = false): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getHomePage(userId, isHome = isHome).cachedIn(viewModelScope)
        return newResult
    }

    fun getHomeDealsCount(userId: String) {
        viewModelScope.launch {
            repository.getHomeDealCount(userId).onEach { homeHitsCount.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getHomeRecommendedData(
        userId: String,
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        val newResult: Flow<PagingData<ProductObject>> =
            repository.getHomeRecommended(userId, isHome = isHome).cachedIn(viewModelScope)
        return newResult
    }

    fun getHomeBanner() {
        viewModelScope.launch {
            repository.getHomeBanner().onEach { homeBanner2.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getBasketResponse(user_id: String) {
        viewModelScope.launch {
            repository.getBasket(user_id).onEach { getBasketResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun putAnalytics(model: AnalyticsModel) {
        viewModelScope.launch {
            repository.putAnalytics(model)
                    .onEach { analyticsResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }
}