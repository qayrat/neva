package uz.neva.kidya.ui.messages

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_messages.*
import kotlinx.android.synthetic.main.fragment_selected_message.*
import kotlinx.coroutines.delay
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.category.ReverseSpacesItemDecoration
import uz.neva.kidya.utils.changeUiStateEnabled
import uz.neva.kidya.utils.hideKeyboard
//import uz.usoft.kidya.utils.showSnackbar
//import uz.usoft.kidya.utils.showSnackbarWithMargin

class SelectedMessageFragment : Fragment() {

    private val messagesVewModel: MessagesVewModel by viewModel()
    var supplier_id=""
    private lateinit var adapter: SelectedMessagesAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_selected_message, container, false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        selectedMessageback.setOnClickListener{
            findNavController().popBackStack()
        }

        supplier_id= arguments?.getString("supplierId").toString()
        selectedMessageBrand.text= arguments?.getString("supplierName").toString()

        adapter= SelectedMessagesAdapter(PrefManager.getUserID(requireContext()))

        selectedMessagesRecycler.layoutManager= LinearLayoutManager(
            requireContext(),
            LinearLayoutManager.VERTICAL,
            true
        )
        selectedMessagesRecycler.adapter=adapter

        selectedMessagesRecycler.addItemDecoration(ReverseSpacesItemDecoration((resources.displayMetrics.density * 16 + 0.5f).toInt()))

        getFeedPosts()


        selectedMessagesRecycler.scrollToPosition(0)
        sendMessage.setOnClickListener {
            if (feedBackComment.text.isNotEmpty())
            {
                messagesVewModel.sendMessages(
                    PrefManager.getUserID(requireContext()),
                    supplier_id,
                    feedBackComment.text.toString()
                )
            }
        }

        messagesVewModel.sendMessagesResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        changeUiStateEnabled(true, progressIndicatorSend, sendMessage)

                    }
                    is Resource.Success -> {
                        getFeedPosts()
                        hideKeyboard()
                        adapter.notifyDataSetChanged()
                        feedBackComment.setText("")
                        feedBackComment.clearFocus()

                        changeUiStateEnabled(false, progressIndicatorSend, sendMessage)
                    }
                    is Resource.GenericError -> {
                        changeUiStateEnabled(false, progressIndicatorSend, sendMessage)
//                        showSnackbarWithMargin(
//                            resource.errorResponse.jsonResponse.getString(
//                                "error"
//                            )
//                        )
                    }
                    is Resource.Error -> {
                        changeUiStateEnabled(false, progressIndicatorSend, sendMessage)
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })
    }

    private fun getFeedPosts() {
        lifecycleScope.launch {
            messagesVewModel.getChatMessagesResponse(
                PrefManager.getUserID(requireContext()),
                supplier_id
            ).collect { product ->

                selectedMessagesRecycler.adapter = adapter
                adapter.submitData(product)
                adapter.notifyDataSetChanged()

                delay(500)
                selectedMessagesRecycler.smoothScrollToPosition(0)
               // selectedMessagesRecycler.layoutManager?.scrollToPosition(0)
                return@collect
            }
        }
    }

}