package uz.neva.kidya.ui.my_orders

import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.*
import android.widget.RatingBar
import androidx.fragment.app.DialogFragment
import androidx.lifecycle.Observer
import kotlinx.android.synthetic.main.fragment_feedback_dialog.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.utils.*

class FeedbackDialogFragment() : DialogFragment() {

    private val homeViewModel: HomeViewModel by viewModel()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
    }


    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        dialog?.requestWindowFeature((Window.FEATURE_NO_TITLE))

        return inflater.inflate(R.layout.fragment_feedback_dialog, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        leaveFeedbackClose.setOnClickListener{
            Log.i("dialog","snding data")
            this.dismiss()
        }

        leaveFeedbackPhone
            .formatPhoneMask()

        if(PrefManager.getUserID(requireContext()).isNotEmpty())
        {
            leaveFeedbackPhoneLay.hide()
            leaveFeedbackNameLay.hide()
        }

        leaveFeedbackButton.setOnClickListener{
            if(PrefManager.getUserID(requireContext()).isNotEmpty()) {
                homeViewModel.sendFeedback(
                    PrefManager.getName(requireContext()),
                    PrefManager.getPhone(requireContext()),
                    leaveFeedbackText.text.toString()
                )
            }
            else
            {
                if(inputValid())
                homeViewModel.sendFeedback(
                    leaveFeedbackName.text.toString(),
                    leaveFeedbackPhone.getMaskedPhoneWithoutSpace(),
                    leaveFeedbackText.text.toString()
                )
            }
        }



        deliveryRatingBar.onRatingBarChangeListener = object : RatingBar.OnRatingBarChangeListener {
            override fun onRatingChanged(p0: RatingBar?, p1: Float, p2: Boolean) {

            }
        }

        homeViewModel.sendFeedbackResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                    }
                    is Resource.Success -> {
                            this.dismiss()
                    }
                    is Resource.GenericError -> {
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })


}
    fun inputValid():Boolean
    {

        leaveFeedbackPhone.showErrorIfNotFilled()
        leaveFeedbackText.showErrorIfNotFilled()
        return isPhoneValid()&&feedbackValid()
    }

    private fun isPhoneValid():Boolean
    {
        if((leaveFeedbackPhone.getMaskedPhoneWithoutSpace().length==12))
        {
            Log.i("add","true")
            return true
        }
        else
            leaveFeedbackPhone.showError()
        return false

    }

    private fun feedbackValid():Boolean
    {
        return leaveFeedbackText.text.isNotEmpty()
    }
    override fun onStart() {
        super.onStart()

        dialog?.window?.setLayout(
            WindowManager.LayoutParams.WRAP_CONTENT,
            WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT));
    }



}