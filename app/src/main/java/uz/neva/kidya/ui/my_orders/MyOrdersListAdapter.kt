package uz.neva.kidya.ui.my_orders

import android.annotation.SuppressLint
import android.content.Context
import android.os.Build
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.my_orders_item.view.*
import uz.neva.kidya.R
import uz.neva.kidya.network.dto.OrderObject
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show

class MyOrdersListAdapter(val context: Context) : RecyclerView.Adapter<MyOrdersListAdapter.ViewHolder>() {
    var orderList: List<OrderObject>?= arrayListOf()


    var onItemClick: ((OrderObject) -> Unit)? = null

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int)= ViewHolder(
        LayoutInflater.from(parent.context).inflate(R.layout.my_orders_item, parent, false)
    )

    fun updateList(orderList: List<OrderObject>) {
        this.orderList = orderList
        notifyDataSetChanged()
    }

    override fun getItemCount()=orderList?.size?:0

    @RequiresApi(Build.VERSION_CODES.O)
    override fun onBindViewHolder(holder: MyOrdersListAdapter.ViewHolder, position: Int) {
        holder.bindData(orderList!![position])
    }


    inner class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private val orderNUmber=itemView.orderNUmber
        private val date=itemView.orderDate
        private val price=itemView.orderPrice
        private val delivery=itemView.delivereyFee
        private val total=itemView.totalPrice
        private val orderlist=itemView.orderlistLayout
        private val timelinePacked=itemView.packedBullet
        private val firstStateText=itemView.firstStateText
        private val secondStateText=itemView.secondStateText
        private val thirdStateText=itemView.thirdStateText
        private val fourthStateText=itemView.fourthStateText
        private val fifthStateText=itemView.fifthStateText
        private val timelinePackedLine=itemView.packedLine
        private val timelineProcessed=itemView.processedBullet
        private val timelineWaiting=itemView.waitingBullet
        private val timelineWaitingLine=itemView.waitingLine
        private val timelineOnRoad=itemView.onRoadBullet
        private val timelineOnRoadLine=itemView.onRoadLine
        private val timelineDelivered=itemView.deliveredBullet
        private val timelineDeliveredLine=itemView.deliveredLine


        init {
            itemView.setOnClickListener {
                onItemClick?.invoke(orderList!![adapterPosition])
            }
        }

        @RequiresApi(Build.VERSION_CODES.O)
        @SuppressLint("UseCompatLoadingForDrawables")
        fun bindData(orderList: OrderObject)
        {
            orderNUmber.text=orderList.id
            date.text=orderList.date
            price.text=orderList.price+" сум"
            delivery.text=orderList.price_delivery+" сум"
            total.text=orderList.total_price+" сум"

            when(orderList.status_name)
            {
                "В обработке"->{

                    firstStateText.text="Оплата"

                    timelineProcessed.setImageResource(R.drawable.ic_bullet)
                    timelinePacked.setImageResource(R.drawable.ic_bullet)

                    timelinePackedLine.setImageResource(R.drawable.ic_line)

                    timelineWaiting.setImageResource(R.drawable.ic_bullet_faded)
                    timelineOnRoad.setImageResource(R.drawable.ic_bullet_faded)
                    timelineDelivered.setImageResource(R.drawable.ic_bullet_faded)

                    timelineWaitingLine.setImageResource(R.drawable.ic_line_faded)
                    timelineOnRoadLine.setImageResource(R.drawable.ic_line_faded)
                    timelineDeliveredLine.setImageResource(R.drawable.ic_line_faded)

                    firstStateText.show()
                    secondStateText.show()
                    thirdStateText.show()
                    fourthStateText.show()

                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                }
                "Возврат"->{

                    firstStateText.text="Оплата"

                    timelineProcessed.setImageResource(R.drawable.ic_bullet)
                    timelinePacked.setImageResource(R.drawable.ic_bullet)
                    timelineWaiting.setImageResource(R.drawable.ic_bullet)
                    timelineOnRoad.setImageResource(R.drawable.ic_bullet)
                    timelineDelivered.setImageResource(R.drawable.ic_bullet)

                    timelinePackedLine.setImageResource(R.drawable.ic_line)
                    timelineWaitingLine.setImageResource(R.drawable.ic_line)
                    timelineOnRoadLine.setImageResource(R.drawable.ic_line)
                    timelineDeliveredLine.setImageResource(R.drawable.ic_line)

                    firstStateText.hide()
                    secondStateText.hide()
                    thirdStateText.hide()
                    fourthStateText.hide()
                    fifthStateText.text="Возврат"

                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))
                }
                "Выполнен"->{

                    firstStateText.text="Оплата"

                    timelineProcessed.setImageResource(R.drawable.ic_bullet)
                    timelinePacked.setImageResource(R.drawable.ic_bullet)
                    timelineWaiting.setImageResource(R.drawable.ic_bullet)
                    timelineOnRoad.setImageResource(R.drawable.ic_bullet)
                    timelineDelivered.setImageResource(R.drawable.ic_bullet)

                    timelinePackedLine.setImageResource(R.drawable.ic_line)
                    timelineWaitingLine.setImageResource(R.drawable.ic_line)
                    timelineOnRoadLine.setImageResource(R.drawable.ic_line)
                    timelineDeliveredLine.setImageResource(R.drawable.ic_line)

                    firstStateText.hide()
                    secondStateText.hide()
                    thirdStateText.hide()
                    fourthStateText.hide()
                    fifthStateText.text="Выполнен"

                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))

                }
                "Доставлено"->{

                    firstStateText.text="Оплата"

                    timelineProcessed.setImageResource(R.drawable.ic_bullet)
                    timelinePacked.setImageResource(R.drawable.ic_bullet)
                    timelineWaiting.setImageResource(R.drawable.ic_bullet)
                    timelineOnRoad.setImageResource(R.drawable.ic_bullet)
                    timelineDelivered.setImageResource(R.drawable.ic_bullet)

                    timelinePackedLine.setImageResource(R.drawable.ic_line)
                    timelineWaitingLine.setImageResource(R.drawable.ic_line)
                    timelineOnRoadLine.setImageResource(R.drawable.ic_line)
                    timelineDeliveredLine.setImageResource(R.drawable.ic_line)

                    firstStateText.hide()
                    secondStateText.hide()
                    thirdStateText.hide()
                    fourthStateText.hide()
                    fifthStateText.text="Доставлено"

                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))

                }
                "Курьер назначен"->{
                    firstStateText.text="Оплата"

                    timelineProcessed.setImageResource(R.drawable.ic_bullet)
                    timelinePacked.setImageResource(R.drawable.ic_bullet)
                    timelineWaiting.setImageResource(R.drawable.ic_bullet)

                    timelinePackedLine.setImageResource(R.drawable.ic_line)
                    timelineWaitingLine.setImageResource(R.drawable.ic_line)

                    timelineOnRoad.setImageResource(R.drawable.ic_bullet_faded)
                    timelineDelivered.setImageResource(R.drawable.ic_bullet_faded)

                    timelineOnRoadLine.setImageResource(R.drawable.ic_line_faded)
                    timelineDeliveredLine.setImageResource(R.drawable.ic_line_faded)
                    firstStateText.show()
                    secondStateText.show()
                    thirdStateText.show()
                    fourthStateText.show()

                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))

                }
                "В пути"->{
                    firstStateText.text="Оплата"

                    timelineProcessed.setImageResource(R.drawable.ic_bullet)
                    timelinePacked.setImageResource(R.drawable.ic_bullet)
                    timelineWaiting.setImageResource(R.drawable.ic_bullet)
                    timelineOnRoad.setImageResource(R.drawable.ic_bullet)

                    timelinePackedLine.setImageResource(R.drawable.ic_line)
                    timelineWaitingLine.setImageResource(R.drawable.ic_line)
                    timelineOnRoadLine.setImageResource(R.drawable.ic_line)

                    timelineDelivered.setImageResource(R.drawable.ic_bullet_faded)

                    timelineDeliveredLine.setImageResource(R.drawable.ic_line_faded)
                    firstStateText.show()
                    secondStateText.show()
                    thirdStateText.show()
                    fourthStateText.show()


                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))
                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                }
                "Принят"->{
                    timelineProcessed.setImageResource(R.drawable.ic_bullet_faded)
                    firstStateText.text="Ждет оплату"


                    timelinePacked.setImageResource(R.drawable.ic_bullet_faded)
                    timelineWaiting.setImageResource(R.drawable.ic_bullet_faded)
                    timelineOnRoad.setImageResource(R.drawable.ic_bullet_faded)
                    timelineDelivered.setImageResource(R.drawable.ic_bullet_faded)

                    timelinePackedLine.setImageResource(R.drawable.ic_line_faded)
                    timelineWaitingLine.setImageResource(R.drawable.ic_line_faded)
                    timelineOnRoadLine.setImageResource(R.drawable.ic_line_faded)
                    timelineDeliveredLine.setImageResource(R.drawable.ic_line_faded)
                    firstStateText.show()
                    secondStateText.show()
                    thirdStateText.show()
                    fourthStateText.show()


                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                }
                "Оплата"->{

                    timelineProcessed.setImageResource(R.drawable.ic_bullet)
                    firstStateText.text="Оплата"


                    timelinePacked.setImageResource(R.drawable.ic_bullet_faded)
                    timelineWaiting.setImageResource(R.drawable.ic_bullet_faded)
                    timelineOnRoad.setImageResource(R.drawable.ic_bullet_faded)
                    timelineDelivered.setImageResource(R.drawable.ic_bullet_faded)

                    timelinePackedLine.setImageResource(R.drawable.ic_line_faded)
                    timelineWaitingLine.setImageResource(R.drawable.ic_line_faded)
                    timelineOnRoadLine.setImageResource(R.drawable.ic_line_faded)
                    timelineDeliveredLine.setImageResource(R.drawable.ic_line_faded)
                    firstStateText.show()
                    secondStateText.show()
                    thirdStateText.show()
                    fourthStateText.show()


                    firstStateText.setTextColor(ContextCompat.getColor(context,R.color.item_color))
                    secondStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    thirdStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fourthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                    fifthStateText.setTextColor(ContextCompat.getColor(context,R.color.text_selector))
                }

            }
        }
    }
}