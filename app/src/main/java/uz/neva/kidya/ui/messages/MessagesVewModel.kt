package uz.neva.kidya.ui.messages

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.Messages
import uz.neva.kidya.network.dto.MessagesObject
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event

class MessagesVewModel constructor(private val repository: KidyaRepository): ViewModel() {
    val sendMessagesResponse = MutableLiveData<Event<Resource<Messages>>>()

    fun sendMessages(user_id: String,supplier_id: String,text:String){
        viewModelScope.launch {
            repository.sendMessage(user_id, supplier_id, text).onEach { sendMessagesResponse.value= Event(it) }.launchIn(viewModelScope)
        }
    }

    fun getMessagesResponse(user_id: String) : Flow<PagingData<MessagesObject>> {
        val newResult: Flow<PagingData<MessagesObject>> = repository.getMessages(user_id)
            .cachedIn(viewModelScope)
        return newResult
    }

    fun getChatMessagesResponse(user_id: String,supplier_id:String) : Flow<PagingData<MessagesObject>> {
        val newResult: Flow<PagingData<MessagesObject>> = repository.getMessagesChat(user_id,supplier_id)
            .cachedIn(viewModelScope)
        return newResult
    }


}