package uz.neva.kidya.ui.messages


import android.util.Log
import androidx.paging.PagingSource
import androidx.paging.PagingState
import retrofit2.HttpException
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.dto.MessagesObject
import java.io.IOException

private const val STARTING_PAGE_INDEX = 1

class MessagesPagingSource(private val api: KidyaApi,val user_id:String="",val isChat:Boolean=false,val supplierId:String=""): PagingSource<Int, MessagesObject>(){
    override suspend fun load(params: LoadParams<Int>): LoadResult<Int, MessagesObject> {
        val position =params.key?: STARTING_PAGE_INDEX

        return try {
            val response = if (!isChat) api.getMessages(user_id, position) else api.getSelectedMessages(user_id,supplierId,position)
            val feedPost=response.data

            Log.d("pagination" ,"a ${response.data}")
            LoadResult.Page(
                data=feedPost,
                prevKey = if(position== STARTING_PAGE_INDEX) null else position-1,
                nextKey = when {
                    response.pagination.total_count==1->{
                        null
                    }
                    feedPost.isEmpty() -> {
                        null
                    }
                    response.pagination.current_page == response.pagination.total_count -> {
                        position + 1

                    }
                    response.pagination.current_page < response.pagination.page_count -> {
                        position + 1
                    }
                    else -> null
                }
            )
        }
        catch (exception: IOException) {
            return LoadResult.Error(exception)
        } catch (exception: HttpException) {
            return LoadResult.Error(exception)
        }
    }

    override fun getRefreshKey(state: PagingState<Int, MessagesObject>): Int? = null
}