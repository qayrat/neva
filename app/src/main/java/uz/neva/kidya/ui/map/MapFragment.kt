package uz.neva.kidya.ui.map

import android.content.Context
import android.content.IntentSender
import android.graphics.Color
import android.location.LocationManager
import android.os.Bundle
import android.os.Handler
import android.util.Log
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.*
import com.google.android.gms.tasks.Task
import com.google.android.material.snackbar.Snackbar
import com.google.gson.JsonObject
import com.yandex.mapkit.Animation
import com.yandex.mapkit.MapKitFactory
import com.yandex.mapkit.geometry.Point
import com.yandex.mapkit.location.LocationStatus
import com.yandex.mapkit.logo.Alignment
import com.yandex.mapkit.logo.HorizontalAlignment
import com.yandex.mapkit.logo.VerticalAlignment
import com.yandex.mapkit.map.CameraListener
import com.yandex.mapkit.map.CameraPosition
import com.yandex.mapkit.map.CameraUpdateReason
import com.yandex.mapkit.map.Map
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_map.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.MainActivity
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show
import uz.neva.kidya.utils.showSnackbarWithMargin

//import uz.usoft.kidya.utils.showSnackbar

class MapFragment : Fragment(), CameraListener {

    private val mapViewModel: MapViewModel by viewModel()

    val handler = Handler()
    lateinit var runnable: Runnable

    var chosenAddress = ""
    var chosenAddressName = ""
    var chosenAddressNumber = ""
    var chosenAddressCity = ""
    var chosenAddressDistrict = ""
    var chosenAddressComment = ""

    var lat = ""
    var long = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            chosenAddressName = it.getString("chosenAddressName") ?: ""
            chosenAddressNumber = it.getString("chosenAddressNumber") ?: ""
            chosenAddressCity = it.getString("chosenAddressCity") ?: ""
            chosenAddressDistrict = it.getString("chosenAddressDistrict") ?: ""
            chosenAddressComment = it.getString("chosenAddressComment") ?: ""
        }


        MapKitFactory.setApiKey("1080018e-efa1-4eb6-821b-82f01120bf4c")
        MapKitFactory.initialize(requireContext())


    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_map, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val mainActivity = requireActivity() as MainActivity

        mainActivity.toolbar.hide()
        mainActivity.bottomNavigationView.hide()

        val alignment = Alignment(HorizontalAlignment.LEFT, VerticalAlignment.BOTTOM)
        mapView.map.logo.setAlignment(alignment)

        val mapView = mapView
        mapView.map.move(
            CameraPosition(Point(41.311081, 69.240562), 14.0f, 0.0f, 0.0f),
            Animation(Animation.Type.SMOOTH, 0f),
            null
        )

        mapView.map.addCameraListener(this)

        if (checkIfGpsEnabled())
            findMe()
        else
            requestLocationSettingsOn()

        mapViewModel.addressData.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                        btnChooseLocation.isEnabled = false
                        findMe.isEnabled = false
                    }
                    is Resource.Success -> {
                        val address = resource.data.response.getAsJsonObject("GeoObjectCollection")
                            .getAsJsonArray("featureMember")[0]
                        if (address.isJsonObject) {
                            val gson = (address as JsonObject).getAsJsonObject("GeoObject")
                                .getAsJsonObject("metaDataProperty")
                                .getAsJsonObject("GeocoderMetaData").get("text")

                            Log.i("mapkitlocation", gson.toString())

                            chosenAddress = gson.toString()
                            streetHouseText.text = chosenAddress
                            Timber.d("chosenAddress: $chosenAddress")


                            streetHouseText.visibility = View.INVISIBLE

                            streetHouseText.setTextColor(Color.parseColor("#000000"))
                            cityText.visibility = View.INVISIBLE
                            streetHouseText.show()
                            mapProgressBar.visibility = View.GONE
                            btnChooseLocation.isEnabled = true
                            findMe.isEnabled = true
                        }

                    }
                    is Resource.GenericError -> {

                        btnChooseLocation.isEnabled = false
                        findMe.isEnabled = true
                        Log.i(
                            "mapkitlocation",
                            resource.errorResponse.jsonResponse.getString("error")
                        )
                        mapProgressBar.visibility = View.GONE
//                        showSnackbar(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        Timber.d("error: ${resource.exception.localizedMessage}")
                        btnChooseLocation.isEnabled = false
                        findMe.isEnabled = true
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                        mapProgressBar.visibility = View.GONE
                        resource.exception.message?.let { it1 -> Log.i("mapkitlocation", it1) }
                    }
                }
            }
        })

        mapBack.setOnClickListener {
            findNavController().popBackStack()
        }
        findMe.setOnClickListener {
            if (checkIfGpsEnabled())
                findMe()
            else
                requestLocationSettingsOn()
        }

        btnChooseLocation.setOnClickListener {
            val address = streetHouseText.text.toString()
            if (address.contains("Узбекистан")) {
                if (!address.contains("Ташкентская область")) {
                    if (!address.contains("Ташкентский район")) {

                        val bundle = Bundle()
                        bundle.putString("chosenAddress", chosenAddress)
                        bundle.putString("chosenAddressLatLong", "$lat,$long")
                        bundle.putString("chosenAddressName", chosenAddressName)
                        bundle.putString("chosenAddressNumber", chosenAddressNumber)
                        bundle.putString("chosenAddressCity", chosenAddressCity)
                        bundle.putString("chosenAddressDistrict", chosenAddressDistrict)
                        bundle.putString("chosenAddressComment", chosenAddressComment)

                        PrefManager.saveStreet(requireContext(), chosenAddress)
                        findNavController().popBackStack()
                        findNavController().navigate(R.id.locationDialogFragment,bundle)
                    } else {
                        showSnackbarWithMargin(
                            resources.getString(R.string.text_location_error),
                            Snackbar.LENGTH_LONG
                        )
                    }
                } else {
                    showSnackbarWithMargin(
                        resources.getString(R.string.text_location_error),
                        Snackbar.LENGTH_LONG
                    )
                }

            }else{
                showSnackbarWithMargin(
                    resources.getString(R.string.text_location_error),
                    Snackbar.LENGTH_LONG
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        val mainActivity = requireActivity() as MainActivity

        mainActivity.toolbar.hide()
        mainActivity.bottomNavigationView.hide()

    }

    fun findMe() {
        mapProgressBar.visibility = View.VISIBLE
        MapKitFactory.getInstance().createLocationManager()
            .requestSingleUpdate(object : com.yandex.mapkit.location.LocationListener {
                override fun onLocationUpdated(location: com.yandex.mapkit.location.Location) {
                    mapView?.map?.move(
                        CameraPosition(
                            Point(
                                location.position.latitude,
                                location.position.longitude
                            ), 14.0f, 0.0f, 0.0f
                        ), Animation(Animation.Type.SMOOTH, 0.5f), null
                    )
                }

                override fun onLocationStatusUpdated(locationStatus: LocationStatus) {
                    Log.d("TagCheck", locationStatus.name)
                }
            })
    }

    override fun onStart() {
        super.onStart()
        mapView.onStart()
        MapKitFactory.getInstance().onStart()
    }

    override fun onStop() {
        super.onStop()
        mapView.onStop()
        MapKitFactory.getInstance().onStop()
    }

    private fun checkIfGpsEnabled(): Boolean {
        val lm: LocationManager =
            requireContext().getSystemService(Context.LOCATION_SERVICE) as LocationManager
        return lm.isProviderEnabled(LocationManager.GPS_PROVIDER)
    }

    fun requestLocationSettingsOn() {
        val builder: LocationSettingsRequest.Builder = LocationSettingsRequest.Builder()
            .addLocationRequest(createLocationRequest()!!)

        val client: SettingsClient = LocationServices.getSettingsClient(requireActivity())
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
            // All loc_pin settings are satisfied. The client can initialize
            // loc_pin requests here.
            findMe()
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                try {
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(
                        requireActivity(),
                        1001
                    )
                } catch (sendEx: IntentSender.SendIntentException) {
                    // Ignore the error.
                    Toast.makeText(
                        requireContext(),
                        getString(R.string.warning_enable_permission),
                        Toast.LENGTH_SHORT
                    ).show()
                }

            }
        }
    }

    private fun createLocationRequest(): LocationRequest? {
        return LocationRequest.create()?.apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }
    }

    override fun onCameraPositionChanged(
        map: Map,
        cameraPosition: CameraPosition,
        updateReason: CameraUpdateReason,
        finishedChanging: Boolean
    ) {
        try {
            if (finishedChanging) {
                runnable = Runnable {
                    if (finishedChanging) {
                        lat = cameraPosition.target.latitude.toString()
                        long = cameraPosition.target.longitude.toString()
                        mapViewModel.getAddressData(
                            "814c74ce-2250-497f-9bee-c9b376d64431",
                            long,
                            lat
                        )
                    }
                }
                handler.postDelayed(runnable, 1500)

            } else {
                streetHouseText.visibility = View.VISIBLE
                streetHouseText.text = getString(R.string.detecting_address)
                streetHouseText.setTextColor(Color.parseColor("#b6b6b6"))
                cityText.visibility = View.INVISIBLE
                btnChooseLocation.isEnabled = false

                handler.removeCallbacks(runnable)

            }
        } catch (ex: Exception) {
        }
    }

    override fun onPause() {
        super.onPause()
        handler.removeCallbacks(runnable)
    }

}