package uz.neva.kidya.ui.check_out

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.paging.PagingData
import androidx.paging.cachedIn
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.launchIn
import kotlinx.coroutines.flow.onEach
import kotlinx.coroutines.launch
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.Basket
import uz.neva.kidya.network.dto.Catalog
import uz.neva.kidya.network.dto.CheckOut
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.response.EmptyResponse
import uz.neva.kidya.repository.KidyaRepository
import uz.neva.kidya.utils.Event
import java.lang.Exception

class CheckOutViewModel constructor(private val repository: KidyaRepository) : ViewModel() {
    val addToBasketResponse = MutableLiveData<Event<Resource<Basket>>>()
    val addToBasketBuyNowResponse = MutableLiveData<Event<Resource<Basket>>>()
    val addToFavsResponse = MutableLiveData<Event<Resource<Catalog>>>()
    val deleteFromFavsResponse = MutableLiveData<Event<Resource<Catalog>>>()
    val getBasketResponse = MutableLiveData<Event<Resource<Basket>>>()
    val getCouponListResponse = MutableLiveData<Event<Resource<CouponModel>>>()
    val getCouponInfoResponse = MutableLiveData<Event<Resource<CouponModelInfo>>>()
    val getCheckOutResponse = MutableLiveData<Event<Resource<CheckOut>>>()
    val makeOrderDelivery = MutableLiveData<Event<Resource<Any>>>()
    val analyticsResponse = MutableLiveData<Event<Resource<EmptyResponse>>>()

    fun getAddToBasketResponse(
        user_id: String,
        type: String,
        quantity: String,
        id: String,
        action: String,
        color: String = "",
        razmer: String = ""
    ) {
        viewModelScope.launch {
            repository.addToBasket(user_id, type, quantity, id, action, color, razmer)
                .onEach { addToBasketResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun getAddToBasketBuyNowResponse(
        user_id: String,
        type: String,
        quantity: String,
        id: String,
        action: String,
        color: String = "",
        razmer: String = ""
    ) {
        viewModelScope.launch {
            repository.addToBasket(user_id, type, quantity, id, action, color, razmer)
                .onEach { addToBasketBuyNowResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }


    fun getAddToFavsResponse(
        user_id: String,
        type: String,
        id: String,
        action: String,
        updatePosition: Int = 0,
        updateStatus: Boolean = false
    ) {
        viewModelScope.launch {
            repository.addToFavs(user_id, type, id, action).onEach {
                if (it is Resource.Success) {
                    it.data.updatedPosition = updatePosition
                    it.data.updatedStatus = updateStatus
                }
                addToFavsResponse.value = Event(it)
            }.launchIn(viewModelScope)
        }
    }

    fun getDeleteFromFavsResponse(user_id: String, type: String, id: String, action: String) {
        viewModelScope.launch {
            repository.addToFavs(user_id, type, id, action)
                .onEach { deleteFromFavsResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun getMakeOrderDeliverResponse(
        user_id: String,
        startTime: String,
        endTime: String,
        order_id: String,
        validDate: String
    ) {
        viewModelScope.launch {
            repository.makeOrderDelivery(user_id, startTime, endTime, order_id, validDate)
                .onEach { makeOrderDelivery.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun getGetCheckOutResponse(
        user_id: String,
        coupon: String? = null
    ) {
        viewModelScope.launch {
            repository.checkOutFirsStep(
                user_id = user_id,
                coupon = coupon
            ).onEach { getCheckOutResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }

    fun getGetBasketResponse(user_id: String) {
        viewModelScope.launch {
            repository.getBasket(user_id).onEach { getBasketResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getCouponList(user_id: String) {
        viewModelScope.launch {
            repository.getCoupoList(user_id).onEach { getCouponListResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun getCouponInfo(user_id: String, coupon: String) {
        viewModelScope.launch {
            repository.getCouponInfo(user_id, coupon)
                .onEach { getCouponInfoResponse.value = Event(it) }
                .launchIn(viewModelScope)
        }
    }

    fun putAnalytics(model: AnalyticsModel) {
        viewModelScope.launch {
            repository.putAnalytics(model)
                    .onEach { analyticsResponse.value = Event(it) }.launchIn(viewModelScope)
        }
    }
}