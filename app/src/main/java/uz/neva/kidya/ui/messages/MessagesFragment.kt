package uz.neva.kidya.ui.messages

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.fragment_landind_page.*
import kotlinx.android.synthetic.main.fragment_messages.*
import kotlinx.android.synthetic.main.fragment_selected_message.*
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.R
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show

class MessagesFragment : Fragment() {


    private val messagesVewModel: MessagesVewModel by viewModel()
    private lateinit var adapter: MessagesListAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_messages, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        adapter= MessagesListAdapter()

        mesagesRecyclerVIew.layoutManager=LinearLayoutManager(requireContext())
        mesagesRecyclerVIew.adapter=adapter

        adapter?.onItemClick={
            val bundle=Bundle()
            bundle.putString("supplierId",it.id)
            bundle.putString("supplierName",it.suppiler)
           findNavController().navigate(R.id.action_nav_messages_to_nav_selected_message,bundle)
        }

        messagesBack.setOnClickListener{
            findNavController().popBackStack(R.id.nav_home,false)
            //findNavController().navigate(R.id.nav_home)
        }
        getFeedPosts()
        viewLifecycleOwner.lifecycleScope.launch {
            adapter.loadStateFlow.collectLatest {loadStates->
                if (loadStates.refresh is LoadState.Loading)
                    progressIndicatorMessages.show()
                else
                    progressIndicatorMessages.hide()
            }
        }
    }

    private fun getFeedPosts() {
        lifecycleScope.launch {
            messagesVewModel.getMessagesResponse(PrefManager.getUserID(requireContext())).collect { product ->

                mesagesRecyclerVIew.adapter = adapter
                adapter.submitData(product)

                return@collect
            }
        }
    }
}