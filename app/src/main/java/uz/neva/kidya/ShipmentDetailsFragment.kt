package uz.neva.kidya

import android.annotation.SuppressLint
import android.app.Activity
import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.Gravity
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.navigation.fragment.findNavController
import com.bumptech.glide.Glide
import com.google.android.material.dialog.MaterialAlertDialogBuilder
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.calendar_layout.*
import kotlinx.android.synthetic.main.calendar_layout.view.*
import kotlinx.android.synthetic.main.fragment_shipment_details.*
import kotlinx.android.synthetic.main.location_layout.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.ui.shipmentDetails.OrderResponseDialogFragment
import uz.neva.kidya.utils.formatPhoneMask
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.hideKeyboard
import uz.neva.kidya.utils.showSnackbarWithMargin
import java.text.SimpleDateFormat
import java.util.*

class ShipmentDetailsFragment : Fragment(R.layout.fragment_shipment_details), SelectedTimeLayout,
    SetOnDataClickListener {

    private val checkOutViewModel: CheckOutViewModel by viewModel()
    private val profileViewModel: ProfileViewModel by viewModel()
    private val homeViewModel: HomeViewModel by viewModel()
    private var date = ""
    private var startTime = ""
    private var endTime = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).bottom_navigation.hide()
    }

    @SuppressLint("SetTextI18n")
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        profileViewModel.getProfile(PrefManager.getUserID(requireContext()))

        profileViewModel.getProfileResponse.observe(
                viewLifecycleOwner,
                {
                    it.getContentIfNotHandled()?.let { resource ->
                        when (resource) {
                            is Resource.Loading -> {
                            }
                            is Resource.Success -> {
                                val name = resource.data.data.name ?: ""
                                val location = resource.data.data.address ?: ""
                                val phone = resource.data.data.phone ?: ""

                                if (PrefManager.getNameDelivery(requireContext()).removePrefix(" ")
                                                .isNotEmpty()
                                ) {
                                    profile_name.text = PrefManager.getNameDelivery(requireContext())
                                } else if (name.isNotEmpty()) {
                                    profile_name.text = name
                                }

                                if (PrefManager.getDeliveryAddress(requireContext()).isNotEmpty()) {
                                    location_text.text =
                                            PrefManager.getDeliveryAddress(requireContext())
                                } else if (location.isNotEmpty()) {
                                    location_text.text = location
                                }

                                if (PrefManager.getPhoneDeliver(requireContext()).isNotEmpty()) {
                                    profile_phone.text = PrefManager.getPhoneDeliver(requireContext())
                                } else if (phone.isNotEmpty()) {
                                    profile_phone.text = "+$phone"
                                }
                            }
                            is Resource.GenericError -> {

                            }
                            is Resource.Error -> {

                            }

                        }
                    }
                })


        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        location_change.setOnClickListener {
            LocationDialogFragment.listener = this
            findNavController().navigate(R.id.locationDialogFragment)
        }

        if (PrefManager.getDate(requireContext())
                .isNotEmpty() && PrefManager.getHour(requireContext()).isNotEmpty()
        ) {
            date_text.text =
                "${PrefManager.getDate(requireContext())} ${PrefManager.getHour(requireContext())}"
        }

        card_change.setOnClickListener {
            findNavController().navigate(R.id.paymentMethodFragment)
        }

        see_all.setOnClickListener {
            findNavController().navigate(R.id.checkBasketFragment)
        }

        val dialog = CalendarDialogFragment(this, this)
        date_change.setOnClickListener {
            if (dialog.isAdded) {
                return@setOnClickListener
            } else {
                dialog.show((requireActivity() as MainActivity).supportFragmentManager, "dialog")
            }
        }

        profileViewModel.getGetOrderDetailsResponse(
                PrefManager.getUserID(requireContext()),
                PrefManager.getOrderId(requireContext()).toString()
        )
        profileViewModel.getOrderDetailsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    is Resource.Success -> {
                        if (PrefManager.getCardNumber(requireContext()).isNotEmpty())
                            card_number.text = PrefManager.getCardNumber(requireContext())
                        product_count.text =
                                "${resources.getString(R.string.text_my_basket)} (${resource.data.products?.size})"
                        order_id.text = PrefManager.getOrderId(requireContext()).toString()

                        val price = resource.data.data.firstOrNull { e ->
                            e.id == PrefManager.getOrderId(
                                    requireContext()
                            ).toString()
                        }?.price
                        val totalPrice = resource.data.data.firstOrNull { e ->
                            e.id == PrefManager.getOrderId(
                                    requireContext()
                            ).toString()
                        }?.total_price
                        val deliveryPrice = resource.data.data.firstOrNull { e ->
                            e.id == PrefManager.getOrderId(
                                    requireContext()
                            ).toString()
                        }?.price_delivery
                        total_price.text = "${totalPrice} сум"
                        order_type.text = "${deliveryPrice} сум"
                        subtotal_price.text = "${price} сум"

                        when (resource.data.products?.size) {
                            1 -> {
                                Glide.with(requireContext())
                                        .load(resource.data.products[0].preview_picture)
                                        .into(image1)
                            }
                            2 -> {
                                Glide.with(requireContext())
                                        .load(resource.data.products[0].preview_picture)
                                        .into(image1)
                                Glide.with(requireContext())
                                        .load(resource.data.products[1].preview_picture)
                                        .into(image2)
                            }
                            3 -> {
                                Glide.with(requireContext())
                                        .load(resource.data.products[0].preview_picture)
                                        .into(image1)
                                Glide.with(requireContext())
                                        .load(resource.data.products[1].preview_picture)
                                        .into(image2)
                                Glide.with(requireContext())
                                        .load(resource.data.products[2].preview_picture)
                                        .into(image3)
                            }
                            4 -> {
                                Glide.with(requireContext())
                                        .load(resource.data.products[0].preview_picture)
                                        .into(image1)
                                Glide.with(requireContext())
                                        .load(resource.data.products[1].preview_picture)
                                        .into(image2)
                                Glide.with(requireContext())
                                        .load(resource.data.products[2].preview_picture)
                                        .into(image3)
                                Glide.with(requireContext())
                                        .load(resource.data.products[3].preview_picture)
                                        .into(image4)
                            }
                            else -> {

                            }
                        }
                    }
                    is Resource.GenericError -> {
                        showSnackbarWithMargin(resource.errorResponse.jsonResponse.getString("error"))
                    }
                    is Resource.Error -> {
                        resource.exception.message?.let { it1 -> showSnackbarWithMargin(it1) }
                    }
                }
            }
        })

        payNow.setOnClickListener {
            if (profile_name.text.toString() != resources.getString(R.string.text_user_surname) && (location_text.text.toString() != resources.getString(
                            R.string.text_delivery_address
                    ))
                && PrefManager.getOrderId(requireContext()) != 0 && profile_phone.text.toString() != "+998 (xx) xxx-xx-xx"
            ) {
                if (PrefManager.getDate(requireContext())
                        .isNotEmpty() && date_text.text.toString() != resources.getString(R.string.text_select_delivery_time)
                ) {
                    ic_error_hour.visibility = View.GONE
                    if (PrefManager.getCardId(requireContext())
                            .isNotEmpty() && card_number.text != resources.getString(R.string.text_card_not_select)
                    ) {
                        ic_error.visibility = View.GONE
                        payNow.startAnimation {
                            homeViewModel.payCard(
                                    PrefManager.getServerToken(
                                            requireContext()
                                    ),
                                    PrefManager.getOrderId(requireContext()).toString(),
                                    PrefManager.getCardId(requireContext()),
                                    profile_name.text.toString(),
                                    profile_phone.text.toString(),
                                    location_text.text.toString(),
                                    location = PrefManager.getDeliveryLocation(requireContext()),
                                    location_text.text.toString()
                            )
                        }
                    } else {
                        showSnackbarWithMargin(resources.getString(R.string.text_select_pay_card))
                        ic_error2.visibility = View.VISIBLE
                    }
//                    if (PrefManager.getHour(requireContext()) != "15:00 - 18:00") {
//
//                    } else {
//                        MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_App_MaterialAlertDialog)
//                            .setMessage(R.string.text_time_message)
//                            .setPositiveButton(getString(R.string.ok)) {dialog, which ->
//                                dialog.dismiss()
//                            }
//                            .show()
//                    }
                } else {
                    showSnackbarWithMargin(resources.getString(R.string.text_select_delivery_time))
                    ic_error_hour.visibility = View.VISIBLE
                }
            } else {
                Toast.makeText(
                        requireContext(),
                        resources.getString(R.string.text_error_input_your_info),
                        Toast.LENGTH_SHORT
                ).show()
                ic_error.visibility = View.VISIBLE
            }
        }
        checkOutViewModel.makeOrderDelivery.observe(viewLifecycleOwner, {
            it?.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {

                    }
                    else -> {
                        payNow.apply {
                            revertAnimation()
                            background = resources.getDrawable(
                                    R.drawable.button_save,
                                    requireActivity().theme
                            )
                        }
                        PrefManager.saveHour(requireContext(), "")

                        val bundle = Bundle()
                        bundle.putString(
                                "order_id",
                                PrefManager.getOrderId(requireContext()).toString()
                        )
                        findNavController().navigate(R.id.orderStatusFragment, bundle)
                    }
                }
            }
        })

        homeViewModel.payCardResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
//                                payNow.isEnabled = false
                    }
                    is Resource.Success -> {

                        if (resource.data.data.error != null) {
                            openMessageDialog(resource.data.data.error.message)
                            payNow.apply {
                                revertAnimation()
                                background = resources.getDrawable(
                                        R.drawable.button_save,
                                        requireActivity().theme
                                )
                            }
                        } else
                            checkOutViewModel.getMakeOrderDeliverResponse(
                                    PrefManager.getUserID(requireContext()),
                                    startTime,
                                    endTime,
                                    PrefManager.getOrderId(requireContext()).toString(),
                                    date
                            )
                    }
                    is Resource.GenericError -> {
                        //      makePayment.isEnabled=true
                        showSnackbarWithMargin(
                                resource.errorResponse.jsonResponse.getString(
                                        "status"
                                )
                        )
                        payNow.apply {
                            revertAnimation()
                            background = resources.getDrawable(
                                    R.drawable.button_save,
                                    requireActivity().theme
                            )
                        }
                    }
                    is Resource.Error -> {
                        //      makePayment.isEnabled=true
                        resource.exception.message?.let { it1 ->
                            showSnackbarWithMargin(
                                    it1
                            )
                        }
                        payNow.apply {
                            revertAnimation()
                            background = resources.getDrawable(
                                    R.drawable.button_save,
                                    requireActivity().theme
                            )
                        }
                    }
                }
            }
        })
    }

    private fun openMessageDialog(error: String) {
        val fragment = OrderResponseDialogFragment()
        val bundle = Bundle()
        bundle.putString("orderResponseMessageDialog", error)
        fragment.arguments = bundle
        fragment.show(childFragmentManager, "OrderResponseDialog")
    }


    override fun isSelect(date: String, hour: String) {
        date_text.text =
            "$date $hour"
        this.date = date
//        this.startTime = "${hour.substring(0, 5)}:00"
//        this.endTime = "${hour.substring(hour.length - 5, hour.length)}:00"
        Timber.d("hour: ${hour}")
    }

    override fun onDataSet(name: String, number: String, address: String) {
        profile_name.text = name
        location_text.text = address
        profile_phone.text = number
    }

    override fun onResume() {
        super.onResume()
        (requireActivity() as MainActivity).bottom_navigation.hide()
    }
}

class LocationDialogFragment() : DialogFragment() {

    companion object {
        var listener: SetOnDataClickListener? = null
        const val TAG = "LocationDialogFragment"
    }

    private var adress = ""
    private var name = ""
    private var number = ""

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen
        )
    }


    @SuppressLint("SetTextI18n")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(
                DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen
        )

        adress = arguments?.getString("chosenAddress") ?: ""
        Log.d(TAG, "onCreateDialog: $adress")
        val builder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.location_layout, null)

        builder
            .setView(dialogView)

        val dialogD = builder.create()
        dialogD.window?.attributes?.windowAnimations = R.style.DialogAnimation;
        val wmlp = dialogD.window?.attributes

        wmlp?.gravity = Gravity.TOP or Gravity.CENTER_HORIZONTAL
        wmlp?.x = 100 //x position

        wmlp?.y = 0 //y position

        dialogView.apply {
            adress = PrefManager.getProfileAddress(requireContext())

            if (PrefManager.getNameDelivery(requireContext()).removePrefix(" ").isNotEmpty())
                et_name_receiver.setText(PrefManager.getNameDelivery(requireContext()))
            else if (PrefManager.getName(requireContext()).removePrefix(" ").isNotEmpty())
                et_name_receiver.setText(PrefManager.getName(requireContext()))

            if (PrefManager.getPhoneDeliver(requireContext()).isNotEmpty())
                et_number_receiver.setText(PrefManager.getPhoneDeliver(requireContext()))
            else if (PrefManager.getPhone(requireContext()).isNotEmpty())
                et_number_receiver.setText(PrefManager.getPhone(requireContext()))

            if (PrefManager.getDeliveryAddress(requireContext()).isNotEmpty())
                et_address_receiver.setText(PrefManager.getDeliveryAddress(requireContext()))
            else if (PrefManager.getProfileAddress(requireContext()).isNotEmpty())
                et_address_receiver.setText(PrefManager.getProfileAddress(requireContext()))

            et_address_receiver.setOnClickListener {
                saveData(this)
            }

            cancel_button.setOnClickListener {
                hideKeyboard()
                dialog?.dismiss()
            }

            save_button.setOnClickListener {
                saveData(this)
                if (et_name_receiver.text!!.isNotEmpty() &&
                        et_number_receiver.text!!.isNotEmpty() &&
                        et_address_receiver.text!!.isNotEmpty()
                ) {
                    hideKeyboard()
                    listener?.onDataSet(
                            PrefManager.getNameDelivery(requireContext()),
                            PrefManager.getPhoneDeliver(requireContext()),
                            PrefManager.getDeliveryAddress(requireContext())
                    )
                    dialog?.dismiss()
                }
            }
            detect_location.setOnClickListener {
                saveData(this)
                findNavController().navigate(R.id.nav_map)
            }
            et_address_receiver.setText(adress)
            et_number_receiver.formatPhoneMask()

            account_detail.setOnClickListener {
            }
        }

        return dialogD
    }

    private fun saveData(dialogView: View) {
        dialogView.apply {

            if (et_name_receiver.text!!.isEmpty()) {
                et_name_receiver.error = resources.getString(R.string.text_error_input_edit_text)
            } else {
                name = et_name_receiver.text.toString()
            }

            if (et_number_receiver.text!!.isEmpty()) {
                et_number_receiver.error = resources.getString(R.string.text_error_input_edit_text)
            } else {
                number = et_number_receiver.text.toString()
            }

            if (et_address_receiver.text!!.isEmpty()) {
                et_address_receiver.error = resources.getString(R.string.text_error_input_edit_text)
            } else {
                adress = et_address_receiver.text.toString()
            }
            PrefManager.savePhoneDelivey(requireContext(), number)
            PrefManager.saveNameDelivery(requireContext(), name)
            PrefManager.saveDeliveryAddress(requireContext(), adress)

        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        val imm: InputMethodManager = requireContext().getSystemService(Activity.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0)
    }
}

interface SetOnDataClickListener {
    fun onDataSet(name: String, number: String, address: String)
}

class CalendarDialogFragment(val listener: SelectedTimeLayout, val fragment: Fragment) :
    DialogFragment() {
    companion object {
        const val TAG = "CalendarDialogFragment"
    }

    private var hour1 = ""
    private var checkMessage = ""
    private var selectedDay = 0
    private var selectedMonth = 0
    private var selectedYear = 0

    override fun onStart() {
        super.onStart()
        dialog?.window?.setLayout(
                WindowManager.LayoutParams.MATCH_PARENT,
                WindowManager.LayoutParams.WRAP_CONTENT
        )
        dialog?.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setStyle(
                DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen
        )
    }

    @SuppressLint("ResourceAsColor")
    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        setStyle(
                DialogFragment.STYLE_NORMAL,
                android.R.style.Theme_DeviceDefault_Light_NoActionBar_Fullscreen
        )

        val builder = AlertDialog.Builder(requireContext())
        val dialogView = layoutInflater.inflate(R.layout.calendar_layout, null)

        builder
            .setView(dialogView)

        val dialogD = builder.create()
        dialogD.window?.attributes?.windowAnimations = R.style.DialogAnimation;
        val wmlp = dialogD.window?.attributes

        wmlp?.gravity = Gravity.TOP or Gravity.CENTER_HORIZONTAL
        wmlp?.x = 100 //x position

        wmlp?.y = 0 //y position


        dialogView.apply {
            val date = Date()
            val c = Calendar.getInstance()
            c.time = date
            val t = c[Calendar.HOUR_OF_DAY] * 100 + c[Calendar.MINUTE]
            var d = c[Calendar.DAY_OF_MONTH]
            Log.d(TAG, "onCreateDialog: $t $d")
            selectedDay = c[Calendar.DAY_OF_MONTH]
            selectedMonth = c[Calendar.MONTH]
            selectedYear = c[Calendar.YEAR]

//            verifyTime(t, dialogView)

            firstTimeType.setOnClickListener {
                this.firstTimeType.isSelected = true
                this.secondTimeType.isSelected = false
                this.thirdTimeType.isSelected = false
                hour1 = textFirstTimeType.text.toString()
                this.textFirstTimeType.setTextColor(Color.WHITE)
                this.textSecondTimeType.setTextColor(R.color.text_time_color)
                this.textThirdTimeType.setTextColor(R.color.text_time_color)
            }

            secondTimeType.setOnClickListener {
                this.firstTimeType.isSelected = false
                this.secondTimeType.isSelected = true
                this.thirdTimeType.isSelected = false
                hour1 = textSecondTimeType.text.toString()
                this.textFirstTimeType.setTextColor(R.color.text_time_color)
                this.textSecondTimeType.setTextColor(Color.WHITE)
                this.textThirdTimeType.setTextColor(R.color.text_time_color)
            }

            thirdTimeType.setOnClickListener {
                this.firstTimeType.isSelected = false
                this.secondTimeType.isSelected = false
                this.thirdTimeType.isSelected = true
                hour1 = textThirdTimeType.text.toString()
                this.textFirstTimeType.setTextColor(R.color.text_time_color)
                this.textSecondTimeType.setTextColor(R.color.text_time_color)
                this.textThirdTimeType.setTextColor(Color.WHITE)
            }

            val calendar: Calendar = Calendar.getInstance()


            val dateMin: Long = if (t > 1801) {
                calendar.getTime().getTime() + 24 * 60 * 60 * 1000
            } else {
                calendar.getTime().getTime()
            }


            val dateMax: Long = calendar.time.time + 120 * 60 * 60 * 1000

            calendar_view.minDate = dateMin
            calendar_view.maxDate = dateMax

            d = c[Calendar.DAY_OF_MONTH]
            val dt = c[Calendar.DATE]

            val df = SimpleDateFormat("yyyy-MM-dd", Locale.getDefault())
            val formattedDate = df.format(calendar.time)

            PrefManager.saveDate(requireContext(), "$formattedDate")

            calendar_view.setOnDateChangeListener { view, year, month, dayOfMonth ->
                selectedDay = dayOfMonth
                selectedMonth = month
                selectedYear = year
//                dialogView.ll_time.isVisible =
//                    dayOfWeek != Calendar.SATURDAY && dayOfWeek != Calendar.SUNDAY
            }

            btn_cancel.setOnClickListener {
                dialog?.dismiss()
            }

            btn_save.setOnClickListener {
                val cal = Calendar.getInstance()
                cal.set(selectedYear, selectedMonth, selectedDay)
                when (cal.get(Calendar.DAY_OF_WEEK)) {
                    Calendar.SUNDAY -> {
                        checkMessage = resources.getString(R.string.text_error_select_weekend)
                        showMessageDialog()
                    }
                    Calendar.SATURDAY -> {
                        checkMessage = resources.getString(R.string.text_error_select_weekend)
                        showMessageDialog()
                    }
                    else -> {
                        val calendar2 = Calendar.getInstance()
                        if (calendar2.get(Calendar.DAY_OF_MONTH) != cal.get(Calendar.DAY_OF_MONTH)) {
                            validTime(d, t, selectedMonth, selectedDay, selectedYear, dialogView)
                            successCheckDate()
                            Timber.d("select day=${cal.get(Calendar.DAY_OF_MONTH)} || today=${calendar2.get(Calendar.DAY_OF_MONTH)}")
                        } else {
                            if (calendar2.get(Calendar.HOUR) in 9..14) {
                                Timber.d("hour in 9..14: hour=${calendar2.get(Calendar.HOUR)}")
                                validTime(d, t, selectedMonth, selectedDay, selectedYear, dialogView)
                                successCheckDate()
                            } else {
                                checkMessage = resources.getString(R.string.text_time_message2)
                                showMessageDialog()
                            }
                        }
                    }
                }
//                if (hour1.isNotEmpty()) {
//                    PrefManager.saveHour(requireContext(), hour1)
//                    listener.isSelect(PrefManager.getDate(requireContext()), hour1)
//                    dialog?.dismiss()
//                } else {
//                    fragment.showSnackbarWithMargin(resources.getString(R.string.text_select_time))
//                }
            }
        }
        return dialogD
    }

    private fun showMessageDialog() {
        MaterialAlertDialogBuilder(requireContext(), R.style.ThemeOverlay_App_MaterialAlertDialog)
                .setMessage(checkMessage)
                .setPositiveButton(getString(R.string.ok)) { dialog, which ->
                    dialog.dismiss() }
                .show()
    }

    private fun successCheckDate() {
        PrefManager.saveHour(requireContext(), hour1)
        listener.isSelect(PrefManager.getDate(requireContext()), hour1)
        dialog?.dismiss()
    }

    private fun validTime(
            currentDay: Int,
            currentTime: Int,
            month: Int,
            dayOfMonth: Int,
            year: Int,
            dialogView: View
    ) {
        if (dayOfMonth != currentDay) {
            dialogView.apply {
//                this.firstTimeType.visibility = View.VISIBLE
//                this.secondTimeType.visibility = View.VISIBLE
//                this.thirdTimeType.visibility = View.VISIBLE
            }
        } else {
//            verifyTime(currentTime, dialogView)
        }

        val month2 = month + 1
        if (month2.toString().length == 1) {
            PrefManager.saveDate(requireContext(), "${year}-0${month2}-${dayOfMonth}")
        } else
            PrefManager.saveDate(requireContext(), "${year}-${month2}-${dayOfMonth}")
    }

    private fun verifyTime(t: Int, dialogView: View) {
        dialogView.apply {
            if (t in 900..1200) {
                firstTimeType.visibility = View.GONE
                secondTimeType.visibility = View.VISIBLE
                thirdTimeType.visibility = View.VISIBLE
            } else if (t in 1201..1500) {
                firstTimeType.visibility = View.GONE
                secondTimeType.visibility = View.GONE
                thirdTimeType.visibility = View.VISIBLE
            } else if (t in 1501..1800) {
                tv_time.text =
                    resources.getString(R.string.text_time_message)
                tv_time.setTextColor(Color.RED)
                firstTimeType.visibility = View.GONE
                secondTimeType.visibility = View.GONE
                thirdTimeType.visibility = View.GONE
            } else {
                firstTimeType.visibility = View.VISIBLE
                secondTimeType.visibility = View.VISIBLE
                thirdTimeType.visibility = View.VISIBLE
            }
        }
    }
}

interface SelectedTimeLayout {
    fun isSelect(date: String, hour: String)
}