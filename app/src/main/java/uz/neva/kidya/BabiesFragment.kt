package uz.neva.kidya

import android.content.Context
import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.mikepenz.fastadapter.FastAdapter
import com.mikepenz.fastadapter.adapters.ItemAdapter
import com.mikepenz.fastadapter.items.AbstractItem
import kotlinx.android.synthetic.main.app_bar_main.*
import kotlinx.android.synthetic.main.fragment_babies.*
import kotlinx.android.synthetic.main.item_baby.view.*
import org.koin.androidx.viewmodel.ext.android.viewModel
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.utils.PrefUtils
import uz.neva.kidya.utils.hide

class BabiesFragment : Fragment(R.layout.fragment_babies) {

    private val itemAdapter = ItemAdapter<BabyModel>()
    private val fastAdapter = FastAdapter.with(itemAdapter)
    private val profileViewModel: ProfileViewModel by viewModel()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        (requireActivity() as MainActivity).toolbar.hide()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        backIcon.setOnClickListener {
            findNavController().popBackStack()
        }

        textView30.setOnClickListener {
            findNavController().navigate(R.id.addBabyFragment)
        }
        addBabyData.setOnClickListener {
            findNavController().navigate(R.id.addBabyFragment)
        }

        babiesList.layoutManager = LinearLayoutManager(requireContext())
        babiesList.adapter = fastAdapter
        itemAdapter.clear()
        itemAdapter.add(PrefUtils.getBaby())

    }

}


data class BabyModel(
    val id: Int,
    val fullName: String,
    val date: String?,
    val gender: String,
) : AbstractItem<BabyModel.VH>() {

    var expand = false

    override var identifier: Long
        get() = id.toLong()
        set(value) {}

    class VH(itemView: View) : FastAdapter.ViewHolder<BabyModel>(itemView) {
        override fun bindView(item: BabyModel, payloads: List<Any>) {
            itemView.apply {
                baby_full_name.text = item.fullName
                baby_date_birth.text = item.date
                baby_gender.text = item.gender
            }
        }

        override fun unbindView(item: BabyModel) {

        }

    }

    override val layoutRes: Int
        get() = R.layout.item_baby
    override val type: Int
        get() = 0

    override fun getViewHolder(v: View): VH = VH(v)
}