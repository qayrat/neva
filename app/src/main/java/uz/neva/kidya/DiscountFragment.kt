package uz.neva.kidya

import android.content.Context
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.core.widget.NestedScrollView
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.navigation.fragment.findNavController
import androidx.paging.LoadState
import androidx.recyclerview.widget.GridLayoutManager
import kotlinx.android.synthetic.main.fragment_discount.*
import kotlinx.android.synthetic.main.fragment_discount.blockerView
import kotlinx.android.synthetic.main.fragment_discount.dealsBack
import kotlinx.android.synthetic.main.fragment_discount.dealsRecycler
import kotlinx.android.synthetic.main.fragment_discount.progressIndicatorDeals
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.collectLatest
import kotlinx.coroutines.launch
import org.koin.androidx.viewmodel.ext.android.viewModel
import timber.log.Timber
import uz.neva.kidya.data.PrefManager
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.dto.ProductObject
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.ViewPageData
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.SpacesItemDecoration
import uz.neva.kidya.ui.category.selectedCategory.SelectedCategoryAdapter
import uz.neva.kidya.ui.category.subcategories.adapter.SubCategoryLoadStateAdapter
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.utils.ClickEvents
import uz.neva.kidya.utils.hide
import uz.neva.kidya.utils.show

class DiscountFragment : Fragment(R.layout.fragment_discount) {
    private val categoriesViewModel: CategoriesViewModel by viewModel()
    private val checkOutViewModel: CheckOutViewModel by viewModel()
    var type = ""
    var title = ""
    private lateinit var categoryAdapter: SelectedCategoryAdapter
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        arguments?.let {
            type = it.getString("type").toString()
            title = it.getString("section_name").toString()
        }
        if (type.isEmpty())
            type = "hits"
        getFeedPostsDeals(type)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        categoriesViewModel.putAnalytics(AnalyticsModel(
                entity = "analytics",
                method = "setEvent",
                ViewPageData(type = "view", page = "Discounts")
        ))
        categoriesViewModel.analyticsResponse.observe(viewLifecycleOwner, {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {}
                    is Resource.Success -> {
                        Timber.d("${resource.data.status}, ${resource.data.data}")
                    }
                    is Resource.GenericError -> {
                        Timber.d(resource.errorResponse.toString())
                    }
                    is Resource.Error -> {
                        Timber.d(resource.exception.localizedMessage)
                    }
                }
            }
        })

        val scale = resources.displayMetrics.density
        val marginpixels = (16 * scale + 0.5f).toInt()
        val footerAdapter = SubCategoryLoadStateAdapter { categoryAdapter.retry() }
        val gridLayoutManager = GridLayoutManager(requireContext(), 2)
        dealsRecycler.apply {
            layoutManager = gridLayoutManager
            addItemDecoration(SpacesItemDecoration(marginpixels, true, 2))
            adapter = categoryAdapter.withLoadStateFooter(
                    footer = footerAdapter
            )
        }
        gridLayoutManager.spanSizeLookup = object : GridLayoutManager.SpanSizeLookup() {
            override fun getSpanSize(position: Int): Int {
                return if (position == categoryAdapter.itemCount && footerAdapter.itemCount > 0) {
                    2
                } else {
                    1
                }
            }
        }

        categoryAdapter?.click = {
            when (it) {
                is ClickEvents.onAddToFav -> {

                    Log.i("fav", "fagment recived index ${it.index}")
                    addToFavs(it.product, it.index)
                }
                is ClickEvents.onItemClick -> {
                    val bundle: Bundle = Bundle()
                    bundle.putString("section_id", it.product.fields.iblock_section_id)
                    bundle.putString("product_id", it.product.fields.id)

                    findNavController().navigate(R.id.nav_selectedItem, bundle)
                }
            }
        }


        checkOutViewModel.addToFavsResponse.observe(viewLifecycleOwner, Observer {
            it.getContentIfNotHandled()?.let { resource ->
                when (resource) {
                    is Resource.Loading -> {
                        blockerView.show()
                    }
                    is Resource.Success -> {
                        blockerView.hide()
                        categoryAdapter.update(
                            resource.data.updatedPosition,
                            resource.data.updatedStatus
                        )

//                        if (resource.data.updatedStatus)
////                            showSnackbarWithMargin("Добавлена")
//                        else
////                            showSnackbarWithMargin("Удален")
                    }
                    is Resource.GenericError -> {

                        blockerView.hide()
//                        showSnackbarWithMargin(
//                            resource.errorResponse.jsonResponse.getString(
//                                "error"
//                            )
//                        )
                    }
                    is Resource.Error -> {

                        blockerView.hide()
//                        resource.exception.message?.let { it1 -> showSnackbar(it1) }
                    }
                }
            }
        })

        dealsBack.setOnClickListener {
            findNavController().popBackStack()
        }

        viewLifecycleOwner.lifecycleScope.launch {
            categoryAdapter.loadStateFlow.collectLatest { loadStates ->
                if (loadStates.refresh is LoadState.Loading)
                    progressIndicatorDeals.show()
                else {
                    progressIndicatorDeals.hide()
                    shimmer_view_discount_container.stopShimmer()
                    shimmer_view_discount_container.visibility = View.GONE
                    dealsRecycler.visibility = View.VISIBLE
                }
            }
        }
    }

    private fun getFeedPostsDeals(type: String = "hits") {
        lifecycleScope.launch {
            categoriesViewModel.getSaleProductsResponse(
                PrefManager.getUserID(requireContext())
            ).collect { product ->

                categoryAdapter = SelectedCategoryAdapter(requireContext())
                categoryAdapter.submitData(product)

                return@collect
            }
        }
    }

    fun addToFavs(product: ProductObject, int: Int) {
        if (PrefManager.getUserID(requireContext()).isEmpty()) {
//            showSnackbarAuth("вам необходимо")
        } else {
            Log.i("fav", "fagment inside function index ${int}")
            if (product.fields.in_favourites != "Y") {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "add",
                    int,
                    true
                )

            } else {
                checkOutViewModel.getAddToFavsResponse(
                    PrefManager.getUserID(requireContext()),
                    "favs",
                    product.fields.id,
                    "delete",
                    int,
                    false
                )
            }
        }
    }

    override fun onResume() {
        super.onResume()
        shimmer_view_discount_container.startShimmer()
    }

    override fun onPause() {
        shimmer_view_discount_container.stopShimmer()
        super.onPause()
    }

}