package uz.neva.kidya.di

import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.dsl.module
import uz.neva.kidya.ui.category.CategoriesViewModel
import uz.neva.kidya.ui.category.filter.FilterViewModel
import uz.neva.kidya.ui.check_out.CheckOutViewModel
import uz.neva.kidya.ui.home.HomeViewModel
import uz.neva.kidya.ui.log_in.LoginViewModel
import uz.neva.kidya.ui.map.MapViewModel
import uz.neva.kidya.ui.messages.MessagesVewModel
import uz.neva.kidya.ui.my_details.ProfileViewModel
import uz.neva.kidya.ui.news.NewsViewModel
import uz.neva.kidya.ui.notification.NotificationViewModel

val viewModelsModule= module {
        viewModel{NewsViewModel(get())}
        viewModel{HomeViewModel(get())}
        viewModel{LoginViewModel(get()) }
        viewModel{CategoriesViewModel(get()) }
        viewModel{CheckOutViewModel(get()) }
        viewModel{ ProfileViewModel(get()) }
        viewModel{ MessagesVewModel(get()) }
        viewModel{ NotificationViewModel(get()) }
        viewModel{ FilterViewModel(get()) }
        viewModel{ MapViewModel(get()) }
}