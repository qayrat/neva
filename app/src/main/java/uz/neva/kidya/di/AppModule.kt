package uz.neva.kidya.di

import org.koin.dsl.module
import uz.neva.kidya.repository.KidyaRepository

val appModule= module {
    single { KidyaRepository(api=get()) }
}