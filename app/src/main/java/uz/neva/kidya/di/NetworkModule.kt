package uz.neva.kidya.di

import android.content.Context
import android.util.Base64
import com.readystatesoftware.chuck.ChuckInterceptor
import com.squareup.moshi.Moshi
import com.squareup.moshi.kotlin.reflect.KotlinJsonAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.ResponseBody
import org.koin.dsl.module
import retrofit2.Converter
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory
import uz.neva.kidya.BuildConfig
import uz.neva.kidya.data.getToken
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.LogoutInterceptor
import uz.neva.kidya.network.dto.ErrorResponse
import java.util.concurrent.TimeUnit

private const val BASE_URL: String="http://bitrix.neva.uz/"
//private const val BASE_URL: String="http://ikids.incorp.uz/"

val networkModule= module {
    single<KidyaApi> {
        val retrofit=get<Retrofit>()
        retrofit.create(KidyaApi::class.java)
    }

    single<Moshi> {
        Moshi.Builder()
            .add(KotlinJsonAdapterFactory())
            .build()
    }

    single<Retrofit>{
        Retrofit.Builder()
            .baseUrl(BASE_URL)
            .client(get<OkHttpClient>())
            .addConverterFactory((MoshiConverterFactory.create(get())))
            .build()
    }
    single {
        val clientBuilder=OkHttpClient.Builder()
            .connectTimeout(3000,TimeUnit.SECONDS)
            .readTimeout(3000,TimeUnit.SECONDS)
            .writeTimeout(3000,TimeUnit.SECONDS)
            .addInterceptor(LogoutInterceptor(context=get()))
            .addInterceptor{chain ->
                val token: String = get<Context>().getToken()
                try {
                    val request = chain.request().newBuilder()
                    request.addHeader("Content-type", "application/json")
                    request.addHeader("X-Requested-With", "XMLHttpRequest")
                    if (token != "") {
                        val credentials: String = "$token:"
                        val base64EncodedCredentials: String =
                            Base64.encodeToString(credentials.toByteArray(), Base64.NO_WRAP)

                        request.addHeader("Authorization", "Basic $base64EncodedCredentials")
                    }
                    return@addInterceptor chain.proceed(request.build())
                } catch (e: Throwable) {

                }
                return@addInterceptor chain.proceed(chain.request())
            }
        if (BuildConfig.DEBUG) {
            clientBuilder.addInterceptor(ChuckInterceptor(get()))
        }
        clientBuilder.build()
    }
    factory<Converter<ResponseBody, ErrorResponse>> {
        get<Retrofit>().responseBodyConverter(
            ErrorResponse::class.java,
            arrayOfNulls<Annotation>(0)
        )
    }
}