package uz.neva.kidya.repository

import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import uz.neva.kidya.network.KidyaApi
import uz.neva.kidya.network.Resource
import uz.neva.kidya.network.safeApiCall
import androidx.paging.Pager
import androidx.paging.PagingConfig
import androidx.paging.PagingData
import timber.log.Timber
import uz.neva.kidya.network.dto.*
import uz.neva.kidya.network.dto.nevaModel.analyticsModel.AnalyticsModel
import uz.neva.kidya.network.dto.nevaModel.profile.ProfileUpdateModel
import uz.neva.kidya.ui.brandShop.BrandShopPagingSource
import uz.neva.kidya.ui.category.CategoriesListPagingSource
import uz.neva.kidya.ui.category.selectedProduct.SelectedCategoryProductListPagingSource
import uz.neva.kidya.ui.category.subcategories.CategoryListNewPagingSource
import uz.neva.kidya.ui.category.subcategories.CategorySortPagingSource
import uz.neva.kidya.ui.messages.MessagesPagingSource
import uz.neva.kidya.ui.news.NewsListPagingSource

class KidyaRepository constructor(
    private val api: KidyaApi
) {
    val NEWS_PAGE_SIZE = 2
    val CATALOG_PAGE_SIZE = 10
    val CATEGORY_PAGE_SIZE = 10
    var totalCount = 0
    suspend fun getReviews(productId: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getReviews(productId) })
    }


    suspend fun getSelectedProduct(
        user_id: String? = "",
        id: String,
        width: Int? = null,
        height: Int? = null
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.getSelectedProduct(
                user_id = user_id,
                id,
                width = width,
                height = height
            )
        })
    }

    suspend fun getSelectedFrequentlyProducts(id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getSelectedFrequentlyProducts(id) })
    }

    suspend fun getSelectedMoreProducts(id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getSelectedMoreProduct(id) })
    }

    suspend fun addReviews(productId: String, user_id: String, text: String, rating: Float) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.addReviews(
                id = productId,
                user_id = user_id,
                text = text,
                rating = rating
            )
        })
    }

    suspend fun getReview(id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getReview(id) })
    }


    suspend fun sendMessage(user_id: String, supplier_id: String, text: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.sendMessages(user_id, supplier_id, text) })
    }


    suspend fun getSelectedNews(id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getSelectedNews(id) })
    }


    fun getNews(isHomeNews: Boolean = false): Flow<PagingData<NewsObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = if (isHomeNews) 6 else NEWS_PAGE_SIZE
            ),
            pagingSourceFactory = { NewsListPagingSource(api, isHomeNews) }
        ).flow
    }

    fun getMessages(user_id: String): Flow<PagingData<MessagesObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10,
                prefetchDistance = 10
            ),
            pagingSourceFactory = { MessagesPagingSource(api, user_id) }
        ).flow
    }

    fun getMessagesChat(user_id: String, supplier_id: String): Flow<PagingData<MessagesObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = 10
            ),
            pagingSourceFactory = { MessagesPagingSource(api, user_id, true, supplier_id) }
        ).flow
    }

    fun getSelectedCategoryProducts(
        section_id: String,
        user_id: String
    ): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    section_id,
                    user_id
                )
            }
        ).flow
    }

    fun getAllSelectedCategoryProducts(
        section_id: String,
        user_id: String
    ): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    section_id,
                    isAllProduct = true,
                    user_id = user_id
                )
            }
        ).flow
    }

    fun getSaleCategoryProducts(
        user_id: String,
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = user_id,
                    isDisCount = true,
                    isHome = isHome
                )
            }
        ).flow
    }

    fun getDealsCategoryProducts(
        user_id: String,
        type: String = "",
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = user_id,
                    isDeal = true,
                    popularType = type,
                    isHome = isHome
                )
            }
        ).flow
    }

    fun getViewHistoryProducts(
        user_id: String,
        isHome: Boolean = false,
        page: Int?
    ): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = user_id,
                    isHistory = true,
                    isHome = isHome,
                )
            }
        ).flow
    }

    fun getFilteredResult(
        user_id: String,
        section_id: String,
        sort: String
    ): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = user_id,
                    section_id = section_id,
                    isFiltered = true,
                    sort = sort
                )
            }
        ).flow
    }

    fun getFavCategoryProducts(user_id: String): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = user_id,
                    getFav = true
                )
            }
        ).flow
    }

    fun getSearchProducts(query: String, user_id: String): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = user_id,
                    isSearch = true,
                    searchValue = query
                )
            }
        ).flow
    }

    fun getHomePage(userId: String, isHome: Boolean = false): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = userId,
                    isDeal = true,
                    isHome = isHome
                )
            }
        ).flow
    } // hit.php

    fun getHomeRecommended(
        userId: String,
        isHome: Boolean = false
    ): Flow<PagingData<ProductObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                SelectedCategoryProductListPagingSource(
                    api,
                    user_id = userId,
                    isRecommended = true,
                    isHome = isHome
                )
            }
        ).flow
    }

    suspend fun getCartCount(user_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getCartCount(user_id) })
    }

    suspend fun getProfile(user_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getProfile(user_id) })
    }

    suspend fun updateProfile(
        user_id: String,
        name: String,
        phone: String,
        address: String,
        district: String,
        street: String,
        gender: String,
        birthday: String,
        email: String? = "",
        interest: String? = ""
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.updateProfile(
                user_id = user_id,
                edit = true,
                name = name,
                phone = phone,
                address = address,
                district = district,
                street = street,
                gender = gender,
                birthday = birthday,
                email = email,
                interest = interest
            )
        })
    }

    suspend fun updateBaby(
        user_id: String,
        kidsName1: String,
        kidsBirthday1: String,
        kidsGander1: String
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.updateBabyProfile(
                user_id = user_id,
                edit = "true",
                kidsName1 = kidsName1,
                kidsBirthday1 = kidsBirthday1,
                kidsGender1 = kidsGander1
            )
        })
    }

    suspend fun getPopularCategories(width: Int? = null, height: Int? = null) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getCategories(width = width, height = height) })
    }

    fun getCategories(section_id: String = ""): Flow<PagingData<CategoriesObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATEGORY_PAGE_SIZE
            ),
            pagingSourceFactory = { CategoriesListPagingSource(api, section_id) }
        ).flow
    }

   suspend fun getAllProductCount(section_id: String)= flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getAllSelectedCategoryProductsCount(section_id = section_id,"true") })
    }

    suspend fun getBrandShopProductsSize(id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getBrandShopProductsSize(id) })
    }

    suspend fun getSearchProductsSize(user_id: String,query: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getSearchResultProductsSize(user_id,query) })
    }

    suspend fun getFlashSaleProduct() = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getFlashSale(null) })
    }

    suspend fun addToBasket(
        user_id: String,
        type: String,
        quantity: String,
        id: String,
        action: String,
        color: String,
        razmer: String
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.addToBasket(user_id, type, quantity, id, action, color, razmer) })
    }

    suspend fun addToFavs(user_id: String, type: String = "favs", id: String, action: String) =
        flow {
            emit(Resource.Loading)
            emit(safeApiCall { api.addToFavs(user_id, type, id, action) })
        }

    suspend fun getBasket(user_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getBasket(user_id) })
    }

    suspend fun getCoupoList(user_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getCouponList(user_id) })
    }

    suspend fun getCouponInfo(user_id: String, coupon: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getCouponInfo(user_id, coupon) })
    }

    suspend fun makePayment(order_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.makePayment(order_id) })
    }

    suspend fun getOrderHistory(user_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getOrderHistory(user_id) })
    }

    suspend fun getOrderDetails(user_id: String, order_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getOrderDetails(user_id, order_id) })
    }

    suspend fun returnProduct(userId: String, orderId: String, products: HashMap<String, String>) =
        flow {
            emit(Resource.Loading)
            emit(safeApiCall { api.returnProduct(userId, orderId, products) })
        }

    suspend fun makeOrderDelivery(
        user_id: String,
        startTime: String,
        endTime: String,
        order_id: String,
        validDate: String
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.makeOrderDelivery(
                user_id,
                startTime,
                endTime,
                order_id,
                validDate
            )
        })
    }


    suspend fun getBanners() = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getBanners() })
    }

    suspend fun getShopInfo(width: Int? = null, height: Int? = null) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getShopInfo(width, height) })
    }

    fun getSelectedShop( user_id: String, id: String, sort: String) : Flow<PagingData<ProductObject>> {
        return Pager(
                config = PagingConfig(
                        pageSize = CATALOG_PAGE_SIZE
                ),
                pagingSourceFactory = {
                    BrandShopPagingSource(
                            api,
                            user_id,
                            section_id = id,
                            sort = sort
                    )
                }
        ).flow
    }

    suspend fun getSearchHistory(id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getSearchHistory(id) })
    }

    suspend fun getLiveSearch(value: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getLiveSearch(value) })
    }

    suspend fun clearHistory(id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.clearHistory(id) })
    }


    suspend fun clearOnlyOneHistory(id: String, historyId: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.clearOnlyOneHistory(user_id = id, id = historyId) })
    }

    suspend fun getNotifications(user_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getNotifications(user_id) })
    }

    suspend fun notificationsMarkAsRead(user_id: String, id: String? = null, all: String? = null) =
        flow {
            emit(Resource.Loading)
            emit(safeApiCall { api.notificationsMarkAsRead(user_id, id, all) })
        }

    suspend fun getFilters(section_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getFilters(section_id) })
    }

    suspend fun getFiltersSale() = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getFiltersSale() })
    }

    suspend fun getSubCategory(section_id: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getSubCategories(section_id = section_id,null) })
    }

    suspend fun checkOutFirsStep(
        user_id: String,
        coupon: String? = null
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.checkOutFirstStep(
                user_id = user_id,
                coupon = coupon,
                action = "save",
                paysystem = "payme",
                delivery = "new"
            )
        })
    }

    suspend fun authFirstStep(phone: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.authFirstStep(phone) })
    }

    suspend fun authSecondStep(phone: String, code: String, fcm: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.authSecondStep(phone, code, fcm) })
    }

    suspend fun sendFeedback(name: String, phone: String, text: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.sendFeedback(name, phone, text) })
    }

    suspend fun getFaq(user_id: String? = null) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getFAQ(user_id) })
    }

    suspend fun getCardList(user_id: String, token: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getCardList(user_id, token) })
    }

    suspend fun addCard(
        user_id: String,
        token: String,
        name: String,
        number: String,
        expire: String
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.addCard(
                user_id,
                token,
                add = "true",
                name,
                number,
                expire
            )
        })
    }

    suspend fun verifyCard(token: String, code: String, id_card: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.verifyCard(token, verify = "true", code, id_card) })
    }

    suspend fun deleteCard(token: String, id_card: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.deleteCard(token, delete = "true", id_card) })
    }

    suspend fun payCard(
        token: String,
        order_id: String,
        id_card: String,
        fio: String,
        phoneNumber: String,
        region: String,
        location: String? = null,
        street: String
    ) = flow {
        emit(Resource.Loading)
        emit(safeApiCall {
            api.payCard(
                token, pay = "true",
                order_id,
                id_card,
                fio = fio,
                phoneNumber = phoneNumber,
                region = region,
                location = location,
                street = street
            )
        })
    }

    suspend fun getHomeBanner() = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getHomeBanner() })
    }

    suspend fun getHomeDealCount(userId: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.getHomeHitsCount(
            user_id = userId,
            1,
            count = "8",
            width = 120,
            height = 144
        ) })
    }

    fun getCategorySortProducts(
        section_id: String,
        user_id: String,
        sort: String
    ): Flow<PagingData<ProductSortObject>> {
        return Pager(
            config = PagingConfig(
                pageSize = CATALOG_PAGE_SIZE
            ),
            pagingSourceFactory = {
                CategorySortPagingSource(
                    api,
                    section_id,
                    user_id = user_id,
                    sort
                )
            }
        ).flow
    }

    suspend fun updateProfile(data: ProfileUpdateModel, userId: String) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.updateProfile(userId, data)})
    }

    fun getAllSelectedCategoryProductsNew(
            section_id: String,
            user_id: String,
            allProduct: Boolean,
            sort: String
    ): Flow<PagingData<ProductObjectNew>> {
        Timber.d(section_id)
        return Pager(
                config = PagingConfig(
                        pageSize = CATALOG_PAGE_SIZE
                ),
                pagingSourceFactory = {
                    CategoryListNewPagingSource(
                            api,
                            section_id,
                            isAllProduct = allProduct,
                            user_id = user_id,
                            sort
                    )
                }
        ).flow
    }

    suspend fun putAnalytics(data: AnalyticsModel) = flow {
        emit(Resource.Loading)
        emit(safeApiCall { api.putAnalyticsApi(data)})
    }
}